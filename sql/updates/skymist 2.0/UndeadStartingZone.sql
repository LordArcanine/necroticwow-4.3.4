UPDATE `creature_template` SET `ScriptName`="npc_darnell" WHERE `entry`=49337;
DELETE FROM `waypoint_data` WHERE `Id`=49337 OR `Id`=49338;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`delay`,`move_flag`) VALUES
(49337, 1, 1644.167358, 1663.640503, 132.477295, 0, 1),
(49337, 2, 1643.309937, 1677.539307, 126.931885, 0, 1),
(49337, 3, 1664.275146, 1678.069946, 120.530273, 0, 1),
(49338, 1, 1663.668091, 1662.494629, 120.719307, 5000, 1),
(49338, 2, 1659.798218, 1684.343750, 121.024834, 0, 1),
(49338, 3, 1656.291992, 1688.685913, 120.718796, 5000, 1),
(49338, 4, 1664.275146, 1678.069946, 120.530273, 0, 1),
(49338, 5, 1673.465332, 1670.061523, 120.718964, 5000, 1),
(49338, 6, 1664.219482, 1687.752563, 121.025108, 0, 1),
(49338, 7, 1663.963379, 1694.138916, 120.719017, 5000, 1);
DELETE FROM `script_texts` WHERE `npc_entry`=49337;
INSERT INTO `script_texts` (`npc_entry`,`entry`,`content_default`,`type`,`comment`) VALUES
(49337, -1000022, "Greetings, $N.", 0, "Darnell SAY_GREETINGS"),
(49337, -1000023, "The Shadow Grave is this way. Follow me, $N.", 0, "Darnell SAY_1"),
(49337, -1000024, "Let's see now... where could they be...", 0, "Darnell SAY_2"),
(49337, -1000025, "Now, where could those supplies be?", 0, "Darnell SAY_3"),
(49337, -1000026, "Hey, give me a hand, $N! I can't find the supplies that Mordo needed!", 0, "Darnell SAY_4"),
(49337, -1000027, "No, not over here.", 0, "Darnell SAY_5"),
(49337, -1000028, "Hmm...", 0, "Darnell SAY_6"),
(49337, -1000029, "Maybe they're over here?", 0, "Darnell SAY_7"),
(49337, -1000030, "Nice work! You've found them. Let's bring these back to Mordo.", 0, "Darnell SAY_COMPLETE_1"),
(49337, -1000031, "I saw someone up there whose jaw fell off. I wonder if Mordo can fix him up?", 0, "Darnell SAY_COMPLETE_2");
DELETE FROM `waypoint_data` WHERE `id`=49231;
INSERT INTO waypoint_data (`id`,`point`,`position_x`,`position_y`,`position_z`,`delay`,`move_flag`,`action`) VALUES
(49231,1,1739.360352,1703.588989,128.513733,0,0,0),
(49231,2,1729.872803,1701.173096,127.827408,0,0,0),
(49231,3,1719.652588,1692.137573,131.858429,0,0,0),
(49231,4,1700.514404,1678.819580,134.286392,0,0,0),
(49231,5,1692.620117,1675.563477,134.964539,0,0,0);
UPDATE `quest_template` SET `NextQuestIdChain`=26799 WHERE `Id`=28608;
UPDATE `quest_template` SET `NextQuestIdChain`=28652 WHERE `Id`=26799;
UPDATE `quest_template` SET `NextQuestIdChain`=24960 WHERE `Id`=28652;
UPDATE `quest_template` SET `NextQuestIdChain`=25089 WHERE `Id`=24960;
UPDATE `quest_template` SET `NextQuestIdChain`=26800 WHERE `Id`=25089;
UPDATE `quest_template` SET `NextQuestIdChain`=28653 WHERE `Id`=26800;
UPDATE `quest_template` SET `NextQuestIdChain`=26801 WHERE `Id`=28653;
UPDATE `quest_template` SET `NextQuestIdChain`=0 WHERE `Id`=26801;
UPDATE `quest_template` SET `StartScript`=25089 WHERE `Id`=25089;
DELETE FROM `script_texts` WHERE `npc_entry`=49141;
INSERT INTO `script_texts` (`npc_entry`,`entry`,`content_default`,`type`,`comment`) VALUES
(49141, -1000032, "Hello again.", 0, "Darnell SAY_HELLO"),
(49141, -1000033, "I know the way to Deathknell. Come with me!", 0, "Darnell SAY_8"),
(49141, -1000034, "Good, you're still here. Now, where's Deathguard Saltain?", 0, "Darnell SAY_9"),
(49141, -1000035, "Ah, here he is.", 0, "Darnell SAY_10");
DELETE FROM `waypoint_data` WHERE `Id`=49339;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`delay`,`move_flag`,`action`) VALUES
(49339,1,1723.532349,1631.841553,119.999260,0,1,0),
(49339,2,1733.724731,1626.103149,117.776321,0,1,0),
(49339,3,1762.803833,1602.697021,110.071106,0,1,0),
(49339,4,1789.149292,1593.908325,103.833618,0,1,0),
(49339,5,1817.585815,1589.575317,96.273346,0,1,0);
UPDATE `creature_template` SET `ScriptName`="npc_darnell2" WHERE `entry`=49141;
DELETE FROM `quest_start_scripts` WHERE `Id`=25089;
INSERT INTO `quest_start_scripts` (`id`,`command`,`datalong`,`datalong2`) VALUES
(25089, 15, 91576, 2);
UPDATE `quest_template` SET `OfferRewardText`="Those val'kyr have been really busy resurrecting you newbies lately. I've already seen a couple dozen new bodies run down that hill since this morning.$B$BYou, however, are the most promising $c I've seen today." WHERE `Id`=25089;
UPDATE `quest_template` SET `Flags`=524288 WHERE `Id`=26800;
UPDATE `quest_template` SET `OfferRewardText`="Great work $N. I knew you weren't useless.$B$BHere, take one of these. We deathguards have piles of these things just sitting around. ", `RequestItemsText`="Have you managed to scavenge up some corpses?", `EndText`="", `RewardOrRequiredMoney`=65, `RewardMoneyMaxLevel`=120 WHERE `Id`=26800;
DELETE FROM `script_texts` WHERE `npc_entry`=49141;
INSERT INTO `script_texts` (`npc_entry`,`entry`,`content_default`,`type`,`comment`) VALUES
(49141, -1000036, "Let's get moving, $N. Saltain said that we'd find some corpses up here.", 0, "Darnell SAY_11"),
(49141, -1000037, "I think I see some corpses up ahead. Let's go, $N! You do the searching and fighting. I'll do the lifting.", 0, "Darnell SAY_12");
UPDATE `creature_template` SET `dynamicflags`=32 WHERE `entry`=49340;
UPDATE `quest_template` SET `StartScript`=26800 WHERE `Id`=26800;
DELETE FROM `quest_start_scripts` WHERE `Id`=26800;
INSERT INTO `quest_start_scripts` (`id`,`command`,`datalong`,`datalong2`) VALUES
(26800, 15, 91576, 2);
UPDATE `creature_template` SET `dynamicflags`=0 WHERE `entry`=44794;
UPDATE `creature_template_addon` SET `auras`="" WHERE `entry`=44794;
UPDATE `creature_template` SET `RegenHealth`=0 WHERE `entry`=44795;
UPDATE `creature_template` SET `npcflag`=1, `gossip_menu_id`=0, `ScriptName`="npc_demonic_trainer" WHERE `entry` in (6374, 16267, 6373, 6328, 5750, 12776, 6382, 5749, 6027, 5815, 5753, 23535, 5520, 16649, 15494);
DELETE FROM `npc_text` WHERE `ID`=49294 OR `ID`=49295;
INSERT INTO `npc_text` (`ID`,`text0_0`,`text0_1`,`lang0`,`WDBVerified`) VALUES
(49294, "Sorry, but my services are only of use to warlocks.", "Sorry, but my services are only of use to warlocks.", 0, 13623),
(49295, "Weary of one of your demonic charges? Hoping for one with a more pleasing name?$B$BFor a price, I can assist you in the obliteration of a current minion so that you may summon an entirely new one...", "Weary of one of your demonic charges? Hoping for one with a more pleasing name?$B$BFor a price, I can assist you in the obliteration of a current minion so that you may summon an entirely new one...", 0, 13623);
UPDATE `gameobject_template` SET `name`="Mailbox" WHERE `entry`=202577;
SET @ENTRY = 1737;
UPDATE `creature_template` SET `ScriptName`="npc_deathguard_oliver" WHERE `entry`=@ENTRY;
DELETE FROM `waypoint_data` WHERE `Id`=@ENTRY OR `Id`=@Entry+1;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`delay`,`move_flag`,`action`) VALUES
(@ENTRY, 1, 1807.130005, 1605.920044, 102.885002, 12000, 0, 0),
(@ENTRY, 2, 1796.688110, 1601.154053, 102.934296, 0, 0, 0),
(@ENTRY, 3, 1796.012817, 1597.209595, 102.146713, 0, 0, 0),
(@ENTRY, 4, 1812.806763, 1591.286621, 97.436989, 0, 0, 0),
(@ENTRY, 5, 1823.775635, 1592.508667, 95.128952, 0, 0, 0),
(@ENTRY, 6, 1827.709717, 1609.090332, 95.789444, 0, 0, 0),
(@ENTRY, 7, 1826.395142, 1644.268555, 95.621941, 0, 0, 0),
(@ENTRY, 8, 1827.560669, 1596.134033, 95.176559, 0, 0, 0),
(@ENTRY+1, 1, 1822.115479, 1591.157593, 95.421455, 0, 0, 0),
(@ENTRY+1, 2, 1807.128052, 1593.422607, 99.190536, 0, 0, 0),
(@ENTRY+1, 3, 1802.462402, 1593.594116, 100.275421, 0, 0, 0),
(@ENTRY+1, 4, 1796.012817, 1597.209595, 102.146713, 0, 0, 0),
(@ENTRY+1, 5, 1796.688110, 1601.154053, 102.934296, 0, 0, 0),
(@ENTRY+1, 6, 1807.130005, 1605.920044, 102.885002, 0, 0, 0);
SET @ENTRY = 2315;
UPDATE `creature` SET `position_x`=1857.328857, `position_y`=1575.144287, `position_z`=94.314171, `orientation`=6.270551 WHERE `guid`=251818;
DELETE FROM `waypoint_data` WHERE `Id`=@Entry;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`delay`,`move_flag`,`action`) VALUES
(@ENTRY, 1, 1857.328857, 1575.144287, 94.314171, 25000, 0, 0),
(@ENTRY, 2, 1860.635376, 1575.060181, 94.314163, 0, 0, 0),
(@ENTRY, 3, 1861.266968, 1585.064819, 92.481918, 8000, 0, 0),
(@ENTRY, 4, 1848.002563, 1590.019165, 93.179382, 0, 0, 0),
(@ENTRY, 5, 1842.897583, 1600.940552, 94.550858, 0, 0, 0),
(@ENTRY, 6, 1843.691284, 1609.814819, 96.212364, 0, 0, 0),
(@ENTRY, 7, 1842.960571, 1633.821289, 96.933861, 14000, 0, 0),
(@ENTRY, 8, 1843.841431, 1612.489746, 96.957458, 0, 0, 0),
(@ENTRY, 9, 1843.727783, 1592.270630, 93.392845, 0, 0, 0),
(@ENTRY, 10, 1847.228638, 1589.367554, 93.190437, 0, 0, 0),
(@ENTRY, 11, 1860.283569, 1588.538208, 92.446968, 8000, 0, 0),
(@ENTRY, 12, 1860.872925, 1579.431396, 93.630386, 0, 0, 0),
(@ENTRY, 13, 1860.659058, 1575.371948, 94.313538, 0, 0, 0);
UPDATE `creature_template_addon` SET `path_id`=@ENTRY WHERE `entry`=@ENTRY;
SET @ENTRY = 1739;
UPDATE `creature_template_addon` SET `path_id`=@ENTRY WHERE `entry`=@ENTRY;
DELETE FROM `waypoint_data` WHERE `Id`=@Entry;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`delay`,`move_flag`,`action`) VALUES
(@ENTRY, 1, 1820.54, 1583.21, 95.7225, 15000, 0, @ENTRY),
(@ENTRY, 2, 1817.169434, 1573.591919, 95.683228, 0, 0, 0),
(@ENTRY, 3, 1820.606445, 1567.917236, 95.639053, 0, 0, 0),
(@ENTRY, 4, 1827.916870, 1565.922974, 95.623146, 10000, 0, 0),
(@ENTRY, 5, 1830.644165, 1578.082153, 95.621925, 0, 0, 0),
(@ENTRY, 6, 1834.816772, 1583.433472, 94.438713, 0, 0, 0),
(@ENTRY, 7, 1846.638672, 1582.289673, 94.214165, 0, 0, 0),
(@ENTRY, 8, 1847.993042, 1562.534546, 94.942146, 13000, 0, 0),
(@ENTRY, 9, 1850.336304, 1562.411255, 94.766083, 0, 0, 0),
(@ENTRY, 10, 1852.0708369, 1582.568848, 93.710602, 0, 0, 0),
(@ENTRY, 11, 1863.397339, 1588.487183, 92.098557, 0, 0, 0),
(@ENTRY, 12, 1879.723999, 1588.302124, 90.181305, 8000, 0, 0),
(@ENTRY, 13, 1873.800293, 1590.780518, 90.943077, 0, 0, 0),
(@ENTRY, 14, 1827.813110, 1591.853271, 94.482704, 0, 0, 0);
DELETE FROM `waypoint_scripts` WHERE `ID`=@ENTRY;
INSERT INTO `waypoint_scripts` VALUES (@ENTRY, 1, 30, 0, 0, 0, 0, 0, 0, 0.035759, 250749);
SET @ENTRY = 50372;
SET @ENTRY2= 50374;
DELETE FROM `waypoint_data` WHERE `Id`=@Entry OR `Id`=@Entry2;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`delay`,`move_flag`,`action`) VALUES
(@ENTRY, 1, 1707.439941, 1700.439941, 136.850998, 12000, 0, 0),
(@ENTRY, 2, 1698.250000, 1689.310059, 137.589005, 9000, 0, 0),
(@ENTRY, 3, 1697.109985, 1681.760010, 137.244003, 10000, 0, 0),
(@ENTRY, 4, 1693.520020, 1674.380005, 139.205994, 0, 0, 0),
(@ENTRY, 5, 1677.750000, 1672.040039, 143.759003, 0, 0, 0),
(@ENTRY, 6, 1673.761719, 1681.286377, 143.429642, 0, 0, 0),
(@ENTRY, 7, 1676.178833, 1695.536377, 144.685013, 0, 0, 0),
(@ENTRY, 8, 1686.406128, 1695.342651, 142.231476, 0, 0, 0),
(@ENTRY, 9, 1688.287842, 1689.079590, 139.371002, 0, 0, 0),
(@ENTRY, 10, 1686.977661, 1686.907715, 139.269882, 0, 0, 0),
(@ENTRY, 11, 1685.013672, 1685.938232, 139.209778, 9000, 0, 0),
(@ENTRY, 12, 1684.339600, 1688.524048, 140.164261, 0, 0, 0),
(@ENTRY, 13, 1686.042358, 1688.965820, 140.983978, 0, 0, 0),
(@ENTRY, 14, 1690.308105, 1682.712524, 139.787735, 0, 0, 0),
(@ENTRY, 15, 1699.109375, 1679.995483, 140.360992, 0, 0, 0),
(@ENTRY, 16, 1701.420898, 1680.545288, 140.360992, 0, 0, 0),
(@ENTRY, 17, 1703.667969, 1683.091309, 140.360992, 0, 0, 0),
(@ENTRY, 18, 1707.143311, 1690.643555, 139.481155, 0, 0, 0);
UPDATE `creature_template` SET `ScriptName`="npc_aradne" WHERE `entry`=@ENTRY;
UPDATE `creature_template` SET `ScriptName`="npc_risen_dead" WHERE `entry`=@ENTRY2;
DELETE FROM `script_texts` WHERE `npc_entry`=@Entry OR `npc_entry`=@Entry2;
INSERT INTO `script_texts` (`npc_entry`,`entry`,`content_default`,`type`,`comment`) VALUES
(@ENTRY, -1000038, "Rise from death's slumber and join your brothers!", 0, "ARADNE SAY_RESURRECTION_1"),
(@ENTRY, -1000039, "In the name of Sylvanas, the Banshee Queen, I bestow this gift upon you!", 0, "ARADNE SAY_RESURRECTION_2"),
(@ENTRY, -1000040, "You are returned to life. The Banshee Queen asks for your service.", 0, "ARADNE SAY_RESURRECTION_3"),
(@ENTRY, -1000041, "Rise from the grave and serve the Dark Lady!", 0, "ARADNE SAY_RESURRECTION_4"),
(@ENTRY, -1000042, "Waken, sleeper. Your new life awaits.", 0, "ARADNE SAY_RESURRECTION_5"),
(@ENTRY2, -1000043, "What... what's happening to me? Why have you done this to me?", 0, "RISEN DEAD SAY_FIRST_ACTION_1"),
(@ENTRY2, -1000044, "I should be dead and to death I will return!", 0, "RISEN DEAD SAY_FIRST_ACTION_2"),
(@ENTRY2, -1000045, "I... I don't remember... Why have you done this?", 0, "RISEN DEAD SAY_SECOND_ACTION_1"),
(@ENTRY2, -1000046, "What use has the Dark Lady for me?", 0, "RISEN DEAD SAY_SECOND_ACTION_2"),
(@ENTRY2, -1000047, "This is Lady Sylvanas's doing?", 0, "RISEN DEAD SAY_SECOND_ACTION_3"),
(@ENTRY2, -1000048, "What magic is this that turns back the hand of death?", 0, "RISEN DEAD SAY_SECOND_ACTION_4"),
(@ENTRY2, -1000049, "If the Banshee Queen has offered me this chance, I will gladly serve.", 0, "RISEN DEAD SAY_SECOND_ACTION_5"),
(@ENTRY2, -1000050, "I never asked for this! Leave me alone!", 0, "RISEN DEAD SAY_THIRD_ACTION_1");
DELETE FROM `waypoint_data` WHERE `Id`=@ENTRY+1 OR `Id`=@Entry2;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`delay`,`move_flag`,`action`) VALUES
(@ENTRY+1, 1, 1691.600098, 1660.037720, 131.215485, 0, 0, 0),
(@ENTRY+1, 2, 1698.519775, 1653.410400, 128.668808, 0, 0, 0),
(@ENTRY2, 1, 1702.839355, 1679.705322, 134.095139, 1000, 1, 0),
(@ENTRY2, 2, 1670.371826, 1662.767578, 140.203354, 0, 1, 0);
UPDATE `creature_template` SET `npcflag`=2 WHERE `entry` in (1661, 1569);