-- --
-- "The Wakening"
-- --

-- Lilian Voss
SET @Entry  := 38895;
SET @SOURCETYPE := 0;
UPDATE creature_template SET gossip_menu_id=17564 WHERE entry=@Entry;

DELETE FROM gossip_menu WHERE entry IN (17564,17565);
REPLACE INTO gossip_menu VALUES(17564,17564);
REPLACE INTO gossip_menu VALUES(17565,17565);

DELETE FROM gossip_menu_option WHERE menu_id IN (17564,17565);
REPLACE INTO gossip_menu_option VALUES (17564,0,0,"I\'m not an abomination, I\'m simply undead. I just want to speak with you.",1,1,17565,0,0,0,0,NULL);
REPLACE INTO gossip_menu_option VALUES (17565,0,0,"Lilian, do you realize that you are undead yourself?",1,1,0,0,0,0,0,NULL);

DELETE FROM npc_text WHERE ID IN (17564,17565);
REPLACE INTO npc_text SET ID=17564,text0_0="Get away from me, you abomination!",WDBVerified=1;
REPLACE INTO npc_text SET ID=17565,text0_0="The undead are a taint upon Azeroth! Every one of you creatures deserves to be destroyed!",WDBVerified=1;

DELETE FROM creature_text WHERE entry=@Entry;
REPLACE INTO creature_text VALUES (@Entry,0,0,"No. You\'re lying! My father will protect me!",12,0,100,0,0,0,NULL);
REPLACE INTO creature_text VALUES (@Entry,0,1,"You don\'t understand... I CAN\'T be undead! Not me, not now...",12,0,100,0,0,0,NULL);

UPDATE creature_template SET AIName="SmartAI" WHERE entry=@Entry LIMIT 1;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@Entry AND `source_type`=@SOURCETYPE;
REPLACE INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Entry,@SOURCETYPE,0,0,62,0,100,0,17565,0,0,0,22,1,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"shift phase on gossip select"),
(@Entry,@SOURCETYPE,1,0,1,1,100,0,0,0,0,0,72,0,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"close gossip menu"),
(@Entry,@SOURCETYPE,2,0,1,1,100,0,0,0,0,0,1,0,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Talk"),
(@Entry,@SOURCETYPE,3,0,52,1,100,0,0,@Entry,0,0,22,2,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Set phase 2"),
(@Entry,@SOURCETYPE,4,0,1,2,100,0,0,0,0,0,33,@Entry,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Kill Monster");
 
-- Marshal Redpath
SET @Entry  := 49230;
SET @SOURCETYPE := 0;
UPDATE creature_template SET gossip_menu_id=17566 WHERE entry=@Entry;
 
DELETE FROM gossip_menu WHERE entry IN (17566,17567);
REPLACE INTO gossip_menu VALUES(17566,17566);
REPLACE INTO gossip_menu VALUES(17567,17567);

DELETE FROM gossip_menu_option WHERE menu_id IN (17566,17567);
REPLACE INTO gossip_menu_option VALUES (17566,0,0,"I\'m not here to fight you. I\'ve only been asked to speak with you.",1,1,17567,0,0,0,0,NULL);
REPLACE INTO gossip_menu_option VALUES (17567,0,0,"You are free to do whatever you like.",1,1,0,0,0,0,0,NULL);

DELETE FROM npc_text WHERE ID IN (17566,17567);
REPLACE INTO npc_text SET ID=17566,text0_0="Stand back, monster. You want a fight? Because I\'ll fight you.$B$BI\'ll fight any one of you creatures! Do you hear me?",WDBVerified=1;
REPLACE INTO npc_text SET ID=17567,text0_0="Oh, really?$B$BFine. I don\'t want to join you and your Forsaken. Maybe I\'ll start my own Forsaken! Maybe I\'ll invent Forsaken with elbows!",WDBVerified=1;

DELETE FROM creature_text WHERE entry=@Entry;
REPLACE INTO creature_text VALUES (@Entry,0,0,"BLEEAAAGGHHH! I\'m a monster, don\'t look at me!",14,0,100,0,0,0,NULL);
REPLACE INTO creature_text VALUES (@Entry,0,1,"Who are you calling a monster? You\'re the monster! I\'m just a man who died.",12,0,100,0,0,0,NULL);

UPDATE creature_template SET AIName="SmartAI" WHERE entry=@Entry LIMIT 1;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@Entry AND `source_type`=@SOURCETYPE;
REPLACE INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Entry,@SOURCETYPE,0,0,62,0,100,0,17567,0,0,0,22,1,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"shift phase on gossip select"),
(@Entry,@SOURCETYPE,1,0,1,1,100,0,0,0,0,0,72,0,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"close gossip menu"),
(@Entry,@SOURCETYPE,2,0,1,1,100,0,0,0,0,0,1,0,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Talk"),
(@Entry,@SOURCETYPE,3,0,52,1,100,0,0,@Entry,0,0,22,2,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Set phase 2"),
(@Entry,@SOURCETYPE,4,0,1,2,100,0,0,0,0,0,33,@Entry,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Kill Monster");
 
SET @Entry  := 49231;
SET @SOURCETYPE := 0;
UPDATE creature_template SET gossip_menu_id=12487 WHERE entry=@Entry; 
DELETE FROM gossip_menu WHERE entry IN (12487,12488);
REPLACE INTO gossip_menu VALUES(12487,17569);
REPLACE INTO gossip_menu VALUES(12488,17570);

DELETE FROM gossip_menu_option WHERE menu_id IN (12487,12488);
REPLACE INTO gossip_menu_option VALUES (12487,0,0,"Calm down, Valdred.  Undertaker Mordo probably sewed some new ones on for you.",1,1,12488,0,0,0,0,NULL);
REPLACE INTO gossip_menu_option VALUES (12488,0,0,"Don\'t you remember?  You died.",1,1,0,0,0,0,0,NULL);

DELETE FROM npc_text WHERE ID IN (17569,17570);
REPLACE INTO npc_text SET ID=17569,text0_0="What... what\'s going on? Who are you? What happened to me?",WDBVerified=1;
REPLACE INTO npc_text SET ID=17570,text0_0="I... died? Yes, you\'re right. I died. It was an orc... he cut off my hands, and left me to die. <Valdred looks down at his hands.> These aren\'t my hands! THESE AREN\'T MY HANDS!",WDBVerified=1;

DELETE FROM creature_text WHERE entry=@Entry;
REPLACE INTO creature_text VALUES (@Entry,0,0,"Valdred Moray, reporting for duty, sir!",14,0,100,0,0,0,NULL);
REPLACE INTO creature_text VALUES (@Entry,0,1,"I see. Well then, let\'s get to work, $N! The Dark Lady needs us, right?",12,0,100,0,0,0,NULL);

UPDATE creature_template SET AIName="SmartAI" WHERE entry=@Entry LIMIT 1;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@Entry AND `source_type`=@SOURCETYPE;
REPLACE INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Entry,@SOURCETYPE,0,0,62,0,100,0,12488,0,0,0,22,1,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"shift phase on gossip select"),
(@Entry,@SOURCETYPE,1,0,1,1,100,0,0,0,0,0,72,0,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"close gossip menu"),
(@Entry,@SOURCETYPE,2,0,1,1,100,0,0,0,0,0,1,0,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Talk"),
(@Entry,@SOURCETYPE,3,0,52,1,100,0,0,@Entry,0,0,22,2,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Set phase 2"),
(@Entry,@SOURCETYPE,4,0,1,2,100,0,0,0,0,0,33,@Entry,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Kill Monster");

-- --
-- "Recruitment"
-- --

-- http://www.wowhead.com/quest=26800 Recruitment
UPDATE quest_template SET Method=2, Flags=0 WHERE id=26800;
 
-- http://www.wowhead.com/npc=49340  Scarlet Corpse
UPDATE creature_template SET npcflag=1 WHERE entry=49340;
DELETE FROM creature_queststarter WHERE id=49340 and quest=26800;
 
-- create a gossip menu for the Scarlet Corpse
DELETE FROM gossip_menu WHERE entry=49340;
INSERT INTO gossip_menu values(49340,49340);
 
DELETE FROM npc_text WHERE ID=49340;
INSERT INTO npc_text set ID=49340, text0_0="Someone we should bury!";
 
UPDATE creature_template SET gossip_menu_id=49340 WHERE entry=49340;
 
-- Scarlet Corpse
SET @ENTRY := 49340;
SET @SOURCETYPE := 0;
 
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
REPLACE INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,@SOURCETYPE,0,0,64,0,100,0,0,0,0,0,33,49340,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"receive a monster kill"),
(@ENTRY,@SOURCETYPE,1,0,64,0,100,0,0,0,0,0,41,0,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"delete corps");

UPDATE creature_template set npcflag=0 WHERE entry in (1501,1502,1890);
DELETE FROM creature_queststarter WHERE id in (1501,1890);

-- --
-- The Truth of the Grave
-- --

UPDATE quest_template SET Method=2, Flags=0, SpecialFlags=0 WHERE Id=24961;

-- http://www.wowhead.com/npc=38910
UPDATE creature_template set npcflag=1, gossip_menu_id=15486 WHERE entry=38910;
DELETE FROM creature_queststarter WHERE id=38910 and quest=24961;

DELETE FROM `npc_text` WHERE `ID` = 15486;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `lang0`, `prob0`, `em0_0`, `em0_1`, `em0_2`, `em0_3`, `em0_4`, `em0_5`, `text1_0`, `text1_1`, `lang1`, `prob1`, `em1_0`, `em1_1`, `em1_2`, `em1_3`, `em1_4`, `em1_5`, `text2_0`, `text2_1`, `lang2`, `prob2`, `em2_0`, `em2_1`, `em2_2`, `em2_3`, `em2_4`, `em2_5`, `text3_0`, `text3_1`, `lang3`, `prob3`, `em3_0`, `em3_1`, `em3_2`, `em3_3`, `em3_4`, `em3_5`, `text4_0`, `text4_1`, `lang4`, `prob4`, `em4_0`, `em4_1`, `em4_2`, `em4_3`, `em4_4`, `em4_5`, `text5_0`, `text5_1`, `lang5`, `prob5`, `em5_0`, `em5_1`, `em5_2`, `em5_3`, `em5_4`, `em5_5`, `text6_0`, `text6_1`, `lang6`, `prob6`, `em6_0`, `em6_1`, `em6_2`, `em6_3`, `em6_4`, `em6_5`, `text7_0`, `text7_1`, `lang7`, `prob7`, `em7_0`, `em7_1`, `em7_2`, `em7_3`, `em7_4`, `em7_5`, `WDBVerified`) VALUES
(15486, '', 'Get away from me, you monster! Don''t look at me! I''m hideous! ', 0, 1, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 15211); -- 15486

UPDATE `creature_template` SET `gossip_menu_id` = 11132 WHERE `entry` = 38910;

DELETE FROM `gossip_menu_option` WHERE menu_id=11132;
INSERT INTO gossip_menu_option(menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, action_poi_id, box_coded, box_money, box_text) VALUES 
(11132, 0, 0, 'You''re not hideous, Lilian... you''re one of us. Here, look in this mirror, see for yourself.', 1, 1, 0, 0, 0, 0, '');

-- Lilian Voss
SET @ENTRY := 38910;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,1,62,0,100,0,11132,0,0,0,80,3891000,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"On gossip Select do action"),
(@ENTRY,@SOURCETYPE,1,0,61,0,100,0,0,0,0,0,72,0,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Close gossip");

DELETE FROM creature_text WHERE entry=38910;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(38910, 0, 0, 'You don''t understand... I CAN''T be undead! Not me, not now...', 12, 0, 100, 18, 0, 0, 'Lilian Voss');

-- Lilian Voss
SET @ENTRY := 3891000;
SET @SOURCETYPE := 9;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,0,0,100,0,2000,2000,2000,2000,1,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Say text 0"),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,2000,2000,2000,2000,33,38910,0,0,0,0,0,17,0,30,0,0.0,0.0,0.0,0.0,"Give credit to player"),
(@ENTRY,@SOURCETYPE,2,0,0,0,100,0,0,0,0,0,53,1,3891000,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Do wp"),
(@ENTRY,@SOURCETYPE,3,0,0,0,100,0,20000,20000,20000,20000,37,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Die self");

DELETE FROM waypoints WHERE entry=3891000;
INSERT INTO waypoints(entry, pointid, position_x, position_y, position_z) VALUES 
(3891000, 1, 1856.48, 1555.65, 94.7939);

DELETE FROM `creature` WHERE `id`=38910;
INSERT INTO `creature` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(192728, 38910, 0, 1, 1, 0, 0, 1857.19, 1556.15, 94.7914, 3.33774, 20, 0, 0, 98, 115, 0, 0, 0, 0);

-- --
-- Assault on the Rotbrain Encampment
-- --

-- Marshal Redpath
SET @ENTRY := 49424;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,1,0,100,0,0,0,0,0,5,36,0,0,0,0,0,11,49428,5,0,0.0,0.0,0.0,0.0,"Showfight"),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,1,0,0,0,0,11,83015,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Cast 83015 on victim"),
(@ENTRY,@SOURCETYPE,2,0,0,0,100,0,6000,9000,5000,9000,11,33239,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"cast 33239 on victim"),
(@ENTRY,@SOURCETYPE,3,0,4,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"on aggro talk");

DELETE FROM creature_text WHERE entry=49424;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(49424, 0, 0, 'BLEEAAAGGHHH! I''m a monster, don''t look at me!', 14, 0, 100, 0, 0, 0, 'Marschall Rotpfad');

-- --
-- The Thrill of the Hunt
-- --

-- Xavier the Huntsman
DELETE FROM `gossip_menu_option` WHERE menu_id=11109;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`) VALUES
('11109', '0', '3', 'I seek training.', '5', '16', '0', '0', '0', '0', '0', ''),
('11109', '1', '0', 'I want to reset my talents.', '16', '16', '0', '0', '0', '0', '0', ''),
('11109', '2', '0', 'I wish to know about Dual Talent Specialization.', '1', '1', '10371', '0', '0', '0', '0', '');

-- Quest
UPDATE `quest_template` SET `RequiredRaces`='65535', `Flags`='2621440', `RequiredSpellCast1`='56641', `RequiredNpcOrGo1`='44794' WHERE (`Id`='24964');