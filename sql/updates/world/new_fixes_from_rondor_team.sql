UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='47' WHERE (`entry`='17134') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='48' WHERE (`entry`='17135') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='48' WHERE (`entry`='17136') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='47' WHERE (`entry`='17137') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='47' WHERE (`entry`='18037') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='47' WHERE (`entry`='18138') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='48' WHERE (`entry`='18064') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='46' WHERE (`entry`='18065') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='63' WHERE (`entry`='18351') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='45' WHERE (`entry`='18352') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='48' WHERE (`entry`='18413') AND (`item`='25433');
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`='35' WHERE (`entry`='18423') AND (`item`='25433');
UPDATE `item_template` SET `stackable`='250' WHERE (`entry`='69815');
UPDATE `creature_template` SET `lootid`='52530' WHERE (`entry`='52530');
delete from creature_loot_template where entry=52530;
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','71141','66','1','0','1','3');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','69237','40','1','0','1','3');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','69815','26','1','0','18','66');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70990','18','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','68983','14','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70989','14','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70738','12','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70735','12','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70734','12','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70739','12','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70736','12','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70733','11','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70986','9','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70988','9','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70985','9','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70987','9','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','70737','8','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','71665','2','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','71776','1.7','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','71785','1.6','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','71782','1.6','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','71780','1.6','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','71775','1.5','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','71779','1.2','1','0','1','1');
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('52530','71787','1.2','1','0','1','1');
UPDATE `creature_template` SET `Health_mod`='9940' WHERE (`entry`='26582');
UPDATE `creature_template` SET `Health_mod`='9940' WHERE (`entry`='26583');
UPDATE `quest_template` SET `SpecialFlags`='1' WHERE (`Id`='10077');
