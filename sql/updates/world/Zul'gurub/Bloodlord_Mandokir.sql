-- Bloodlord Mandokir
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (52151, 0, 0, 0, 0, 0, 37816, 0, 0, 0, 'Bloodlord Mandokir', '', '', 0, 87, 87, 3, 14, 14, 0, 1, 1.14286, 1, 3, 2, 2, 0, 24, 1, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 7, 2124, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 4813650, 1, 1, 0, 0, 0, 0, 0, 0, 0, 144, 1, 52151, 604708828, 1, '', 1);

DELETE FROM `creature` WHERE `id`=52151;
REPLACE INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(52151, 859, 2, 1, 0, 0, -12362, -1911.49, 127.32, 1.32645, 86400, 0, 0, 4813652, 0, 0, 0, 0, 0);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (52157, 0, 0, 0, 0, 0, 37787, 0, 0, 0, 'Ohgan', '', '', 0, 87, 87, 3, 14, 14, 0, 1, 1.14286, 1, 1, 12985, 18594, 0, 23684, 1, 1500, 0, 1, 32832, 0, 6, 0, 0, 0, 0, 1, 1, 0, 0, 72, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 207485, 1, 1, 0, 0, 0, 0, 0, 0, 0, 113, 1, 0, 613097436, 0, '', 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (52156, 0, 0, 0, 0, 0, 37794, 37793, 37796, 37797, 'Chained Spirit', '', '', 0, 1, 1, 3, 35, 35, 0, 1, 1.14286, 1, 0, 2, 2, 0, 24, 1, 2000, 0, 1, 0, 0, 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 12449, 1, 1, 0, 0, 0, 0, 0, 0, 0, 144, 1, 0, 0, 0, '', 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (52606, 0, 0, 0, 0, 0, 37968, 37969, 37970, 37971, 'Gurubashi Warmonger', '', '', 0, 86, 86, 3, 14, 14, 0, 1, 1.14286, 1, 1, 2, 2, 0, 24, 1, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 962340, 10, 1, 0, 0, 0, 0, 0, 0, 0, 144, 1, 52606, 0, 0, '', 1);

DELETE FROM `creature` WHERE `id`=52606;
REPLACE INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(52606, 859, 2, 1, 0, 0, -12257.1, -1849.39, 131.858, 0.925025, 86400, 0, 0, 962340, 0, 0, 0, 0, 0),
(52606, 859, 2, 1, 0, 0, -12305.2, -1828.46, 130.358, 0.0523599, 86400, 0, 0, 962340, 0, 0, 0, 0, 0),
(52606, 859, 2, 1, 0, 0, -12334, -1856.88, 130.379, 0.733038, 86400, 0, 0, 962340, 0, 0, 0, 0, 0),
(52606, 859, 2, 1, 0, 0, -12329.1, -1861.4, 130.344, 0.785398, 86400, 0, 0, 962340, 0, 0, 0, 0, 0),
(52606, 859, 2, 1, 0, 0, -12369.7, -1819.31, 130.343, 5.86431, 86400, 0, 0, 962340, 0, 0, 0, 0, 0),
(52606, 859, 2, 1, 0, 0, -11652.5, -1440.21, 59.1482, 5.20108, 86400, 0, 0, 962340, 0, 0, 0, 0, 0);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (52956, 0, 0, 0, 0, 0, 38102, 0, 0, 0, 'Zandalari Juggernaut', '', '', 0, 86, 86, 3, 14, 14, 0, 1, 1.14286, 1, 1, 530, 713, 0, 827, 7.5, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 399, 559, 169, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8312, 30230, '', 0, 1, 1122730, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '', 1);

DELETE FROM `creature` WHERE `id`=52956;
REPLACE INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(52956, 859, 2, 1, 0, 0, -11785.9, -1560.69, 19.9247, 1.64061, 86400, 0, 0, 1122730, 0, 0, 0, 0, 0),
(52956, 859, 2, 1, 0, 0, -12298.4, -1825.69, 130.328, 4.34587, 86400, 0, 0, 1122730, 0, 0, 0, 0, 0),
(52956, 859, 2, 1, 0, 0, -12367.6, -1850.67, 130.545, 4.83456, 86400, 0, 0, 1122730, 0, 0, 0, 0, 0),
(52956, 859, 2, 1, 0, 0, -12392.6, -1860.34, 130.339, 5.32325, 86400, 0, 0, 1122730, 0, 0, 0, 0, 0),
(52956, 859, 2, 1, 0, 0, -11753.4, -1875.77, 55.5243, 5.07891, 86400, 0, 0, 1122730, 0, 0, 0, 0, 0),
(52956, 859, 2, 1, 0, 0, -11764.1, -1890.07, 56.3693, 0.0698132, 86400, 0, 0, 1122730, 0, 0, 0, 0, 0),
(52956, 859, 2, 1, 0, 0, -11678.7, -1764.92, 13.5905, 3.19395, 86400, 0, 0, 1122730, 0, 0, 0, 0, 0);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (52076, 0, 0, 0, 0, 0, 37973, 37974, 37975, 37976, 'Gurubashi Cauldron-Mixer', '', '', 0, 1, 1, 3, 14, 14, 0, 1, 1.14286, 1, 0, 2, 2, 0, 24, 1, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 7, 72, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 464940, 89080, 1, 0, 0, 0, 0, 0, 0, 0, 144, 1, 0, 0, 0, '', 1);

DELETE FROM `creature` WHERE `id`=52076;
REPLACE INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(52076, 859, 2, 1, 0, 0, -12296.8, -1833.62, 130.395, 2.33874, 86400, 0, 0, 464940, 89080, 0, 0, 0, 0),
(52076, 859, 2, 1, 0, 0, -12337.2, -1843.21, 130.881, 2.47837, 86400, 0, 0, 464940, 89080, 0, 0, 0, 0);


-- Update Script
UPDATE `creature_template` SET `ScriptName`='npc_chained_spirit' WHERE `entry`=52156;
UPDATE `creature_template` SET `ScriptName`='npc_ohgan' WHERE `entry`=52157;
UPDATE `creature_template` SET `ScriptName`='boss_mandokir' WHERE `entry`=52151;

DELETE FROM `creature` WHERE `id`=52156;

DELETE FROM `spell_script_names` WHERE `spell_id` IN (96684,96776,96761,96821,96721,96722,96724,96740,28471);
REPLACE INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(96684,'spell_mandokir_decapitate'),
(96776,'spell_mandokir_bloodletting'),
(96761,'spell_mandokir_devastating_slam'),
(96821,'spell_mandokir_spirit_vengeance_cancel'),
(96721,'spell_mandokir_ohgan_orders'),
(96722,'spell_mandokir_ohgan_orders_trigger'),
(96724,'spell_mandokir_reanimate_ohgan'),
(96740,'spell_mandokir_devastating_slam_damage'),
(28471,'spell_clear_all');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`IN (96724,96721);
REPLACE INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 3, 96724, 0, 0, 31, 0, 3, 52157, 0, 0, 0, '', 'Earth Spike - Ohgan'),
(13, 1, 96721, 0, 0, 31, 0, 3, 52156, 0, 0, 0, '', 'Ohgan''s Orders - Chained Spirit');

DELETE FROM `achievement_criteria_data` WHERE `criteria_id`=16848;
REPLACE INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`, `ScriptName`) VALUES
(16848,11,0,0,'achievement_ohganot_so_fast'),
(16848,12,1,0,'');

UPDATE `creature_template` SET `lootid`='52151' WHERE `entry`=52151;

DELETE FROM `creature_loot_template` WHERE (`entry`=52151);
REPLACE INTO `creature_loot_template` VALUES 
(52151, 69608, 21, 1, 0, 1, 1),
(52151, 69606, 21, 1, 0, 1, 1),
(52151, 69605, 20, 1, 0, 1, 1),
(52151, 69609, 20, 1, 0, 1, 1),
(52151, 69607, 20, 1, 0, 1, 1),
(52151, 68823, 1, 1, 0, 1, 1);

