DELETE FROM `creature_loot_template` WHERE `entry` = 23682;
INSERT INTO `creature_loot_template` (`entry`,`item`,`chanceOrQuestChance`,`lootmode`,`groupid`,`mincountorref`,`maxcount`) VALUES 
(23682,34068,40,1,0,1,1),
(23682,71328,21,1,0,1,1),
(23682,71327,20,1,0,1,1),
(23682,71330,20,1,0,1,1),
(23682,71329,20,1,0,1,1),
(23682,54516,100,1,0,1,1);

DELETE FROM `item_loot_template` WHERE `entry` = 54516;
INSERT INTO `item_loot_template` (`entry`,`item`,`chanceOrQuestChance`,`lootmode`,`groupid`,`mincountorref`,`maxcount`) VALUES 
(54516,33226,93,1,0,1,1),
(54516,33292,31,1,0,1,1),
(54516,33154,26,1,0,1,1),
(54516,37011,18,1,0,1,1),
(54516,71326,4,1,0,1,1),
(54516,71325,4,1,0,1,1),
(54516,34003,4,1,0,1,1),
(54516,34001,4,1,0,1,1),
(54516,20563,4,1,0,1,1),
(54516,34000,4,1,0,1,1),
(54516,20564,4,1,0,1,1),
(54516,34002,4,1,0,1,1),
(54516,20562,4,1,0,1,1),
(54516,20561,4,1,0,1,1),
(54516,20392,4,1,0,1,1),
(54516,20391,4,1,0,1,1),
(54516,49212,4,1,0,1,1),
(54516,49210,4,1,0,1,1),
(54516,49215,4,1,0,1,1),
(54516,20565,4,1,0,1,1),
(54516,20566,4,1,0,1,1),
(54516,20574,4,1,0,1,1),
(54516,20573,4,1,0,1,1),
(54516,20569,4,1,0,1,1),
(54516,20570,4,1,0,1,1),
(54516,20571,4,1,0,1,1),
(54516,20572,4,1,0,1,1),
(54516,20567,4,1,0,1,1),
(54516,20568,4,1,0,1,1),
(54516,49216,4,1,0,1,1);

UPDATE `creature_template` SET `npcflag`='80' WHERE `entry`=3363;
UPDATE `creature_template` SET `npcflag`='80' WHERE `entry`=11017;
UPDATE `creature_template` SET `npcflag`='80' WHERE `entry`=3365;
UPDATE `creature_template` SET `npcflag`='80' WHERE `entry`=3357;

DELETE FROM `spell_script_names` WHERE `scriptname`='spell_dk_corpse_explosion';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_dk_spell_deflection';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_dru_starfall_aoe';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_hun_aspect_of_the_beast';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_warl_ritual_of_doom_effect';

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(25040, 'spell_mark_of_nature');

INSERT INTO `spell_script_names`(`spell_id`, `ScriptName`) VALUES
(28441, 'spell_item_ashbringer');

UPDATE `gameobject_template` SET `ScriptName`='go_large_gjalerbron_cage' WHERE `entry`=186490;
UPDATE `gameobject_template` SET `ScriptName`='go_gjalerbron_cage' WHERE `name`='Gjalerbron Cage';
UPDATE `gameobject_template` SET `ScriptName`='go_large_gjalerbron_cage' WHERE `entry`=186490;
update gameobject_template SET ScriptName='go_veil_skith_cage' WHERE entry in (185202, 185203, 185204, 185205);
UPDATE `gameobject_template` SET `ScriptName` = 'go_frostblade_shrine' WHERE `entry` = '186649';
UPDATE `gameobject_template` SET `ScriptName`= 'go_midsummer_bonfire' WHERE `entry` IN (187559,187564,187914,187916,187917,187919,187920,187921,187922,187923,187924,187925,187926,187927,187928,187929,187930,187931,187932,187933,187934,187935,187936,187937,187938,187939,187940,187941,187942,187943,187944,187945,187946,187947,187948,187949,187950,187951,187952,187953,187954,187955,187956,187957,187958,187959,187960,187961,187962,187963,187964,187965,187966,187967,187968,187969,187970,187971,187972,187973,187974,187975,194032,194033,194034,194035,194036,194037,194038,194039,194040,194042,194043,194044,194045,194046,194048,194049);
UPDATE `creature_template` SET `ScriptName`='npc_fire_elemental' WHERE `entry`=15438;
UPDATE `creature_template` SET `ScriptName`='npc_earth_elemental' WHERE `entry`=15352;
UPDATE `creature_template` SET `ScriptName`='npc_firework',`AIName`='',`faction_A`=35,`faction_H`=35,`unit_flags`=33555200 WHERE `entry` IN (15879,15880,15881,15882,15883,15884,15885,15886,15887,15888,15889,15890,15872,15873,15874,15875,15876,15877,15911,15912,15913,15914,15915,15916,15918);
UPDATE `creature_template` SET `ScriptName`= 'npc_generic_harpoon_cannon' WHERE `entry` IN (27714,30066,30337); 
UPDATE `creature_template`  SET ScriptName = "mob_SonOfFlame" WHERE entry = 12143;
UPDATE `creature_template` SET `ScriptName`='boss_kalecgos' WHERE `entry`=24850;
UPDATE creature_template SET scriptname = 'boss_sathrovarr' WHERE entry = 24892;
UPDATE `creature_template` SET `ScriptName` = 'boss_nalorakk' WHERE `entry` = 23576;
UPDATE `instance_template` SET `script`='instance_zulaman' WHERE `map`=568;
UPDATE gameobject_template SET ScriptName='go_gong_of_bethekk' WHERE entry=180526;
UPDATE `creature_template` SET `Scriptname`='boss_infinite_corruptor' WHERE `entry`=32273;
UPDATE `creature_template` SET `AIName` = '', `ScriptName`='npc_create_helper_cot' WHERE `entry`=27827;
UPDATE `instance_template` SET `script`='instance_firelands' WHERE `map`=720;
UPDATE `instance_template` SET `script`='instance_bastion_of_twilight' WHERE `map`=671;
UPDATE `gameobject_template` SET `ScriptName`='go_princess_stillpines_cage' WHERE `entry`=181928;
UPDATE `gameobject_template` SET `ScriptName`='go_demon_portal' WHERE `entry` IN (177243,177365,177369,177397,177398,177399,177400,177366,177367,177368);
UPDATE `gameobject_template` SET `ScriptName`='go_blackhoof_cage' WHERE `entry`=186287;
UPDATE `creature_template` SET `ScriptName`='npc_omen' WHERE `entry`=15467;
UPDATE `creature_template` SET `ScriptName`='npc_giant_spotlight' WHERE `entry`=15902;
UPDATE `gameobject_template` SET `ScriptName`='go_wind_stone' WHERE `entry` IN (180456,180461,180466,180518,180529,180534,180539,180544,180549,180554,180559,180564);
UPDATE `gameobject_template` SET `ScriptName`='go_strange_pool' WHERE `entry`=184956;
UPDATE `gameobject_template` SET `AIName`='',`ScriptName`='go_thunderspike' WHERE `entry`=184729;
UPDATE `creature_template` SET `ScriptName` = 'npc_simon_bunny' WHERE `entry` = 22923;
UPDATE `creature_template` SET `ScriptName`='boss_flame_leviathan' WHERE `entry`=31133;
UPDATE `creature_template` SET `ScriptName`='boss_flame_leviathan_defense_cannon' WHERE `entry`=33139;
UPDATE `creature_template` SET `ScriptName` = 'boss_razorscale_controller' WHERE `entry` = 33233;
UPDATE `creature_template` SET `ScriptName` = 'boss_razorscale' WHERE `entry` = 33186;
UPDATE `gameobject_template` SET `ScriptName`='go_razorscale_harpoon' WHERE `entry` IN (194519,194541,194542,194543);
UPDATE `creature_template` SET `ScriptName`='boss_malygos' WHERE `entry`=28859;
UPDATE `creature_template` SET `ScriptName`='npc_power_spark' WHERE `entry`=30084;
UPDATE `creature_template` SET `ScriptName`='npc_portal_eoe' WHERE `entry`=30118;
UPDATE `creature_template` SET `ScriptName`='npc_hover_disk' WHERE `entry` IN (30234,30248);
UPDATE `creature_template` SET `ScriptName`='npc_arcane_overload' WHERE `entry`=30282;
UPDATE `creature_template` SET `ScriptName`='npc_wyrmrest_skytalon' WHERE `entry`=30161;
UPDATE `creature_template` SET `ScriptName`='npc_alexstrasza_eoe' WHERE `entry`=32295;
UPDATE `creature_template` SET `ScriptName` = 'npc_devouring_flame' WHERE `entry` = 34188;
UPDATE `creature_template` SET `ScriptName`='npc_hodir_priest' WHERE `entry` IN (32897,33326,32948,33330);
UPDATE `creature_template` SET `ScriptName`='npc_hodir_shaman' WHERE `entry` IN (33328,32901,33332,32950);
UPDATE `creature_template` SET `ScriptName`='npc_hodir_druid' WHERE `entry` IN (33325,32900,32941,33333);
UPDATE `creature_template` SET `ScriptName`='npc_hodir_mage' WHERE `entry` IN (32893,33327,33331,32946);
UPDATE `creature_template` SET `ScriptName`='npc_toasty_fire' WHERE `entry`=33342;
UPDATE `creature_template` SET `ScriptName`='npc_flash_freeze' WHERE `entry`=32926;
UPDATE `creature_template` SET `ScriptName`='npc_icicle' WHERE `entry` IN (33169,33173);
UPDATE `creature_template` SET `ScriptName`='npc_snowpacked_icicle' WHERE `entry`=33174;
UPDATE `creature_template` SET `ScriptName`='boss_hodir' WHERE `entry`=32845;
UPDATE `creature_template` SET `ScriptName`='npc_ice_block' WHERE `entry`=32938;
UPDATE `creature_template` SET `ScriptName`='boss_algalon_the_observer' WHERE `entry`=32871;
UPDATE `creature_template` SET `ScriptName`='npc_collapsing_star' WHERE `entry`=32955;
UPDATE `creature_template` SET `ScriptName`='npc_living_constellation' WHERE `entry`=33052;
UPDATE `creature_template` SET `ScriptName`='npc_brann_bronzebeard_algalon' WHERE `entry`=34064;
UPDATE `gameobject_template` SET `ScriptName`='go_celestial_planetarium_access' WHERE `entry` IN (194628,194752);
UPDATE `creature_template` SET `ScriptName`='npc_stinkbeard' WHERE `entry`=30017;
UPDATE `creature_template` SET `ScriptName`='npc_korrak_bloodrager' WHERE `entry`=30023;
UPDATE `creature_template` SET `InhabitType` = 4, `ScriptName` = 'npc_hyldsmeet_protodrake' WHERE `entry` = 29679;
UPDATE `creature_template` SET `ScriptName`='npc_icefang' WHERE `entry`=29602;
UPDATE `creature_template` SET `ScriptName`='npc_brunnhildar_prisoner' WHERE `entry`=29639;
UPDATE `creature_template` SET `ScriptName`='npc_jungle_punch_target' WHERE `entry` IN(27986,28047,28568);
UPDATE `creature_template` SET `ScriptName`='npc_vereth_the_cunning' WHERE `entry`=30944;
UPDATE `creature_template` SET `ScriptName` = 'npc_onyx_flamecaller' WHERE `entry` = '39814';
UPDATE `creature_template` SET `ScriptName`= 'boss_general_zarithrian' WHERE `entry`=39746;
UPDATE `creature_template` SET `ScriptName`= 'boss_saviana_ragefire' WHERE `entry`=39747;
UPDATE `creature_template` SET `ScriptName`= 'boss_baltharus_the_warborn' WHERE `entry`=39751;
UPDATE `creature_template` SET `ScriptName`= 'npc_xerestrasza' WHERE `entry`=40429;
UPDATE `creature_template` SET `ScriptName`='npc_frostwing_vrykul' WHERE `entry` IN (37132,38125,37127,37134,37133);
UPDATE `creature_template` SET `ScriptName`='boss_ichoron' WHERE `entry`=29313;
UPDATE `creature_template` SET `ScriptName`='boss_skadi' WHERE `entry`=26693;
UPDATE `creature_template` SET `AIName` = '', `ScriptName` = 'npc_scourge_hulk' WHERE `entry` = 26555;
UPDATE `creature_template` SET `ScriptName` = 'npc_spectator' WHERE `entry` = 26667;
UPDATE `creature_template` SET `ScriptName` = 'npc_ritual_channeler' WHERE `entry` = 27281;
UPDATE `creature_template` SET `ScriptName`='npc_vrykul_skeleton' WHERE `entry`=23970;
UPDATE `creature_template` SET `ScriptName` = 'npc_iron_roots' WHERE `entry` in(33168,33088);
UPDATE `creature_template` SET `ScriptName`='npc_unstable_sun_beam' WHERE `entry`=33050;
UPDATE `creature_template` SET `ScriptName`='npc_healthy_spore' WHERE `entry`=33215;
UPDATE `creature_template` SET `ScriptName`='npc_eonars_gift' WHERE `entry`=33228;
UPDATE `creature_template` SET `ScriptName`='npc_nature_bomb' WHERE `entry`=34129;
UPDATE `creature_template` SET `ScriptName`='npc_sun_beam' WHERE `entry`=33170;
UPDATE `creature_template` SET `ScriptName`='npc_detonating_lasher' WHERE `entry`=32918;
UPDATE `creature_template` SET `ScriptName`='npc_ancient_water_spirit' WHERE `entry`=33202;
UPDATE `creature_template` SET `ScriptName`='npc_storm_lasher' WHERE `entry`=32919;
UPDATE `creature_template` SET `ScriptName`='npc_snaplasher' WHERE `entry`=32916;
UPDATE `creature_template` SET `ScriptName`='npc_ancient_conservator' WHERE `entry`=33203;
UPDATE `creature_template` SET `ScriptName`='boss_elder_brightleaf' WHERE `entry`=32915;
UPDATE `creature_template` SET `ScriptName`='boss_elder_ironbranch' WHERE `entry`=32913;
UPDATE `creature_template` SET `ScriptName`='boss_elder_stonebark' WHERE `entry`=32914;
UPDATE `creature_template` SET `ScriptName`='boss_freya' WHERE `entry`=32906;
UPDATE `creature_template` SET `ScriptName` = 'mob_rune_of_power' WHERE `entry` = 33705;
UPDATE `creature_template` SET `ScriptName`='npc_saronite_vapors' WHERE `entry`=33488;
UPDATE `creature_template` SET `ScriptName`='npc_saronite_animus' WHERE `entry`=33524;
UPDATE `creature_template` SET `ScriptName`='boss_general_vezax' WHERE `entry`=33271;
UPDATE `creature_template` SET `ScriptName`='npc_pool_of_tar' WHERE `entry`=33090;
UPDATE `creature_template` SET `ScriptName`='npc_sanctum_sentry' WHERE `entry`=34014;
UPDATE `creature_template` SET `ScriptName`='npc_feral_defender' WHERE `entry`=34035;
UPDATE `creature_template` SET `ScriptName`='npc_auriaya_seeping_trigger' WHERE `entry` =34098;
UPDATE `creature_template` SET `ScriptName`='npc_image_belgaristrasz' WHERE `entry`=28012;
UPDATE creature_template SET ScriptName='boss_eregos' WHERE entry = 27656;
UPDATE `creature_template` SET `ScriptName`='npc_azure_ring_captain' WHERE `entry`=28236;
UPDATE `creature_template` SET `ScriptName`='boss_varos' WHERE `entry`=27447;
UPDATE `creature_template` SET `AIName`='', `ScriptName`='npc_kelthuzad_abomination' WHERE `entry`=16428;
UPDATE `creature_template` SET `scriptname`='mob_bullet_controller' WHERE `entry` = 34743;
UPDATE `creature_template` SET `ScriptName`='boss_paletress' WHERE `entry`=34928;
UPDATE `creature_template` SET `ScriptName`='boss_drakkari_colossus' WHERE `entry`=29307;
UPDATE `creature_template` SET `ScriptName`='npc_overlord_morghor' WHERE `entry`=23139;
UPDATE `gameobject_template` SET `ScriptName`='go_captain_tyralius_prison' WHERE `entry`=184588;
UPDATE `gameobject_template` SET `ScriptName`='go_warmaul_prison' WHERE `entry` IN (182484,182486,182487,182488,182489,182490,182491,182492,182493,182494,182495,182496,182497,182498,182499,182501,182502,182503,182504);
UPDATE `creature_template` SET `ScriptName`='npc_kurenai_captive' WHERE `entry`=18209;
UPDATE `gameobject_template` SET `ScriptName` = 'go_corkis_prison' WHERE `entry` IN (182349,182350,182521);
UPDATE `creature_template` SET `ScriptName` = 'npc_corki' WHERE `entry` IN (18445,20812,18369);
UPDATE `gameobject_template` SET `ScriptName` = 'go_apexis_relic' WHERE `entry` IN (185944,185890);
UPDATE `creature_template` SET `ScriptName`='npc_proto_behemoth' WHERE `entry`=44687;
UPDATE `creature_template` SET `ScriptName`='npc_princess_stillpine' WHERE `entry`=17682;
UPDATE `gameobject_template` SET `flags` = 16,`ScriptName` = 'go_simon_cluster' WHERE `displayId` IN (7364,7365,7366,7367,7369,7371,7373,7375);

UPDATE `playercreateinfo_spell` SET `spell`='28730' WHERE `spell`=50613;
UPDATE `playercreateinfo_spell` SET `spell`='28730' WHERE `spell`=25046;

UPDATE `creature_template` SET `VehicleId`='200' WHERE `entry` IN (50269,62454);

-- Ruins Of Gilneas
UPDATE `quest_template` SET `requiredraces`='2097152' WHERE `zoneorsort`='4706';

-- Gilneas City
UPDATE `quest_template` SET `requiredraces`='2097152' WHERE `zoneorsort`='4755';

-- Gilneas
UPDATE `quest_template` SET `requiredraces`='2097152' WHERE `zoneorsort`='4714';

delete from item_template where `entry`=20558;
delete from item_template where `entry`=20559;
delete from item_template where `entry`=20560;
delete from item_template where `entry`=29024;
delete from item_template where `entry`=29434;
delete from item_template where `entry`=37836;
delete from item_template where `entry`=40752;
delete from item_template where `entry`=40753;
delete from item_template where `entry`=42425;
delete from item_template where `entry`=43228;
delete from item_template where `entry`=43307;
delete from item_template where `entry`=43308;
delete from item_template where `entry`=43589;
delete from item_template where `entry`=45624;
delete from item_template where `entry`=47242;
delete from item_template where `entry`=47395;
delete from item_template where `entry`=49426;

delete from item_loot_template where `item`=20558;
delete from item_loot_template where `item`=20559;
delete from item_loot_template where `item`=20560;
delete from item_loot_template where `item`=29024;
delete from item_loot_template where `item`=29434;
delete from item_loot_template where `item`=37836;
delete from item_loot_template where `item`=40752;
delete from item_loot_template where `item`=40753;
delete from item_loot_template where `item`=42425;
delete from item_loot_template where `item`=43228;
delete from item_loot_template where `item`=43307;
delete from item_loot_template where `item`=43308;
delete from item_loot_template where `item`=43589;
delete from item_loot_template where `item`=45624;
delete from item_loot_template where `item`=47242;
delete from item_loot_template where `item`=47395;
delete from item_loot_template where `item`=49426;

delete from gameobject_loot_template where `item`=20558;
delete from gameobject_loot_template where `item`=20559;
delete from gameobject_loot_template where `item`=20560;
delete from gameobject_loot_template where `item`=29024;
delete from gameobject_loot_template where `item`=29434;
delete from gameobject_loot_template where `item`=37836;
delete from gameobject_loot_template where `item`=40752;
delete from gameobject_loot_template where `item`=40753;
delete from gameobject_loot_template where `item`=42425;
delete from gameobject_loot_template where `item`=43228;
delete from gameobject_loot_template where `item`=43307;
delete from gameobject_loot_template where `item`=43308;
delete from gameobject_loot_template where `item`=43589;
delete from gameobject_loot_template where `item`=45624;
delete from gameobject_loot_template where `item`=47242;
delete from gameobject_loot_template where `item`=47395;
delete from gameobject_loot_template where `item`=49426;

delete from creature_loot_template where `item`=20558;
delete from creature_loot_template where `item`=20559;
delete from creature_loot_template where `item`=20560;
delete from creature_loot_template where `item`=29024;
delete from creature_loot_template where `item`=29434;
delete from creature_loot_template where `item`=37836;
delete from creature_loot_template where `item`=40752;
delete from creature_loot_template where `item`=40753;
delete from creature_loot_template where `item`=42425;
delete from creature_loot_template where `item`=43228;
delete from creature_loot_template where `item`=43307;
delete from creature_loot_template where `item`=43308;
delete from creature_loot_template where `item`=43589;
delete from creature_loot_template where `item`=45624;
delete from creature_loot_template where `item`=47242;
delete from creature_loot_template where `item`=47395;
delete from creature_loot_template where `item`=49426;
