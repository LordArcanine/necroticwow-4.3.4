UPDATE `gameobject_template` SET `ScriptName`='go_jaina_staff_fragment' WHERE `entry`='209318';

DELETE FROM `gameobject` WHERE `id`='209318';
INSERT INTO `gameobject` VALUES ('700399', '209318', '938', '2', '65535', '3070.11', '447.027', '26.6006', '4.94933', '0', '0', '0.618574', '-0.785727', '300', '0', '1');
INSERT INTO `gameobject` VALUES ('700401', '209318', '938', '2', '65535', '3062.79', '542.08', '21.3177', '0.284436', '0', '0', '0.141739', '0.989904', '300', '0', '1');
INSERT INTO `gameobject` VALUES ('700403', '209318', '938', '2', '65535', '3110.99', '557.674', '21.3609', '5.2671', '0', '0', '0.486467', '-0.873699', '300', '0', '1');
INSERT INTO `gameobject` VALUES ('700405', '209318', '938', '2', '65535', '3134.36', '534.873', '22.0742', '4.2427', '0', '0', '0.852235', '-0.523159', '300', '0', '1');
INSERT INTO `gameobject` VALUES ('191383', '209318', '938', '3', '1', '3110.36', '589.464', '24.4798', '0', '0', '0', '0', '1', '7200', '255', '1');
INSERT INTO `gameobject` VALUES ('191384', '209318', '938', '3', '1', '3118.65', '618.646', '26.359', '0', '0', '0', '0', '1', '7200', '255', '1');
INSERT INTO `gameobject` VALUES ('191385', '209318', '938', '3', '1', '3147.8', '506.083', '21.0151', '0', '0', '0', '0', '1', '7200', '255', '1');
INSERT INTO `gameobject` VALUES ('191386', '209318', '938', '3', '1', '3166.66', '583.168', '27.125', '0', '0', '0', '0', '1', '7200', '255', '1');
INSERT INTO `gameobject` VALUES ('191387', '209318', '938', '3', '1', '3170.8', '487.295', '27.6434', '0', '0', '0', '0', '1', '7200', '255', '1');
INSERT INTO `gameobject` VALUES ('191388', '209318', '938', '3', '1', '3139.27', '456.038', '25.2111', '0', '0', '0', '0', '1', '7200', '255', '1');
INSERT INTO `gameobject` VALUES ('191389', '209318', '938', '3', '1', '3066.62', '583.221', '23.0493', '0', '0', '0', '0', '1', '7200', '255', '1');
INSERT INTO `gameobject` VALUES ('191390', '209318', '938', '3', '1', '3189.56', '536.498', '27.4626', '0', '0', '0', '0', '1', '7200', '255', '1');
INSERT INTO `gameobject` VALUES ('191391', '209318', '938', '3', '1', '3035.12', '457.46', '27.9422', '0', '0', '0', '0', '1', '7200', '255', '1');
INSERT INTO `gameobject` VALUES ('191392', '209318', '938', '3', '1', '3103.98', '429.269', '26.5905', '0', '0', '0', '0', '1', '7200', '255', '1');

UPDATE `creature_template` SET `ScriptName`='boss_echo_of_jaina' WHERE `entry`='54445';
UPDATE `creature_template` SET `ScriptName`='jaina_frost_blades',`modelid1`='11686' WHERE `entry`='54494';
UPDATE `creature_template` SET `ScriptName`='npc_flarecore',`modelid1`='11686',`minlevel`='87',`maxlevel`='87' WHERE `entry`='54446';

-- Echo of Jaina
DELETE FROM `creature_text` WHERE `entry`=54445;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
('54445','1','0','I don\'t know who you are, but I\'ll defend this shrine with my life. Leave, now, before we come to blows.','12','0','100','0','0','0','Intro'),
('54445','2','0','You asked for it.','14','0','100','0','0','0','SAY_AGGRO_1'),
('54445','3','0','I hate resorting to violence.','14','0','100','0','0','0','SAY_AGGRO_2'),
('54445','4','0','Why won\'t you give up?!','14','0','100','0','0','0','SAY_FROST_BLADES'),
('54445','5','0','Perhaps this will cool your heads...','14','0','100','0','0','0','SAY_FROSTBOLT_VOLLEY'),
('54445','6','0','A little ice ought to quench the fire in your hearts...!','14','0','100','0','0','0','SAY_BLINK'),
('54445','7','0','You forced my hand.!','14','0','100','0','0','0','SAY_SLAY_1'),
('54445','8','0','I didn\'t want to do that.!','14','0','100','0','0','0','SAY_SLAY_2'),
('54445','9','0','I wish you\'d surrendered!','14','0','100','0','0','0','SAY_SLAY_3'),
('54445','10','0','I understand, now. Farewell, and good luck.','12','0','100','0','0','0','SAY_DEATH');
