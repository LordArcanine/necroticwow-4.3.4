-- Two vendors on instance, he are die when getting damage from AOE boss
UPDATE `creature_template` SET `unit_flags`='2' WHERE `entry` IN (54401,54402);

-- Golems on 25H has aggressive to other npc
UPDATE `creature_template` SET `faction_A`='14',`faction_H`='14' WHERE `entry`='54149';

-- ### Instance Template & Access Requirement ###
DELETE FROM `instance_template` WHERE map = 720;
REPLACE INTO `instance_template` (`map`, `parent`, `script`, `allowMount`) VALUES (720, 0, 'instance_firelands', 1);

DELETE FROM `access_requirement` WHERE `mapId` = 720;
REPLACE INTO `access_requirement` (`mapId`, `difficulty`, `level_min`, `level_max`, `item`, `item2`, `quest_done_A`, `quest_done_H`, `completed_achievement`, `quest_failed_text`, `comment`) VALUES (720, 0, 85, 0, 0, 0, 0, 0, 0, NULL, 'Firelands (Enterence)');
REPLACE INTO `access_requirement` (`mapId`, `difficulty`, `level_min`, `level_max`, `item`, `item2`, `quest_done_A`, `quest_done_H`, `completed_achievement`, `quest_failed_text`, `comment`) VALUES (720, 1, 85, 0, 0, 0, 0, 0, 0, NULL, 'Firelands (Enterence)');
REPLACE INTO `access_requirement` (`mapId`, `difficulty`, `level_min`, `level_max`, `item`, `item2`, `quest_done_A`, `quest_done_H`, `completed_achievement`, `quest_failed_text`, `comment`) VALUES (720, 2, 85, 0, 0, 0, 0, 0, 0, NULL, 'Firelands (Enterence)');
REPLACE INTO `access_requirement` (`mapId`, `difficulty`, `level_min`, `level_max`, `item`, `item2`, `quest_done_A`, `quest_done_H`, `completed_achievement`, `quest_failed_text`, `comment`) VALUES (720, 3, 85, 0, 0, 0, 0, 0, 0, NULL, 'Firelands (Enterence)');
