-- when torment ends this is applied, spell_linked_spell needed for aura removal apply.
DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (99256,-99256,100230,-100230,100231,-100231,100232,-100232);
REPLACE INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES
(-99256,99257,0,'Baloroc - 10N'),
(-100230,99402,0,'Baloroc - 25N'),
(-100231,99403,0,'Baloroc - 10H'),
(-100232,99404,0,'Baloroc - 25H');

-- Loot normal

UPDATE `creature_template` SET `lootid`='53494' WHERE `entry`='53494';

DELETE FROM `creature_loot_template` WHERE `entry`='53494';
REPLACE INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES
(53494,69237,39,1,0,1,2),
(53494,68982,20,1,0,1,1),
(53494,71343,16,1,0,1,1),
(53494,71323,13,1,0,1,1),
(53494,71315,12,1,0,1,1),
(53494,71314,12,1,0,1,1),
(53494,71312,12,1,0,1,1),
(53494,70917,12,1,0,1,1),
(53494,71345,12,1,0,1,1),
(53494,71342,10,1,0,1,1),
(53494,71340,10,1,0,1,1),
(53494,70915,10,1,0,1,1),
(53494,71341,10,1,0,1,1),
(53494,70916,8,1,0,1,1),
(53494,71785,2,1,0,1,1),
(53494,71776,2,1,0,1,1),
(53494,71775,2,1,0,1,1),
(53494,71780,2,1,0,1,1),
(53494,71782,2,1,0,1,1),
(53494,71787,2,1,0,1,1),
(53494,71779,2,1,0,1,1);