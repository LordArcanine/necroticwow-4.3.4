DELETE FROM `spell_script_names` WHERE `spell_id`='98450';
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('98450','spell_searing_seeds');

-- Loot normal
UPDATE `creature_template` SET `lootid`='52571' WHERE `entry`='52571';
DELETE FROM `creature_loot_template` WHERE `entry`='52571';
REPLACE INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES
(52571,69237,39,1,0,1,2),
(52571,71674,34,1,0,1,1),
(52571,71688,34,1,0,1,1),
(52571,71681,34,1,0,1,1),
(52571,71348,20,1,0,1,1),
(52571,68927,18,1,0,1,1),
(52571,71350,16,1,0,1,1),
(52571,68926,16,1,0,1,1),
(52571,71346,15,1,0,1,1),
(52571,71344,14,1,0,1,1),
(52571,69897,14,1,0,1,1),
(52571,71313,14,1,0,1,1),
(52571,71347,13,1,0,1,1),
(52571,71351,11,1,0,1,1),
(52571,71349,11,1,0,1,1),
(52571,70920,10,1,0,1,1),
(52571,71780,2,1,0,1,1),
(52571,71782,2,1,0,1,1),
(52571,71785,2,1,0,1,1),
(52571,71776,2,1,0,1,1),
(52571,71775,2,1,0,1,1),
(52571,71779,2,1,0,1,1);

UPDATE `gameobject` SET `position_x`='570.760' WHERE `id`='208906';



