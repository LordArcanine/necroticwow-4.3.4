DELETE FROM `creature` WHERE `id`='54019' AND `map`='720';
DELETE FROM `creature` WHERE `id`='53089';
REPLACE INTO `creature` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`) VALUES (15441675,53089,720,15,1,0.760,-305.456,53.5113,5.59747,300);
UPDATE `creature_template` SET `ScriptName`='mob_molten_feather' WHERE `entry`='53089';
UPDATE `creature_template` SET `ScriptName`='boss_alysrazor' WHERE `entry`='52530';
UPDATE `creature_template` SET `ScriptName`='npc_fier_tornado' WHERE `entry`='53693';
UPDATE `creature_template` SET `ScriptName`='npc_voracious_hatchling' WHERE `entry`='53509';
UPDATE `creature_template` SET `ScriptName`='npc_blazing_broodmother' WHERE `entry`='53680';
UPDATE `creature_template` SET `ScriptName`='npc_blazing_talon_clawshaper' WHERE `entry`='53734';
UPDATE `creature_template` SET `ScriptName`='npc_blazing_talon' WHERE `entry`='53896';
UPDATE `creature_template` SET `ScriptName`='npc_brushfire' WHERE `entry`='53372';
UPDATE `creature_template` SET `ScriptName`='npc_molten_egg' WHERE `entry`='53681';
UPDATE `creature_template` SET `ScriptName`='npc_fiery_tornado',`minlevel`='85',`maxlevel`='85' WHERE `entry`='53698';
UPDATE `creature_template` SET `ScriptName`='npc_plumb_lava_worm',`faction_A`='14',`faction_H`='14',`unit_flags`='33554438' WHERE `entry`='53520';
UPDATE `creature_template` SET `ScriptName`='mob_incendiary_cloud',`scale`='2'  WHERE `entry`='53541';
UPDATE `creature_template` SET `ScriptName`='mob_blazing_power',`scale`='2' WHERE `entry`='53554';
UPDATE `creature_template` SET `minlevel`='85',`maxlevel`='85',`ScriptName`='npc_alysrazor_volcano' WHERE `entry`='53158';
DELETE FROM `creature_template_addon` WHERE `entry`='53693';
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('97128','spell_molthen_feater');
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('-100698','spell_molthen');
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('-99464','spell_molthen');
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('-99504','spell_molthen');
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('-100699','spell_molthen');
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('-101223','spell_fieroblast');
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('-101294','spell_fieroblast');
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('-101295','spell_fieroblast');
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES ('-101296','spell_fieroblast');
REPLACE INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES 
('-100024','spell_hatchling_debuff'),
('-100721','spell_hatchling_debuff'),
('-100722','spell_hatchling_debuff'),
('-100723','spell_hatchling_debuff');

UPDATE `creature_template` SET `mindmg`='10000', `maxdmg`='20000', `attackpower`='2500', `Health_mod`='38651400', `Mana_mod`='90000000' WHERE `entry`='52530';
UPDATE `creature_template` SET `Health_mod`='8019500' WHERE `entry`='53734';
UPDATE `creature_template` SET `Health_mod`='320780' WHERE `entry`='53896';
UPDATE `creature_template` SET `mindmg`='8000', `maxdmg`='16000', `attackpower`='2300', `dmg_multiplier`='15', `Health_mod`='13942992', `Mana_mod`='13942992' WHERE `entry`='53509';

-- loot 10 normal
UPDATE `creature_template` SET `lootid`='52530' WHERE `entry`='52530';

DELETE FROM `creature_loot_template` WHERE `entry`='52530';
REPLACE INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES
(52530,69237,39,1,0,1,2),
(52530,70990,20,1,0,1,1),
(52530,68983,15,1,0,1,1),
(52530,70989,15,1,0,1,1),
(52530,70734,14,1,0,1,1),
(52530,70735,14,1,0,1,1),
(52530,70739,14,1,0,1,1),
(52530,70738,14,1,0,1,1),
(52530,70736,14,1,0,1,1),
(52530,70733,14,1,0,1,1),
(52530,70987,12,1,0,1,1),
(52530,70985,12,1,0,1,1),
(52530,70986,12,1,0,1,1),
(52530,70988,12,1,0,1,1),
(52530,70737,12,1,0,1,1),
(52530,71665,3,1,0,1,1),
(52530,71776,2,1,0,1,1),
(52530,71780,2,1,0,1,1),
(52530,71782,2,1,0,1,1),
(52530,71785,2,1,0,1,1),
(52530,71775,2,1,0,1,1),
(52530,71787,2,1,0,1,1),
(52530,71779,2,1,0,1,1);