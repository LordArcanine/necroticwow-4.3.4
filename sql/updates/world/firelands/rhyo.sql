UPDATE `creature_template` SET `ScriptName`='npc_left_leg',`faction_A`='14',`faction_H`='14' WHERE `entry`='52577';
UPDATE `creature_template` SET `ScriptName`='npc_right_leg',`faction_A`='14',`faction_H`='14' WHERE `entry`='53087';
UPDATE `creature_template` SET `ScriptName`='boss_lord_rhyolith' WHERE `entry`='52558';
UPDATE `creature_template` SET `VehicleId` = '1606',`Health_mod`='13113000' WHERE `entry` IN (52558);
DELETE FROM `creature` WHERE `id` IN (52577,53087);
REPLACE INTO `creature` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`)
VALUES (15444435,52577,720,15,1,-374.201,-318.474,100.412,1.67546,86400),
(15444365,53087,720,15,1,-374.201,-318.474,100.412,1.67546,86400);
UPDATE `creature` SET `position_x`='-379.919',`position_y`='-276.294',`position_z`='100.441',`orientation`='4.867908' WHERE `id` IN (52558,52577,53087);
UPDATE `creature_template` SET `Health_mod`='154780' WHERE `entry` IN (52577,53087);
UPDATE `creature_template` SET `modelid1`='38054' WHERE `entry`='52582';
UPDATE `creature_template` SET `modelid1`='38063' WHERE `entry`='52866';
UPDATE `creature_template` SET `Health_mod`='135608' WHERE `entry`='52620';
UPDATE `creature_template` SET `Health_mod`='1449064' WHERE `entry`='53211';
UPDATE `creature_template` SET `ScriptName`='npc_spark_of_rhyolith' WHERE `entry`='53211';
UPDATE `creature_template` SET `ScriptName`='npc_rhyolith_volcano',`faction_A`='14',`faction_H`='14' WHERE `entry`='52582';
UPDATE `creature_template` SET `ScriptName`='npc_rhyolith_crater',`faction_A`='14',`faction_H`='14' WHERE `entry`='52866';
UPDATE `creature_template` SET `ScriptName`='npc_lava_line',`faction_A`='14',`faction_H`='14' WHERE `entry`='53585';

