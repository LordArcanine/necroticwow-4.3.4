UPDATE `gameobject_template` SET `flags`='0' WHERE `entry`IN (208779, 208780, 208781, 208782, 208783, 208784, 208785, 208786, 208787, 208788, 208789);

UPDATE `gameobject_template` SET `flags`='0' WHERE `entry`=208694;

-- Batle Gilneas
UPDATE `gameobject_template` SET `flags`='32', `castBarCaption`='' WHERE `entry`=208673;
UPDATE `gameobject_template` SET `data1`='1479', `data2`='3000', `data3`='180100',`data4`='1', `data5`='1', `data6`='37190',`data7`='0',`data8`='1' WHERE `entry`=208673;

UPDATE `gameobject_template` SET `flags`='32' WHERE `entry`=208763;
UPDATE `gameobject_template` SET `data1`='1479' WHERE `entry`=208763;

UPDATE `gameobject_template` SET `flags`='32', `castBarCaption`='Capturing' WHERE `entry`=208748;
UPDATE `gameobject_template` SET `data1`='1479', `data2`='3000', `data3`='180101',`data4`='1', `data5`='1', `data6`='37190',`data7`='0',`data8`='1' WHERE `entry`=208748;

UPDATE `gameobject_template` SET `flags`='32' WHERE `entry`=208733;
UPDATE `gameobject_template` SET `data1`='1955' WHERE `entry`=208733;

UPDATE `gameobject_template` SET `flags`='48' WHERE `entry`=180100;
UPDATE `gameobject_template` SET `flags`='48' WHERE `entry`=180101;
UPDATE `gameobject_template` SET `flags`='32' WHERE `entry`=180102;

-- Batle Gilneas
DELETE FROM `gameobject` WHERE `map`=761;

-- Lighthouse banner
UPDATE `gameobject_template` SET `flags`='32' WHERE `entry`=208779;
UPDATE `gameobject_template` SET `data0`='1479',`data1`='0' WHERE `entry`=208779;

-- Mine banner
UPDATE `gameobject_template` SET `castBarCaption`='Capturing' WHERE `entry`=208782;
UPDATE `gameobject_template` SET `data0`='1479', `data3`='3000', `data10`='23935',`data11`='1', `data13`='1', `data14`='37190',`data16`='1' WHERE `entry`=208782;

-- Waterworks banner
UPDATE `gameobject_template` SET `castBarCaption`='Capturing' WHERE `entry`=208785;
UPDATE `gameobject_template` SET `data0`='1479', `data3`='3000', `data10`='23936',`data11`='1', `data13`='1', `data14`='37190',`data16`='1' WHERE `entry`=208785;

-- Door Battle for Gilneas
UPDATE `gameobject_template` SET `flags`='1' WHERE `entry`=207177;
UPDATE `gameobject_template` SET `flags`='1' WHERE `entry`=207178;

UPDATE `gameobject_template` SET `faction`='83' WHERE `entry`=208673;
UPDATE `gameobject_template` SET `faction`='83' WHERE `entry`=208763;

UPDATE `gameobject_template` SET `faction`='84' WHERE `entry`=208748;
UPDATE `gameobject_template` SET `faction`='84' WHERE `entry`=208733;
