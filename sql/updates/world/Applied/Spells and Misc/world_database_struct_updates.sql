ALTER TABLE `spell_enchant_proc_data`
	CHANGE COLUMN `procEx` `procEx` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `PPMChance`;
	
ALTER TABLE `smart_scripts`
	CHANGE COLUMN `source_type` `source_type` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0' AFTER `entryorguid`;
	
ALTER TABLE `smart_scripts`
	CHANGE COLUMN `event_type` `event_type` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0' AFTER `link`;
	
ALTER TABLE `smart_scripts`
	CHANGE COLUMN `action_type` `action_type` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' AFTER `event_param4`;
	
ALTER TABLE `smart_scripts`
	CHANGE COLUMN `target_type` `target_type` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0' AFTER `action_param6`;
	
ALTER TABLE `creature_onkill_reputation`
	CHANGE COLUMN `CurrencyId1` `CurrencyId1` MEDIUMINT(6) UNSIGNED NOT NULL DEFAULT '0' AFTER `TeamDependent`,
	CHANGE COLUMN `CurrencyId2` `CurrencyId2` MEDIUMINT(6) UNSIGNED NOT NULL DEFAULT '0' AFTER `CurrencyId1`,
	CHANGE COLUMN `CurrencyId3` `CurrencyId3` MEDIUMINT(6) UNSIGNED NOT NULL DEFAULT '0' AFTER `CurrencyId2`;