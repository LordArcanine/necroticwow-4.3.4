DELETE FROM `lfg_dungeon_rewards` WHERE dungeonId IN (300,301,434);
INSERT INTO `lfg_dungeon_rewards` (`dungeonId`, `maxLevel`, `firstQuestId`, `firstMoneyVar`, `firstXPVar`, `otherQuestId`, `otherMoneyVar`, `otherXPVar`) VALUES (300, 85, 28907, 0, 0, 28908, 0, 0);
INSERT INTO `lfg_dungeon_rewards` (`dungeonId`, `maxLevel`, `firstQuestId`, `firstMoneyVar`, `firstXPVar`, `otherQuestId`, `otherMoneyVar`, `otherXPVar`) VALUES (301, 85, 28905, 0, 0, 28906, 0, 0);
INSERT INTO `lfg_dungeon_rewards` (`dungeonId`, `maxLevel`, `firstQuestId`, `firstMoneyVar`, `firstXPVar`, `otherQuestId`, `otherMoneyVar`, `otherXPVar`) VALUES (434, 85, 29185, 0, 0, 29183, 0, 0);


UPDATE `quest_template` SET `RewCurrencyCount1`=150 WHERE  `Id`=28905 LIMIT 1;
UPDATE `quest_template` SET `RewCurrencyId1`=396, `RewCurrencyCount1`=140 WHERE  `Id`=29185 LIMIT 1;
UPDATE `quest_template` SET `RewCurrencyCount1`=150 WHERE  `Id`=28907 LIMIT 1;