CREATE TABLE `guild_challenges` (
	`challengeId` INT(5) UNSIGNED NOT NULL DEFAULT '0',
	`challengeRewardId` INT(5) UNSIGNED NULL DEFAULT NULL,
	`challengeType` TINYINT(1) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`challengeId`),
	INDEX `challengeRewardId` (`challengeRewardId`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;

ALTER TABLE `guild_challenges`
	ADD COLUMN `challengeEntry` INT(5) UNSIGNED NULL DEFAULT NULL AFTER `challengeType`;
	
CREATE TABLE `guild_challenges_rewards` (
	`rewardId` INT(5) UNSIGNED NOT NULL DEFAULT '0',
	`xpReward` INT(5) UNSIGNED NULL DEFAULT NULL,
	`goldReward` INT(5) UNSIGNED NULL DEFAULT NULL,
	`goldExtraReward` INT(5) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`rewardId`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;
