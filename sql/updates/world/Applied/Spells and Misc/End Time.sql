INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `data0`, `data1`, `data2`, `data3`, `data4`, `data5`, `data6`, `data7`, `data8`, `data9`, `data10`, `data11`, `data12`, `data13`, `data14`, `data15`, `data16`, `data17`, `data18`, `data19`, `data20`, `data21`, `data22`, `data23`, `data24`, `data25`, `data26`, `data27`, `data28`, `data29`, `data30`, `data31`, `size`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`) VALUES
(209440, 10, 10794, 'Time Transit Device', '', '', '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13321, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1.38, 0, 0, 0, 0, 0, 0), -- 209440
(209547, 3, 11020, 'Murozond"s Temporal Cache', '', '', '', 1634, 40346, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 85, 0, 0, 0, 0, 1271, 0, 0, 0, 0, 0, 0, 3.5, 0, 0, 0, 0, 0, 0); -- 209547

SET @OGUID = 4000;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) VALUES
(@OGUID+0, 209439, 938, 1, 1, 4346.827, 1285.944, 147.4501, 1.047198, 0, 0, 0, 1, 7200, 255, 1), -- 209439 (Area: -1)
(@OGUID+1, 209990, 938, 1, 1, 4418.551, 1391.46, 119.0086, 4.677484, 0, 0, 0, 1, 7200, 255, 1), -- 209990 (Area: -1)
(@OGUID+2, 209990, 938, 1, 1, 4339.283, 1391.693, 118.8672, 3.961899, 0, 0, 0, 1, 7200, 255, 1), -- 209990 (Area: -1)
(@OGUID+3, 209442, 938, 1, 1, 2991.208, 558.9045, 24.79633, 5.88176, 0, 0, 0, 1, 7200, 255, 1), -- 209442 (Area: -1)
(@OGUID+4, 209318, 938, 1, 1, 3046.747, 529.0087, 20.62789, 0, 0, 0, 0, 1, 7200, 255, 1), -- 209318 (Area: -1)
(@OGUID+5, 209318, 938, 1, 1, 3065.833, 481.4358, 21.45999, 0, 0, 0, 0, 1, 7200, 255, 1), -- 209318 (Area: -1)
(@OGUID+6, 209318, 938, 1, 1, 2987.576, 524.9028, 27.52452, 0, 0, 0, 0, 1, 7200, 255, 1), -- 209318 (Area: -1)
(@OGUID+7, 209318, 938, 1, 1, 3038.577, 512.0261, 21.46424, 0, 0, 0, 0, 1, 7200, 255, 0), -- 209318 (Area: -1)
(@OGUID+8, 209318, 938, 1, 1, 3035.124, 457.4601, 27.94224, 0, 0, 0, 0, 1, 7200, 255, 1), -- 209318 (Area: -1)
(@OGUID+9, 209318, 938, 1, 1, 3147.804, 506.0833, 21.01509, 0, 0, 0, 0, 1, 7200, 255, 1), -- 209318 (Area: -1)
(@OGUID+10, 209318, 938, 1, 1, 3139.271, 456.0382, 25.21105, 0, 0, 0, 0, 1, 7200, 255, 1), -- 209318 (Area: -1)
(@OGUID+11, 209318, 938, 1, 1, 3103.984, 429.2691, 26.59054, 0, 0, 0, 0, 1, 7200, 255, 1), -- 209318 (Area: -1)
(@OGUID+12, 209318, 938, 1, 1, 3107.006, 475.5521, 21.39811, 0, 0, 0, 0, 1, 7200, 255, 1), -- 209318 (Area: -1)
(@OGUID+13, 209318, 938, 1, 1, 3118.646, 618.6458, 26.35898, 0, 0, 0, 0, 1, 7200, 255, 1), -- 209318 (Area: -1)
(@OGUID+14, 209440, 938, 1, 1, 4032.276, -352.3837, 122.2206, 5.489062, 0, 0, 0, 1, 7200, 255, 1), -- 209440 (Area: -1)
(@OGUID+15, 209249, 938, 1, 1, 4110.726, -429.0799, 121.0752, 1.762782, 0, 0, 0, 1, 7200, 255, 1), -- 209249 (Area: -1)
(@OGUID+16, 209547, 938, 1, 1, 4190.462, -441.5955, 120.3372, 2.844883, 0, 0, 0, 1, 7200, 255, 1); -- 209547 (Area: -1)

UPDATE `creature_template` SET `speed_run`=0.857142865657806, `Faction_A`=188, `Faction_H`=188, `unit_flags`=0x20040100, `Modelid1`=2177, `DynamicFlags`=0x20, `resistance4`=1 WHERE `entry`=45439; -- 45439
UPDATE `creature_template` SET `speed_walk`=0.8, `speed_run`=0.28571429848671, `minlevel`=85, `maxlevel`=85, `Faction_A`=2035, `Faction_H`=2035, `unit_flags`=0x2008000, `Modelid1`=23767, `resistance4`=1 WHERE `entry`=54550; -- 54550
UPDATE `creature_template` SET `HoverHeight`=6, `speed_walk`=1.2, `speed_run`=1.71428573131561, `minlevel`=85, `maxlevel`=85, `Faction_A`=103, `unit_flags`=0x8000, `Modelid1`=39524, `resistance4`=1 WHERE `entry`=54543; -- 54543
UPDATE `creature_template` SET `speed_walk`=1.2, `speed_run`=1.71428573131561, `minlevel`=85, `maxlevel`=85, `Faction_A`=103, `unit_flags`=0x8000, `Modelid1`=8311, `resistance4`=1 WHERE `entry`=54552; -- 54552
UPDATE `creature_template` SET `speed_walk`=1.2, `speed_run`=1.71428573131561, `minlevel`=85, `maxlevel`=85, `Faction_A`=103, `unit_flags`=0x8000, `Modelid1`=8249, `resistance4`=1 WHERE `entry`=54553; -- 54553
UPDATE `creature_template` SET `speed_run`=1.28968274593353, `minlevel`=85, `maxlevel`=85, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x2000040, `Modelid1`=38848, `resistance4`=1 WHERE `entry`=54556; -- 54556
UPDATE `creature_template` SET `speed_walk`=2.4, `speed_run`=1.42857146263123, `minlevel`=87, `maxlevel`=87, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x2008040, `Modelid1`=38795, `resistance4`=1 WHERE `entry`=54496; -- 54496
UPDATE `creature_template` SET `speed_walk`=2.4, `speed_run`=1.42857146263123, `minlevel`=87, `maxlevel`=87, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x8040, `BaseAttackTime`=1500, `Modelid1`=38791, `resistance4`=1 WHERE `entry`=54431; -- 54431
UPDATE `creature_template` SET `speed_run`=0.857142865657806, `Faction_A`=1629, `Faction_H`=1629, `unit_flags`=0x308, `Modelid1`=38539, `resistance4`=1 WHERE `entry`=53884; -- 53884
UPDATE `creature_template` SET `speed_walk`=0.8, `speed_run`=0.28571429848671, `minlevel`=85, `maxlevel`=85, `Faction_A`=2035, `Faction_H`=2035, `unit_flags`=0x2008000, `Modelid1`=23767, `resistance4`=1 WHERE `entry`=54585; -- 54585
UPDATE `creature_template` SET `speed_run`=1, `minlevel`=85, `maxlevel`=85, `Faction_A`=1629, `Faction_H`=1629, `unit_flags`=0x808, `Modelid1`=19074, `resistance4`=1 WHERE `entry`=2523; -- 2523
UPDATE `creature_template` SET `speed_run`=1, `minlevel`=85, `maxlevel`=85, `Faction_A`=1629, `Faction_H`=1629, `unit_flags`=0x808, `Modelid1`=19073, `resistance4`=1 WHERE `entry`=5874; -- 5874
UPDATE `creature_template` SET `speed_run`=1, `minlevel`=85, `maxlevel`=85, `Faction_A`=1629, `Faction_H`=1629, `unit_flags`=0x808, `Modelid1`=19075, `resistance4`=1 WHERE `entry`=3573; -- 3573
UPDATE `creature_template` SET `speed_run`=1, `minlevel`=85, `maxlevel`=85, `Faction_A`=1629, `Faction_H`=1629, `unit_flags`=0x808, `Modelid1`=19071, `resistance4`=1 WHERE `entry`=15447; -- 15447
UPDATE `creature_template` SET `speed_walk`=2.4, `speed_run`=1.42857146263123, `VehicleId`=1733, `minlevel`=87, `maxlevel`=87, `unit_flags`=0x8040, `Modelid1`=38796, `npcflag`=0x1000000, `resistance4`=1 WHERE `entry`=54434; -- 54434
UPDATE `creature_template` SET `speed_walk`=2.4, `speed_run`=1.42857146263123, `minlevel`=87, `maxlevel`=87, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x2008340, `Modelid1`=38795, `resistance4`=1 WHERE `entry`=54433; -- 54433
UPDATE `creature_template` SET `HoverHeight`=2.4, `speed_walk`=1.2, `speed_run`=0.992062866687775, `minlevel`=85, `maxlevel`=85, `Faction_A`=1629, `Faction_H`=1629, `unit_flags`=0x2088808, `Modelid1`=35573, `resistance2`=23422, `resistance4`=1, `unit_class`=2 WHERE `entry`=46506; -- 46506
UPDATE `creature_template` SET `speed_run`=0.857142865657806, `unit_flags`=0x2000300, `Modelid1`=26622, `resistance4`=1 WHERE `entry`=29888; -- 29888
UPDATE `creature_template` SET `speed_walk`=0.8, `speed_run`=0.28571429848671, `minlevel`=85, `maxlevel`=85, `Faction_A`=103, `unit_flags`=0x2008000, `Modelid1`=11686, `resistance4`=1 WHERE `entry`=54566; -- 54566
UPDATE `creature_template` SET `speed_run`=1.42857146263123, `minlevel`=85, `maxlevel`=85, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x8040, `Modelid1`=38962, `resistance4`=1 WHERE `entry`=54687; -- 54687
UPDATE `creature_template` SET `speed_run`=1.42857146263123, `minlevel`=85, `maxlevel`=85, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x8040, `Modelid1`=38963, `resistance2`=23422, `resistance4`=1, `unit_class`=2 WHERE `entry`=54690; -- 54690
UPDATE `creature_template` SET `speed_run`=1.42857146263123, `minlevel`=85, `maxlevel`=85, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x8040, `Modelid1`=38965, `resistance4`=1 WHERE `entry`=54693; -- 54693
UPDATE `creature_template` SET `speed_run`=1.42857146263123, `minlevel`=85, `maxlevel`=85, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x8040, `Modelid1`=38964, `resistance2`=17418, `resistance4`=1, `unit_class`=8 WHERE `entry`=54691; -- 54691
UPDATE `creature_template` SET `minlevel`=87, `maxlevel`=87, `Faction_A`=14, `Faction_H`=14, `unit_flags`=0x2000000, `Modelid1`=11686, `resistance4`=1 WHERE `entry`=54542; -- 54542
UPDATE `creature_template` SET `minlevel`=87, `maxlevel`=87, `unit_flags`=0x2000000, `Modelid1`=11686, `resistance4`=1 WHERE `entry`=54639; -- 54639
UPDATE `creature_template` SET `speed_run`=2.14285707473755, `minlevel`=87, `maxlevel`=87, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x2008300, `Modelid1`=38894, `resistance2`=5402, `resistance4`=1, `unit_class`=2 WHERE `entry`=54641; -- 54641
UPDATE `creature_template` SET `speed_run`=0.992062866687775, `minlevel`=85, `maxlevel`=85, `Faction_A`=16, `Faction_H`=16, `Modelid1`=27769, `resistance4`=1 WHERE `entry`=54795; -- 54795
UPDATE `creature_template` SET `speed_walk`=0.8, `speed_run`=2, `minlevel`=87, `maxlevel`=87, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x8140, `Modelid1`=38802, `resistance2`=5402, `resistance4`=1, `unit_class`=2 WHERE `entry`=54445; -- 54445
UPDATE `creature_template` SET `minlevel`=87, `maxlevel`=87, `Faction_A`=14, `Faction_H`=14, `unit_flags`=0x2000000, `Modelid1`=11686, `resistance4`=1 WHERE `entry`=54446; -- 54446
UPDATE `creature_template` SET `minlevel`=87, `maxlevel`=87, `Faction_A`=14, `Faction_H`=14, `unit_flags`=0x2000000, `Modelid1`=11686, `resistance4`=1 WHERE `entry`=54494; -- 54494
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `Faction_A`=2075, `unit_flags`=0x8040, `Modelid1`=19059, `resistance4`=1 WHERE `entry`=54923; -- 54923
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `Faction_A`=2075, `unit_flags`=0x8040, `Modelid1`=19061, `resistance2`=17418, `resistance4`=1, `unit_class`=8 WHERE `entry`=54920; -- 54920
UPDATE `creature_template` SET `minlevel`=88, `maxlevel`=88, `Faction_A`=2010, `unit_flags`=0x8300, `BaseAttackTime`=3000, `Modelid1`=38754, `npcflag`=0x3, `resistance2`=5547, `resistance4`=1, `unit_class`=2 WHERE `entry`=54751; -- 54751
UPDATE `creature_template` SET `speed_walk`=0.8, `speed_run`=0.428571432828903, `minlevel`=87, `maxlevel`=87, `unit_flags`=0x2000040, `Modelid1`=31882, `resistance4`=1 WHERE `entry`=54945; -- 54945
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `unit_flags`=0x2000000, `Modelid1`=11686, `resistance4`=1 WHERE `entry`=54928; -- 54928
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `Faction_A`=31, `Faction_H`=31, `Modelid1`=11686, `resistance2`=17418, `resistance4`=257, `unit_class`=8 WHERE `entry`=54435; -- 54435
UPDATE `creature_template` SET `speed_walk`=1.2, `minlevel`=85, `maxlevel`=85, `Faction_A`=1, `Faction_H`=1, `unit_flags`=0x808, `BaseAttackTime`=1500, `Modelid1`=19110, `resistance2`=23422, `resistance3`=43285, `resistance4`=1, `resistance5`=886, `unit_class`=2 WHERE `entry`=19668; -- 19668
UPDATE `creature_template` SET `minlevel`=60, `maxlevel`=60, `unit_flags`=0x2000100, `Modelid1`=11686, `resistance4`=1 WHERE `entry`=28332; -- 28332
UPDATE `creature_template` SET `speed_run`=0.857142865657806, `Faction_A`=31, `Faction_H`=31, `Modelid1`=134, `resistance4`=1 WHERE `entry`=1412; -- 1412
UPDATE `creature_template` SET `speed_run`=0.857142865657806, `Faction_A`=4, `unit_flags`=0x308, `Modelid1`=29807, `resistance4`=1 WHERE `entry`=35395; -- 35395
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=25655, `npcflag`=0x2D3, `resistance4`=257 WHERE `entry`=28742; -- 28742
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=534, `unit_flags`=0x8340, `Modelid1`=30764, `npcflag`=0x2, `resistance2`=3268, `resistance4`=1, `unit_class`=8 WHERE `entry`=37776; -- 37776
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `Modelid1`=29835, `npcflag`=0x1080, `resistance4`=257 WHERE `entry`=35497; -- 35497
UPDATE `creature_template` SET `minlevel`=74, `maxlevel`=74, `unit_flags`=0x2000200, `Modelid1`=11686, `resistance4`=1 WHERE `entry`=31517; -- 31517
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `Modelid1`=26309, `npcflag`=0x1080, `resistance2`=2754, `resistance4`=257, `unit_class`=8 WHERE `entry`=29494; -- 29494
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=28154, `npcflag`=0x1, `resistance2`=3268, `resistance4`=257, `unit_class`=8 WHERE `entry`=32686; -- 32686
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x200, `Modelid1`=26313, `npcflag`=0x1080, `resistance4`=257 WHERE `entry`=29499; -- 29499
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=28150, `npcflag`=0x1, `resistance2`=3268, `resistance4`=257, `unit_class`=8 WHERE `entry`=32683; -- 32683
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `Modelid1`=25878, `npcflag`=0x1080, `resistance4`=257 WHERE `entry`=28992; -- 28992
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=25609, `npcflag`=0x80, `resistance4`=257 WHERE `entry`=28692; -- 28692
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `Modelid1`=26321, `npcflag`=0x1080, `resistance4`=257 WHERE `entry`=29523; -- 29523
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `Modelid1`=29834, `npcflag`=0x1080, `resistance4`=257 WHERE `entry`=35500; -- 35500
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `Modelid1`=25880, `npcflag`=0x80, `resistance2`=2754, `resistance4`=257, `unit_class`=8 WHERE `entry`=28994; -- 28994
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x200, `Modelid1`=25877, `npcflag`=0x1080, `resistance4`=257 WHERE `entry`=28991; -- 28991
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=27956, `npcflag`=0x10283, `resistance4`=1 WHERE `entry`=32413; -- 32413
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=26070, `resistance2`=3268, `resistance4`=1, `unit_class`=8 WHERE `entry`=29254; -- 29254
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x200, `Modelid1`=26311, `npcflag`=0x1080, `resistance4`=257 WHERE `entry`=29496; -- 29496
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=25608, `npcflag`=0x280, `resistance4`=257 WHERE `entry`=28691; -- 28691
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x200, `Modelid1`=26312, `npcflag`=0x1080, `resistance4`=257 WHERE `entry`=29497; -- 29497
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=27962, `npcflag`=0x280, `resistance4`=1 WHERE `entry`=32421; -- 32421
UPDATE `creature_template` SET `speed_walk`=0.4, `minlevel`=70, `maxlevel`=70, `unit_flags`=0x2000300, `Modelid1`=17200, `resistance4`=1 WHERE `entry`=32265; -- 32265
UPDATE `creature_template` SET `speed_run`=0.714285731315613, `Faction_A`=31, `Faction_H`=31, `Modelid1`=1141, `resistance4`=1 WHERE `entry`=32428; -- 32428
UPDATE `creature_template` SET `speed_walk`=0.888888, `speed_run`=1.58730006217957, `minlevel`=82, `maxlevel`=82, `unit_flags`=0x2000300, `Modelid1`=30807 WHERE `entry`=37859; -- 37859
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=27958, `npcflag`=0x280, `resistance4`=1 WHERE `entry`=32416; -- 32416
UPDATE `creature_template` SET `speed_run`=1, `Faction_A`=1375, `unit_flags`=0x82040300, `Modelid1`=26517, `resistance4`=1 WHERE `entry`=29763; -- 29763
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `unit_flags`=0x8000, `Modelid1`=25623, `npcflag`=0x80, `resistance4`=257 WHERE `entry`=28707; -- 28707
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x200, `Modelid1`=26347, `npcflag`=0x80, `resistance2`=2754, `resistance4`=257, `unit_class`=8 WHERE `entry`=29529; -- 29529
UPDATE `creature_template` SET `speed_run`=1, `Faction_A`=1375, `unit_flags`=0x82040300, `Modelid1`=26518, `resistance4`=1 WHERE `entry`=29764; -- 29764
UPDATE `creature_template` SET `speed_run`=1, `Faction_A`=1375, `unit_flags`=0x82040300, `Modelid1`=26519, `resistance4`=1 WHERE `entry`=29765; -- 29765
UPDATE `creature_template` SET `speed_run`=1, `Faction_A`=1375, `unit_flags`=0x82040300, `Modelid1`=26521, `resistance4`=1 WHERE `entry`=29766; -- 29766
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8300, `Modelid1`=26078, `npcflag`=0x80, `resistance2`=2754, `resistance4`=2, `unit_class`=8 WHERE `entry`=29261; -- 29261
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x200, `Modelid1`=26396, `npcflag`=0x80, `resistance2`=2754, `resistance4`=257, `unit_class`=8 WHERE `entry`=29528; -- 29528
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8300, `Modelid1`=19744, `npcflag`=0x3, `resistance2`=3268, `resistance4`=1, `unit_class`=8 WHERE `entry`=20735; -- 20735
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=25674, `npcflag`=0x81, `resistance4`=257 WHERE `entry`=28776; -- 28776
UPDATE `creature_template` SET `minlevel`=77, `maxlevel`=77, `unit_flags`=0x8000, `Modelid1`=24607, `npcflag`=0x1, `resistance4`=1 WHERE `entry`=33031; -- 33031
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x200, `Modelid1`=26395, `npcflag`=0x82, `resistance2`=2754, `resistance4`=257, `unit_class`=8 WHERE `entry`=29527; -- 29527
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=1732, `unit_flags`=0x8300, `Modelid1`=28188, `resistance2`=3673, `resistance4`=1, `unit_class`=2 WHERE `entry`=32704; -- 32704
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=25602, `npcflag`=0x280, `resistance2`=2754, `resistance4`=1, `unit_class`=8 WHERE `entry`=28682; -- 28682
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `Modelid1`=37403, `npcflag`=0x80, `resistance4`=257 WHERE `entry`=51512; -- 51512
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=25673, `npcflag`=0xC0001, `resistance2`=2754, `resistance4`=257, `unit_class`=8 WHERE `entry`=28774; -- 28774
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=27287, `npcflag`=0x1, `resistance4`=257 WHERE `entry`=30726; -- 30726
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=28143, `resistance2`=3268, `resistance4`=257, `unit_class`=8 WHERE `entry`=32675; -- 32675
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x200, `Modelid1`=27998, `npcflag`=0x80, `resistance4`=257 WHERE `entry`=32509; -- 32509
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=28161, `npcflag`=0x1, `resistance2`=3268, `resistance4`=257, `unit_class`=8 WHERE `entry`=32691; -- 32691
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8100, `Modelid1`=27449, `npcflag`=0x82, `resistance2`=2754, `resistance4`=1, `unit_class`=8 WHERE `entry`=31032; -- 31032
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `Modelid1`=27211, `npcflag`=0x280, `resistance4`=257 WHERE `entry`=29547; -- 29547
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=25621, `npcflag`=0x53, `resistance4`=1 WHERE `entry`=28705; -- 28705
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `unit_flags`=0x300, `Modelid1`=26464, `npcflag`=0x1080, `resistance4`=1 WHERE `entry`=54652; -- 54652
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `unit_flags`=0x200, `Modelid1`=26463, `npcflag`=0x1080, `resistance4`=1 WHERE `entry`=54653; -- 54653
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `unit_flags`=0x300, `Modelid1`=26465, `npcflag`=0x1080, `resistance4`=1 WHERE `entry`=54651; -- 54651
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `unit_flags`=0x8300, `Modelid1`=27509, `npcflag`=0x1 WHERE `entry`=31136; -- 31136
UPDATE `creature_template` SET `speed_run`=0.857142865657806, `Faction_A`=1, `Faction_H`=1, `unit_flags`=0x8308, `Modelid1`=27627, `resistance4`=1 WHERE `entry`=31575; -- 31575
UPDATE `creature_template` SET `speed_run`=0.857142865657806, `Modelid1`=16259, `resistance4`=1 WHERE `entry`=30095; -- 30095
UPDATE `creature_template` SET `speed_run`=0.857142865657806, `minlevel`=80, `maxlevel`=80, `Modelid1`=26759, `resistance4`=1 WHERE `entry`=30094; -- 30094
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x300, `BaseAttackTime`=1500, `Modelid1`=36880, `npcflag`=0x1, `resistance2`=4394, `unit_class`=2 WHERE `entry`=50160; -- 50160
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=25604, `npcflag`=0x10000, `resistance2`=2754, `resistance4`=1, `unit_class`=8 WHERE `entry`=28686; -- 28686
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=1718, `unit_flags`=0x40, `Modelid1`=27216, `resistance4`=1 WHERE `entry`=30659; -- 30659
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2025, `unit_flags`=0x300, `BaseAttackTime`=1500, `Modelid1`=36881, `npcflag`=0x1, `resistance2`=4394, `unit_class`=2 WHERE `entry`=50157; -- 50157
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `unit_flags`=0x200, `Modelid1`=27398, `npcflag`=0x80, `resistance2`=2754, `resistance4`=1, `unit_class`=8 WHERE `entry`=30885; -- 30885
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `unit_flags`=0x200, `Modelid1`=29076, `npcflag`=0x1080, `resistance4`=1 WHERE `entry`=40212; -- 40212
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=36883, `npcflag`=0x1 WHERE `entry`=50155; -- 50155
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x300, `BaseAttackTime`=1500, `Modelid1`=36879, `npcflag`=0x1, `resistance2`=3268, `unit_class`=8 WHERE `entry`=50156; -- 50156
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=27157, `npcflag`=0x20000, `resistance4`=257 WHERE `entry`=30604; -- 30604
UPDATE `creature_template` SET `speed_run`=0.857142865657806, `Faction_A`=1629, `Faction_H`=1629, `unit_flags`=0x308, `Modelid1`=37814, `resistance4`=1 WHERE `entry`=52226; -- 52226
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=27965, `npcflag`=0x280, `resistance4`=1 WHERE `entry`=32426; -- 32426
UPDATE `creature_template` SET `minlevel`=78, `maxlevel`=78, `unit_flags`=0x300, `Modelid1`=28194, `resistance4`=1 WHERE `entry`=32743; -- 32743
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=190, `Faction_H`=190, `unit_flags`=0x8300, `Modelid1`=28755, `npcflag`=0x3, `resistance2`=4394, `resistance4`=1, `unit_class`=2 WHERE `entry`=36669; -- 36669
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x300, `BaseAttackTime`=1500, `Modelid1`=36882, `npcflag`=0x1, `unit_class`=4 WHERE `entry`=50158; -- 50158
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=27158, `npcflag`=0x20000, `resistance4`=257 WHERE `entry`=30605; -- 30605
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `unit_flags`=0x8300, `Modelid1`=25603, `npcflag`=0x280, `resistance4`=1 WHERE `entry`=28685; -- 28685
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=27160, `npcflag`=0x20000, `resistance4`=257 WHERE `entry`=30607; -- 30607
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x300, `Modelid1`=25983, `npcflag`=0x80, `resistance4`=1 WHERE `entry`=28993; -- 28993
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `unit_flags`=0x40, `Modelid1`=27465, `npcflag`=0x2, `resistance2`=4394, `resistance4`=1, `unit_class`=2 WHERE `entry`=31080; -- 31080
UPDATE `creature_template` SET `minlevel`=74, `maxlevel`=74, `unit_flags`=0x8300, `Modelid1`=26779, `npcflag`=0x2, `resistance4`=1 WHERE `entry`=30137; -- 30137
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2027, `Faction_H`=2027, `unit_flags`=0x8300, `Modelid1`=27963, `npcflag`=0x280, `resistance4`=1 WHERE `entry`=32424; -- 32424
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=27159, `npcflag`=0x20000, `resistance4`=257 WHERE `entry`=30606; -- 30606
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `Faction_A`=2007, `Faction_H`=2007, `unit_flags`=0x8000, `Modelid1`=27161, `npcflag`=0x20000, `resistance4`=257 WHERE `entry`=30608; -- 30608
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=1638, `Faction_H`=1638, `unit_flags`=0x300, `Modelid1`=36894, `npcflag`=0x1, `resistance2`=4394, `unit_class`=2 WHERE `entry`=50174; -- 50174
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `Faction_A`=2027, `Faction_H`=2027, `Modelid1`=30311, `resistance4`=1, `unit_class`=4 WHERE `entry`=36774; -- 36774
UPDATE `creature_template` SET `minlevel`=75, `maxlevel`=75, `unit_flags`=0x8000, `Modelid1`=26373, `npcflag`=0x10283, `resistance4`=257 WHERE `entry`=29532; -- 29532
UPDATE `creature_template` SET `speed_run`=0.857142865657806, `Faction_A`=188, `Faction_H`=188, `Modelid1`=6295, `resistance4`=1 WHERE `entry`=32470; -- 32470
UPDATE `creature_template` SET `speed_run`=0.992062866687775, `minlevel`=60, `maxlevel`=60, `Faction_A`=114, `unit_flags`=0x2000000, `Modelid1`=16925, `resistance4`=1 WHERE `entry`=23472; -- 23472
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.571428596973419, `minlevel`=70, `maxlevel`=70, `Faction_A`=1770, `Faction_H`=1770, `unit_flags`=0x80900, `Modelid1`=27916, `resistance4`=1 WHERE `entry`=32322; -- 32322
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `Faction_A`=2101, `Faction_H`=2101, `unit_flags`=0x100, `Modelid1`=27925, `resistance2`=2241, `resistance4`=1, `unit_class`=8 WHERE `entry`=32343; -- 32343
UPDATE `creature_template` SET `speed_walk`=0.5, `speed_run`=0.571428596973419, `minlevel`=70, `maxlevel`=70, `Faction_A`=2101, `Faction_H`=2101, `unit_flags`=0x80900, `Modelid1`=27920, `resistance4`=1 WHERE `entry`=32321; -- 32321
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `Faction_A`=1770, `Faction_H`=1770, `unit_flags`=0x100, `Modelid1`=27922, `resistance2`=2241, `resistance4`=1, `unit_class`=8 WHERE `entry`=32325; -- 32325
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `unit_flags`=0x2000100, `Modelid1`=27766, `resistance2`=2241, `resistance4`=1, `unit_class`=8 WHERE `entry`=32328; -- 32328
UPDATE `creature_template` SET `minlevel`=70, `maxlevel`=70, `unit_flags`=0x2000100, `Modelid1`=27766, `resistance2`=2241, `resistance4`=1, `unit_class`=8 WHERE `entry`=32339; -- 32339
UPDATE `creature_template` SET `speed_run`=1.42857146263123, `minlevel`=80, `maxlevel`=80, `unit_flags`=0x8000, `Modelid1`=26998, `resistance4`=256 WHERE `entry`=30352; -- 30352
UPDATE `creature_template` SET `speed_run`=0.992062866687775, `minlevel`=79, `maxlevel`=79, `Faction_A`=16, `Faction_H`=16, `unit_flags`=0x8000, `Modelid1`=28618, `resistance2`=3165, `resistance4`=1, `unit_class`=8 WHERE `entry`=33422; -- 33422
UPDATE `creature_template` SET `speed_walk`=0.8, `minlevel`=76, `maxlevel`=76, `Faction_A`=1999, `Faction_H`=1999, `unit_flags`=0x8000, `Modelid1`=10957, `resistance4`=1 WHERE `entry`=31236; -- 31236
UPDATE `creature_template` SET `VehicleId`=245, `minlevel`=80, `maxlevel`=80, `unit_flags`=0x2000300, `Modelid1`=17200, `resistance4`=1, `unit_class`=4 WHERE `entry`=30342; -- 30342
UPDATE `creature_template` SET `VehicleId`=230, `minlevel`=80, `maxlevel`=80, `unit_flags`=0x2000300, `Modelid1`=17200, `resistance4`=1, `unit_class`=4 WHERE `entry`=30343; -- 30343
UPDATE `creature_template` SET `speed_walk`=4, `speed_run`=3.57142853736877, `minlevel`=80, `maxlevel`=80, `Faction_A`=974, `unit_flags`=0x40, `Modelid1`=25254, `resistance4`=1 WHERE `entry`=28243; -- 28243
UPDATE `creature_template` SET `HoverHeight`=9, `speed_walk`=1.4, `speed_run`=2.57142853736877, `minlevel`=83, `maxlevel`=83, `Faction_A`=21, `unit_flags`=0x8000, `Modelid1`=27982, `resistance4`=1 WHERE `entry`=32492; -- 32492
UPDATE `creature_template` SET `minlevel`=60, `maxlevel`=60, `unit_flags`=0x2000100, `Modelid1`=21342, `resistance4`=1 WHERE `entry`=32520; -- 32520
UPDATE `creature_template` SET `speed_walk`=2.4, `speed_run`=1.57142853736877, `minlevel`=80, `maxlevel`=80, `Faction_A`=14, `Faction_H`=14, `Modelid1`=27060, `resistance4`=1 WHERE `entry`=24812; -- 24812
UPDATE `creature_template` SET `speed_walk`=1.2, `speed_run`=1.42857146263123, `minlevel`=78, `maxlevel`=78, `Faction_A`=1953, `Faction_H`=1953, `unit_flags`=0x8000, `Modelid1`=24235, `resistance4`=1 WHERE `entry`=31233; -- 31233


REPLACE INTO `creature_template_addon` (`entry`, `mount`, `bytes1`, `bytes2`, `auras`) VALUES
(45439, 0, 0x0, 0x1, '29266'), -- 45439 - 29266
(54550, 0, 0x0, 0x1, '102045'), -- 54550 - 102045
(54543, 0, 0x3000000, 0x1, ''), -- 54543
(54552, 0, 0x0, 0x1, ''), -- 54552
(54553, 0, 0x0, 0x1, ''), -- 54553
(54556, 0, 0x0, 0x1, '97699 98250'), -- 54556 - 97699, 98250
(54496, 0, 0x0, 0x1, ''), -- 54496
(54431, 0, 0x0, 0x1, '101624 101834'), -- 54431 - 101624, 101834
(53884, 0, 0x0, 0x1, '69641 61854 61853'), -- 53884 - 69641, 61854, 61853
(54585, 0, 0x0, 0x1, ''), -- 54585
(2523, 0, 0x0, 0x1, '77747'), -- 2523 - 77747
(5874, 0, 0x0, 0x1, ''), -- 5874
(3573, 0, 0x0, 0x1, ''), -- 3573
(15447, 0, 0x0, 0x1, ''), -- 15447
(54434, 0, 0x0, 0x1, '101594'), -- 54434 - 101594
(54433, 0, 0x0, 0x1, '46598'), -- 54433 - 46598
(46506, 0, 0x0, 0x1, '86703'), -- 46506 - 86703
(29888, 0, 0x0, 0x1, ''), -- 29888
(54566, 0, 0x0, 0x1, '102128'), -- 54566 - 102128
(54687, 0, 0x0, 0x1, ''), -- 54687
(54690, 0, 0x0, 0x1, ''), -- 54690
(54693, 0, 0x0, 0x1, ''), -- 54693
(54691, 0, 0x0, 0x1, ''), -- 54691
(54542, 0, 0x0, 0x1, ''), -- 54542
(54639, 0, 0x0, 0x1, '102206'), -- 54639 - 102206
(54641, 0, 0x0, 0x1, ''), -- 54641
(54795, 0, 0x0, 0x1, '102406'), -- 54795 - 102406
(54445, 0, 0x0, 0x1, ''), -- 54445
(53884, 0, 0x0, 0x1, '69641'), -- 53884 - 69641
(54446, 0, 0x0, 0x1, '101588'), -- 54446 - 101588
(54494, 0, 0x0, 0x1, '101338'), -- 54494 - 101338
(54923, 0, 0x0, 0x1, ''), -- 54923
(54920, 0, 0x0, 0x1, ''), -- 54920
(54751, 0, 0x0, 0x1, '102602'), -- 54751 - 102602
(54945, 0, 0x0, 0x1, ''), -- 54945
(54928, 0, 0x0, 0x1, ''), -- 54928
(54435, 0, 0x0, 0x101, '69676'), -- 54435 - 69676
(19668, 0, 0x0, 0x1, '34429'), -- 19668 - 34429
(28332, 0, 0x0, 0x1, ''), -- 28332
(1412, 0, 0x0, 0x1, ''), -- 1412
(35395, 0, 0x0, 0x1, '67426'), -- 35395 - 67426
(28742, 0, 0x0, 0x101, ''), -- 28742
(37776, 0, 0x0, 0x1, ''), -- 37776
(35497, 0, 0x0, 0x101, ''), -- 35497
(31517, 0, 0x0, 0x1, ''), -- 31517
(29494, 0, 0x0, 0x101, ''), -- 29494
(32686, 0, 0x0, 0x101, ''), -- 32686
(29499, 0, 0x0, 0x101, ''), -- 29499
(32683, 0, 0x0, 0x101, ''), -- 32683
(28992, 0, 0x0, 0x101, ''), -- 28992
(28692, 0, 0x0, 0x101, '60913'), -- 28692 - 60913
(29523, 0, 0x0, 0x101, '60913 61354'), -- 29523 - 60913, 61354
(35500, 0, 0x0, 0x101, ''), -- 35500
(28994, 0, 0x0, 0x101, '60913'), -- 28994 - 60913
(28991, 0, 0x0, 0x101, ''), -- 28991
(32413, 0, 0x0, 0x1, ''), -- 32413
(29254, 0, 0x0, 0x1, ''), -- 29254
(29496, 0, 0x0, 0x101, ''), -- 29496
(28691, 0, 0x8, 0x101, ''), -- 28691
(29497, 0, 0x0, 0x101, '60913'), -- 29497 - 60913
(32421, 0, 0x0, 0x1, ''), -- 32421
(32265, 0, 0x3000000, 0x1, ''), -- 32265
(32428, 0, 0x0, 0x1, ''), -- 32428
(37859, 0, 0x3000000, 0x0, ''), -- 37859
(32416, 0, 0x0, 0x1, ''), -- 32416
(29763, 0, 0x0, 0x1, ''), -- 29763
(28707, 0, 0x0, 0x101, ''), -- 28707
(29529, 0, 0x0, 0x101, ''), -- 29529
(29764, 0, 0x0, 0x1, ''), -- 29764
(29765, 0, 0x0, 0x1, ''), -- 29765
(29766, 0, 0x0, 0x1, ''), -- 29766
(29261, 0, 0x0, 0x2, ''), -- 29261
(29528, 0, 0x0, 0x101, '60913'), -- 29528 - 60913
(20735, 0, 0x0, 0x1, ''), -- 20735
(28776, 0, 0x0, 0x101, '60913'), -- 28776 - 60913
(33031, 0, 0x0, 0x1, ''), -- 33031
(29527, 0, 0x0, 0x101, ''), -- 29527
(32704, 0, 0x0, 0x1, ''), -- 32704
(28682, 0, 0x0, 0x1, ''), -- 28682
(51512, 0, 0x0, 0x101, ''), -- 51512
(28774, 0, 0x0, 0x101, ''), -- 28774
(30726, 0, 0x0, 0x101, ''), -- 30726
(32675, 0, 0x0, 0x101, ''), -- 32675
(32509, 0, 0x0, 0x101, ''), -- 32509
(32691, 0, 0x0, 0x101, ''), -- 32691
(31032, 0, 0x0, 0x1, ''), -- 31032
(29547, 0, 0x0, 0x101, ''), -- 29547
(28705, 0, 0x0, 0x1, ''), -- 28705
(54652, 0, 0x0, 0x1, ''), -- 54652
(54653, 0, 0x0, 0x1, ''), -- 54653
(54651, 0, 0x0, 0x1, ''), -- 54651
(31136, 0, 0x0, 0x0, ''), -- 31136
(31575, 0, 0x0, 0x1, ''), -- 31575
(30095, 0, 0x0, 0x1, ''), -- 30095
(30094, 0, 0x0, 0x1, ''), -- 30094
(50160, 0, 0x0, 0x0, '79962 79963 63510 63514 63531'), -- 50160 - 79962, 79963, 63510, 63514, 63531
(28686, 0, 0x0, 0x1, ''), -- 28686
(30659, 0, 0x0, 0x1, ''), -- 30659
(50157, 0, 0x0, 0x0, ''), -- 50157
(30885, 0, 0x0, 0x1, ''), -- 30885
(40212, 0, 0x0, 0x1, ''), -- 40212
(50155, 0, 0x0, 0x0, ''), -- 50155
(50156, 0, 0x0, 0x0, ''), -- 50156
(30604, 0, 0x0, 0x101, ''), -- 30604
(52226, 0, 0x0, 0x1, '96573 61853'), -- 52226 - 96573, 61853
(32426, 0, 0x0, 0x1, ''), -- 32426
(32743, 0, 0x1, 0x1, ''), -- 32743
(36669, 0, 0x0, 0x1, ''), -- 36669
(50158, 0, 0x0, 0x0, '10022'), -- 50158 - 10022
(30605, 0, 0x0, 0x101, ''), -- 30605
(28685, 0, 0x0, 0x1, ''), -- 28685
(30607, 0, 0x0, 0x101, ''), -- 30607
(28993, 0, 0x0, 0x1, ''), -- 28993
(31080, 0, 0x0, 0x1, ''), -- 31080
(30137, 0, 0x0, 0x1, ''), -- 30137
(32424, 0, 0x0, 0x1, ''), -- 32424
(30606, 0, 0x8, 0x101, ''), -- 30606
(30608, 0, 0x0, 0x101, ''), -- 30608
(50174, 0, 0x0, 0x0, '79892'), -- 50174 - 79892
(36774, 0, 0x0, 0x1, ''), -- 36774
(29532, 0, 0x0, 0x101, ''), -- 29532
(32470, 0, 0x0, 0x1, ''), -- 32470
(23472, 0, 0x0, 0x1, ''), -- 23472
(32322, 0, 0x0, 0x1, '32724 9128'), -- 32322 - 32724, 9128
(32343, 0, 0x0, 0x1, '35775'), -- 32343 - 35775
(32321, 0, 0x0, 0x1, '35775 9128'), -- 32321 - 35775, 9128
(32325, 0, 0x0, 0x1, '32724'), -- 32325 - 32724
(32328, 0, 0x0, 0x1, ''), -- 32328
(32339, 0, 0x0, 0x1, ''), -- 32339
(30352, 0, 0x0, 0x100, ''), -- 30352
(33422, 0, 0x0, 0x1, ''), -- 33422
(31236, 0, 0x0, 0x1, ''), -- 31236
(30342, 0, 0x0, 0x1, ''), -- 30342
(30343, 0, 0x0, 0x1, ''), -- 30343
(28243, 0, 0x0, 0x1, ''), -- 28243
(32492, 0, 0x3000000, 0x1, ''), -- 32492
(32520, 0, 0x0, 0x1, '55228'), -- 32520 - 55228
(32520, 0, 0x0, 0x1, '55230'), -- 32520 - 55230
(32520, 0, 0x0, 0x1, '55167'), -- 32520 - 55167
(32520, 0, 0x0, 0x1, '61179'), -- 32520 - 61179
(32520, 0, 0x10000, 0x1, '49414 56644'), -- 32520 - 49414, 56644
(32520, 0, 0x0, 0x1, ''), -- 32520
(24812, 0, 0x0, 0x1, ''), -- 24812
(24812, 0, 0x0, 0x1, '44385'), -- 24812 - 44385
(31233, 0, 0x0, 0x1, ''); -- 31233

REPLACE INTO `creature_model_info` (`modelid`, `bounding_radius`, `combat_reach`, `gender`) VALUES
(134, 1.3, 1.95, 2),
(1141, 1, 1, 2),
(2177, 0.1, 1, 2),
(6295, 0.2, 1, 2),
(8249, 1.5, 1.5, 1),
(8311, 2.9505, 2.25, 0),
(10957, 1.05, 1.05, 0),
(11686, 0.5, 1, 2),
(16259, 0.25, 0.25, 2),
(16925, 0.5, 1, 2),
(17200, 0.125, 0.25, 2),
(19059, 0.3, 1, 0),
(19061, 0.3, 1, 1),
(19071, 1, 1, 2),
(19073, 1, 1, 2),
(19074, 1, 1, 2),
(19075, 1, 1, 2),
(19110, 0.5, 1, 2),
(19744, 0.4213, 1.65, 0),
(21342, 0.5, 1, 0),
(23767, 0.39, 1, 2),
(24235, 1.2, 0, 0),
(24607, 0.306, 1.5, 0),
(24890, 0.1, 0, 2),
(24895, 0.12, 0, 2),
(24898, 0.1, 0, 2),
(25254, 3.1, 10, 0),
(25602, 0.3519, 1.725, 1),
(25603, 0.383, 1.5, 1),
(25604, 0.383, 1.5, 0),
(25608, 0.208, 1.5, 1),
(25609, 0.306, 1.5, 0),
(25621, 0.208, 1.5, 1),
(25623, 0.306, 1.5, 0),
(25655, 0.208, 1.5, 1),
(25673, 0.306, 1.5, 0),
(25674, 0.208, 1.5, 1),
(25877, 0.383, 1.5, 0),
(25878, 0.208, 1.5, 1),
(25880, 0.208, 1.5, 1),
(25983, 0.208, 1.5, 1),
(26067, 0.383, 1.5, 0),
(26068, 0.383, 1.5, 0),
(26069, 0.383, 1.5, 1),
(26070, 0.383, 1.5, 1),
(26078, 0.3519, 1.725, 0),
(26309, 0.306, 1.5, 0),
(26311, 0.208, 1.5, 1),
(26312, 0.306, 1.5, 0),
(26313, 0.306, 1.5, 0),
(26321, 0.347, 1.5, 0),
(26347, 0.306, 1.5, 1),
(26373, 0.306, 1.5, 0),
(26395, 0.306, 1.5, 0),
(26396, 0.208, 1.5, 1),
(26463, 0.306, 1.5, 0),
(26464, 0.306, 1.5, 1),
(26465, 0.306, 1.5, 0),
(26517, 0.2, 0.2, 2),
(26518, 0.2, 0.2, 2),
(26519, 0.2, 0.2, 2),
(26521, 0.2, 0.2, 2),
(26622, 0.1, 0, 2),
(26759, 2, 2, 2),
(26779, 0.306, 1.5, 0),
(26997, 0.306, 1.5, 0),
(26998, 0.306, 1.5, 0),
(27060, 0.62, 4, 0),
(27157, 0.306, 1.5, 0),
(27158, 0.306, 1.5, 0),
(27159, 0.306, 1.5, 0),
(27160, 0.306, 1.5, 0),
(27161, 0.306, 1.5, 0),
(27211, 0.5205, 3, 2),
(27216, 0.3366, 1.65, 0),
(27287, 0.208, 1.5, 1),
(27331, 0.208, 1.5, 1),
(27398, 0.306, 1.5, 0),
(27449, 0.306, 1.5, 0),
(27465, 0.3519, 1.725, 0),
(27509, 0.383, 1.5, 0),
(27627, 2, 2, 2),
(27766, 0.75, 1.5, 0),
(27769, 1, 15, 2),
(27916, 0.3536, 2.55, 1),
(27920, 1.65699, 6.885, 0),
(27922, 0.5899, 2.55, 1),
(27925, 0.5202, 2.55, 0),
(27956, 0.383, 1.5, 1),
(27958, 0.306, 1.5, 0),
(27962, 0.208, 1.5, 1),
(27963, 0.347, 1.5, 1),
(27965, 0.347, 1.5, 1),
(27982, 0.93, 9.9, 2),
(27998, 0.347, 1.5, 0),
(28143, 0.3519, 1.725, 0),
(28150, 0.208, 1.5, 1),
(28154, 0.306, 1.5, 0),
(28161, 0.306, 1.5, 0),
(28188, 0.306, 1.5, 0),
(28194, 0.372, 1.5, 0),
(28618, 0.3875, 1.25, 0),
(28755, 0.383, 1.5, 0),
(29076, 0.3366, 1.65, 0),
(29807, 1, 1, 2),
(29834, 0.347, 1.5, 1),
(29835, 0.306, 1.5, 0),
(30311, 0.383, 1.5, 1),
(30764, 0.208, 1.5, 1),
(30807, 0.124, 2.4, 0),
(31882, 1.5, 1.5, 2),
(35573, 0.186, 0.6, 0),
(36879, 0.383, 1.5, 0),
(36880, 0.383, 1.5, 0),
(36881, 0.383, 1.5, 1),
(36882, 0.383, 1.5, 0),
(36883, 0.383, 1.5, 0),
(36894, 1, 1.5, 1),
(37403, 0.383, 1.5, 1),
(37814, 0.1155, 0.495, 0),
(38539, 0.3, 0.6, 2),
(38754, 0.4596, 1.8, 0),
(38791, 1.9494, 8.1, 0),
(38795, 0.78, 2, 2),
(38796, 1.2, 1.2, 2),
(38802, 0.62, 0, 1),
(38848, 0.05, 0, 2),
(38894, 0.62, 0, 1),
(38962, 0.459, 2.25, 0),
(38963, 0.5745, 2.25, 0),
(38964, 0.5745, 2.25, 1),
(38965, 0.5205, 2.25, 0),
(39524, 0.31, 6, 2);

SET @CGUID = 10201;
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `MovementType`) VALUES
(@CGUID+0, 45439, 938, 1, 1, 4342.79, 1311.722, 148.695, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+1, 45439, 938, 1, 1, 4386.134, 1277.319, 148.1504, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+2, 54550, 938, 1, 1, 4392.598, 1313.728, 147.0604, 0.561187, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+3, 54550, 938, 1, 1, 4395.964, 1302.704, 145.4518, 5.166426, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+4, 54550, 938, 1, 1, 4384.32, 1322.452, 146.8586, 1.476864, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+5, 54550, 938, 1, 1, 4386.973, 1314.4, 147.6351, 5.282788, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+6, 54550, 938, 1, 1, 4381.93, 1296.821, 146.378, 6.066635, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+7, 54550, 938, 1, 1, 4384.976, 1308.791, 147.7867, 4.245247, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+8, 54550, 938, 1, 1, 4383.47, 1316.287, 147.858, 4.371796, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+9, 54550, 938, 1, 1, 4396.809, 1318.699, 145.1445, 0.363381, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+10, 54550, 938, 1, 1, 4397.308, 1299.176, 144.8813, 3.646319, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+11, 45439, 938, 1, 1, 4409.694, 1303.144, 148.8376, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+12, 45439, 938, 1, 1, 4423.49, 1331.155, 144.114, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+13, 54550, 938, 1, 1, 4408.434, 1318.208, 145.9359, 0.06643828, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+14, 54550, 938, 1, 1, 4400.991, 1315.02, 145.1386, 4.164766, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+15, 54550, 938, 1, 1, 4410.694, 1326.322, 144.2513, 2.329249, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+16, 45439, 938, 1, 1, 4342.189, 1337.795, 158.103, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+17, 45439, 938, 1, 1, 4350.655, 1339.974, 144.1165, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+18, 54550, 938, 1, 1, 4380.473, 1349.608, 141.5467, 2.232268, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+19, 54550, 938, 1, 1, 4384.203, 1347.001, 142.1454, 5.971526, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+20, 54550, 938, 1, 1, 4375.534, 1364.679, 138.1356, 2.722079, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+21, 54550, 938, 1, 1, 4333.003, 1364.08, 159.4431, 4.237825, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+22, 54550, 938, 1, 1, 4388.891, 1384.478, 129.3622, 5.182587, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+23, 54550, 938, 1, 1, 4356.592, 1365.748, 137.1934, 1.641199, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+24, 54550, 938, 1, 1, 4357.853, 1352.507, 140.8739, 6.200044, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+25, 54550, 938, 1, 1, 4315.987, 1363.413, 171.3779, 5.373555, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+26, 54550, 938, 1, 1, 4370.442, 1341.225, 142.6778, 0.4855271, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+27, 54550, 938, 1, 1, 4399.02, 1360.922, 137.3451, 3.91318, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+28, 54550, 938, 1, 1, 4385.041, 1351.933, 140.9871, 3.650684, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+29, 54550, 938, 1, 1, 4395.021, 1351.256, 140.6605, 3.332097, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+30, 54550, 938, 1, 1, 4341.682, 1334.504, 155.9933, 1.238542, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+31, 45439, 938, 1, 1, 4402.943, 1365.333, 136.3008, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+32, 54550, 938, 1, 1, 4408.151, 1343.66, 142.7129, 5.108307, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+33, 54550, 938, 1, 1, 4399.819, 1342.433, 142.4414, 3.692225, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+34, 54543, 938, 1, 1, 4394.689, 1305.934, 151.1577, 2.984513, 7200, 0, 0), -- 54543 (Area: -1)
(@CGUID+35, 54552, 938, 1, 1, 4389.746, 1302.391, 147.9358, 2.635447, 7200, 0, 0), -- 54552 (Area: -1)
(@CGUID+36, 54553, 938, 1, 1, 4389.772, 1305.988, 147.8819, 2.75762, 7200, 0, 0), -- 54553 (Area: -1)
(@CGUID+37, 54552, 938, 1, 1, 4387.021, 1311.379, 147.8887, 3.862359, 7200, 0, 0), -- 54552 (Area: -1)
(@CGUID+38, 54553, 938, 1, 1, 4388.489, 1308.249, 147.8887, 3.805369, 7200, 0, 0), -- 54553 (Area: -1)
(@CGUID+39, 54556, 938, 1, 1, 4404.695, 1280.943, 157.635, 0, 7200, 0, 0), -- 54556 (Area: -1)
(@CGUID+40, 54556, 938, 1, 1, 4328.203, 1265.259, 165.1421, 0, 7200, 0, 0), -- 54556 (Area: -1)
(@CGUID+41, 54543, 938, 1, 1, 4383.164, 1305.919, 153.8887, 3.894926, 7200, 0, 0), -- 54543 (Area: -1)
(@CGUID+42, 54496, 938, 1, 1, 4337.902, 1456.681, 132.3431, 0, 7200, 0, 0), -- 54496 (Area: -1)
(@CGUID+43, 54496, 938, 1, 1, 4377.944, 1482.354, 132.3906, 0, 7200, 0, 0), -- 54496 (Area: -1)
(@CGUID+44, 54496, 938, 1, 1, 4379.835, 1410.281, 132.4122, 0, 7200, 0, 0), -- 54496 (Area: -1)
(@CGUID+45, 54496, 938, 1, 1, 4409.184, 1445.623, 132.3694, 0, 7200, 0, 0), -- 54496 (Area: -1)
(@CGUID+46, 54431, 938, 1, 1, 4380.664, 1482.352, 132.2782, 4.660029, 7200, 0, 0), -- 54431 (Area: -1)
(@CGUID+47, 54556, 938, 1, 1, 4437.101, 1353.821, 163.6857, 0, 7200, 0, 0), -- 54556 (Area: -1)
(@CGUID+48, 54543, 938, 1, 1, 4352.512, 1354.795, 146.6941, 6.143559, 7200, 0, 0), -- 54543 (Area: -1)
(@CGUID+49, 54552, 938, 1, 1, 4354.748, 1357.566, 139.3173, 5.637414, 7200, 0, 0), -- 54552 (Area: -1)
(@CGUID+50, 54553, 938, 1, 1, 4351.11, 1360.583, 139.724, 5.61996, 7200, 0, 0), -- 54553 (Area: -1)
(@CGUID+51, 54556, 938, 1, 1, 4326.59, 1355.175, 162.0462, 0, 7200, 0, 0), -- 54556 (Area: -1)
(@CGUID+52, 54543, 938, 1, 1, 4367.208, 1355.176, 145.6714, 2.108472, 7200, 0, 0), -- 54543 (Area: -1)
(@CGUID+53, 54553, 938, 1, 1, 4366.32, 1355.025, 139.9011, 5.28677, 7200, 0, 0), -- 54553 (Area: -1)
(@CGUID+54, 54552, 938, 1, 1, 4368.932, 1353.593, 140.0985, 6.125743, 7200, 0, 0), -- 54552 (Area: -1)
(@CGUID+55, 54550, 938, 1, 1, 4406.264, 1375.597, 134.9119, 0.7881606, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+56, 45439, 938, 1, 1, 4339.944, 1409.507, 133.8307, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+57, 45439, 938, 1, 1, 4428.346, 1402.547, 132.3557, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+58, 45439, 938, 1, 1, 4444.255, 1421.899, 146.3931, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+59, 54550, 938, 1, 1, 4426.738, 1391.384, 133.2651, 1.584467, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+60, 54550, 938, 1, 1, 4430.174, 1409.499, 128.8993, 1.088365, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+61, 54550, 938, 1, 1, 4452.44, 1386.065, 170.347, 1.708964, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+62, 54550, 938, 1, 1, 4438.93, 1345.275, 171.3077, 4.785715, 7200, 0, 0), -- 54550 (Area: -1)
(@CGUID+63, 45439, 938, 1, 1, 4443.146, 1458.203, 133.0238, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+64, 45439, 938, 1, 1, 4425.683, 1492.208, 133.8816, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+65, 45439, 938, 1, 1, 4302.61, 1488.701, 133.1618, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+66, 54433, 938, 1, 1, 4360.25, 1447.956, 128.9067, 1.218074, 7200, 0, 0), -- 54433 (Area: -1)
(@CGUID+67, 54433, 938, 1, 1, 4394.876, 1456.607, 129.2721, 2.309731, 7200, 0, 0), -- 54433 (Area: -1)
(@CGUID+68, 54433, 938, 1, 1, 4367.638, 1480.624, 129.4093, 6.192595, 7200, 0, 0), -- 54433 (Area: -1)
(@CGUID+69, 54433, 938, 1, 1, 4395.124, 1459.588, 129.1276, 2.455025, 7200, 0, 0), -- 54433 (Area: -1)
(@CGUID+70, 54687, 938, 1, 1, 2992.393, 508.929, 26.3895, 2.35836, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+71, 54687, 938, 1, 1, 2996.653, 504.6866, 26.48465, 2.358289, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+72, 54690, 938, 1, 1, 2996.638, 513.1689, 26.69662, 2.358957, 7200, 0, 0), -- 54690 (Area: -1)
(@CGUID+73, 54693, 938, 1, 1, 2988.142, 513.1624, 26.48465, 2.358527, 7200, 0, 0), -- 54693 (Area: -1)
(@CGUID+74, 54691, 938, 1, 1, 2988.162, 504.6744, 26.48465, 2.358223, 7200, 0, 0), -- 54691 (Area: -1)
(@CGUID+75, 45439, 938, 1, 1, 3047.869, 437.4809, 28.25405, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+76, 45439, 938, 1, 1, 2980.706, 497.7743, 27.10397, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+77, 54542, 938, 1, 1, 3016.827, 487.9844, 27.38586, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+78, 54542, 938, 1, 1, 3005.825, 523.7205, 28.17572, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+79, 54542, 938, 1, 1, 3050.788, 493.6111, 21.50989, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+80, 54542, 938, 1, 1, 2999.208, 501.9774, 26.53789, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+81, 45439, 938, 1, 1, 2989.401, 434.8333, 34.82639, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+82, 45439, 938, 1, 1, 3014.675, 450.0642, 26.31597, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+83, 54542, 938, 1, 1, 3034.321, 515.618, 21.60052, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+84, 54691, 938, 1, 1, 3061.689, 491.571, 21.67805, 5.517753, 7200, 0, 0), -- 54691 (Area: -1)
(@CGUID+85, 54687, 938, 1, 1, 3066.017, 487.4127, 21.66775, 5.517905, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+86, 54693, 938, 1, 1, 3065.84, 495.9017, 21.65922, 5.517122, 7200, 0, 0), -- 54693 (Area: -1)
(@CGUID+87, 45439, 938, 1, 1, 3115.684, 505.2326, 6.433718, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+88, 54542, 938, 1, 1, 3074.481, 467.2066, 21.83661, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+89, 54542, 938, 1, 1, 3074.481, 467.2066, 21.83661, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+90, 54542, 938, 1, 1, 3078.087, 441.2361, 27.26732, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+91, 54687, 938, 1, 1, 3070.166, 491.7457, 21.16503, 5.518205, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+92, 54542, 938, 1, 1, 3008.868, 563.3976, 26.75361, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+93, 54542, 938, 1, 1, 3034.635, 571.1649, 27.98734, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+94, 54542, 938, 1, 1, 3059.043, 566.6476, 21.51219, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+95, 54542, 938, 1, 1, 3064.288, 592.5538, 26.04465, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+96, 54542, 938, 1, 1, 3046.601, 548.5955, 22.33627, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+97, 54542, 938, 1, 1, 3023.108, 544.2031, 24.3756, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+98, 54691, 938, 1, 1, 3067.365, 577.1158, 21.5849, 0.4593928, 7200, 0, 0), -- 54691 (Area: -1)
(@CGUID+99, 54687, 938, 1, 1, 3067.201, 584.4001, 23.54767, 0.8182708, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+100, 54693, 938, 1, 1, 3064.797, 581.3686, 22.17491, 1.152945, 7200, 0, 0), -- 54693 (Area: -1)
(@CGUID+101, 54690, 938, 1, 1, 3107.149, 566.4019, 21.44468, 4.981111, 7200, 0, 0), -- 54690 (Area: -1)
(@CGUID+102, 54693, 938, 1, 1, 3114.526, 562.2123, 21.37549, 4.981844, 7200, 0, 0), -- 54693 (Area: -1)
(@CGUID+103, 45439, 938, 1, 1, 3080.62, 555.1059, 20.76182, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+104, 54687, 938, 1, 1, 3071.56, 580.3007, 21.7671, 0.8130583, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+105, 54639, 938, 1, 1, 3044.612, 515.7483, 21.54332, 3.228859, 7200, 0, 0), -- 54639 (Area: -1)
(@CGUID+106, 54687, 938, 1, 1, 3112.162, 565.1376, 21.15749, 5.266335, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+107, 54687, 938, 1, 1, 3142.225, 470.1071, 21.34583, 1.634312, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+108, 54687, 938, 1, 1, 3141.844, 476.1006, 20.96464, 1.634271, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+109, 54690, 938, 1, 1, 3136.238, 469.7342, 21.42455, 1.634474, 7200, 0, 0), -- 54690 (Area: -1)
(@CGUID+110, 54693, 938, 1, 1, 3142.606, 464.1148, 22.39238, 1.634312, 7200, 0, 0), -- 54693 (Area: -1)
(@CGUID+111, 54542, 938, 1, 1, 3145.919, 492.4323, 20.66947, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+112, 54542, 938, 1, 1, 3144.001, 461.9826, 23.62674, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+113, 45439, 938, 1, 1, 3121.199, 417.5521, 25.70999, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+114, 54542, 938, 1, 1, 3111.466, 459.4271, 22.01747, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+115, 54542, 938, 1, 1, 3111.466, 459.4271, 22.01747, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+116, 54691, 938, 1, 1, 3147.568, 482.9555, 21.13014, 1.533684, 7200, 0, 0), -- 54691 (Area: -1)
(@CGUID+117, 54687, 938, 1, 1, 3130.816, 552.4254, 21.47324, 3.567407, 7200, 0, 0), -- 54687 (Area: -1)
(@CGUID+118, 54542, 938, 1, 1, 3173.878, 492.3004, 26.97824, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+119, 54542, 938, 1, 1, 3160.658, 524.1441, 21.06857, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+120, 54691, 938, 1, 1, 3125.138, 558.8535, 21.32113, 3.141593, 7200, 0, 0), -- 54691 (Area: -1)
(@CGUID+121, 54542, 938, 1, 1, 3180.344, 517.2014, 27.81145, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+122, 45439, 938, 1, 1, 3189.445, 450.2379, 25.06092, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+123, 54542, 938, 1, 1, 3146.885, 553.6077, 21.04269, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+124, 45439, 938, 1, 1, 3202.955, 489.8611, 24.96133, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+125, 54542, 938, 1, 1, 3180.78, 549.0261, 26.33423, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+126, 45439, 938, 1, 1, 3205.953, 555.5677, 31.21457, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+127, 54542, 938, 1, 1, 3159.581, 577.5799, 27.15213, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+128, 54542, 938, 1, 1, 3101.55, 599.8958, 28.65366, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+129, 54542, 938, 1, 1, 3130.356, 594.2864, 26.82118, 0, 7200, 0, 0), -- 54542 (Area: -1)
(@CGUID+130, 45439, 938, 1, 1, 3130.898, 626.0052, 32.00659, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+131, 45439, 938, 1, 1, 3098.208, 621.882, 27.33134, 0, 7200, 0, 0), -- 45439 (Area: -1)
(@CGUID+132, 54445, 938, 1, 1, 3044.78, 515.7292, 21.54332, 3.124139, 7200, 0, 0), -- 54445 (Area: -1)
(@CGUID+133, 54923, 938, 1, 1, 4124.252, -407.8351, 122.1545, 4.13643, 7200, 0, 0), -- 54923 (Area: -1)
(@CGUID+134, 54923, 938, 1, 1, 4088.726, -414.4115, 120.6957, 5.707227, 7200, 0, 0), -- 54923 (Area: -1)
(@CGUID+135, 54920, 938, 1, 1, 4085.556, -430.5104, 121.0706, 6.178465, 7200, 0, 0), -- 54920 (Area: -1)
(@CGUID+136, 54920, 938, 1, 1, 4104.933, -405.5556, 120.7335, 4.834562, 7200, 0, 0), -- 54920 (Area: -1)
(@CGUID+137, 54751, 938, 1, 1, 4031.427, -362.526, 121.9653, 5.811946, 7200, 0, 0), -- 54751 (Area: -1)
(@CGUID+138, 54923, 938, 1, 1, 4112.26, -367.369, 119.1349, 0.1879136, 7200, 0, 0), -- 54923 (Area: -1)
(@CGUID+139, 54920, 938, 1, 1, 4111.137, -361.4754, 118.7603, 0.1880589, 7200, 0, 0), -- 54920 (Area: -1)
(@CGUID+140, 54923, 938, 1, 1, 4113.38, -373.2641, 118.927, 0.1881316, 7200, 0, 0), -- 54923 (Area: -1)
(@CGUID+141, 54920, 938, 1, 1, 4114.501, -379.1583, 119.2132, 0.1881015, 7200, 0, 0), -- 54920 (Area: -1)
(@CGUID+142, 54945, 938, 1, 1, 4058.536, -390.611, 121.2034, 4.767601, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+143, 54945, 938, 1, 1, 4182.376, -458.3618, 121.6453, 4.666115, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+144, 54945, 938, 1, 1, 4062.652, -439.6867, 121.8756, 4.737252, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+145, 54945, 938, 1, 1, 4184.455, -413.4899, 120.6964, 4.666091, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+146, 54928, 938, 1, 1, 4110.196, -429.158, 136.6789, 2.338741, 7200, 0, 0), -- 54928 (Area: -1)
(@CGUID+147, 54945, 938, 1, 1, 4192.948, -336.349, 120.4357, 4.631589, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+148, 54945, 938, 1, 1, 4054.855, -324.0035, 120.2911, 4.767601, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+149, 54945, 938, 1, 1, 4055.06, -319.5174, 120.2911, 4.764887, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+150, 54945, 938, 1, 1, 4181.126, -336.349, 119.0671, 4.714683, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+151, 54945, 938, 1, 1, 4188.23, -331.8594, 119.0671, 4.666141, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+152, 54945, 938, 1, 1, 4188.026, -336.4219, 119.0671, 4.666091, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+153, 54945, 938, 1, 1, 4059.775, -324.0035, 120.2911, 4.737252, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+154, 54945, 938, 1, 1, 4054.855, -324.0035, 120.2911, 4.767601, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+155, 54945, 938, 1, 1, 4054.855, -324.0035, 120.2911, 4.767601, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+156, 54945, 938, 1, 1, 4188.026, -336.349, 119.0671, 4.666115, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+157, 54945, 938, 1, 1, 4192.948, -336.349, 120.4357, 4.631589, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+158, 54945, 938, 1, 1, 4188.026, -336.4219, 119.0671, 4.666091, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+159, 54945, 938, 1, 1, 4054.855, -324.0035, 120.2911, 4.767601, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+160, 54945, 938, 1, 1, 4054.855, -324.0035, 120.2911, 4.767601, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+161, 54945, 938, 1, 1, 4181.126, -336.349, 119.0671, 4.714683, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+162, 54945, 938, 1, 1, 4059.775, -324.0035, 120.2911, 4.737252, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+163, 54945, 938, 1, 1, 4055.06, -319.5174, 120.2911, 2.443461, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+164, 54945, 938, 1, 1, 4188.23, -331.8594, 119.0671, 1.117013, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+165, 54945, 938, 1, 1, 4054.855, -324.0035, 120.2911, 2.443461, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+166, 54945, 938, 1, 1, 4188.026, -336.349, 119.0671, 1.117013, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+167, 54945, 938, 1, 1, 4188.026, -336.4219, 119.0671, 1.117013, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+168, 54945, 938, 1, 1, 4192.948, -336.349, 120.4357, 1.117013, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+169, 54945, 938, 1, 1, 4054.855, -324.0035, 120.2911, 4.767601, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+170, 54945, 938, 1, 1, 4181.126, -336.349, 119.0671, 4.714683, 7200, 0, 0), -- 54945 (Area: -1)
(@CGUID+171, 54945, 938, 1, 1, 4059.775, -324.0035, 120.2911, 4.737252, 7200, 0, 0); -- 54945 (Area: -1)

DELETE FROM `npc_text` WHERE `Id`=18848;
INSERT INTO `npc_text` VALUES
(18848, 'Select a destination.', 'Select a destination.', 0, 1, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1);

DELETE FROM `gossip_menu` WHERE (`entry`=13321 AND `text_id`=18848);
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES
(13321, 18848); -- 209441

DELETE FROM `gossip_menu_option` WHERE (`menu_id`=13321 AND `id`=4);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `box_coded`, `box_money`, `box_text`) VALUES
(13321, 4, 0, 'Obsidian Sanctum dragons', 0, 0, ''); -- 209441