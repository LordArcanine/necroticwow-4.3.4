-- Warlock pvp sets
UPDATE `item_template` SET `itemset`='910' WHERE `entry` IN (70314,70315,70316,70317,70318,72459,72460,72461,72462,72463,73483,73486,73487,73484,73485,73656,73659,73660,73657,73658);
-- Warlock pve sets
UPDATE `item_template` SET `itemset`='1008' WHERE `entry` IN (71594,71595,71596,71597,71598);
UPDATE `item_template` SET `itemset`='1072' WHERE `entry` IN (78776,78797,78816,78844,78825,78681,78702,78721,78749,78730);

-- Warrior pvp sets
UPDATE `item_template` SET `itemset`='909' WHERE `entry` IN (70477,70478,70479,70480,70481,70254,70255,70256,70257,70258,72464,72465,72466,72467,72468,73482,73481,73480,73479,73478,73655,73654,73653,73652,73651);
-- Warrior pve sets
UPDATE `item_template` SET `itemset`='1018' WHERE `entry` IN (71604,71605,71606,71607,71608);
UPDATE `item_template` SET `itemset`='1017' WHERE `entry` IN (71599,71600,71601,71602,71603);
UPDATE `item_template` SET `itemset`='1074' WHERE `entry` IN (78753,78784,78800,78829,78658,78689,78669,78705,78734);
UPDATE `item_template` SET `itemset`='1073' WHERE `entry` IN (78752,78763,78783,78801,78830,76983,76984,76985,76986,76987);

-- Druid pvp sets
UPDATE `item_template` SET `itemset`='921' WHERE `entry` IN (70428,70429,70447,70448,70449,70289,70290,70291,70292,70293,72353,72354,72355,72356,72357);
UPDATE `item_template` SET `itemset`='922' WHERE `entry` IN (70438,70439,70484,70485,70486,70279,70280,70281,70282,70283,72337,72338,72339,72340,72341);
UPDATE `item_template` SET `itemset`='923' WHERE `entry` IN (70430,70431,70432,70436,70437,70284,70285,70286,70287,70288,72345,72346,72347,72348,72349);
-- Druid pve sets
UPDATE `item_template` SET `itemset`='1003' WHERE `entry` IN (71496,71497,71498,71499,71500);
UPDATE `item_template` SET `itemset`='1004' WHERE `entry` IN (71491,71492,71493,71494,71495);
UPDATE `item_template` SET `itemset`='1002' WHERE `entry` IN (71486,71487,71488,71489,71490);
UPDATE `item_template` SET `itemset`='1058' WHERE `entry` IN (78684,78694,78713,78665,78743);
UPDATE `item_template` SET `itemset`='1059' WHERE `entry` IN (78680,78690,78710,78740,78660);
UPDATE `item_template` SET `itemset`='1060' WHERE `entry` IN (78696,78676,78714,78744,78662);

-- Priest pvp sets
UPDATE `item_template` SET `itemset`='915' WHERE `entry` IN (70470,70471,70472,70473,70474,70309,70310,70311,70312,70313,72405,72406,72407,72408,72409);
UPDATE `item_template` SET `itemset`='916' WHERE `entry` IN (70304,70305,70306,70307,70308,70450,70451,70452,70453,70475,72400,72401,72402,72403,72404);
-- Priest pve sets
UPDATE `item_template` SET `itemset`='935' WHERE `entry` IN (65229,65230,65231,65232,65233);
UPDATE `item_template` SET `itemset`='936' WHERE `entry` IN (65238,65237,65236,65235,65234);
UPDATE `item_template` SET `itemset`='1009' WHERE `entry` IN (71271,71272,71273,71274,71275);
UPDATE `item_template` SET `itemset`='1010' WHERE `entry` IN (71532,71533,71534,71535,71536);
UPDATE `item_template` SET `itemset`='1066' WHERE `entry` IN (78682,78703,78722,78750,78731);
UPDATE `item_template` SET `itemset`='1067' WHERE `entry` IN (78700,78683,78719,78747,78728);

-- Mage pvp sets
UPDATE `item_template` SET `itemset`='919' WHERE `entry` IN (70299,70300,70301,70302,70303,70454,70455,70461,70462,70463,72373,72374,72375,72376,72377);
-- Mage pve sets
UPDATE `item_template` SET `itemset`='931' WHERE `entry` IN (65213,65212,65211,65210,65209);
UPDATE `item_template` SET `itemset`='1007' WHERE `entry` IN (71507,71508,71509,71510,71511);
UPDATE `item_template` SET `itemset`='1062' WHERE `entry` IN (78671,78701,78720,78748,78729);

-- Hunter pvp sets
UPDATE `item_template` SET `itemset`='920' WHERE `entry` IN (70434,70435,70440,70441,70476,70259,70260,70261,70262,70263,72368,72369,72370,72371,72372);
-- Hunter pve sets
UPDATE `item_template` SET `itemset`='930' WHERE `entry` IN (65204,65205,65206,65207,65208);
UPDATE `item_template` SET `itemset`='1005' WHERE `entry` IN (71501,71502,71503,71504,71505);
UPDATE `item_template` SET `itemset`='1061' WHERE `entry` IN (78674,78698,78709,78737,78661);

-- Paladin pvp sets
UPDATE `item_template` SET `itemset`='917' WHERE `entry` IN (70482,70483,70487,70488,70489,70249,70250,70251,70252,70253,72378,72379,72380,72381,72382);
UPDATE `item_template` SET `itemset`='918' WHERE `entry` IN (70415,70416,70417,70418,70419,70353,70354,70355,70356,70357,72389,72390,72391,72392,72393);
-- Paladin pve sets
UPDATE `item_template` SET `itemset`='1012' WHERE `entry` IN (71512,71513,71514,71515,71516);
UPDATE `item_template` SET `itemset`='1011' WHERE `entry` IN (71517,71518,71519,71520,71521);
UPDATE `item_template` SET `itemset`='1013' WHERE `entry` IN (71522,71523,71524,71525,71526);
UPDATE `item_template` SET `itemset`='1063' WHERE `entry` IN (78726,78673,78717,78692,78746);
UPDATE `item_template` SET `itemset`='1065' WHERE `entry` IN (78727,78675,78693,78712,78742);
UPDATE `item_template` SET `itemset`='1064' WHERE `entry` IN (78732,78695,78677,78715,78745);

-- Rogue pvp sets
UPDATE `item_template` SET `itemset`='914' WHERE `entry` IN (70294,70295,70296,70297,70298,70442,70443,70444,70445,70446,72422,72423,72424,72425,72426);
-- Rogue pve sets
UPDATE `item_template` SET `itemset`='937' WHERE `entry` IN (65243,65242,65241,65240,65239);
UPDATE `item_template` SET `itemset`='1006' WHERE `entry` IN (71537,71538,71539,71540,71541);
UPDATE `item_template` SET `itemset`='1068' WHERE `entry` IN (78679,78699,78708,78738,78664);

-- Dk pvp sets
UPDATE `item_template` SET `itemset`='924' WHERE `entry` IN (70490,70491,70492,70493,70494,70244,70245,70246,70247,70248,72332,72333,72334,72335,72336);
-- Dk pve sets
UPDATE `item_template` SET `itemset`='1001' WHERE `entry` IN (71481,71482,71483,71484,71485);
UPDATE `item_template` SET `itemset`='1000' WHERE `entry` IN (71476,71477,71478,71479,71480);
UPDATE `item_template` SET `itemset`='1056' WHERE `entry` IN (78659,78670,78707,78687,78736);
UPDATE `item_template` SET `itemset`='1057' WHERE `entry` IN (78663,78697,78678,78716,78751);

-- Shaman pvp sets
UPDATE `item_template` SET `itemset`='913' WHERE `entry` IN (70264,70265,70266,70267,70268,70433,70464,70465,70466,70467,72432,72433,72434,72435,72436);
UPDATE `item_template` SET `itemset`='911' WHERE `entry` IN (70274,70275,70276,70277,70278,70420,70421,70422,70423,70424,72443,72444,72445,72446,72447);
UPDATE `item_template` SET `itemset`='912' WHERE `entry` IN (70456,70457,70458,70459,70460,70269,70270,70271,70272,70273,72437,72438,72439,72440,72441);
-- Shaman pve sets
UPDATE `item_template` SET `itemset`='1016' WHERE `entry` IN (71552,71553,71554,71555,71556);
UPDATE `item_template` SET `itemset`='1014' WHERE `entry` IN (71300,71299,71298,71297,71296);
UPDATE `item_template` SET `itemset`='1015' WHERE `entry` IN (71547,71548,71549,71550,71551);
UPDATE `item_template` SET `itemset`='1070' WHERE `entry` IN (78691,78672,78718,78739,78725);
UPDATE `item_template` SET `itemset`='1069' WHERE `entry` IN (78666,78723,78685,78711,78741);
UPDATE `item_template` SET `itemset`='1071' WHERE `entry` IN (78724,78667,78686,78704,78733);