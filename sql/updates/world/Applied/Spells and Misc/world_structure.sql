== 16.08 == ALREADY APPLIED.

ALTER TABLE `reputation_spillover_template` CHANGE `faction5` `faction5` smallint(5) unsigned NOT NULL DEFAULT '0';
ALTER TABLE `game_event_npc_vendor` DROP INDEX `PRIMARY`, ADD PRIMARY KEY (`guid`, `item`, `ExtendedCost`, `type`);
ALTER TABLE `npc_vendor` DROP INDEX `PRIMARY`, ADD PRIMARY KEY (`entry`, `item`, `ExtendedCost`, `type`);

ALTER TABLE `quest_template` CHANGE `RewardFactionValueIdOverride1` `RewardFactionValueIdOverride1` int(11) NOT NULL DEFAULT '0';
ALTER TABLE `quest_template` CHANGE `RewardFactionValueIdOverride2` `RewardFactionValueIdOverride2` int(11) NOT NULL DEFAULT '0';
ALTER TABLE `quest_template` CHANGE `RewardFactionValueIdOverride3` `RewardFactionValueIdOverride3` int(11) NOT NULL DEFAULT '0';
ALTER TABLE `quest_template` CHANGE `RewardFactionValueIdOverride4` `RewardFactionValueIdOverride4` int(11) NOT NULL DEFAULT '0';
ALTER TABLE `quest_template` CHANGE `RewardFactionValueIdOverride5` `RewardFactionValueIdOverride5` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `quest_template` CHANGE `MinLevel` `MinLevel` SMALLINT(6) NOT NULL DEFAULT '0';
ALTER TABLE `quest_template` CHANGE `MaxLevel` `MaxLevel` SMALLINT(6) NOT NULL DEFAULT '0';