-- Vampiric Touch
DELETE FROM `spell_script_names` WHERE `spell_id` IN (34914,-34914);

-- Blast wave
DELETE FROM `spell_script_names` WHERE `spell_id` IN (11113);

DELETE FROM `spell_script_names` WHERE `spell_id` IN (88685, 88687);
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES
(88685,'spell_pri_chakra_sanctuary_heal');

-- Refocus
DELETE FROM `spell_script_names` WHERE `spell_id` IN (24531);