UPDATE `creature_template` SET `VehicleId`='292', `InhabitType`='4' WHERE entry IN (29501, 29488);

REPLACE INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`) VALUES ('29488', '54568', '1', '0');

DELETE FROM conditions WHERE SourceGroup = 29488;

UPDATE `npc_spellclick_spells` SET `cast_flags`='1' WHERE (`npc_entry`='29501');

UPDATE `creature_template` SET `npcflag`='16777216' WHERE `entry`=29488;