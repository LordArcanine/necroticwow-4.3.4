ALTER TABLE `game_event` CHANGE `eventEntry` `eventEntry` tinyint(3) unsigned NOT NULL COMMENT 'Entry of the game event';

ALTER TABLE `game_event_creature` ADD COLUMN `eventEntryNew` tinyint(4) NOT NULL DEFAULT '0' FIRST;
UPDATE `game_event_creature` SET eventEntryNew = eventEntry;
ALTER TABLE `game_event_creature` DROP PRIMARY KEY;
ALTER TABLE `game_event_creature` DROP COLUMN `eventEntry`;
ALTER TABLE `game_event_creature` CHANGE `eventEntryNew` `eventEntry` tinyint(4) NOT NULL COMMENT 'Entry of the game event. Put negative entry to remove during event.';
ALTER TABLE `game_event_creature` ADD PRIMARY KEY (`guid`, `eventEntry`);

ALTER TABLE `game_event_gameobject` ADD COLUMN `eventEntryNew` tinyint(4) NOT NULL DEFAULT '0' FIRST;
UPDATE `game_event_gameobject` SET eventEntryNew = eventEntry;
ALTER TABLE `game_event_gameobject` DROP PRIMARY KEY;
ALTER TABLE `game_event_gameobject` DROP COLUMN `eventEntry`;
ALTER TABLE `game_event_gameobject` CHANGE `eventEntryNew` `eventEntry` tinyint(4) NOT NULL COMMENT 'Entry of the game event. Put negative entry to remove during event.';
ALTER TABLE `game_event_gameobject` ADD PRIMARY KEY (`guid`, `eventEntry`);