DELETE FROM `creature` WHERE `id` IN (46180,46181,47581,47582,47584,47586,47587,47588,47589,51099);

DELETE FROM `creature_template` WHERE `entry` IN (54471,54472,54473,54441,54442,54443);
INSERT INTO `creature_template` (`entry`, `name`, `subname`, `IconName`, `type_flags`, `type_flags2`, `type`, `family`, `rank`, `KillCredit1`, `KillCredit2`, `PetSpellDataId`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `Health_mod`, `Mana_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `exp_unk`, `WDBVerified`) VALUES
(54471, 'Thaumaturge Zajir', 'Arcane Reforger', 'Reforge', 0, 0, 7, 0, 0, 0, 0, 0, 38799, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15595), -- Thaumaturge Zajir
(54472, 'Vaultkeeper Jazra', 'Void Storage', 'Voidstorage', 0, 0, 7, 0, 0, 0, 0, 0, 38800, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15595), -- Vaultkeeper Jazra
(54473, 'Warpweaver Dushar', 'Transmogrifier', 'Transmogrify', 0, 0, 7, 0, 0, 0, 0, 0, 38804, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15595), -- Warpweaver Dushar
(54441, 'Thaumaturge Vashreen', 'Arcane Reforger', 'Reforge', 0, 0, 7, 0, 0, 0, 0, 0, 38799, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15595), -- Thaumaturge Vashreen
(54442, 'Warpweaver Hashom', 'Transmogrifier', 'Transmogrify', 0, 0, 7, 0, 0, 0, 0, 0, 38804, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15595), -- Warpweaver Hashom
(54443, 'Vaultkeeper Razhid', 'Void Storage', 'Voidstorage', 0, 0, 7, 0, 0, 0, 0, 0, 38800, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15595); -- Vaultkeeper Razhid

UPDATE `creature_template` SET `npcflag`='268435456' WHERE `entry` IN (54473,54442);
UPDATE `creature_template` SET `npcflag`='536870912' WHERE `entry` IN (54443,54472);
UPDATE `creature_template` SET `npcflag`='134217728' WHERE `entry` IN (54471,54441);

DELETE FROM `creature` WHERE `id` IN (54471,54472,54473,54441,54442,54443);
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`MovementType`) VALUES
(54473,1,1,1,1727.328,-4519.503,32.6442,1.291544,120,0,0),
(54471,1,1,1,1721.135,-4513.193,31.32894,0.3141593,120,0,0),
(54472,1,1,1,1725.561,-4515.259,30.7725,0.6108652,120,0,0),
(54441,0,1,1,-8693.46,843.486,98.8665,2.2668,120,0,0),
(54442,0,1,1,-8699.68,838.473,99.0406,2.26117,120,0,0),
(54443,0,1,1,-8696.57,840.811,98.6857,2.20685,120,0,0);


UPDATE `creature_template` SET `faction_a`='35', `faction_h`='35' WHERE `entry` IN (54471,54472,54473);
UPDATE `creature_template` SET `faction_a`='35', `faction_h`='35' WHERE `entry` IN (54441,54442,54443);
