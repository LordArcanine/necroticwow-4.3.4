UPDATE `spell_script_names` SET `spell_id`=1535 WHERE `spell_id`=-1535;

DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_astral_shift';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_cleansing_totem_pulse';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_mana_spring_totem';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_sentry_totem';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_earthbind_totem';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_earthen_power';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_fire_nova';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_ancestral_awakening_proc';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_healing_stream_totem';
DELETE FROM `spell_script_names` WHERE `scriptname`='spell_sha_lava_lash';

DELETE FROM `spell_script_names` WHERE `spell_id` IN (39610,16191);
