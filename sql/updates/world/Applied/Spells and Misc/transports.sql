-- Add Krazzworks <-> Dragonmaw Port Transports
DELETE FROM `transports` WHERE `entry` IN (206328, 206329);
INSERT INTO `transports` (`entry`, `name`, `period`, `ScriptName`) VALUES
(206328, 'Krazzworks to Dragonmaw Port', 205674, ''),
(206329, 'Dragonmaw Port to Krazzworks', 205674, '');

-- Transport flags
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry` IN (206328, 206329);

-- Add Passengers
DELETE FROM `creature_transport` WHERE `transport_entry` IN (206328, 206329);
INSERT INTO `creature_transport` (`guid`, `transport_entry`, `npc_entry`, `TransOffsetX`, `TransOffsetY`, `TransOffsetZ`, `TransOffsetO`, `emote`) VALUES
(1, 206328, 46763, 4.26206, -11.6361, -17.82397, 3.420845, 0), -- Captain Fizzlesneer
(2, 206328, 48535, -15.9296, -10.8046, -15.32037, 4.258604, 0), -- Krazzworks Sky Marshall
(3, 206328, 48535, 2.31688, -7.91102, -23.63947, 0, 0), -- Krazzworks Sky Marshall
(4, 206328, 48535, -4.130146, -2.779428, -17.79507, 0.01120401, 0), -- Krazzworks Sky Marshall
(1, 206329, 46764, 4.15988, -11.1731, -17.82867, 3.368485, 0), -- Captain Smoldersmitten
(2, 206329, 48535, -11.77357, -10.21906, -16.61713, 1.694214, 0), -- Krazzworks Sky Marshall
(3, 206329, 48535, 6.63632, -3.07199, -17.71427, 1.22173, 0), -- Krazzworks Sky Marshall
(4, 206329, 48535, 7.3889, -7.71648, -23.53657, 3.246312, 0); -- Krazzworks Sky Marshall

-- Add Equip Template
DELETE FROM `creature_equip_template` WHERE `entry` IN (46763, 46764, 48535);
INSERT INTO `creature_equip_template` (`entry`, `itemEntry1`, `itemEntry2`, `itemEntry3`) VALUES
(46763, 3364, 0, 0), -- Captain Fizzlesneer
(46764, 3364, 0, 0), -- Captain Smoldersmitten
(48535, 53583, 0, 45937); -- Krazzworks Sky Marshall

-- Add Template Addon
DELETE FROM `creature_template_addon` WHERE `entry` IN (46763, 46764, 48535);
INSERT INTO `creature_template_addon` (`entry`, `mount`, `bytes1`, `bytes2`, `auras`) VALUES
(46763, 0, 0x0, 0x101, ''), -- Captain Fizzlesneer
(46764, 0, 0x0, 0x101, ''), -- Captain Smoldersmitten
(48535, 0, 0x0, 0x1, ''); -- Krazzworks Sky Marshall

UPDATE `transports` SET `period`=255895 WHERE `entry`=164871;
UPDATE `transports` SET `period`=244531 WHERE `entry`=175080;
UPDATE `transports` SET `period`=599143 WHERE `entry`=190549;
UPDATE `transports` SET `period`=231236 WHERE `entry`=20808;
UPDATE `transports` SET `period`=230016 WHERE `entry`=176231;
UPDATE `transports` SET `period`=235783 WHERE `entry`=176310;
UPDATE `transports` SET `period`=238658 WHERE `entry`=181646;
UPDATE `transports` SET `period`=298829 WHERE `entry`=186238;
UPDATE `transports` SET `period`=319210 WHERE `entry`=176495;
UPDATE `transports` SET `period`=446334 WHERE `entry`=181688;
UPDATE `transports` SET `period`=214579 WHERE `entry`=181689;
UPDATE `transports` SET `period`=484348 WHERE `entry`=186371;
UPDATE `transports` SET `period`=307953 WHERE `entry`=187038;
UPDATE `transports` SET `period`=445220 WHERE `entry`=187568;
UPDATE `transports` SET `period`=502354 WHERE `entry`=188511;
UPDATE `transports` SET `period`=271979 WHERE `entry`=190536;
UPDATE `transports` SET `period`=1431158 WHERE `entry`=192241;
UPDATE `transports` SET `period`=1051388 WHERE `entry`=192242;
UPDATE `transports` SET `period`=599143 WHERE `entry`=190549;



DELETE FROM `transports` WHERE `entry` IN (203466, 203626, 197195, 207227, 204018, 203428);
INSERT INTO `transports` (`entry`, `name`, `period`, `ScriptName`) VALUES
(203466, 'Ship to Vashj''ir - (Horde)', 327895, ''),
(203626, 'The Spear of Durotar', 534650, ''),
(197195, 'Ship to Vashj''ir - (Alliance)', 317922, ''),
(207227, 'Krazzworks Attack Zeppelin', 71606, ''),
(204018, 'Deepholm - Alliance Gunship', 178136, ''),
(203428, 'Worgen area - Orc Gunship', 316236, '');


UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=203428; -- Orc Gunship
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=204018; -- Alliance Gunship
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=164871; -- Zeppelin (The Thundercaller)
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=20808; -- Ship (The Maiden's Fancy)
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=175080; -- Zeppelin (The Iron Eagle)
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=176231; -- Ship (The Lady Mehley)
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=176310; -- Ship (The Bravery)
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=181646; -- Ship, Night Elf (Elune's Blessing)
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=186238; -- Zeppelin, Horde (The Mighty Wind)
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=190549; -- The Zephyr
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=197195; -- Ship to Vashj'ir
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=203466; -- Ship to Vashj'ir
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=203626; -- The Spear of Durotar
UPDATE `gameobject_template` SET `flags`=40 WHERE `entry`=207227; -- Krazzworks Attack Zeppelin

-- Rut'theran Village, Teldrassil and Auberdine, Darkshore ("The Moonspray")
DELETE FROM `transports` WHERE `entry`=176244;

-- Remove creatures of the transport
DELETE FROM `creature_transport` WHERE `transport_entry`=176244;

-- Need default 0
ALTER TABLE creature_transport ALTER COLUMN emote SET DEFAULT 0;

-- Deepholm Ship
DELETE FROM `creature_transport` WHERE `transport_entry`=204018;
INSERT INTO `creature_transport` (`guid`, `transport_entry`, `npc_entry`, `TransOffsetX`, `TransOffsetY`, `TransOffsetZ`, `TransOffsetO`) VALUES
(1, 204018, 42885, -54.99634, -7.373413, -5.197235, 2.366248),
(2, 204018, 42885, -23.40527, -24.4469, -5.236877, 1.179137),
(3, 204018, 42885, 21.1759, -13.75055, 9.600037, 4.709668),
(4, 204018, 42801, -64.83681, 0.033795, 9.903969, 0),
(5, 204018, 42885, 15.50969, -17.88296, -5.163431, 0.6208096),
(6, 204018, 42681, -28.55183, -12.62104, 20.55605, 1.221731),
(7, 204018, 42681, -36.9719, -12.35286, 20.52748, 2.094395),
(8, 204018, 42681, -8.098283, -10.13041, 20.51006, 4.328416),
(9, 204018, 42885, -44.20203, 21.69708, 9.606781, 0.9522773),
(10, 204018, 42885, -1.110718, 21.1604, 9.630798, 0.5497429),
(11, 204018, 42885, 14.22437, 10.63971, 9.554291, 2.169876),
(12, 204018, 42885, -39.12793, 18.85876, -5.200531, 0.1506691),
(13, 204018, 42681, -12.88364, 0.360375, 25.36376, 5.462881),
(14, 204018, 40350, -32.20043, 0.574289, 13.4672, 0.541052),
(15, 204018, 42681, 4.631958, -13.97084, 20.88808, 1.413717),
(16, 204018, 42682, -43.78606, 0.475003, 20.60984, 6.178465),
(17, 204018, 42809, -34.16956, 0.603002, 8.555613, 0),
(18, 204018, 42681, -36.3671, 8.293001, 20.53229, 5.462881),
(19, 204018, 42682, -11.36899, 8.655779, 20.51657, 0.7330384),
(20, 204018, 42885, 25.26886, 0.7057495, -4.921814, 5.96848),
(21, 204018, 42885, 5.521973, 21.10114, -5.195404, 2.243494),
(22, 204018, 42681, -58.60949, -0.095605, 23.56651, 1.32645),
(23, 204018, 42682, 7.348988, 1.012339, 20.63929, 2.199114),
(24, 204018, 42681, -33.68518, 14.85802, 20.70304, 3.368485),
(25, 204018, 42681, -13.73099, 14.58074, 20.47051, 5.742133),
(26, 204018, 42682, -47.46341, 10.64703, 22.57558, 4.991642),
(27, 204018, 42682, 18.27893, -11.43421, 20.46279, 4.328416),
(28, 204018, 42681, 16.12486, -3.936981, 20.51413, 4.852015),
(29, 204018, 42681, -62.30529, 4.799966, 23.54879, 0.4537854),
(30, 204018, 42885, 34.90405, 8.44812, 9.605408, 5.832501),
(31, 204018, 42682, 3.028534, 18.12543, 20.53766, 5.253441),
(32, 204018, 42885, 43.29504, -4.26123, 9.285492, 3.197539),
(33, 204018, 42681, 32.83448, -24.09242, 25.11685, 5.742133),
(34, 204018, 42885, 42.10449, -4.010803, -2.083954, 1.74742),
(35, 204018, 42682, 31.65753, -7.46235, 23.36306, 1.361357),
(36, 204018, 42681, 15.77466, 16.16105, 20.59039, 2.076942),
(37, 204018, 42681, 31.94202, 1.224564, 23.37182, 4.764749),
(38, 204018, 42682, 39.30655, -41.2642, 25.11442, 2.199114),
(39, 204018, 43681, 28.9662, 7.74904, 23.36593, 6.230825),
(40, 204018, 43082, 28.9031, 7.92859, 23.36993, 3.385939),
(41, 204018, 42681, 41.02726, -20.22445, 25.11503, 3.420845),
(42, 204018, 42681, 38.54572, -4.734725, 40.167, 5.742133),
(43, 204018, 42682, 35.79272, 6.766162, 40.16405, 5.253441),
(44, 204018, 42885, 53.87292, 7.720337, -2.054565, 1.592655),
(45, 204018, 42681, 35.64383, 22.14937, 25.1158, 5.742133),
(46, 204018, 40350, 50.3677, 1.59352, 42.24823, 3.944444),
(47, 204018, 40350, -22.0063, 53.3257, 4.203373, 4.956735),
(48, 204018, 42684, 48.71556, 11.32662, 40.50675, 3.787364),
(49, 204018, 40350, -21.9116, 57.4225, 25.87903, 3.944444),
(50, 204018, 42682, 35.15972, 33.27727, 25.11475, 5.550147),
(51, 204018, 40350, -24.6196, 55.3271, 5.002563, 4.782202),
(52, 204018, 40350, -17.385, 55.5016, 4.217743, 4.956735),
(53, 204018, 40350, -21.9806, 57.688, 9.419393, 3.944444),
(54, 204018, 42716, 49.72736, 18.22787, 42.58023, 3.839724),
(55, 204018, 40350, 49.58121, 18.193, 46.16101, 3.944444),
(56, 204018, 40350, -24.3035, 61.4472, 3.728523, 1.256637),
(57, 204018, 40350, -18.6721, 62.316, 4.217683, 1.256637),
(58, 204018, 40350, 37.0378, 42.39633, 41.92483, 3.944444),
(59, 204018, 42681, 40.88038, 43.12596, 25.11707, 5.044002),
(60, 204018, 40350, 3.007076, 0.687804, -35.96375, 0.541052),
(61, 204018, 40350, 6.583054, -26.1589, -42.17334, 0.541052);