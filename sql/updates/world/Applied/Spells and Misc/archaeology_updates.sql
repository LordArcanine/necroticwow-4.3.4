RENAME TABLE `dig_sites` TO `archaeology_digsites`;
RENAME TABLE `digsite_positions` TO `archaeology_digsites_positions`;
DROP TABLE `completed_digsites`;
ALTER TABLE `archaeology_digsites`
	CHANGE COLUMN `Entry` `Entry` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0 FIRST;