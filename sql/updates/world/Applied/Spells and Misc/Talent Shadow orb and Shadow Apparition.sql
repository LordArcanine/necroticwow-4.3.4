UPDATE `creature_template` SET `modelid1`=11686, `ScriptName`='npc_shadowy_apparition' WHERE `entry`=46954;

-- (78202) Shadowy Apparition (Rank 1)
DELETE FROM `spell_proc_event` WHERE `entry` IN (78202);
INSERT INTO `spell_proc_event` VALUES (78202, 0x01, 0x06, 0x00000000, 0x00000000, 0x00000000, 0x00040000, 0x00000000, 0, 100, 0);

-- (78203) Shadowy Apparition (Rank 2)
DELETE FROM `spell_proc_event` WHERE `entry` IN (78203);
INSERT INTO `spell_proc_event` VALUES (78203, 0x01, 0x06, 0x00000000, 0x00000000, 0x00000000, 0x00040000, 0x00000000, 0, 100, 0);

-- (33191) Harnessed Shadows (Rank 1)
DELETE FROM `spell_proc_event` WHERE `entry` IN (33191);
INSERT INTO `spell_proc_event` VALUES (33191, 0x01, 0x06, 0x00808000, 0x00000400, 0x00000000, 0x000222A8, 0x00000002, 0, 50, 0);

-- (78228) Harnessed Shadows (Rank 2)
DELETE FROM `spell_proc_event` WHERE `entry` IN (78228);
INSERT INTO `spell_proc_event` VALUES (78228, 0x01, 0x06, 0x00808000, 0x00000400, 0x00000000, 0x000222A8, 0x00000002, 0, 100, 0);