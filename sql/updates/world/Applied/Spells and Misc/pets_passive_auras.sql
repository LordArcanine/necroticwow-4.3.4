DELETE FROM `spell_script_names` WHERE `spell_id` IN (34947,34956,34957,34958,61013,35695,34902,34903,34904,61017,19591,35697,54566,51996,61697,110474);
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES 
('34947','spell_warl_pet_scaling_01'),
('34956','spell_warl_pet_scaling_02'),
('34957','spell_warl_pet_scaling_03'),
('34958','spell_warl_pet_scaling_04'),
('61013','spell_warl_pet_scaling_05'),
('35695','spell_warl_pet_passive'),
('34902','spell_hun_pet_scaling_01'),
('34903','spell_hun_pet_scaling_02'),
('34904','spell_hun_pet_scaling_03'),
('61017','spell_hun_pet_scaling_04'),
('19591','spell_hun_pet_passive_crit'),
('54566','spell_dk_pet_scaling_01'),
('51996','spell_dk_pet_scaling_02'),
('61697','spell_dk_pet_scaling_03'),
('110474','spell_dk_crit');

DELETE FROM `spell_bonus_data` WHERE `entry` IN (3110,54049);