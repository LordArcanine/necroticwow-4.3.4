DELETE FROM `creature` WHERE `id`=54476;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54476, 938, 3, 1, 0, 0, 3702.83, -359.562, 113.859, 5.80161, 300, 0, 0, 42946000, 0, 0, 0, 0, 0);

DELETE FROM `creature` WHERE `id`=57864;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(57864, 939, 2, 1, 0, 0, 3238.68, -4978.79, 194.094, 3.91155, 300, 0, 0, 4646, 58681, 0, 0, 0, 0),
(57864, 938, 3, 1, 0, 0, 3700.26, -362.512, 113.806, 5.65811, 300, 0, 0, 77490, 0, 0, 0, 0, 0);

UPDATE `gameobject` SET `spawnmask`='3' WHERE `id`=209438;

SET @ENTRY := 57864;
SET @SPELL := 109297;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,1,6000,6000,6000,6000,11,@SPELL,2,0,0,0,0,1,0,0,0,0,0,0,0, 'Transform: Alurmi');

DELETE FROM `creature_questrelation` WHERE `id` = 54476;
INSERT INTO `creature_questrelation` (`id`,`quest`) VALUES (54476,30096);
INSERT INTO `creature_questrelation` (`id`,`quest`) VALUES (54476,30098);
DELETE FROM `creature_involvedrelation` WHERE `id` = 54476;
INSERT INTO `creature_involvedrelation` (`id`,`quest`) VALUES (54476,30096);
INSERT INTO `creature_involvedrelation` (`id`,`quest`) VALUES (54476,30094);
INSERT INTO `creature_involvedrelation` (`id`,`quest`) VALUES (54476,30095);

DELETE FROM `creature_questrelation` WHERE `quest` = 30096;
DELETE FROM `gameobject_questrelation` WHERE `quest` = 30096;
UPDATE `item_template` SET `StartQuest`=0 WHERE `StartQuest` = 30096;
INSERT INTO `creature_questrelation` (`id`, `quest`) VALUES (54476, 30096);
UPDATE `creature_template` SET `npcflag`=`npcflag`|2 WHERE `entry` = 54476;
DELETE FROM `creature_involvedrelation` WHERE `quest` = 30096;
DELETE FROM `gameobject_involvedrelation` WHERE `quest` = 30096;
INSERT INTO `creature_involvedrelation` (`id`, `quest`) VALUES (54476, 30096);
UPDATE `creature_template` SET `npcflag`=`npcflag`|2 WHERE `entry`=54476;
REPLACE INTO `quest_template` (`Id`, `Method`, `Level`, `MinLevel`, `MaxLevel`, `ZoneOrSort`, `Type`, `RequiredClasses`, `SuggestedPlayers`, `LimitTime`, `RequiredRaces`, `RequiredSkillId`, `RequiredSkillPoints`, `RequiredFactionId1`, `RequiredFactionId2`, `RequiredFactionValue1`, `RequiredFactionValue2`, `RequiredMinRepFaction`, `RequiredMaxRepFaction`, `RequiredMinRepValue`, `RequiredMaxRepValue`, `PrevQuestId`, `NextQuestId`, `ExclusiveGroup`, `NextQuestIdChain`, `RewardXPId`, `RewardOrRequiredMoney`, `RewardMoneyMaxLevel`, `RewardSpell`, `RewardSpellCast`, `RewardHonor`, `RewardHonorMultiplier`, `RewardMailTemplateId`, `RewardMailDelay`, `SourceItemId`, `SourceItemCount`, `SourceSpellId`, `Flags`, `SpecialFlags`, `RewardTitleId`, `RequiredPlayerKills`, `RewardTalents`, `RewardArenaPoints`, `RewardItemId1`, `RewardItemId2`, `RewardItemId3`, `RewardItemId4`, `RewardItemCount1`, `RewardItemCount2`, `RewardItemCount3`, `RewardItemCount4`, `RewardChoiceItemId1`, `RewardChoiceItemId2`, `RewardChoiceItemId3`, `RewardChoiceItemId4`, `RewardChoiceItemId5`, `RewardChoiceItemId6`, `RewardChoiceItemCount1`, `RewardChoiceItemCount2`, `RewardChoiceItemCount3`, `RewardChoiceItemCount4`, `RewardChoiceItemCount5`, `RewardChoiceItemCount6`, `RewardFactionId1`, `RewardFactionId2`, `RewardFactionId3`, `RewardFactionId4`, `RewardFactionId5`, `RewardFactionValueId1`, `RewardFactionValueId2`, `RewardFactionValueId3`, `RewardFactionValueId4`, `RewardFactionValueId5`, `RewardFactionValueIdOverride1`, `RewardFactionValueIdOverride2`, `RewardFactionValueIdOverride3`, `RewardFactionValueIdOverride4`, `RewardFactionValueIdOverride5`, `PointMapId`, `PointX`, `PointY`, `PointOption`, `Title`, `Objectives`, `Details`, `EndText`, `OfferRewardText`, `RequestItemsText`, `CompletedText`, `RequiredNpcOrGo1`, `RequiredNpcOrGo2`, `RequiredNpcOrGo3`, `RequiredNpcOrGo4`, `RequiredNpcOrGoCount1`, `RequiredNpcOrGoCount2`, `RequiredNpcOrGoCount3`, `RequiredNpcOrGoCount4`, `RequiredSourceItemId1`, `RequiredSourceItemId2`, `RequiredSourceItemId3`, `RequiredSourceItemId4`, `RequiredSourceItemCount1`, `RequiredSourceItemCount2`, `RequiredSourceItemCount3`, `RequiredSourceItemCount4`, `RequiredItemId1`, `RequiredItemId2`, `RequiredItemId3`, `RequiredItemId4`, `RequiredItemId5`, `RequiredItemId6`, `RequiredItemCount1`, `RequiredItemCount2`, `RequiredItemCount3`, `RequiredItemCount4`, `RequiredItemCount5`, `RequiredItemCount6`, `RequiredSpellCast1`, `RequiredSpellCast2`, `RequiredSpellCast3`, `RequiredSpellCast4`, `Unknown0`, `ObjectiveText1`, `ObjectiveText2`, `ObjectiveText3`, `ObjectiveText4`, `DetailsEmote1`, `DetailsEmote2`, `DetailsEmote3`, `DetailsEmote4`, `DetailsEmoteDelay1`, `DetailsEmoteDelay2`, `DetailsEmoteDelay3`, `DetailsEmoteDelay4`, `EmoteOnIncomplete`, `EmoteOnComplete`, `OfferRewardEmote1`, `OfferRewardEmote2`, `OfferRewardEmote3`, `OfferRewardEmote4`, `OfferRewardEmoteDelay1`, `OfferRewardEmoteDelay2`, `OfferRewardEmoteDelay3`, `OfferRewardEmoteDelay4`, `StartScript`, `CompleteScript`, `WDBVerified`) VALUES (30096, 2, 85, 85, 0, 5789, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 30098, 8, 376000, 330750, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 72873, 72874, 72875, 76152, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Murozond', 'Slay Murozond.', 'If we are to destroy Deathwing, we will need the power of the Dragon Soul. To obtain the Dragon Soul, I would have to take you back to the Sundering, ten thousand years in the past... but that time passage is blocked to me.$b$bThe creature that blocks us from accessing this time passage is named Murozond. He must be slain, here, in this twisted future, before we can continue our plot to unmake Deathwing.$b$bThis is why you were brought here, $N.', '', '', '', 'Speak with Nozdormu at the Bronze Dragonshrine in the End Time.', 54432, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15595);

DELETE FROM `creature_questrelation` WHERE `id` = 57864;
INSERT INTO `creature_questrelation` (`id`,`quest`) VALUES (57864,30097);
INSERT INTO `creature_questrelation` (`id`,`quest`) VALUES (57864,30104);
DELETE FROM `creature_involvedrelation` WHERE `id` = 54476;
INSERT INTO `creature_involvedrelation` (`id`,`quest`) VALUES (57864,30097);
INSERT INTO `creature_involvedrelation` (`id`,`quest`) VALUES (57864,30104);

DELETE FROM `creature_questrelation` WHERE `quest` = 30097;
DELETE FROM `gameobject_questrelation` WHERE `quest` = 30097;
UPDATE `item_template` SET `StartQuest`=0 WHERE `StartQuest` = 30097;
INSERT INTO `creature_questrelation` (`id`, `quest`) VALUES (57864, 30097);
UPDATE `creature_template` SET `npcflag`=`npcflag`|2 WHERE `entry` = 57864;
DELETE FROM `creature_involvedrelation` WHERE `quest` = 30097;
DELETE FROM `gameobject_involvedrelation` WHERE `quest` = 30097;
INSERT INTO `creature_involvedrelation` (`id`, `quest`) VALUES (57864, 30097);
UPDATE `creature_template` SET `npcflag`=`npcflag`|2 WHERE `entry`=57864;
REPLACE INTO `quest_template` (`Id`, `Method`, `Level`, `MinLevel`, `MaxLevel`, `ZoneOrSort`, `Type`, `RequiredClasses`, `SuggestedPlayers`, `LimitTime`, `RequiredRaces`, `RequiredSkillId`, `RequiredSkillPoints`, `RequiredFactionId1`, `RequiredFactionId2`, `RequiredFactionValue1`, `RequiredFactionValue2`, `RequiredMinRepFaction`, `RequiredMaxRepFaction`, `RequiredMinRepValue`, `RequiredMaxRepValue`, `PrevQuestId`, `NextQuestId`, `ExclusiveGroup`, `NextQuestIdChain`, `RewardXPId`, `RewardOrRequiredMoney`, `RewardMoneyMaxLevel`, `RewardSpell`, `RewardSpellCast`, `RewardHonor`, `RewardHonorMultiplier`, `RewardMailTemplateId`, `RewardMailDelay`, `SourceItemId`, `SourceItemCount`, `SourceSpellId`, `Flags`, `SpecialFlags`, `RewardTitleId`, `RequiredPlayerKills`, `RewardTalents`, `RewardArenaPoints`, `RewardItemId1`, `RewardItemId2`, `RewardItemId3`, `RewardItemId4`, `RewardItemCount1`, `RewardItemCount2`, `RewardItemCount3`, `RewardItemCount4`, `RewardChoiceItemId1`, `RewardChoiceItemId2`, `RewardChoiceItemId3`, `RewardChoiceItemId4`, `RewardChoiceItemId5`, `RewardChoiceItemId6`, `RewardChoiceItemCount1`, `RewardChoiceItemCount2`, `RewardChoiceItemCount3`, `RewardChoiceItemCount4`, `RewardChoiceItemCount5`, `RewardChoiceItemCount6`, `RewardFactionId1`, `RewardFactionId2`, `RewardFactionId3`, `RewardFactionId4`, `RewardFactionId5`, `RewardFactionValueId1`, `RewardFactionValueId2`, `RewardFactionValueId3`, `RewardFactionValueId4`, `RewardFactionValueId5`, `RewardFactionValueIdOverride1`, `RewardFactionValueIdOverride2`, `RewardFactionValueIdOverride3`, `RewardFactionValueIdOverride4`, `RewardFactionValueIdOverride5`, `PointMapId`, `PointX`, `PointY`, `PointOption`, `Title`, `Objectives`, `Details`, `EndText`, `OfferRewardText`, `RequestItemsText`, `CompletedText`, `RequiredNpcOrGo1`, `RequiredNpcOrGo2`, `RequiredNpcOrGo3`, `RequiredNpcOrGo4`, `RequiredNpcOrGoCount1`, `RequiredNpcOrGoCount2`, `RequiredNpcOrGoCount3`, `RequiredNpcOrGoCount4`, `RequiredSourceItemId1`, `RequiredSourceItemId2`, `RequiredSourceItemId3`, `RequiredSourceItemId4`, `RequiredSourceItemCount1`, `RequiredSourceItemCount2`, `RequiredSourceItemCount3`, `RequiredSourceItemCount4`, `RequiredItemId1`, `RequiredItemId2`, `RequiredItemId3`, `RequiredItemId4`, `RequiredItemId5`, `RequiredItemId6`, `RequiredItemCount1`, `RequiredItemCount2`, `RequiredItemCount3`, `RequiredItemCount4`, `RequiredItemCount5`, `RequiredItemCount6`, `RequiredSpellCast1`, `RequiredSpellCast2`, `RequiredSpellCast3`, `RequiredSpellCast4`, `Unknown0`, `ObjectiveText1`, `ObjectiveText2`, `ObjectiveText3`, `ObjectiveText4`, `DetailsEmote1`, `DetailsEmote2`, `DetailsEmote3`, `DetailsEmote4`, `DetailsEmoteDelay1`, `DetailsEmoteDelay2`, `DetailsEmoteDelay3`, `DetailsEmoteDelay4`, `EmoteOnIncomplete`, `EmoteOnComplete`, `OfferRewardEmote1`, `OfferRewardEmote2`, `OfferRewardEmote3`, `OfferRewardEmote4`, `OfferRewardEmoteDelay1`, `OfferRewardEmoteDelay2`, `OfferRewardEmoteDelay3`, `OfferRewardEmoteDelay4`, `StartScript`, `CompleteScript`, `WDBVerified`) VALUES (30097, 2, 85, 85, 0, 5789, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 376000, 330750, 0, 0, 0, 0, 0, 0, 77955, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 72852, 72871, 72872, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Archival Purposes', 'Slay two of the Echoes within the End Time, then allow Alurmi\'s Vessel to archive their information.', 'From what I understand, you intend to avoid this future. If this is true, then we must archive data from this timestrand before it is lost to us.$b$bWe believe that the most interesting and valuable elements of this future are the powerful "echoes" that roam the dragonshrines.$b$bTake this vessel with you as you silence these echoes. A portion of their energies will be archived within the Bronze Dragonflight\'s depository.', '', '', '', 'Return to Alurmi within the End Time.', 57855, 0, 0, 0, 2, 0, 0, 0, 77939, 77940, 77941, 77942, 1, 1, 1, 1, 0, 77955, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Echo archived', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15595);

DELETE FROM `gameobject_loot_template` WHERE (`entry`=0);

DELETE FROM `creature` WHERE `id`=54751;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54751, 938, 3, 1, 0, 0, 3822.24, 1085.18, 80.3603, 1.63192, 300, 0, 0, 42946000, 1, 0, 0, 0, 0),
(54751, 938, 3, 1, 0, 0, 4347.15, 1311.94, 149.039, 4.97845, 300, 0, 0, 42946000, 1, 0, 0, 0, 0),
(54751, 938, 3, 1, 0, 0, 4031.43, -362.526, 121.965, 5.81195, 300, 0, 0, 42946000, 1, 0, 0, 0, 0),
(54751, 938, 3, 1, 0, 0, 2933.73, 79.6484, 6.8148, 5.53865, 300, 0, 0, 42946000, 1, 0, 0, 0, 0),
(54751, 938, 3, 1, 0, 0, 2995.93, 550.841, 25.7641, 1.07909, 300, 0, 0, 42946000, 1, 0, 0, 0, 0);

DELETE FROM `creature` WHERE `id`=54543;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54543, 938, 3, 1, 0, 0, 4352.51, 1354.8, 146.694, 6.14356, 300, 0, 0, 774900, 0, 0, 0, 0, 0),
(54543, 938, 3, 1, 0, 0, 4383.16, 1305.92, 153.889, 3.89493, 300, 0, 0, 774900, 0, 0, 0, 0, 0),
(54543, 938, 3, 1, 0, 0, 4387.61, 1332.38, 149.494, 2.89553, 300, 0, 0, 774900, 0, 0, 0, 0, 0);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (54543, 0, 0, 0, 0, 0, 39524, 0, 0, 0, 'Time-Twisted Drake', '', '', 0, 85, 85, 0, 103, 14, 0, 1.2, 1.71429, 1, 1, 0, 0, 0, 0, 1, 2000, 0, 1, 32768, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 54543, 0, 54543, 0, 0, 0, 1, 0, 0, 102134, 102135, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 4, 774900, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '', 1);

DELETE FROM `skinning_loot_template` WHERE (`entry`=54543);
INSERT INTO `skinning_loot_template` VALUES
(54543, 52979, 54, 1, 0, 1, 5),
(54543, 52976, 98, 1, 0, 1, 6);

DELETE FROM `creature_loot_template` WHERE (`entry`=54543);
INSERT INTO `creature_loot_template` VALUES
(54543, 60388, 116, 1, 0, 2, 4),
(54543, 62782, 73, 1, 0, 1, 1),
(54543, 60389, 27, 1, 0, 2, 4);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (54552, 0, 0, 0, 0, 0, 8311, 0, 0, 0, 'Time-Twisted Breaker', '', '', 0, 85, 85, 0, 103, 14, 0, 1.2, 1.71429, 1, 1, 0, 0, 0, 0, 1, 2000, 0, 1, 32768, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 102132, 102124, 0, 0, 0, 0, 0, 0, 0, 0, 8321, 19321, '', 0, 1, 542430, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '', 1);

DELETE FROM `creature` WHERE `id`=54552;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54552, 938, 3, 1, 0, 0, 4389.75, 1302.39, 147.936, 2.63545, 7200, 0, 0, 542430, 1, 0, 0, 0, 0),
(54552, 938, 3, 1, 0, 0, 4387.02, 1311.38, 147.889, 3.86236, 7200, 0, 0, 542430, 1, 0, 0, 0, 0),
(54552, 938, 3, 1, 0, 0, 4354.75, 1357.57, 139.317, 5.63741, 7200, 0, 0, 542430, 1, 0, 0, 0, 0),
(54552, 938, 3, 1, 0, 0, 4368.93, 1353.59, 140.098, 6.12574, 7200, 0, 0, 542430, 1, 0, 0, 0, 0);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (54517, 0, 0, 0, 0, 0, 25630, 0, 0, 0, 'Time-Twisted Shadowtalon', '', '', 0, 85, 85, 0, 14, 14, 0, 1, 1.14286, 1, 1, 0, 0, 0, 0, 1, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 4, 77490, 62356, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '', 1);

DELETE FROM `creature` WHERE `id`=54517;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54517, 938, 3, 1, 0, 0, 3803.82, 997.399, 59.1015, 5.94712, 300, 0, 0, 77490, 0, 0, 0, 0, 0),
(54517, 938, 3, 1, 0, 0, 3753.27, 988.611, 123.543, 0.979482, 300, 0, 0, 77490, 0, 0, 0, 0, 0),
(54517, 938, 3, 1, 0, 0, 3764.02, 977.289, 122.672, 0.751714, 300, 0, 0, 77490, 0, 0, 0, 0, 0),
(54517, 938, 3, 1, 0, 0, 3744.54, 1004.42, 116.05, 6.07641, 300, 0, 0, 77490, 0, 0, 0, 0, 0),
(54517, 938, 3, 1, 0, 0, 3740.86, 986.639, 115.954, 0.908502, 300, 0, 0, 77490, 0, 0, 0, 0, 0),
(54517, 938, 3, 1, 0, 0, 3761.16, 995.713, 72.8031, 0.724378, 300, 0, 0, 77490, 0, 0, 0, 0, 0),
(54517, 938, 3, 1, 0, 0, 3774.23, 1013.42, 60.0366, 0.516247, 300, 0, 0, 77490, 0, 0, 0, 0, 0),
(54517, 938, 3, 1, 0, 0, 3782.09, 991.72, 64.1618, 1.05031, 300, 0, 0, 77490, 0, 0, 0, 0, 0);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (54511, 0, 0, 0, 0, 0, 25592, 0, 0, 0, 'Time-Twisted Geist', '', '', 0, 85, 85, 0, 14, 14, 0, 1, 1.14286, 1, 1, 0, 0, 0, 0, 1, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 109952, 109944, 109945, 102066, 109943, 0, 0, 0, 0, 0, 8332, 15232, '', 0, 1, 309960, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '', 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (54507, 0, 0, 0, 0, 0, 23138, 0, 0, 0, 'Time-Twisted Scourge Beast', '', '', 0, 85, 85, 0, 14, 14, 0, 1, 1.14286, 1, 1, 0, 0, 0, 0, 1, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 101888, 102063, 101891, 0, 0, 0, 0, 0, 0, 0, 8321, 19123, '', 0, 1, 697410, 62356, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '', 1);

DELETE FROM `creature` WHERE `id`=54507;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54507, 938, 3, 1, 0, 0, 3813.8, 1028.15, 58.2516, 2.09903, 300, 0, 0, 697410, 0, 0, 0, 0, 0),
(54507, 938, 3, 1, 0, 0, 3856.07, 980.739, 58.1833, 2.20102, 300, 0, 0, 697410, 0, 0, 0, 0, 0);

DELETE FROM `creature` WHERE `id`=54511;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54511, 938, 3, 1, 0, 0, 3819.04, 1023.13, 58.0591, 2.41495, 300, 0, 0, 309960, 0, 0, 0, 0, 0),
(54511, 938, 3, 1, 0, 0, 3810.78, 1022.48, 57.7093, 2.55326, 300, 0, 0, 309960, 0, 0, 0, 0, 0),
(54511, 938, 3, 1, 0, 0, 3820.09, 1032.93, 58.9502, 2.4172, 300, 0, 0, 309960, 0, 0, 0, 0, 0),
(54511, 938, 3, 1, 0, 0, 3807.35, 1033.42, 57.851, 2.3407, 300, 0, 0, 309960, 0, 0, 0, 0, 0),
(54511, 938, 3, 1, 0, 0, 3852.08, 986.202, 57.637, 2.20102, 300, 0, 0, 309960, 0, 0, 0, 0, 0),
(54511, 938, 3, 1, 0, 0, 3849.21, 977.225, 56.9609, 2.18763, 300, 0, 0, 309960, 0, 0, 0, 0, 0),
(54511, 938, 3, 1, 0, 0, 3861.22, 974.639, 58.6562, 2.27844, 300, 0, 0, 309960, 0, 0, 0, 0, 0),
(54511, 938, 3, 1, 0, 0, 3861.01, 984.866, 58.5309, 2.43641, 300, 0, 0, 309960, 0, 0, 0, 0, 0);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (54123, 0, 0, 0, 0, 0, 39559, 0, 0, 0, 'Echo of Sylvanas', '', '', 0, 87, 87, 0, 14, 14, 0, 1, 1.14286, 1, 1, 0, 0, 0, 0, 1, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 54123, 0, 0, 0, 0, 0, 0, 0, 0, 101404, 101401, 100686, 101348, 101412, 101411, 0, 0, 0, 0, 23212, 42312, '', 0, 4, 6639520, 186200, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 54123, 0, 0, '', 1);

DELETE FROM `creature_loot_template` WHERE (`entry`=54123);
INSERT INTO `creature_loot_template` VALUES
(54123, 44731, 5, 1, 0, 1, 1),
(54123, 72798, 7, 1, 0, 1, 1),
(54123, 72799, 10, 1, 0, 1, 1),
(54123, 72800, 7, 1, 0, 1, 1),
(54123, 72801, 11, 1, 0, 1, 1),
(54123, 72802, 10, 1, 0, 1, 1),
(54123, 72803, 8, 1, 0, 1, 1),
(54123, 72804, 10, 1, 0, 1, 1),
(54123, 72805, 11, 1, 0, 1, 1),
(54123, 72806, 7, 1, 0, 1, 1),
(54123, 72807, 7, 1, 0, 1, 1),
(54123, 72810, 35, 1, 0, 1, 1),
(54123, 72811, 58, 1, 0, 1, 1);

DELETE FROM `creature` WHERE `id`=54123;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54123, 938, 3, 1, 0, 54123, 3847.89, 906.194, 56.0853, 1.83336, 300, 0, 0, 6639520, 0, 0, 0, 0, 0);

SET @ENTRY := 54123;
SET @SPELL := 100686;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,1,1000,1000,1000,1000,11,@SPELL,2,0,0,0,0,1,0,0,0,0,0,0,0, 'Calling of the Highborne');

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (54432, 0, 0, 0, 0, 0, 38931, 0, 0, 0, 'Murozond', 'The Lord of the Infinite', '', 0, 87, 87, 0, 14, 14, 0, 1, 1.14286, 1, 1, 0, 0, 0, 0, 1, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 101984, 107550, 102569, 108589, 102381, 101592, 0, 0, 0, 0, 0, 0, '', 0, 1, 18258680, 931000, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '', 1);

DELETE FROM `creature` WHERE `id`=54432;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54432, 938, 3, 1, 0, 0, 4174.47, -419.986, 119.466, 3.02287, 300, 0, 0, 18258680, 931000, 0, 0, 0, 0);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (54923, 0, 0, 0, 0, 0, 19059, 0, 0, 0, 'Infinite Warden', '', '', 0, 85, 85, 0, 2075, 14, 0, 1, 1.14286, 1, 1, 0, 0, 0, 0, 1, 2000, 0, 1, 32832, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 102599, 102598, 0, 0, 0, 0, 0, 0, 0, 0, 8232, 1932, '', 0, 1, 619920, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 54923, 0, 0, '', 1);

DELETE FROM `creature` WHERE `id`=54923;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(54923, 938, 3, 1, 0, 54923, 4124.5, -404.485, 121.914, 1.82874, 300, 0, 0, 619920, 0, 0, 0, 0, 0),
(54923, 938, 3, 1, 0, 0, 4124.25, -407.835, 122.155, 4.13643, 7200, 0, 0, 619920, 1, 0, 0, 0, 0),
(54923, 938, 3, 1, 0, 0, 4088.73, -414.411, 120.696, 5.70723, 7200, 0, 0, 619920, 1, 0, 0, 0, 0),
(54923, 938, 3, 1, 0, 0, 4112.26, -367.369, 119.135, 0.187914, 7200, 0, 0, 619920, 1, 0, 0, 0, 0),
(54923, 938, 3, 1, 0, 0, 4113.38, -373.264, 118.927, 0.188132, 7200, 0, 0, 619920, 1, 0, 0, 0, 0);

-- Thanx Burnham
-- Instance End Time (mapId 938)
-- ScriptNames
-- Echo of Baine http://www.wowhead.com/npc=54431
UPDATE `creature_template` SET `AIName`='', `ScriptName` = 'boss_echo_of_baine' WHERE `entry`=54431;
-- Echo of Jaina http://www.wowhead.com/npc=54445
UPDATE `creature_template` SET `AIName`='', `ScriptName` = 'boss_echo_of_jaina' WHERE `entry`=54445;
-- Echo of Sylvanas http://www.wowhead.com/npc=54123
UPDATE `creature_template` SET `AIName`='', `ScriptName` = 'boss_echo_of_sylvanas' WHERE `entry`=54123;
-- Echo of Tyrande http://www.wowhead.com/npc=54544
UPDATE `creature_template` SET `AIName`='', `ScriptName` = 'boss_echo_of_tyrande' WHERE `entry`=54544;
-- Murozond <The Lord of the Infinite>Murozond http://www.wowhead.com/npc=54432
UPDATE `creature_template` SET `AIName`='', `ScriptName` = 'boss_murozond' WHERE `entry`=54432;

-- Teleport Time Transit Device - 209438
UPDATE `gameobject_template` SET `ScriptName` = 'end_time_teleport' WHERE `entry`=209438;

-- Instance
UPDATE `instance_template` SET `script` = 'instance_end_time' WHERE `map` = 938;

DELETE FROM `gameobject` WHERE `id`In (209438,209442);
INSERT INTO `gameobject` (`id`,`map`,`spawnMask`,`phaseMask`,`position_x`,`position_y`,`position_z`,`orientation`,`rotation0`,`rotation1`,`rotation2`,`rotation3`,`spawntimesecs`,`animprogress`,`state`) VALUES
(209438, 938, 3, 1, 2991.55, 558.515, 24.8347, 6.22929, 0, 0, 0.0269458, -0.999637, 1, 0, 1),
(209438, 938, 3, 1, 4346.73, 1286.68, 147.503, 0.861131, 0, 0, 0.417385, 0.90873, 300, 0, 1),
(209438, 938, 3, 1, 3828.87, 1109.74, 84.147, 4.75332, 0, 0, 0.692487, -0.72143, 300, 0, 1),
(209438, 938, 3, 1, 4031.92, -353.127, 122.225, 5.59575, 0, 0, 0.336989, -0.941508, 300, 0, 1),
(209438, 938, 3, 1, 3696.27, -367.944, 113.913, 6.14, 0, 0, 0.0715323, -0.997438, 300, 0, 1),
(209438, 938, 3, 1, 2937.26, 85.1825, 6.1408, 5.1157, 0, 0, 0.551152, -0.834405, 300, 0, 1);
