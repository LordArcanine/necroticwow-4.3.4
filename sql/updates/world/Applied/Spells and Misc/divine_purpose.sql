INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES 
('20271','spell_pal_divine_purpose'),
('879','spell_pal_divine_purpose'),
('85256','spell_pal_divine_purpose'),
('53385','spell_pal_divine_purpose'),
('84963','spell_pal_divine_purpose'),
('2812','spell_pal_divine_purpose'),
('24275','spell_pal_divine_purpose');
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES
(85256, '-90174','0','Divine purpose'),
(85696, '-90174','0','Divine purpose'),
(84963, '-90174','0','Divine purpose'),
(85673, '-90174','0','Divine purpose');
