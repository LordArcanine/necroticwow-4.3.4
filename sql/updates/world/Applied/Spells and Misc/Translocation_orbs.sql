UPDATE `gameobject_template` SET `data10`='26572' WHERE (`entry`='180912'); 
UPDATE `gameobject_template` SET `data10`='26566' WHERE (`entry`='180911'); 

DELETE FROM spell_target_position WHERE id IN (26572,26566);
INSERT INTO spell_target_position (id, target_map, target_position_x, target_position_y, target_position_z, target_orientation) VALUES 
(26572, 0, 1805.92, 330.39, 70.39, 4.75),
(26566, 530, 10038.7, -7000.9, 61.86, 3.05);
