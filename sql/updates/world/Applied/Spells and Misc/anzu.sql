DELETE FROM `creature` WHERE `id` = 23035;
DELETE FROM `creature` WHERE `guid` IN (83285,83282,83261,83262,83278,12161,12163,83283,49348,83289,83266,83290,49353,49354,83267,83281,83268,83287,83260,83264,83284,83265);

DELETE FROM `gameobject` WHERE `id` = 185554;
DELETE FROM `gameobject` WHERE `id` = 183050;
DELETE FROM `gameobject` WHERE `guid` = 28507;

DELETE FROM `creature` WHERE `id`=23035;
INSERT INTO `creature` (`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(23035, 556, 2, 1, 0, 0, -86.9838, 287.745, 26.4832, 3.19287, 300, 0, 0, 1, 0, 0, 0, 0, 0);

DELETE FROM `gameobject` WHERE `id`=183398;
INSERT INTO `gameobject` (`id`,`map`,`spawnMask`,`phaseMask`,`position_x`,`position_y`,`position_z`,`orientation`,`rotation0`,`rotation1`,`rotation2`,`rotation3`,`spawntimesecs`,`animprogress`,`state`) VALUES
(183398, 556, 3, 1, 44.8935, 158.699, 0.0555385, 4.74924, 0, 0, 0.693958, -0.720016, 300, 0, 1);

DELETE FROM `creature_loot_template` WHERE `entry` = 23035;
INSERT INTO `creature_loot_template` (`entry`,`item`,`chanceOrQuestChance`,`lootmode`,`groupid`,`mincountorref`,`maxcount`) VALUES 
(23035,28558,39,1,0,1,1)
,(23035,32768,2,1,0,1,1)
,(23035,32769,23,1,1,1,1)
,(23035,32778,25,1,1,1,1)
,(23035,32779,21,1,1,1,1)
,(23035,32780,21,1,1,1,1)
,(23035,32781,23,1,1,1,1)
,(23035,30553,11,1,0,1,1)
,(23035,30554,11,1,0,1,1)
,(23035,30552,11,1,0,1,1)
,(23035,17056,10,1,0,1,1)
,(23035,27854,5,1,0,1,1)
,(23035,27860,2,1,0,1,1)
,(23035,22829,1.5,1,0,1,1)
,(23035,22832,0.8,1,0,1,1)
;