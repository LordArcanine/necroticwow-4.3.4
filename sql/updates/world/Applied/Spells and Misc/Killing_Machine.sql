DELETE FROM `spell_proc_event` WHERE `entry` IN (51123);
INSERT INTO `spell_proc_event` VALUES (51123, 0x01, 0x0F, 0x00000000, 0x00000000, 0x00000000, 0x00000004, 0x00000000, 1, 100, 0);

DELETE FROM `spell_proc_event` WHERE `entry` IN (51127);
INSERT INTO `spell_proc_event` VALUES (51127, 0x01, 0x0F, 0x00000000, 0x00000000, 0x00000000, 0x00000004, 0x00000000, 3, 100, 0);

DELETE FROM `spell_proc_event` WHERE `entry` IN (51128);
INSERT INTO `spell_proc_event` VALUES (51128, 0x01, 0x0F, 0x00000000, 0x00000000, 0x00000000, 0x00000004, 0x00000000, 5, 100, 0);

DELETE FROM `spell_script_names` WHERE `spell_id` IN (-49020, -49143);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (-49020, 'spell_dk_obliterate_frost_strike'), (-49143, 'spell_dk_obliterate_frost_strike');