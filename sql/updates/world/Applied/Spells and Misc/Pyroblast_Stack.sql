-- Pyroblast! Stack
DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (11366,92315,92316);
INSERT INTO spell_linked_spell(spell_trigger,spell_effect,`type`,`comment`) VALUES
(92315, -48108, 0,'Pyroblast! & Hot Streak'),
(11366, -92315, 1,'Pyroblast! Stack'),
(92315, -11366, 1,'Pyroblast Stack');
