DELETE FROM `spell_linked_spell` WHERE `spell_effect`in (11327,-1784);
INSERT INTO spell_linked_spell (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
(1856, 11327, 0, 'Vanish aplica Improved stealth'),
(11327, -1784, 0, 'Cast stealth at vanish 3 seconds'),
(1856, -1784, 0, 'Improved stealth for security');

