-- Gurthalak, Voice of the Deeps - Normal
-- Tentacle of the Old Ones
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=57220;

DELETE FROM `smart_scripts` WHERE `entryorguid`=57220 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(57220,0,0,0,0,0,100,0,2000,5000,4000,6000,11,52586,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"cast mindflay");

-- Summon Tentacle of the Old Ones
DELETE FROM `spell_proc_event` WHERE `entry` IN (107818);
INSERT INTO `spell_proc_event` VALUES 
(107818, 0, 0, 0, 0, 0, 20, 0, 0, 2, 0);

-- Gurthalak, Voice of the Deeps - Heroic
-- Tentacle of the Old Ones
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=58078;

DELETE FROM `smart_scripts` WHERE `entryorguid`=58078 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(58078,0,0,0,0,0,100,0,2000,5000,4000,6000,11,52586,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"cast mindflay");

-- Summon Tentacle of the Old Ones
DELETE FROM `spell_proc_event` WHERE `entry` IN (109840);
INSERT INTO `spell_proc_event` VALUES 
(109840, 0, 0, 0, 0, 0, 20, 0, 0, 2, 0);

-- Gurthalak, Voice of the Deeps - Raid Finder
-- Tentacle of the Old Ones
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=58077;

DELETE FROM `smart_scripts` WHERE `entryorguid`=58077 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(58077,0,0,0,0,0,100,0,2000,5000,4000,6000,11,52586,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"cast mindflay");

-- Summon Tentacle of the Old Ones
DELETE FROM `spell_proc_event` WHERE `entry` IN (109838);
INSERT INTO `spell_proc_event` VALUES 
(109838, 0, 0, 0, 0, 0, 20, 0, 0, 2, 0);