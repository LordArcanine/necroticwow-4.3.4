-- Drop old npc and his positions, only voljin in heroic correct
DELETE FROM `creature` WHERE `map`='568' AND `id` != '52924';

-- A O Npc
UPDATE `creature_template` SET `Health_mod`='232470' WHERE `entry` IN (52925,52938);
UPDATE `creature_template` SET `Health_mod`='10593' WHERE `entry`='52933';

REPLACE INTO `creature` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`curhealth`) VALUES
(15437535,52938,568,2,1,102.623,1671.94,42.0215,4.68735,300,232470),
(15437537,52938,568,2,1,137.039,1671.94,42.0218,4.70698,300,232470),
(15437920,52938,568,2,1,129.63,1714.16,42.0216,1.51649,300,232470),
(15437922,52938,568,2,1,111.484,1714.16,42.0216,1.52433,300,232470),

(15436774,52925,568,2,1,129.434,1676.09,42.0215,4.45322,300,232470),
(15437154,52933,568,2,1,111.521,1676.09,42.0215,4.45322,300,10593);

-- Voodoo Pile
DELETE FROM `gameobject` WHERE `id`='208549';
REPLACE INTO `gameobject` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`) VALUES
(10005693,208549,568,2,1,95.5803,1650.37,42.0216,1.58876,300);
UPDATE `gameobject_template` SET `size`='3' WHERE `entry`='208549';
-- Quest Voodoo Pile
DELETE FROM `gameobject_questrelation` WHERE `id`='208549';
REPLACE INTO `gameobject_questrelation` (`id`,`quest`) VALUES (208549,29261);

-- Voljin
-- Set on raptor
DELETE FROM `creature_template_addon` WHERE `entry` IN (52924);
REPLACE INTO `creature_template_addon` (`entry`,`mount`) VALUES (52924,15289);

UPDATE `creature_template` SET `ScriptName`='npc_zulaman_voljin' WHERE `entry`='52924';
DELETE FROM `npc_text` WHERE `ID`='66672';
REPLACE INTO `npc_text` (`ID`,`text0_0`,`prob0`) VALUES ('66672','Da Zandalari be a threat to both our peoples, $r. It will take da strenght o` both our forces to break them.',1);

-- Objects
DELETE FROM `gameobject` WHERE `id`='186728';
REPLACE INTO `gameobject` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`position_x`,`position_y`,`position_z`,`orientation`, `rotation2`, `rotation3`,`spawntimesecs`,`state`) VALUES
(10005910,186728,568,2,1,120.531,1605.64,63.395,3.09307,0.0284024,0.999597,300,1);

DELETE FROM `gameobject_template` WHERE `entry`='187359';
REPLACE INTO `gameobject_template` (`entry`,`type`,`displayId`,`name`,`flags`,`size`,`data2`) VALUES (187359,18,1827,'Strange gong',16,1,45226);