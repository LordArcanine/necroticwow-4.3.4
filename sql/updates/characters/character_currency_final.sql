/*Table structure for table `character_currency` */

DROP TABLE IF EXISTS `character_currency`;

CREATE TABLE `character_currency` (
  `guid` int(11) unsigned NOT NULL,
  `id` smallint(5) unsigned NOT NULL,
  `countTotal` int(11) unsigned NOT NULL,
  `countWeek` int(11) unsigned NOT NULL,
  PRIMARY KEY (`guid`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;