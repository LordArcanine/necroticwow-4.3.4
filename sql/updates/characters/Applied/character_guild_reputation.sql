-- Guild Reputation Weekly
DELETE FROM `worldstates` WHERE `entry` = '20007';
INSERT INTO `worldstates` VALUES (20007, 0, 'NextGuildRepReset');

DROP TABLE IF EXISTS `character_guild_reputation`;
CREATE TABLE `character_guild_reputation` (
 `guid` int(10) unsigned NOT NULL,
 `guildid` int(10) unsigned NOT NULL COMMENT 'Guild Identificator',
 `disband_time` int(10) unsigned NOT NULL DEFAULT '0',
 `weekly_rep` bigint(20) NOT NULL DEFAULT '0',
 UNIQUE KEY `guid_key` (`guid`),
 KEY `guildid_key` (`guildid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Characters Guild Reputation System';

-- Populate Table character_guild_reputation
INSERT INTO `character_guild_reputation` (`guid`, `guildid`) SELECT `guid`, `guildid` FROM `guild_member`;