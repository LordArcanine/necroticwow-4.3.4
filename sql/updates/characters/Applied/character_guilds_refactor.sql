RENAME TABLE `guild_news_log` TO `guild_newslog`;

ALTER TABLE `guild_newslog`
CHANGE COLUMN `guild` `guildid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Guild Identificator',
CHANGE COLUMN `id` `LogGuid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Log record identificator - auxiliary column',
CHANGE COLUMN `eventType` `EventType` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Event type',
CHANGE COLUMN `playerGuid` `PlayerGuid` int(10) unsigned NOT NULL DEFAULT '0',
CHANGE COLUMN `data` `Value` int(10) unsigned NOT NULL DEFAULT '0',
CHANGE COLUMN `flags` `Flags` int(10) unsigned NOT NULL DEFAULT '0' AFTER `PlayerGuid`,
CHANGE COLUMN `date` `TimeStamp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Event UNIX time',
ADD KEY `guildid_key` (`guildid`),
ADD KEY `Idx_PlayerGuid` (`PlayerGuid`),
ADD KEY `Idx_LogGuid` (`LogGuid`);

-- Create missing tabs

CREATE TABLE IF NOT EXISTS `guild_member_withdraw` (
  `guid` int(10) unsigned NOT NULL,
  `tab0` int(10) unsigned NOT NULL DEFAULT '0',
  `tab1` int(10) unsigned NOT NULL DEFAULT '0',
  `tab2` int(10) unsigned NOT NULL DEFAULT '0',
  `tab3` int(10) unsigned NOT NULL DEFAULT '0',
  `tab4` int(10) unsigned NOT NULL DEFAULT '0',
  `tab5` int(10) unsigned NOT NULL DEFAULT '0',
  `tab6` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `tab7` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `money` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Guild Member Daily Withdraws';

ALTER TABLE `guild_member` DROP COLUMN `BankRemMoney`;
ALTER TABLE `guild_member` DROP COLUMN `BankRemSlotsTab0`;
ALTER TABLE `guild_member` DROP COLUMN `BankRemSlotsTab1`;
ALTER TABLE `guild_member` DROP COLUMN `BankRemSlotsTab2`;
ALTER TABLE `guild_member` DROP COLUMN `BankRemSlotsTab3`;
ALTER TABLE `guild_member` DROP COLUMN `BankRemSlotsTab4`;
ALTER TABLE `guild_member` DROP COLUMN `BankRemSlotsTab5`;
ALTER TABLE `guild_member` DROP COLUMN `BankRemSlotsTab6`;
ALTER TABLE `guild_member` DROP COLUMN `BankRemSlotsTab7`;
ALTER TABLE `guild_member` DROP COLUMN `BankResetTimeMoney`;
ALTER TABLE `guild_member` DROP COLUMN `BankResetTimeTab0`;
ALTER TABLE `guild_member` DROP COLUMN `BankResetTimeTab1`;
ALTER TABLE `guild_member` DROP COLUMN `BankResetTimeTab2`;
ALTER TABLE `guild_member` DROP COLUMN `BankResetTimeTab3`;
ALTER TABLE `guild_member` DROP COLUMN `BankResetTimeTab4`;
ALTER TABLE `guild_member` DROP COLUMN `BankResetTimeTab5`;
ALTER TABLE `guild_member` DROP COLUMN `BankResetTimeTab6`;
ALTER TABLE `guild_member` DROP COLUMN `BankResetTimeTab7`;

DELETE FROM `worldstates` WHERE `entry`=20006;
INSERT INTO `worldstates` (`entry`,`value`,`comment`) VALUES (20006,0, 'Guild daily reset');

ALTER TABLE `character_guild_reputation` MODIFY COLUMN `weekly_rep` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `character_guild_reputation` ADD COLUMN `total_rep` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0' AFTER `weekly_rep`;