
/*Table structure for table `character_currency` */

DROP TABLE IF EXISTS `character_currency`;

CREATE TABLE `character_currency` (
  `guid` int(10) NOT NULL,
  `id` int(5) NOT NULL,
  `countTotal` int(7) NOT NULL,
  `countWeek` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
