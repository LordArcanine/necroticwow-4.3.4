CREATE TABLE `guild_challenges_completed` (
	`guildId` INT(10) UNSIGNED NULL DEFAULT NULL,
	`challengeId` INT(10) UNSIGNED NULL DEFAULT NULL,
	`dateCompleted` INT(10) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`guildId`),
	INDEX `challengeId` (`challengeId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;