/*Table structure for table `character_archaeology_digsites` */

DROP TABLE IF EXISTS `character_archaeology_digsites`;

CREATE TABLE `character_archaeology_digsites` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `siteId` smallint(5) unsigned NOT NULL DEFAULT '0',
  `mapId` smallint(5) unsigned NOT NULL DEFAULT '0',
  `areaId` smallint(5) unsigned NOT NULL DEFAULT '0',
  `find1PosX` float NOT NULL DEFAULT '0',
  `find2PosX` float NOT NULL DEFAULT '0',
  `find3PosX` float NOT NULL DEFAULT '0',
  `find1PosY` float NOT NULL DEFAULT '0',
  `find2PosY` float NOT NULL DEFAULT '0',
  `find3PosY` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`siteId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Archaeology System';

/*Table structure for table `character_completed_digsites` */

DROP TABLE IF EXISTS `character_completed_digsites`;

CREATE TABLE `character_completed_digsites` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `digsiteId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`guid`,`digsiteId`),
  KEY `digsiteId` (`digsiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;