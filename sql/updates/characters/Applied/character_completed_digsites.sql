CREATE TABLE `character_completed_digsites` (
	`guid` INT(10) UNSIGNED NULL,
	`digsiteId` INT(10) UNSIGNED NULL,
	PRIMARY KEY (`guid`),
	INDEX `digsiteId` (`digsiteId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE `character_archaeology_projects` (
	`guid` INT(10) UNSIGNED NULL,
	`projectId` INT(3) UNSIGNED NULL,
	`pointsEarned` INT(3) UNSIGNED NULL,
	`completed` INT(1) NULL DEFAULT '0',
	PRIMARY KEY (`guid`),
	INDEX `projectId` (`projectId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `character_digsites`
	RENAME TO `character_archaeology_digsites`;