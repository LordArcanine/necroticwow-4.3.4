DROP TABLE IF EXISTS `character_completed_projects`;

CREATE TABLE `character_completed_projects` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `projectId` int(10) unsigned NOT NULL,
  `completionCount` int(10) unsigned NOT NULL DEFAULT '0',
  `completionTime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `character_archaeology_projects`;

CREATE TABLE `character_archaeology_projects` (
  `guid` int(10) unsigned NOT NULL,
  `branchId` int(10) NOT NULL DEFAULT '0',
  `projectEntry` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`branchId`,`projectEntry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `character_completed_digsites`;

CREATE TABLE `character_completed_digsites` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `digsiteId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`guid`,`digsiteId`),
  KEY `digsiteId` (`digsiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;