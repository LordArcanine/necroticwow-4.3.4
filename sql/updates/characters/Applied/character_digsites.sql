/*Table structure for table `character_digsites` */

DROP TABLE IF EXISTS `character_digsites`;

CREATE TABLE `character_digsites` (
  `guid` int(10) unsigned NOT NULL default '0',
  `siteId` smallint(5) unsigned NOT NULL default '0',
  `mapId` smallint(5) unsigned NOT NULL default '0',
  `areaId` smallint(5) unsigned NOT NULL default '0',
  `findNumber` smallint(5) unsigned NOT NULL default '0',
  `findPosX` float NOT NULL default '0',
  `findPosY` float NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;