/*
 * Copyright (C) 2012 Sky-Mist
 * 
 * By reading the terms of rights you aknowledge you have a lot of free time, you are a cunt by all
 * means and have aided or commited intelectual theft.
 *
 * ASSHOLES! Read this if you want to use the script! VERY IMPORTANT!
 *
 * If you are suppose to have this script, you are not an asshole. In which case read this for fun.
 * If not by all means go ahead and take our hard work. Or go ahead and delete you main partition. 
 * Or, please use your dads razor on tongue till you choke on your own blood.
 * 
 * You dead yet? Good. Now listen. You are not suppose to have this shit. It was posted by some jerk
 * somewhere for some unknown reason. I can't do jack to stop you from using this except asking you to
 * not, but since you already are reading this, you already accepted the terms of rights.
 * 
 * If I can ask you to give a fuck though, e-mail me at skiesadvent@gmail.com in hope that I may track
 * down the soon to be bleeding vagina of a face that leaked this work. Thank you! Hope you die of a 
 * horrible disease!
 */

#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "throne_of_the_four_winds.h"

enum Spells
{
	SPELL_TORNADO_VISUAL = 87856
};

class npc_tornado_moving : public CreatureScript
{
    public:
        npc_tornado_moving() :  CreatureScript("npc_tornado_moving") { }

        struct npc_tornado_movingAI : public ScriptedAI
        {
            npc_tornado_movingAI(Creature* creature) : ScriptedAI(creature)
            {

            }

			bool MoveSide; // true = right, false = left
			float defaultDistX;
			float defaultDistY;

            void IsSummonedBy(Unit* /*summoner*/)
            {
				//me->AddAura(SPELL_TORNADO_VISUAL,me);

				defaultDistX = GetAlakir()->GetPositionX() > me->GetPositionX() ? GetAlakir()->GetPositionX() - me->GetPositionX() : me->GetPositionX() - GetAlakir()->GetPositionX();
				defaultDistY = GetAlakir()->GetPositionY() > me->GetPositionY() ? GetAlakir()->GetPositionY() - me->GetPositionY() : me->GetPositionY() - GetAlakir()->GetPositionY();

				std::list<Creature*> tornados;
			    GetCreatureListWithEntryInGrid(tornados, me, me->GetEntry(), 40.0f);

				if (!tornados.empty() && tornados.size() < 4)
					//me->SummonCreature(me->GetEntry(),me->GetPositionX() - (cos(me->GetOrientation())*2),me->GetPositionY() - (sin(me->GetOrientation())*2),me->GetPositionZ());

				if (me->GetEntry() == 48854)
					MoveSide = true; // west
				else
					MoveSide = false; // east
            }

            void UpdateAI(uint32 const diff)
            {
				if (GetAlakir() && GetAlakir()->isAlive())
				{
					float distanceX = GetAlakir()->GetPositionX() > me->GetPositionX() ? GetAlakir()->GetPositionX() - me->GetPositionX() : me->GetPositionX() - GetAlakir()->GetPositionX();
					float distanceY = GetAlakir()->GetPositionY() > me->GetPositionY() ? GetAlakir()->GetPositionY() - me->GetPositionY() : me->GetPositionY() - GetAlakir()->GetPositionY();
					me->GetMotionMaster()->MovePoint(0, (distanceX < defaultDistX) ? me->GetPositionX() + MoveSide ? 1 : - 1 : me->GetPositionX(),(distanceY < defaultDistY) ? me->GetPositionY() + MoveSide ? 1 : - 1 : me->GetPositionY(),me->GetPositionZ());
				}
            }

			Creature* GetAlakir()
			{
				return me->FindNearestCreature(BOSS_ALAKIR, 5000.0f, true);
			}

        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_tornado_movingAI(creature);
        }
};

void AddSC_boss_alakir()
{
    //new boss_alakir();
	new npc_tornado_moving();
}