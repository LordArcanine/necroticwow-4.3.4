 /*
 * Copyright (C) 2013 Skymist Project
 *
 * This file is NOT free software. You may NOT copy, redistribute it or modify it.
 */

#include "ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Weather.h"

#include "throne_of_the_tides.h"

enum Yells
{
    SAY_INTRO_1    = 0,
    SAY_INTRO_2    = 1,
    SAY_1          = 2,
    SAY_2          = 3,
    SAY_3          = 4,
    SAY_CLEANSED   = 5,
    SAY_DEATH      = 6,
    SAY_FILTH      = 7,
    SAY_66         = 8,
    SAY_33         = 9,
    SAY_TIDSURGE   = 10,
    SAY_OUTRO_1    = 11,
    SAY_OUTRO_2    = 12
};

enum Spells
{
    // Neptulon
    SPELL_CHANNEL_CAST        = 74375,
    SPELL_TIDAL_SURGE         = 76133,
    // Ozumat
    SPELL_ENTANGLING_GRASP    = 83463,
    SPELL_OZUMAT_BLIGHT       = 83525,
    SPELL_OZUMAT_STAY_GLOBE   = 84110,
    SPELL_VISUAL_INK_AURA     = 82953, // Let's get purple!
    SPELL_DUMMY_BLIGHT        = 83672  // Mouth open, shit comes out.
};

enum Events
{
    EVENT_INTRO_2,

    EVENT_SUMMON_MURLOCS      = 1,
    EVENT_SUMMON_MINDLASHERS,
    EVENT_SUMMON_BEHEMOTH,

    EVENT_SUMMON_BEAST,
    EVENT_SUMMON_SAPPERS,
    EVENT_SUMMON_OZUMAT,

    EVENT_PHASE_2,
    EVENT_PHASE_3,

    EVENT_OUTRO_1,
    EVENT_OUTRO_2,

    EVENT_RESET_ENCOUNTER,
};

enum Actions
{
    ACTION_EVENTS_START = 1
};

const Position spawnPos[] = // Left and right sides summon positions.
{
    {-122.412041f, 947.492188f, 231.579025f, 2.279974f},
    {-118.400780f, 1014.799866f, 230.195724f, 4.366778f},
};

class npc_neptulon : public CreatureScript
{
public:
    npc_neptulon() : CreatureScript("npc_neptulon") { }

    bool OnGossipHello(Player* player, Creature* creature)
    {
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "We are ready!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
        player->SEND_GOSSIP_MENU(40792, creature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
    {
        if (action == GOSSIP_ACTION_INFO_DEF+1) // Start battle
        {
            creature->GetAI()->DoAction(ACTION_EVENTS_START);
            player->PlayerTalkClass->ClearMenus();
            player->CLOSE_GOSSIP_MENU();
        }

        return true;
    }

    struct npc_neptulonAI : public ScriptedAI
    {
        npc_neptulonAI(Creature* creature) : ScriptedAI(creature), summons(me)
        {
            instance = creature->GetInstanceScript();
            introDone = false;
            introDone2 = false;
        }

        InstanceScript* instance;
        SummonList summons;
        EventMap events;

        bool InProgress, introDone, introDone2, phaseThree, said66, said33, ozumatSummoned, amDead, cleansed;
        Creature* Ozumat;
        uint32 murlocsSummoned;
        uint32 lashersSummoned;
        uint32 deadSappers;

        void Reset()
        {
            events.Reset();
			summons.DespawnAll();
            if (instance)
                instance->SetData(DATA_OZUMAT_EVENT, NOT_STARTED);

            InProgress = false;
            phaseThree = false;
            said66 = false;
            said33 = false;
            ozumatSummoned = false;
            cleansed = false;
            amDead = false;

            murlocsSummoned = 0;
            lashersSummoned = 0;
            deadSappers = 0;
        }

        void EnterEvadeMode()
        {
            Reset();
			me->RemoveAllAuras();

            if (!me->HasFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP))
                me->SetFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);

            if (instance)
            {
                instance->SetData(DATA_OZUMAT_EVENT, FAIL);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
            }
        }

        void MoveInLineOfSight(Unit* who)
        {
            if (!introDone && me->IsWithinDistInMap(who, 50) && who->GetTypeId() == TYPEID_PLAYER)
            {
                Talk(SAY_INTRO_1);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                events.ScheduleEvent(EVENT_INTRO_2, 5000);
                introDone = true;
            }
        }

        void DoAction(const int32 action)
        {
            switch(action)
            {
                case ACTION_EVENTS_START:
                    me->RemoveFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
                    me->SetReactState(REACT_PASSIVE);
                    Talk(SAY_1);
                    InProgress = true;

                    if (instance)
                    {
                        instance->SetData(DATA_OZUMAT_EVENT, IN_PROGRESS);
                        instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me); // Add
                    }
                    
                    DoCast(me, SPELL_CHANNEL_CAST);
                    events.ScheduleEvent(EVENT_SUMMON_MURLOCS, 100);
                    events.ScheduleEvent(EVENT_SUMMON_MINDLASHERS, 5000);
                    events.ScheduleEvent(EVENT_SUMMON_BEHEMOTH, 15000);
                    events.ScheduleEvent(EVENT_PHASE_2, 60000);
                    break;
            }
        }

        void EnterCombat(Unit* /*who*/) {}

        void DamageTaken(Unit* /*doneBy*/, uint32& damage)
        {
            if (amDead)
                damage = 0;
        }

        void SummonedCreatureDespawn(Creature* summon)
        {
            switch(summon->GetEntry())
            {
                case NPC_SAPPER:
                    deadSappers += 1;
                    break;
            }
        }

        void JustSummoned(Creature* summon)
        {
            summons.Summon(summon);
			summon->setActive(true);

            switch (summon->GetEntry())
            {
                case NPC_DEEP_MURLOC:
                    summon->AI()->AttackStart(me);
                    break;

                case NPC_MINDLASHER:
                    summon->AI()->AttackStart(me);
                    break;

                case NPC_BEHEMOTH:
                    summon->AI()->AttackStart(me);
                    break;

                case NPC_SAPPER:
                    summon->CastSpell(me, SPELL_ENTANGLING_GRASP, false);
                    break;

                case BOSS_OZUMAT:
                    summon->SetInCombatWithZone();
                    summon->SetReactState(REACT_PASSIVE);
                    summon->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                    break;
            }
        }

        void SummonAdd(uint32 entry)
        {
            if (entry == BOSS_OZUMAT)
            {
                Ozumat = me->SummonCreature(entry, -200.249f, 926.618f, 266.355f, 0.824733f, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 30000);
                Ozumat->CastSpell(Ozumat, SPELL_OZUMAT_STAY_GLOBE, true);
                return;
            }

            if (entry == NPC_SAPPER)
            {
                me->SummonCreature(entry, -143.599869f, 985.389221f, 230.390076f, 0.024302f, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 3000);
                me->SummonCreature(entry, -130.816040f, 968.372253f, 230.172058f, 1.370475f, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 3000);
                me->SummonCreature(entry, -140.050064f, 1004.192871f, 229.926407f, 5.370507f, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 3000);
                return;
            }

            if (entry == NPC_BEAST)
                me->SummonCreature(entry, RAND(spawnPos[0], spawnPos[1]), TEMPSUMMON_CORPSE_TIMED_DESPAWN, 3000);

            if (entry == NPC_DEEP_MURLOC)
                for (uint32 x = 0; x <= 6; ++x)
                    me->SummonCreature(entry, RAND(spawnPos[0], spawnPos[1]), TEMPSUMMON_CORPSE_TIMED_DESPAWN, 3000);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!InProgress && introDone2)
                return;

            // Checks

            if (deadSappers >= 3 && ozumatSummoned && !phaseThree)
            {
                events.ScheduleEvent(EVENT_PHASE_3, 1000);
                phaseThree = true;
            }

            if (ozumatSummoned && Ozumat->HealthBelowPct(11) && !cleansed)
            {
                Talk(SAY_CLEANSED);
                Ozumat->SetWalk(false);
                Ozumat->SetSpeed(MOVE_WALK, 2.0f);
                Ozumat->SetSpeed(MOVE_RUN, 2.0f);
                Ozumat->GetMotionMaster()->MovePoint(0, Ozumat->GetPositionX(), Ozumat->GetPositionY(), Ozumat->GetPositionZ() + 50);
                Ozumat->DespawnOrUnsummon(3000);

                if (instance)
                {
                    instance->SetData(DATA_OZUMAT_EVENT, DONE);
                    instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, Ozumat); // Remove
                    instance->DoRemoveAurasDueToSpellOnPlayers(SPELL_TIDAL_SURGE);
                    //instance->DoCompleteAchievement(me->GetMap()->IsHeroic() ? 5061 : 4839);
                }

                me->SummonGameObject(DUNGEON_MODE(GO_OZUMAT_CHEST_NORMAL, GO_OZUMAT_CHEST_HEROIC), -125.950981f, 983.343201f, 230.335464f, 3.635565f, 0, 0, 0, 0, 9000000);

                events.ScheduleEvent(EVENT_OUTRO_1, 6500);
                events.ScheduleEvent(EVENT_OUTRO_2, 13000);
                cleansed = true;
            }

            if (me->HealthBelowPct(67) && !said66)
            {
                Talk(SAY_66);
                said66 = true;
            }

            if (me->HealthBelowPct(34) && !said33)
            {
                Talk(SAY_33);
                said33 = true;
            }

            if (me->HealthBelowPct(6) && !amDead)
            {
                Talk(SAY_DEATH);
                events.ScheduleEvent(EVENT_RESET_ENCOUNTER, 5000);
                amDead = true;
            }

            // Events

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_INTRO_2:
                        Talk(SAY_INTRO_2);
                        introDone2 = true;
                        break;

                    case EVENT_RESET_ENCOUNTER:
                        Talk(SAY_DEATH);
                        EnterEvadeMode();
                        break;

                    case EVENT_SUMMON_MURLOCS:
                        if (murlocsSummoned < 3)
                        {
                            SummonAdd(NPC_DEEP_MURLOC);
                            events.ScheduleEvent(EVENT_SUMMON_MURLOCS, 5000);
                            murlocsSummoned++;
                        }
                        break;

                    case EVENT_SUMMON_MINDLASHERS:
                        if (lashersSummoned < 3)
                        {
                            SummonAdd(NPC_MINDLASHER);
                            events.ScheduleEvent(EVENT_SUMMON_MINDLASHERS, 15000);
                            lashersSummoned++;
                        }
                        break;

                    case EVENT_SUMMON_BEHEMOTH:
                        Talk(SAY_FILTH);
                        SummonAdd(NPC_BEHEMOTH);
                        break;

                    case EVENT_PHASE_2:
                        Talk(SAY_2);
                        events.ScheduleEvent(EVENT_SUMMON_BEAST, 6000);
                        events.ScheduleEvent(EVENT_SUMMON_SAPPERS, 1000);
                        events.ScheduleEvent(EVENT_SUMMON_OZUMAT, 10000);
                        break;

                    case EVENT_SUMMON_BEAST:
                        SummonAdd(NPC_BEAST);
                        events.ScheduleEvent(EVENT_SUMMON_BEAST, 6000);
                        break;

                    case EVENT_SUMMON_SAPPERS:
                        SummonAdd(NPC_SAPPER);
                        break;

                    case EVENT_SUMMON_OZUMAT:
                        Talk(SAY_3);
                        SummonAdd(BOSS_OZUMAT);
                        ozumatSummoned = true;
                        break;

                    case EVENT_PHASE_3:
                        Talk(SAY_TIDSURGE);
                        events.CancelEvent(EVENT_SUMMON_BEAST);
                        if (instance)
						{
                            instance->DoCastSpellOnPlayers(SPELL_TIDAL_SURGE);
                            instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, Ozumat); // Add
						}
                        Ozumat->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                        Ozumat->CastSpell(Ozumat, SPELL_OZUMAT_BLIGHT, false);
                        break;

                    case EVENT_OUTRO_1:
                        Talk(SAY_OUTRO_1);
                        break;

                    case EVENT_OUTRO_2:
                        Talk(SAY_OUTRO_2);
                        break;
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_neptulonAI (creature);
    }
};

void AddSC_ozumat()
{
    new npc_neptulon();
}