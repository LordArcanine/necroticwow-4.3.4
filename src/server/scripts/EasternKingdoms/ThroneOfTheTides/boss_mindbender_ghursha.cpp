 /*
 * Copyright (C) 2013 Skymist Project
 *
 * This file is NOT free software. You may NOT copy, redistribute it or modify it.
 */

#include "ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Weather.h"
#include "MoveSplineInit.h"

#include "throne_of_the_tides.h"

enum ErunakYells
{
    SAY_ERUNAK_AGGRO        = 0,
    SAY_WIN_ERUNAK          = 1
};

enum Gurshayells
{
    SAY_GURSHA_AGGRO        = 0,
    SAY_GURSHA_KILL         = 1,
    SAY_GURSHA_DEATH        = 2,
    SAY_GURSHA_MIND_FOG     = 3,
    SAY_GURSHA_ENSLAVE      = 4
};

enum Events
{
    // Erunak
    EVENT_EARTH_SHARDS       = 1,
    EVENT_EMBERSTRIKE,
    EVENT_LAVA_BOLT,
    EVENT_MAGMA_SPLASH,

    EVENT_BERSERK,

    // Ghursha
    EVENT_ENSLAVE,
    EVENT_ABSORB_MAGIC,
    EVENT_MIND_FOG,
    EVENT_UNRELENTING_AGONY,

    EVENT_ENSLAVED_PLAYER_CAST,
    EVENT_KILL_PLAYER_HEROIC
};

enum Spells
{
    // Erunak Stonespeaker
    SPELL_EARTH_SHARDS        = 84931, // Missile - summons npc at location.
    SPELL_EARTH_SHARD_AURA    = 84935,
    SPELL_EARTH_SHARD_SUMMON  = 84934,

    SPELL_EMBERSTRIKE         = 76165,
    SPELL_LAVA_BOLT           = 76171,
    SPELL_MAGMA_SPLASH        = 76170,

    // Mindbender Ghur'sha
    SPELL_PLAYER_VEHICLE_AURA = 76206, // Make players/Erunak vehicles.
    SPELL_ENTER_VEHICLE       = 46598, // Mindbender on players and Erunak.

    SPELL_ENSLAVE             = 76207,
    SPELL_ENSLAVE_BUFF        = 76213, // Should be in SPELL_LINKED_SPELL with SPELL_ENSLAVE

    SPELL_ABSORB_MAGIC        = 76307,
    SPELL_ABSORB_MAGIC_HEAL   = 76308,
    SPELL_MIND_FOG_SUMMON     = 76234,
    SPELL_UNRELENTING_AGONY   = 76339,

    SPELL_BERSERK             = 47008
};

struct NotCharmedTargetSelector : public std::unary_function<Unit *, bool> 
{
    NotCharmedTargetSelector() {}

    bool operator() (const Unit* target) 
	{
        return (!target->isCharmed());
    }
};

class boss_erunak_stonespeaker : public CreatureScript
{
public:
    boss_erunak_stonespeaker() : CreatureScript("boss_erunak_stonespeaker") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_erunak_stonespeakerAI(creature);
    }

    struct boss_erunak_stonespeakerAI : public ScriptedAI
    {
        boss_erunak_stonespeakerAI(Creature* creature) : ScriptedAI(creature), vehicle(creature->GetVehicleKit()), summons(me)
        {
            ASSERT(vehicle);
            instance = creature->GetInstanceScript();
            gurshaSummoned = false;
        }

        InstanceScript* instance;
        Vehicle* vehicle;
        Creature* gursha;
        SummonList summons;
        EventMap events;
        bool isInGhurshaPhase, gurshaSummoned;

        void Reset()
        {
            events.Reset();
            isInGhurshaPhase = false;

            if (instance)
                instance->SetData(DATA_ERUNAK_STONESPEAKER_EVENT, NOT_STARTED);
        }

        void MoveInLineOfSight(Unit* who) // Add Gur'sha to Erunak.
        {
            if (!gurshaSummoned && who->IsWithinDistInMap(me, 50.0f))
            {
                gursha = NULL;
                gurshaSummoned = true;
                Position pos;
                me->GetPosition(&pos);
                gursha = me->SummonCreature(BOSS_MINDBENDER_GHURSHA, pos, TEMPSUMMON_MANUAL_DESPAWN);
                gursha->EnterVehicle(me, 0);
                gursha->SetReactState(REACT_DEFENSIVE);
                gursha->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                gursha->IsAIEnabled = false;
            }
        }

        void EnterEvadeMode()
        {
			me->RemoveAllAuras();
            if (isInGhurshaPhase)
            {
                isInGhurshaPhase = false;
                gurshaSummoned = false;
                me->setFaction(14);
                me->IsAIEnabled = true;
                me->setActive(true);
                if (instance)
                    instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, gursha); // Remove
            }
            Reset();
            me->SetStandState(UNIT_STAND_STATE_STAND);
            if (me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE))
                me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
			me->GetMotionMaster()->MoveTargetedHome();
            me->SetHealth(me->GetMaxHealth());
            me->SetReactState(REACT_AGGRESSIVE);

            if (instance)
            {
                instance->SetData(DATA_ERUNAK_STONESPEAKER_EVENT, FAIL);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
            }
        }

        void EnterCombat(Unit* /*who*/)
        {
            Talk(SAY_ERUNAK_AGGRO);

            if (instance)
            {
                instance->SetData(DATA_ERUNAK_STONESPEAKER_EVENT, IN_PROGRESS);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me); // Add
            }

            if (gursha)
                gursha->SetInCombatWithZone();

            events.ScheduleEvent(EVENT_EARTH_SHARDS, 20000);
            events.ScheduleEvent(EVENT_EMBERSTRIKE, 11000);
            events.ScheduleEvent(EVENT_LAVA_BOLT, 6500);
            events.ScheduleEvent(EVENT_MAGMA_SPLASH, 17000);
            events.ScheduleEvent(EVENT_BERSERK, 10 * MINUTE * IN_MILLISECONDS);
        }

        void JustSummoned(Creature* summon)
        {
            summons.Summon(summon);
        }

        void SummonedCreatureDespawn(Creature* summon)
        {
            if (isInGhurshaPhase && summon->GetEntry() == BOSS_MINDBENDER_GHURSHA && instance && instance->GetData(DATA_ERUNAK_STONESPEAKER_EVENT) != DONE)
                me->AI()->EnterEvadeMode();
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() && !isInGhurshaPhase || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            if (me->HealthBelowPct(51) && !isInGhurshaPhase)
            {
                isInGhurshaPhase = true;
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
                events.CancelEvent(EVENT_EARTH_SHARDS);
                events.CancelEvent(EVENT_EMBERSTRIKE);
                events.CancelEvent(EVENT_LAVA_BOLT);
                events.CancelEvent(EVENT_MAGMA_SPLASH);

                if (gursha)
                {
                    gursha->ExitVehicle(me);
                    me->GetVehicleKit()->RemoveAllPassengers();
			        gursha->SetHealth(gursha->GetMaxHealth() / 2);
                    gursha->ToCreature()->setRegeneratingHealth(false);
                    gursha->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                    gursha->IsAIEnabled = true;
                    gursha->setActive(true);
                    gursha->AI()->Reset();
                    me->SetInCombatWith(gursha);
                    me->SetReactState(REACT_PASSIVE);
                    me->GetMotionMaster()->MovementExpired();
                    me->GetMotionMaster()->Clear();
                    me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                    me->SetStandState(UNIT_STAND_STATE_KNEEL);
                    me->setFaction(35);
                    gursha->SetReactState(REACT_AGGRESSIVE);
                    gursha->AI()->DoZoneInCombat();
                    gursha->AI()->Talk(SAY_GURSHA_AGGRO);
                    gursha->AI()->EnterCombat(gursha->getVictim());
                    if (instance)
                        instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, gursha); // Add
                }
            }

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_EARTH_SHARDS:
                        if (Unit* target = SelectTarget(SELECT_TARGET_FARTHEST, 0, 100, true))
                        {
                            DoCast(target, SPELL_EARTH_SHARDS);

                            if (Creature* shard = me->SummonCreature(NPC_EARTH_SHARD, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(),  me->GetOrientation(), TEMPSUMMON_MANUAL_DESPAWN))
                            {
                                shard->SetReactState(REACT_PASSIVE);
                                shard->SetFacingToObject(target);
                                Movement::MoveSplineInit init(*shard);
                                init.SetOrientationFixed(true);
                                init.Launch();
                                shard->setFaction(14);
                                shard->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                                shard->SetWalk(false);
                                shard->SetSpeed(MOVE_WALK, 0.4f);
                                shard->SetSpeed(MOVE_RUN, 0.4f);
                                shard->AddAura(SPELL_EARTH_SHARD_AURA, shard);
                                float x, y, z;
                                shard->GetClosePoint(x, y, z, me->GetObjectSize() / 3, 60.0f);
                                shard->GetMotionMaster()->MovePoint(1, x, y, z);
                                shard->DespawnOrUnsummon(15000);
                            }
                        }
                        events.ScheduleEvent(EVENT_EARTH_SHARDS, 20000);
                        break;

                    case EVENT_EMBERSTRIKE:
                        DoCastVictim(SPELL_EMBERSTRIKE);
                        events.ScheduleEvent(EVENT_EMBERSTRIKE, 11000);
                        break;

                    case EVENT_LAVA_BOLT:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                            DoCast(target, SPELL_LAVA_BOLT);
                        events.ScheduleEvent(EVENT_LAVA_BOLT, 6500);
                        break;

                    case EVENT_MAGMA_SPLASH:
                        DoCastAOE(SPELL_MAGMA_SPLASH);
                        events.ScheduleEvent(EVENT_MAGMA_SPLASH, 17000);
                        break;

                    case EVENT_BERSERK:
                        DoCast(me, SPELL_BERSERK);
                        break;
                }
            }         

            if (!isInGhurshaPhase)
                DoMeleeAttackIfReady();
        }
    };
};

class boss_mindbender_ghursha : public CreatureScript
{
public:
    boss_mindbender_ghursha() : CreatureScript("boss_mindbender_ghursha") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_mindbender_ghurshaAI (creature);
    }

    struct boss_mindbender_ghurshaAI : public ScriptedAI
    {
        boss_mindbender_ghurshaAI(Creature* creature) : ScriptedAI(creature), summons(me)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;
        SummonList summons;
        EventMap events;
        bool Enslaved;
        Unit* EnslaveTarget;
        Unit* heroicTarget;

        void Reset()
        {
            events.Reset();
			summons.DespawnAll();
            Enslaved = false;
        }

        void EnterEvadeMode()
        {
			summons.DespawnAll();

            if (instance)
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove

            me->DespawnOrUnsummon(); // Causes also Erunak to evade.
        }

        void JustSummoned(Creature* summon)
        {
            summons.Summon(summon);
			summon->setActive(true);

			if (me->isInCombat())
			    summon->AI()->DoZoneInCombat();
        }

        void KilledUnit(Unit* /*victim*/)
        {
			Talk(SAY_GURSHA_KILL);
        }

        void JustDied(Unit* /*killer*/)
        {
            Talk(SAY_GURSHA_DEATH);

			summons.DespawnAll();

            if (Creature* Erunak = me->FindNearestCreature(BOSS_ERUNAK_STONESPEAKER, 150.0f))
            {
                Erunak->AI()->Talk(SAY_WIN_ERUNAK);
                Erunak->RemoveAllAuras();
                Erunak->CombatStop(false);
                Erunak->DeleteThreatList();
            }

            if (instance)
            {
                instance->SetData(DATA_ERUNAK_STONESPEAKER_EVENT, DONE);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
            }
        }

        void EnterCombat(Unit* /*who*/)
        {
            uint8 numb = 0;
            Map::PlayerList const& players = me->GetMap()->GetPlayers();
            for (Map::PlayerList::const_iterator itr = players.begin(); itr != players.end(); ++itr)
                if (Player* player = itr->getSource())
                    ++numb;

            Enslaved = false;

            if (numb > 1) // Can't enslave solo player.
                events.ScheduleEvent(EVENT_ENSLAVE, 13000);
            events.ScheduleEvent(EVENT_ABSORB_MAGIC, 20000);
            events.ScheduleEvent(EVENT_MIND_FOG, urand(6000,12000));
            events.ScheduleEvent(EVENT_UNRELENTING_AGONY, 10000);
        }

        void DamageTaken(Unit* /*doneBy*/, uint32& damage)
        {
            if (me->HasAura(SPELL_ABSORB_MAGIC))
            {
                int32 bp0 = damage * 3;
                me->CastCustomSpell(me, SPELL_ABSORB_MAGIC_HEAL, &bp0, NULL, NULL, true);
            }
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            // Remove Enslaved buff from target.
            if (Enslaved && EnslaveTarget->HealthBelowPct(51))
            {
                Enslaved = false;
                EnslaveTarget->RemoveAurasDueToSpell(SPELL_ENSLAVE);
                EnslaveTarget->RemoveAurasDueToSpell(SPELL_ENSLAVE_BUFF);
                EnslaveTarget->RemoveAurasDueToSpell(SPELL_PLAYER_VEHICLE_AURA);
                EnslaveTarget->RemoveAurasDueToSpell(SPELL_ENTER_VEHICLE);
                me->SetReactState(REACT_AGGRESSIVE);
                me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);

                events.ScheduleEvent(EVENT_ENSLAVE, 30000);
                events.ScheduleEvent(EVENT_ABSORB_MAGIC, 20000);
                events.ScheduleEvent(EVENT_MIND_FOG, urand(6000,12000));
                events.ScheduleEvent(EVENT_UNRELENTING_AGONY, 10000);
                events.CancelEvent(EVENT_ENSLAVED_PLAYER_CAST);
            }

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_ENSLAVE:
                        if (!Enslaved)
                        {
                            if (Unit* target = SelectTarget(SELECT_TARGET_FARTHEST, 0, 100, true)) // Don't CS the tank
                            {
                                Talk(SAY_GURSHA_ENSLAVE);
                                EnslaveTarget = target;
                                DoCast(target, SPELL_ENSLAVE);
                                DoCast(target, SPELL_ENSLAVE_BUFF);
                                target->AddAura(SPELL_PLAYER_VEHICLE_AURA, target);
                                DoCast(target, SPELL_ENTER_VEHICLE);
                                me->SetReactState(REACT_PASSIVE);
                                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                                Enslaved = true;

                                events.CancelEvent(EVENT_ENSLAVE);
                                events.CancelEvent(EVENT_ABSORB_MAGIC);
                                events.CancelEvent(EVENT_MIND_FOG);
                                events.CancelEvent(EVENT_UNRELENTING_AGONY);
                                events.ScheduleEvent(EVENT_ENSLAVED_PLAYER_CAST, 2000);
                                if(IsHeroic())
                                    events.ScheduleEvent(EVENT_KILL_PLAYER_HEROIC, 60000);
                                heroicTarget = target;
                            }
                        }
                        break;

                    case EVENT_ABSORB_MAGIC:
                        DoCast(me, SPELL_ABSORB_MAGIC);
                        events.ScheduleEvent(EVENT_ABSORB_MAGIC, urand(15000, 20000));
                        break;

                    case EVENT_MIND_FOG:
                        Talk(SAY_GURSHA_MIND_FOG);
                        DoCast(me, SPELL_MIND_FOG_SUMMON);
                        events.ScheduleEvent(EVENT_MIND_FOG, 18000);
                        break;

                    case EVENT_UNRELENTING_AGONY:
                        DoCastAOE(SPELL_UNRELENTING_AGONY);
                        events.ScheduleEvent(EVENT_UNRELENTING_AGONY, 20000);
                        break;

                    case EVENT_ENSLAVED_PLAYER_CAST:
                        if (Enslaved)
                        {
                            if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                            {
                                switch(EnslaveTarget->getClass())
                                {
                                    case CLASS_DRUID:
                                        if (urand(0,1) == 0)
                                            EnslaveTarget->CastSpell(pTarget, 8921, false);
                                        else
                                            EnslaveTarget->CastSpell(me, 774, false);
                                        break;

                                    case CLASS_HUNTER:
                                        EnslaveTarget->CastSpell(pTarget, RAND(2643, 1978), false);
                                        break;

                                    case CLASS_MAGE:
                                        EnslaveTarget->CastSpell(pTarget, RAND(44614, 30455), false);
                                        break;

                                    case CLASS_WARLOCK:
                                        EnslaveTarget->CastSpell(pTarget, RAND(980, 686), true);
                                        break;

                                    case CLASS_WARRIOR:
                                        EnslaveTarget->CastSpell(pTarget, RAND(46924, 845), false);
                                        break;

                                    case CLASS_PALADIN:
                                        if (urand(0,1) == 0)
                                            EnslaveTarget->CastSpell(pTarget, 853, false);
                                        else
                                            EnslaveTarget->CastSpell(me, 20473, false);
                                        break;

                                    case CLASS_PRIEST:
                                        if (urand(0,1) == 0)
                                            EnslaveTarget->CastSpell(pTarget, 34914, false);
                                        else
                                            EnslaveTarget->CastSpell(me, 139, false);
                                        break;

                                    case CLASS_SHAMAN:
                                        if (urand(0,1) == 0)
                                            EnslaveTarget->CastSpell(pTarget, 421, false);
                                        else
                                            EnslaveTarget->CastSpell(me, 61295, false);
                                        break;

                                    case CLASS_ROGUE:
                                        EnslaveTarget->CastSpell(pTarget, RAND(16511, 1329), false);
                                        break;

                                    case CLASS_DEATH_KNIGHT:
                                        if (urand(0,1) == 0)
                                            EnslaveTarget->CastSpell(pTarget, 45462, true);
                                        else
                                            EnslaveTarget->CastSpell(pTarget, 49184, true);
                                        break;
                                }
                            }

                            events.ScheduleEvent(EVENT_ENSLAVED_PLAYER_CAST, 3000);
                        }
                        break;

                    case EVENT_KILL_PLAYER_HEROIC:
                        if (Enslaved)
                            if (heroicTarget->GetGUID() == EnslaveTarget->GetGUID()) // check target with old one, if they match means same target > 1 minute.
                            {
                                EnslaveTarget->RemoveAurasDueToSpell(SPELL_ENSLAVE);
                                EnslaveTarget->RemoveAurasDueToSpell(SPELL_ENSLAVE_BUFF);
                                EnslaveTarget->RemoveAurasDueToSpell(SPELL_PLAYER_VEHICLE_AURA);
                                EnslaveTarget->RemoveAurasDueToSpell(SPELL_ENTER_VEHICLE);
                                me->Kill(EnslaveTarget);
                                Enslaved = false;
                            }
                        break;
                }
            }

            if(!Enslaved)
                DoMeleeAttackIfReady();
        }
    };
};

void AddSC_boss_erunak_stonespeaker()
{
    new boss_erunak_stonespeaker();
    new boss_mindbender_ghursha();
}