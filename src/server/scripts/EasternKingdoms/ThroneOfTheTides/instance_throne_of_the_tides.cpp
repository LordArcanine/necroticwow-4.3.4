 /*
 * Copyright (C) 2013 Skymist Project
 *
 * This file is NOT free software. You may NOT copy, redistribute it or modify it.
 */

#include "ScriptPCH.h"
#include "throne_of_the_tides.h"

class instance_throne_of_the_tides : public InstanceMapScript
{
    public:
        instance_throne_of_the_tides() : InstanceMapScript("instance_throne_of_the_tides", 643) {}

        InstanceScript* GetInstanceScript(InstanceMap* map) const
        {
            return new instance_throne_of_the_tides_InstanceMapScript(map);
        }

        struct instance_throne_of_the_tides_InstanceMapScript : public InstanceScript
        {
            instance_throne_of_the_tides_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetBossNumber(MAX_ENCOUNTER);
            }

            uint64 LadyNazjar;
            uint64 JellyFishElevator;
            uint64 CommanderUlthok;
            uint64 ErunakStonespeaker;
            uint64 MindbenderGhursha;
            uint64 Ozumat;
            uint64 Neptulon;

            uint64 LadyNazjarDoor;
            uint64 CommanderUlthokDoor;
            uint64 ErunakStonespeakerDoor;
            uint64 OzumatDoor;
            uint64 leftTentacle;
            uint64 rightTentacle;
            uint64 cinematicTrigger;

            uint32 encounter[MAX_ENCOUNTER];

            std::string str_data;

            void Initialize()
            {
                memset(&encounter, 0, sizeof(encounter));

                for (uint8 i = 0; i < MAX_ENCOUNTER; ++i)
                   encounter[i] = NOT_STARTED;

                LadyNazjar = 0;
                JellyFishElevator = 0;
                CommanderUlthok = 0;
                ErunakStonespeaker = 0;
                MindbenderGhursha = 0;
                Ozumat = 0;
                Neptulon = 0;

                LadyNazjarDoor = 0;
                CommanderUlthokDoor = 0;
                ErunakStonespeakerDoor = 0;
                OzumatDoor = 0;

                leftTentacle = 0;
                rightTentacle = 0;
                cinematicTrigger = 0;
            }

            bool IsEncounterInProgress() const
            {
                for (uint8 i = 0; i < MAX_ENCOUNTER; ++i)
                    if (encounter[i] == IN_PROGRESS) return true;

                return false;
            }

            void OnCreatureCreate(Creature* creature)
            {
                Map::PlayerList const &players = instance->GetPlayers();
                uint32 TeamInInstance = 0;

                if (!players.isEmpty())
                {
                    if (Player* player = players.begin()->getSource())
                        TeamInInstance = player->GetTeam();
                }
                switch (creature->GetEntry())
                {
                    case BOSS_LADY_NAZJAR:
                        LadyNazjar = creature->GetGUID();
                        break;
                    case BOSS_COMMANDER_ULTHOK:
                        CommanderUlthok = creature->GetGUID();
                        break;
                    case BOSS_ERUNAK_STONESPEAKER:
                        ErunakStonespeaker = creature->GetGUID();
                        break;
                    case BOSS_MINDBENDER_GHURSHA:
                        MindbenderGhursha = creature->GetGUID();
                        break;
                    case BOSS_OZUMAT:
                        Ozumat = creature->GetGUID();
                        break;
                    case BOSS_NEPTULON:
                        Neptulon = creature->GetGUID();
                        break;
                    case NPC_CINEMATIC_TRIGGER:
                        cinematicTrigger = creature->GetGUID();
                        break;
                    case NPC_NAZGRIM:
                    {
                        if (ServerAllowsTwoSideGroups())
                            creature->setFaction(35);

                        if (TeamInInstance == ALLIANCE)
                            creature->UpdateEntry(NPC_TAYLOR, ALLIANCE);
                        break;
                    }
                }
            }

            void OnGameObjectCreate(GameObject* go)
            {
                switch(go->GetEntry())
                {
                    case GO_LADY_NAZJAR_DOOR:
                        LadyNazjarDoor = go->GetGUID();
                        break;
                    case GO_COMMANDER_ULTHOK_DOOR:
                        CommanderUlthokDoor = go->GetGUID();
                        break;
                    case GO_ERUNAK_STONESPEAKER_DOOR:
                        ErunakStonespeakerDoor = go->GetGUID();
                        break;
                    case GO_OZUMAT_DOOR:
                        OzumatDoor = go->GetGUID();
                        break;
                    case GO_TENTACLE_LEFT:
                        leftTentacle = go->GetGUID();
                        break;
                    case GO_TENTACLE_RIGHT:
                        rightTentacle = go->GetGUID();
                        break;
                }
            }

            void SetData(uint32 type, uint32 data)
            {
                switch(type)
                {
                    case DATA_LADY_NAZJAR_EVENT:
                        encounter[0] = data;
                        if (data == IN_PROGRESS)
                            HandleGameObject(LadyNazjarDoor, false);
                        if (data == FAIL)
                            HandleGameObject(LadyNazjarDoor, true);
                        break;
                    case DATA_COMMANDER_ULTHOK_EVENT:
                        encounter[1] = data;
                        if (data == IN_PROGRESS)
                            HandleGameObject(CommanderUlthokDoor, false);
                        if (data == DONE)
                        {
                            HandleGameObject(CommanderUlthokDoor, true);
                            HandleGameObject(ErunakStonespeakerDoor, true);
                            HandleGameObject(OzumatDoor, true);
                        }
                        if (data == FAIL)
                            HandleGameObject(CommanderUlthokDoor, true);
                        break;
                    case DATA_ERUNAK_STONESPEAKER_EVENT:
                        encounter[2] = data;
                        if (data == IN_PROGRESS)
                            HandleGameObject(ErunakStonespeakerDoor, false);
                        if (data == NOT_STARTED || data == DONE || data == FAIL)
                            HandleGameObject(ErunakStonespeakerDoor, true);
                        break;
                    case DATA_OZUMAT_EVENT:
                        encounter[3] = data;
                        if (data == IN_PROGRESS)
                            HandleGameObject(OzumatDoor, false);
                        if (data == NOT_STARTED || data == DONE || data == FAIL)
                            HandleGameObject(OzumatDoor, true);
                        break;
                }

                if (data == DONE)
                    SaveToDB();
            }

            uint32 GetData(uint32 type) const
            {
                switch(type)
                {
                    case DATA_LADY_NAZJAR_EVENT: return encounter[0];
                    case DATA_COMMANDER_ULTHOK_EVENT: return encounter[1];
                    case DATA_ERUNAK_STONESPEAKER_EVENT: return encounter[2];
                    case DATA_OZUMAT_EVENT: return encounter[3];
                }
                return 0;
            }

            uint64 GetData64(uint32 identifier) const
            {
                switch(identifier)
                {
                    case DATA_LADY_NAZJAR: return LadyNazjar;
                    case DATA_COMMANDER_ULTHOK: return CommanderUlthok;
                    case DATA_ERUNAK_STONESPEAKER: return ErunakStonespeaker;
                    case BOSS_MINDBENDER_GHURSHA: return MindbenderGhursha;
                    case DATA_OZUMAT: return Ozumat;
                    case DATA_NEPTULON: return Neptulon;
                }
                return 0;
            }

            std::string GetSaveData()
            {
                OUT_SAVE_INST_DATA;

                std::ostringstream saveStream;
                saveStream << "T o t T " << encounter[0] << " " << encounter[1] << " "
                    << encounter[2] << " " << encounter[3];

                str_data = saveStream.str();

                OUT_SAVE_INST_DATA_COMPLETE;
                return str_data;
            }

            void Load(const char* in)
            {
                if (!in)
                {
                    OUT_LOAD_INST_DATA_FAIL;
                    return;
                }

                OUT_LOAD_INST_DATA(in);

                char dataHead1, dataHead2, dataHead3, dataHead4;
                uint16 data0, data1, data2, data3;

                std::istringstream loadStream(in);
                loadStream >> dataHead1 >> dataHead2 >> dataHead3 >> dataHead4 >> data0 >> data1 >> data2 >> data3;

                if (dataHead1 == 'T' && dataHead2 == 'o' && dataHead3 == 't' && dataHead4 == 'T')
                {
                    encounter[0] = data0;
                    encounter[1] = data1;
                    encounter[2] = data2;
                    encounter[3] = data3;

                    for (uint8 i = 0; i < MAX_ENCOUNTER; ++i)
                        if (encounter[i] == IN_PROGRESS)
                            encounter[i] = NOT_STARTED;

                    if(encounter[0] == DONE && encounter[1] == NOT_STARTED)
                    {
                        if(Creature* summoner = instance->GetCreature(Neptulon))
                            summoner->SummonCreature(BOSS_COMMANDER_ULTHOK, 59.185f, 802.251f, 805.730f, 0, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 60480000);
                    }
                } else OUT_LOAD_INST_DATA_FAIL;

                OUT_LOAD_INST_DATA_COMPLETE;
            }
        };
};

void AddSC_instance_throne_of_the_tides()
{
    new instance_throne_of_the_tides();
}