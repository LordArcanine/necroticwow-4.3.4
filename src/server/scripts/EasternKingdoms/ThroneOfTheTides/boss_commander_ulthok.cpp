 /*
 * Copyright (C) 2013 Skymist Project
 *
 * This file is NOT free software. You may NOT copy, redistribute it or modify it.
 */

#include "ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Weather.h"

#include "throne_of_the_tides.h"

enum Yells
{
    SAY_KILL                  = 0,
    SAY_KILL_WHISPER          = 1,
    SAY_DEATH                 = 2,
    SAY_DEATH_WHISPER         = 3,
    SAY_AGGRO                 = 4,
    SAY_AGGRO_WHISPER         = 5
};

enum Spells
{
    // Intro
    SPELL_ULTHOK_FLY_STATE    = 76017,
    SPELL_ULTHOK_SMASH        = 76018,
    SPELL_ULTHOK_BREAK_CORAL  = 82960,
    // Boss
    SPELL_SQUEEZE             = 76026,
    SPELL_ENRAGE              = 76100,
    SPELL_CURSE_OF_FATIGUE    = 76094,
    SPELL_ENTER_VEHICLE       = 46598,
    SPELL_DARK_FISSURE_SUMMON = 76047,

    SPELL_BERSERK             = 47008,

    // Dark Fissure
    SPELL_DARK_FISSURE_AURA   = 76066,
    SPELL_DARK_FISSURE_HEROIC = 91371
};

enum Events
{
    EVENT_ENRAGE              = 1,
    EVENT_CURSE_OF_FATIGUE,
    EVENT_SQUEEZE,
    EVENT_DARK_FISSURE,
    EVENT_BERSERK,

    EVENT_SET_FLIGHT,
    EVENT_FALL,
    EVENT_SMASH_CORAL,
    EVENT_INTRO_OVER,

    EVENT_FISSURE_DAMAGE
};

class boss_commander_ulthok : public CreatureScript
{
public:
    boss_commander_ulthok() : CreatureScript("boss_commander_ulthok") { }

    struct boss_commander_ulthokAI : public ScriptedAI
    {
        boss_commander_ulthokAI(Creature* creature) : ScriptedAI(creature), summons(me)
        {
            instance = creature->GetInstanceScript();
            introDone = false;
        }

        InstanceScript* instance;
        SummonList summons;
        EventMap events;
        bool introDone;

        void Reset()
        {
			events.Reset();
			summons.DespawnAll();

            if (instance)
                instance->SetData(DATA_COMMANDER_ULTHOK_EVENT, NOT_STARTED);
        }

        void EnterEvadeMode()
        {
            Reset();
			me->RemoveAllAuras();
			me->GetMotionMaster()->MoveTargetedHome();

            if (instance)
            {
                instance->SetData(DATA_COMMANDER_ULTHOK_EVENT, FAIL);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
            }
        }

        void DoAction(const int32 action)
        {
            switch(action)
            {
                case ACTION_ULTHOK_INTRO:
                    events.ScheduleEvent(EVENT_SET_FLIGHT, 10);
                    events.ScheduleEvent(EVENT_FALL, 39500);
                    events.ScheduleEvent(EVENT_SMASH_CORAL, 40500);
                    events.ScheduleEvent(EVENT_INTRO_OVER, 41000);
                    break;
            }
        }

        void EnterCombat(Unit* /*who*/)
        {
            Talk(SAY_AGGRO);

            if (instance)
            {
                instance->SetData(DATA_COMMANDER_ULTHOK_EVENT, IN_PROGRESS);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me); // Add
            }

            uint8 numb = 0;
            Map::PlayerList const& players = me->GetMap()->GetPlayers();
            for (Map::PlayerList::const_iterator itr = players.begin(); itr != players.end(); ++itr)
                if (Player* player = itr->getSource())
				{
                    Talk(SAY_AGGRO_WHISPER, player->GetGUID());
                    ++numb;
				}

            events.ScheduleEvent(EVENT_ENRAGE, urand(12000, 18000));
            events.ScheduleEvent(EVENT_CURSE_OF_FATIGUE, 32000);
            if (numb > 1) // Can't squeeze solo player, causes boss to evade.
                events.ScheduleEvent(EVENT_SQUEEZE, 25500); // 6 sec.
            events.ScheduleEvent(EVENT_DARK_FISSURE, 22500);
            events.ScheduleEvent(EVENT_BERSERK, 10 * MINUTE * IN_MILLISECONDS);
        }

        void KilledUnit(Unit* /*victim*/)
        {
			Talk(SAY_KILL);

            Map::PlayerList const& players = me->GetMap()->GetPlayers();
            for (Map::PlayerList::const_iterator itr = players.begin(); itr != players.end(); ++itr)
                if (Player* player = itr->getSource())
                    Talk(SAY_KILL_WHISPER, player->GetGUID());
        }

        void JustDied(Unit* /*killer*/)
        {
            Talk(SAY_DEATH);
			summons.DespawnAll();

            Map::PlayerList const& players = me->GetMap()->GetPlayers();
            for (Map::PlayerList::const_iterator itr = players.begin(); itr != players.end(); ++itr)
                if (Player* player = itr->getSource())
                    Talk(SAY_DEATH_WHISPER, player->GetGUID());

            if (instance)
            {
                instance->SetBossState(DATA_COMMANDER_ULTHOK_EVENT, DONE);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove

                if (GameObject* door = me->FindNearestGameObject(GO_COMMANDER_ULTHOK_DOOR, 100.0f))
                    instance->HandleGameObject(0, true, door);
            }
        }

        void JustSummoned(Creature* summon)
        {
            summons.Summon(summon);
			summon->setActive(true);

			if (me->isInCombat())
			    summon->AI()->DoZoneInCombat();
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() && introDone || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_SET_FLIGHT:
                        me->SetDisableGravity(true);
                        me->SendMovementFlagUpdate();
                        me->SetReactState(REACT_PASSIVE);
                        me->SetByteFlag(UNIT_FIELD_BYTES_1, 3, 0x02);
                        me->AddUnitMovementFlag(MOVEMENTFLAG_CAN_FLY | MOVEMENTFLAG_FLYING);
                        DoCast(me, SPELL_ULTHOK_FLY_STATE);
                        break;

                    case EVENT_FALL:
                        me->SetDisableGravity(false);
                        me->SendMovementFlagUpdate();
                        me->RemoveByteFlag(UNIT_FIELD_BYTES_1, 3, 0x02);
                        me->RemoveUnitMovementFlag(MOVEMENTFLAG_CAN_FLY | MOVEMENTFLAG_FLYING);
                        me->RemoveAurasDueToSpell(SPELL_ULTHOK_FLY_STATE);
                        break;

                    case EVENT_SMASH_CORAL:
                        DoCast(me, SPELL_ULTHOK_SMASH);
                        DoCast(me, SPELL_ULTHOK_BREAK_CORAL);
                        me->SetReactState(REACT_AGGRESSIVE);
                        if (GameObject* Corals = me->FindNearestGameObject(GO_CORALES, 200.f))
                            instance->HandleGameObject(Corals->GetGUID(), true);
                        break;

                    case EVENT_INTRO_OVER:
                        {
                            if (GameObject* Corals = me->FindNearestGameObject(GO_CORALES, 200.f))
                                Corals->SetPhaseMask(2, true);
                            float x, y, z;
                            me->GetClosePoint(x, y, z, me->GetObjectSize() / 3, 5.0f);
                            me->GetMotionMaster()->MovePoint(1, x, y, z);
                            introDone = true;
                        }
                        break;

                    case EVENT_ENRAGE:
                        DoCast(me, SPELL_ENRAGE);
                        events.ScheduleEvent(EVENT_ENRAGE, urand(12000, 15000));
                        break;

                    case EVENT_CURSE_OF_FATIGUE:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                            DoCast(target, SPELL_CURSE_OF_FATIGUE, true);
                        events.ScheduleEvent(EVENT_CURSE_OF_FATIGUE, 22500);
                        break;

                    case EVENT_SQUEEZE:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                            DoCast(target, SPELL_SQUEEZE);
                        events.ScheduleEvent(EVENT_SQUEEZE, 22500);
                        break;

                    case EVENT_DARK_FISSURE:
                        DoCastVictim(SPELL_DARK_FISSURE_SUMMON);
                        events.ScheduleEvent(EVENT_DARK_FISSURE, 22500);
                        break;

                    case EVENT_BERSERK:
                        DoCast(me, SPELL_BERSERK);
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_commander_ulthokAI (creature);
    }
};

/***********************
** Dark Fissure trigger
************************/

class npc_fissure : public CreatureScript
{
    public:
        npc_fissure() : CreatureScript("npc_fissure") { }

        struct npc_fissureAI : public ScriptedAI
        {
			npc_fissureAI(Creature* creature) : ScriptedAI(creature)
            {
                instance = creature->GetInstanceScript();
				creature->AddAura(IsHeroic()? SPELL_DARK_FISSURE_AURA : SPELL_DARK_FISSURE_HEROIC, creature);
                creature->SetReactState(REACT_PASSIVE);
                creature->DespawnOrUnsummon(60000);
            }

            InstanceScript* instance;
            void EnterCombat(Unit* /*who*/) {}
			void UpdateAI(const uint32 diff) {}
		};

    CreatureAI* GetAI(Creature* creature) const
	{
        return new npc_fissureAI(creature);
    }
};

/****************************
** Spell Fissure Range Ulthok
*****************************/

class ExactDistanceCheck
{
    public:
        ExactDistanceCheck(WorldObject* source, float dist) : _source(source), _dist(dist) {}

        bool operator()(WorldObject* unit)
        {
            return _source->GetExactDist2d(unit) > _dist;
        }

    private:
        WorldObject* _source;
        float _dist;
};

class spell_ulthok_fissure : public SpellScriptLoader // 91375 (Heroic)
{
    public:
        spell_ulthok_fissure() : SpellScriptLoader("spell_ulthok_fissure") { }

        class spell_ulthok_fissure_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_ulthok_fissure_SpellScript);

            void TargetSelect(std::list<WorldObject*>& targets)
            {
                targets.remove_if(ExactDistanceCheck(GetCaster(), 10.0f * GetCaster()->GetFloatValue(OBJECT_FIELD_SCALE_X)));
            }

            void ChangeDamageAndGrow()
            {
                SetHitDamage(int32(GetHitDamage() * GetCaster()->GetFloatValue(OBJECT_FIELD_SCALE_X)));
            }

            void Register()
            {
				OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_ulthok_fissure_SpellScript::TargetSelect, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
                OnHit += SpellHitFn(spell_ulthok_fissure_SpellScript::ChangeDamageAndGrow);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_ulthok_fissure_SpellScript();
        }
};

class spell_ulthok_squeeze : public SpellScriptLoader // 76026
{
public:
    spell_ulthok_squeeze() : SpellScriptLoader("spell_ulthok_squeeze") { }

    class spell_ulthok_squeeze_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_ulthok_squeeze_AuraScript);

        void HandleEffectApply(AuraEffect const * /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            if (Unit* target = GetTarget())
                if (Unit* caster = GetCaster())
                    target->CastSpell(caster, SPELL_ENTER_VEHICLE, true);
        }

        void HandleEffectRemove(AuraEffect const * /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            if (Unit* target = GetTarget())
                if (Unit* caster = GetCaster())
                {
                    caster->RemoveAurasDueToSpell(SPELL_ENTER_VEHICLE);
                    target->RemoveAurasDueToSpell(SPELL_ENTER_VEHICLE);
                }
        }

        void Register()
        {
            OnEffectApply += AuraEffectApplyFn(spell_ulthok_squeeze_AuraScript::HandleEffectApply, EFFECT_0, SPELL_AURA_MOD_STUN, AURA_EFFECT_HANDLE_REAL);
            OnEffectRemove += AuraEffectRemoveFn(spell_ulthok_squeeze_AuraScript::HandleEffectRemove, EFFECT_0, SPELL_AURA_MOD_STUN, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const
    {
        return new spell_ulthok_squeeze_AuraScript();
    }
};

void AddSC_boss_commander_ulthok()
{
    new boss_commander_ulthok();
	new npc_fissure();
    new spell_ulthok_fissure();
    new spell_ulthok_squeeze();
}