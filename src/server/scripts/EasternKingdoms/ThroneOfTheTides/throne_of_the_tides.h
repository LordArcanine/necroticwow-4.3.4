 /*
 * Copyright (C) 2013 Skymist Project
 *
 * This file is NOT free software. You may NOT copy, redistribute it or modify it.
 */

#ifndef DEF_THRONEOFTHETIDES_H
#define DEF_THRONEOFTHETIDES_H

enum Defines
{
    ACTION_OZUMAT_PREPARE_EVENT = 1,
    ACTION_ULTHOK_INTRO        = 1,

    NPC_NAZGRIM                = 50272,
    NPC_TAYLOR                 = 50270,

    // Naz'jar First Sight Event
    NPC_NAZJAR_INVADER         = 40584,
    NPC_NAZJAR_SPIRITMENDER    = 41096,
    NPC_DEEP_MURLOC_DRUDGE     = 39960,
    NPC_TEMPEST_WITCH          = 40634,

    // Lady Naz'jar
    BOSS_LADY_NAZJAR           = 40586,
    NPC_SUMMONED_WITCH         = 44404, // 2 of them
    NPC_SUMMONED_GUARD         = 40633,
    NPC_SUMMONED_WATERSPOUT    = 48571,
    NPC_SUMMONED_WATERSPOUT_HC = 49108,
    NPC_SUMMONED_GEYSER        = 40597,

    // Commander Ulthok
    BOSS_COMMANDER_ULTHOK      = 40765,
    NPC_DARK_FISSURE           = 40784,

    // Erunak Stonespeaker & Mindbender Ghur'sha
    BOSS_ERUNAK_STONESPEAKER   = 40823,
    BOSS_MINDBENDER_GHURSHA    = 40788,
    NPC_EARTH_SHARD            = 45469,
    NPC_MIND_FOG               = 40861,

    // Ozumat
    BOSS_OZUMAT                = 44566,
    BOSS_NEPTULON              = 40792,
    NPC_DEEP_MURLOC            = 44658,
    NPC_MINDLASHER             = 44715,
    NPC_BEHEMOTH               = 44648,
    NPC_SAPPER                 = 44752,
    NPC_BEAST                  = 44841,
    NPC_BLIGHT_OF_OZUMAT       = 44801,
    NPC_OZUMAT_VISUAL_TRIGGER  = 44809,

    NPC_CINEMATIC_TRIGGER      = 44864,
    NPC_LIGHTNING_STALKER      = 40163,

    NPC_OZUMAT_INTRO           = 40655,

    // Game Objects
    GO_COMMANDER_ULTHOK_DOOR   = 204338,
    GO_CORALES                 = 205542,
    GO_LADY_NAZJAR_DOOR        = 204339,
    GO_ERUNAK_STONESPEAKER_DOOR = 204340,
    GO_OZUMAT_DOOR             = 204341,
    GO_OZUMAT_CHEST_NORMAL     = 205216,
    GO_OZUMAT_CHEST_HEROIC     = 207973,
    GO_TENTACLE_RIGHT          = 208302,
    GO_TENTACLE_LEFT           = 208301,
    GO_CONTROL_SYSTEM          = 203199,
    GO_GHOST_GATE_1            = 207997,
    GO_GHOST_GATE_2            = 207998,
    GO_JELLYFISH_ELEVATOR      = 207209,

    SPELL_CINEMATIC_SHOCK      = 85170, // Cinematic lightning visuals.
    SPELL_CHAINS_VISUAL        = 75843, // Naz'jar intro event.
    SPELL_TENTACLE_KNOCKBACK   = 84566,
    SPELL_CINEMATIC_SHAKE      = 80134
};

enum Data
{
    DATA_LADY_NAZJAR_EVENT         = 0,
    DATA_COMMANDER_ULTHOK_EVENT    = 1,
    DATA_ERUNAK_STONESPEAKER_EVENT = 2,
    DATA_OZUMAT_EVENT              = 3
};

enum Data64
{
    DATA_LADY_NAZJAR,
    DATA_COMMANDER_ULTHOK,
    DATA_MINDBENDER_GHURSHA,
    DATA_ERUNAK_STONESPEAKER,
    DATA_OZUMAT,
    DATA_NEPTULON,
};

#define MAX_ENCOUNTER 4

#endif