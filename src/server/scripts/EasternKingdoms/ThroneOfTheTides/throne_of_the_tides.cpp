 /*
 * Copyright (C) 2013 Skymist Project
 *
 * This file is NOT free software. You may NOT copy, redistribute it or modify it.
 */

#include "ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedEscortAI.h"
#include "SpellScript.h"
#include "MoveSplineInit.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"

#include "throne_of_the_tides.h"

Position const EventPositions[] =
{
{-139.756f, 802.663f, 796.641f, 3.136672f},          // Start Point
{-132.084763f, 798.554138f, 796.976257f, 3.155597f},  // Invader Spawnpoints
{-132.03f, 806.99f, 797.f, 3.06f},
{-103.577f, 806.394f, 796.965f, 3.06048f},
{-72.4102f, 798.265f, 796.97f, 3.12723f},             // Spiritmender Spawnpoints
{-72.2161f, 806.563f, 796.966f, 3.09581f},
{-104.031f, 798.42f, 796.957f, 3.14216f},
{-45.626f, 802.385f, 797.117f, 3.1178f},              // Murloc Spawnposition
{23.623f, 802.433f, 806.317f, 6.254f},                // Naz'jar despawn Point
{32.1444f, 802.431f, 806.317f, 3.11295f},             // 2. Event Part Tempest Witch
{33.6948f, 805.856f, 806.317f, 3.06583f},             // Invader
{33.4031f, 799.003f, 806.317f, 3.12473f},
{35.352394f, 809.527f, 806.317f, 3.05f},              // Spiritmender
{34.6015f, 795.f, 806.317f, 3.081633f}
};

enum EventNazjarYells
{
    SAY_ARMIES = 0,
    SAY_GNATS  = 1
};

class mob_lady_nazjar_event : public CreatureScript // 39959
{
public:
    mob_lady_nazjar_event() : CreatureScript("mob_lady_nazjar_event") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new mob_lady_nazjar_eventAI (creature);
    }

    struct mob_lady_nazjar_eventAI : public ScriptedAI
    {
        mob_lady_nazjar_eventAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;

        bool eventProgress;
        uint8 nextStep;
        uint32 timer;
        uint8 murlocGroups;

        void Reset()
        {
            if (instance->GetData(DATA_LADY_NAZJAR) == DONE)
                me->DespawnOrUnsummon(1);

            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
            me->SetReactState(REACT_PASSIVE);
            ChainMurlocs();
            eventProgress = false;
            nextStep = 0;
            timer = 1000;
            murlocGroups = 2;
        }

        void ChainMurlocs()
        {
            std::list<Creature*> templist;
            float x, y, z;
            me->GetPosition(x, y, z);

            {
                CellCoord pair(Trinity::ComputeCellCoord(x, y));
                Cell cell(pair);
                cell.SetNoCreate();

                Trinity::AllCreaturesOfEntryInRange check(me, NPC_DEEP_MURLOC_DRUDGE, 30);
                Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange> searcher(me, templist, check);
                TypeContainerVisitor<Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange>, GridTypeMapContainer> cSearcher(searcher);
                cell.Visit(pair, cSearcher, *me->GetMap(), *me, me->GetGridActivationRange());
            }

            if (templist.empty())
                return;

            for (std::list<Creature*>::const_iterator i = templist.begin(); i != templist.end(); ++i)
            {
                Movement::MoveSplineInit init(*(*i));
                init.SetOrientationFixed(true);
                init.Launch();
                (*i)->CastSpell(me, SPELL_CHAINS_VISUAL, true);
            }
        }

        void UnchainMurlocs()
        {
            std::list<Creature*> templist;
            float x, y, z;
            me->GetPosition(x, y, z);

            {
                CellCoord pair(Trinity::ComputeCellCoord(x, y));
                Cell cell(pair);
                cell.SetNoCreate();

                Trinity::AllCreaturesOfEntryInRange check(me, NPC_DEEP_MURLOC_DRUDGE, 30);
                Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange> searcher(me, templist, check);
                TypeContainerVisitor<Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange>, GridTypeMapContainer> cSearcher(searcher);
                cell.Visit(pair, cSearcher, *me->GetMap(), *me, me->GetGridActivationRange());
            }

            if (templist.empty())
                return;

            templist.resize(5);

            for (std::list<Creature*>::const_iterator i = templist.begin(); i != templist.end(); ++i)
            {
                Movement::MoveSplineInit init(*(*i));
                init.SetOrientationFixed(false);
                init.Launch();
                (*i)->RemoveAurasDueToSpell(SPELL_CHAINS_VISUAL);
                (*i)->GetMotionMaster()->MovePoint(0, EventPositions[0]);
            }

            murlocGroups--;
        }

        void UpdateAI(const uint32 diff)
        {
            if (!instance)
                return;

            if (instance->GetData(DATA_LADY_NAZJAR) == DONE)
            {
                me->DisappearAndDie();
                return;
            }

            if (!eventProgress)
            {
                if (timer <= diff)
                {
                    if (Player* target = me->SelectNearestPlayer(130.0f))
                    {
                        if (target->GetDistance(EventPositions[0]) < 15.f)
                        {
                            if (GameObject* door = me->FindNearestGameObject(GO_COMMANDER_ULTHOK_DOOR, 30.0f))
                                instance->HandleGameObject(0, false, door);

                            Talk(SAY_ARMIES);

                            eventProgress = true;

                            for (uint8 i = 1; i <= 3; i++)
                                me->SummonCreature(NPC_NAZJAR_INVADER, EventPositions[i], TEMPSUMMON_MANUAL_DESPAWN);

                            for (uint8 i = 4; i <= 6; i++)
                                me->SummonCreature(NPC_NAZJAR_SPIRITMENDER, EventPositions[i], TEMPSUMMON_MANUAL_DESPAWN);

                            nextStep = 4;
                        }
                    }

                    timer = 1000;

                } else timer -= diff;

            }else
            {
                if (timer <= diff)
                {
                    if (Player* target = me->SelectNearestPlayer(130.0f))
                        if (target->GetDistance(me) <= 130)
                            if (murlocGroups >= 1)
                                UnchainMurlocs();

                    timer = 20000;

                } else timer -= diff;
            }
        }

        void SummonedCreatureDies(Creature* summon, Unit* /*killer*/)
        {
            if (summon->GetEntry() != NPC_DEEP_MURLOC_DRUDGE)
                nextStep--;

            if (nextStep == 0)
            {
                me->RemoveUnitMovementFlag(MOVEMENTFLAG_WALKING);

                if (GameObject* door = me->FindNearestGameObject(GO_COMMANDER_ULTHOK_DOOR, 30.0f))
                    instance->HandleGameObject(0, true, door);

                Talk(SAY_GNATS);
                me->GetMotionMaster()->MovePoint(0, EventPositions[8]);
            }
        }

        void MovementInform(uint32 type, uint32 id)
        {
            if (type != POINT_MOTION_TYPE || id != 0)
                return;

            me->SummonCreature(NPC_TEMPEST_WITCH, EventPositions[9], TEMPSUMMON_CORPSE_TIMED_DESPAWN, 90000);

            for(uint8 i = 10; i <= 11; i++)
                me->SummonCreature(NPC_NAZJAR_INVADER, EventPositions[i], TEMPSUMMON_CORPSE_TIMED_DESPAWN, 90000);

            for(uint8 i = 12; i <= 13; i++)
                me->SummonCreature(NPC_NAZJAR_SPIRITMENDER, EventPositions[i], TEMPSUMMON_CORPSE_TIMED_DESPAWN, 90000);

            if (GameObject* NazJarDoor = me->FindNearestGameObject(GO_LADY_NAZJAR_DOOR, 200.0f))
                instance->HandleGameObject(0, true, NazJarDoor);

            me->DisappearAndDie();
        }
    };
};

enum triggerEvents
{
    EVENT_SEARCH_PLAYER_TENTACLE = 1,
    EVENT_LIGHTNING_MOBS,
    EVENT_TENTACLE_1,
    EVENT_TENTACLE_2,
    EVENT_TENTACLE_1_PHASE,
    EVENT_TENTACLE_2_PHASE,
    EVENT_OZUMAT_LEAVE
};

Creature* cinematicTrigger;

class npc_cinematic_trigger : public CreatureScript // 44864
{
public:
    npc_cinematic_trigger() : CreatureScript("npc_cinematic_trigger") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_cinematic_triggerAI(creature);
    }

    struct npc_cinematic_triggerAI : public ScriptedAI
    {
        npc_cinematic_triggerAI(Creature* creature) : ScriptedAI(creature) 
        {
            instance = creature->GetInstanceScript();
            cinematicTrigger = me;
        }

        InstanceScript* instance;
        EventMap events;

        void Reset()
        {
            events.Reset();
            events.ScheduleEvent(EVENT_SEARCH_PLAYER_TENTACLE, 1000);
        }

        void DoAction(const int32 action)
        {
            switch(action)
            {
                case ACTION_OZUMAT_PREPARE_EVENT:
                    events.ScheduleEvent(EVENT_LIGHTNING_MOBS, 100);
                    events.ScheduleEvent(EVENT_TENTACLE_1, 16000);
                    events.ScheduleEvent(EVENT_TENTACLE_1_PHASE, 16500);
                    events.ScheduleEvent(EVENT_TENTACLE_2, 21000);
                    events.ScheduleEvent(EVENT_TENTACLE_2_PHASE, 21500);
                    events.ScheduleEvent(EVENT_OZUMAT_LEAVE, 30000);
                    break;
            }
        }

        void LightningMobsCastVisual()
        {
            std::list<Creature*> templist;
            float x, y, z;
            me->GetPosition(x, y, z);

            {
                CellCoord pair(Trinity::ComputeCellCoord(x, y));
                Cell cell(pair);
                cell.SetNoCreate();

                Trinity::AllCreaturesOfEntryInRange check(me, NPC_LIGHTNING_STALKER, 500);
                Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange> searcher(me, templist, check);
                TypeContainerVisitor<Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange>, GridTypeMapContainer> cSearcher(searcher);
                cell.Visit(pair, cSearcher, *me->GetMap(), *me, me->GetGridActivationRange());
            }

            if (templist.empty())
                return;

            for (std::list<Creature*>::const_iterator i = templist.begin(); i != templist.end(); ++i)
                (*i)->CastSpell((*i), SPELL_CINEMATIC_SHOCK, true);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!instance)
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_SEARCH_PLAYER_TENTACLE:
						{
                        Map::PlayerList const &PlayerList = me->GetMap()->GetPlayers();
                        if (!PlayerList.isEmpty())
                            for (Map::PlayerList::const_iterator i = PlayerList.begin(); i != PlayerList.end(); ++i)
                                if (i->getSource()->FindNearestGameObject(GO_GHOST_GATE_1, 2.0f) || i->getSource()->FindNearestGameObject(GO_GHOST_GATE_2, 2.0f))
                                i->getSource()->CastSpell(i->getSource(), SPELL_TENTACLE_KNOCKBACK, true);
                        events.ScheduleEvent(EVENT_SEARCH_PLAYER_TENTACLE, 1000);
						}
                        break;

                    case EVENT_LIGHTNING_MOBS:
                        LightningMobsCastVisual();
                        break;

                    case EVENT_TENTACLE_1:
                        if (GameObject* leftTentacle = me->FindNearestGameObject(GO_TENTACLE_LEFT, 300.0f))
                            leftTentacle->Delete();
                        break;

                    case EVENT_TENTACLE_1_PHASE:
                        // if (GameObject* leftTentacle = me->FindNearestGameObject(GO_TENTACLE_LEFT, 300.0f))
                        //     leftTentacle->SetPhaseMask(2, true);
                        if (GameObject* GhostGate1 = me->FindNearestGameObject(GO_GHOST_GATE_1, 200.0f))
                            GhostGate1->SetPhaseMask(2, true);
                        break;

                    case EVENT_TENTACLE_2:
                        if (GameObject* rightTentacle = me->FindNearestGameObject(GO_TENTACLE_RIGHT, 300.0f))
                            rightTentacle->Delete();
                        break;

                    case EVENT_TENTACLE_2_PHASE:
                        // if (GameObject* rightTentacle = me->FindNearestGameObject(GO_TENTACLE_RIGHT, 300.0f))
                        //     rightTentacle->SetPhaseMask(2, true);
                        if (GameObject* GhostGate2 = me->FindNearestGameObject(GO_GHOST_GATE_2, 200.0f))
                            GhostGate2->SetPhaseMask(2, true);
                        break;

                    case EVENT_OZUMAT_LEAVE:
						if (Creature* Ozumat = me->FindNearestCreature(NPC_OZUMAT_INTRO, 300.0f))
                        {
                            Ozumat->GetMotionMaster()->MovePoint(0, Ozumat->GetPositionX() + 60, Ozumat->GetPositionY(), Ozumat->GetPositionZ() + 20);
                            Ozumat->DespawnOrUnsummon(8000);
                        }
                        break;
				}
            }
        }
    };
};

class go_totd_defense_system : public GameObjectScript // 203199
{
public:
    go_totd_defense_system() : GameObjectScript("go_totd_defense_system") { }

    bool OnGossipHello(Player* /*player*/, GameObject* go)
    {
        if (InstanceScript* instance = go->GetInstanceScript())
        {
            go->UseDoorOrButton();
            go->SetFlag(GAMEOBJECT_FLAGS, GO_FLAG_NOT_SELECTABLE);

            Map::PlayerList const &PlayerList = go->GetMap()->GetPlayers();
            if (!PlayerList.isEmpty())
                for (Map::PlayerList::const_iterator i = PlayerList.begin(); i != PlayerList.end(); ++i)
                {
                    i->getSource()->SendCinematicStart(169);
                    i->getSource()->CastSpell(i->getSource(), SPELL_CINEMATIC_SHAKE, true);
                }

            if (GameObject* Door = go->FindNearestGameObject(GO_LADY_NAZJAR_DOOR, 20.f))
                instance->HandleGameObject(0, true, Door);

            if (Creature* ulthok = go->SummonCreature(BOSS_COMMANDER_ULTHOK, 55.493f, 802.509f, 823.377f, 6.21f, TEMPSUMMON_MANUAL_DESPAWN))
                ulthok->AI()->DoAction(ACTION_ULTHOK_INTRO);

            if (cinematicTrigger)
                cinematicTrigger->AI()->DoAction(ACTION_OZUMAT_PREPARE_EVENT);
        }
        return false;
    }
};

void AddSC_throne_of_the_tides()
{
    new mob_lady_nazjar_event();
    new npc_cinematic_trigger();
    new go_totd_defense_system();
}