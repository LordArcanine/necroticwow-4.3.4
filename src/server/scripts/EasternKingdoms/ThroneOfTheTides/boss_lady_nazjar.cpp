 /*
 * Copyright (C) 2013 Skymist Project
 *
 * This file is NOT free software. You may NOT copy, redistribute it or modify it.
 */

#include "ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Weather.h"

#include "throne_of_the_tides.h"

enum Yells
{
    SAY_AGGRO               = 0,
    SAY_66_PRECENT          = 1,
    SAY_33_PRECENT          = 2,
    SAY_KILL                = 3,
    SAY_DEATH               = 4
};

#define SPELL_WATERSPOUT_SUMMON DUNGEON_MODE(90495, 90497) // Summons tornado every 7/3 secs.

enum Spells
{
    /*** BOSS ***/
    SPELL_FUNGAL_SPORES     = 76001,
    SPELL_SHOCK_BLAST       = 76008,
    SPELL_SUMMON_GEYSER     = 75722,
    SPELL_WATERSPOUT        = 75683,

    SPELL_BERSERK           = 47008,

    /*** MOBS ***/
    // Waterspout (tornado)
    SPELL_WATERSPOUT_VISUAL = 90440,
    SPELL_WATERSPOUT_DMGAUR = 90479,
    SPELL_ENTER_VEHICLE     = 46598, // Player on tornado, lasts 5 sec.
    // Geyser
    SPELL_GEYSER_DAMAGE     = 75700,
    SPELL_GEYSER_AURA       = 75699
};

enum Events
{
    // Boss
    EVENT_SHOCK_BLAST       = 1,
    EVENT_FUNGAL_SPORES,
    EVENT_GEYSER,
    EVENT_END_PHASE_2,
    EVENT_BERSERK,
    // Waterspout
    EVENT_SEARCH_PLAYER,
    EVENT_RELEASE_PLAYER,
    // Geyser
    EVENT_BURST
};

enum Phases
{
    PHASE_ALL        = 0,
    PHASE_NORMAL     = 1,
    PHASE_WATERSPOUT = 2
};

enum Creatures
{
    NPC_GEYSER       = 40597,
    NPC_TORNADO      = 48571
};

Position const addPositions[] =
{
    {155.094f, 802.594f, 808.331f, 6.227f},	// Summons spawned at entrance.
};

class boss_lady_nazjar : public CreatureScript
{
public:
    boss_lady_nazjar() : CreatureScript("boss_lady_nazjar") { }

    struct boss_lady_nazjarAI : public ScriptedAI
    {
        boss_lady_nazjarAI(Creature* creature) : ScriptedAI(creature), summons(me)
        {
            creature->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            creature->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
            creature->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, false);
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;
        SummonList summons;
        EventMap events;

        bool Phased;
        uint8 Phase;
        uint8 SpawnCount;
        uint8 PhaseCount;

        void Reset()
        {
			events.Reset();
			summons.DespawnAll();

            Phased = false;
            Phase = PHASE_NORMAL;

            SpawnCount = 3;
            PhaseCount = 0;

            if (GameObject* System = me->FindNearestGameObject(GO_CONTROL_SYSTEM, 200.f))
                System->SetFlag(GAMEOBJECT_FLAGS, GO_FLAG_NOT_SELECTABLE);

            if (instance)
                instance->SetData(DATA_LADY_NAZJAR_EVENT, NOT_STARTED);
        }

        void EnterEvadeMode()
        {
            Reset();
            me->SetHealth(me->GetMaxHealth());
            me->RemoveAllAuras();
            me->GetMotionMaster()->MoveTargetedHome();

            SetCombatMovement(true);
            if (me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE))
                me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);

            if (instance)
            {
                instance->SetData(DATA_LADY_NAZJAR_EVENT, FAIL);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
            }
        }

        void SummonedCreatureDies(Creature* summon, Unit* /*killer*/)
        {
            switch(summon->GetEntry())
            {
                case NPC_SUMMONED_WITCH:
                case NPC_SUMMONED_GUARD:
                    SpawnCount--;
                    break;
            }
        }

        void KilledUnit(Unit* /*victim*/)
        {
			Talk(SAY_KILL);
        }

        void JustSummoned(Creature* summon)
        {
            summons.Summon(summon);
			summon->setActive(true);

            switch (summon->GetEntry())
            {
                case NPC_SUMMONED_WITCH:
                case NPC_SUMMONED_GUARD:
                    if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 150, true))
                        summon->AI()->AttackStart(target);
                    break;
                case NPC_GEYSER:
                case NPC_TORNADO:
			        if (me->isInCombat())
			            summon->AI()->DoZoneInCombat();
                    break;
            }
        }

        void EnterCombat(Unit* /*who*/)
        {
            Talk(SAY_AGGRO);

            if (instance)
            {
                instance->SetData(DATA_LADY_NAZJAR_EVENT, IN_PROGRESS);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me); // Add
            }

            events.ScheduleEvent(EVENT_SHOCK_BLAST, 22000);
            events.ScheduleEvent(EVENT_FUNGAL_SPORES, urand(8000, 13000));
            events.ScheduleEvent(EVENT_GEYSER, urand(11000, 16000));
            events.ScheduleEvent(EVENT_BERSERK, 10 * MINUTE * IN_MILLISECONDS);
        }

        void JustDied(Unit* /*killer*/)
        {
            Talk(SAY_DEATH);
			summons.DespawnAll();

            if (instance)
            {
                instance->SetData(DATA_LADY_NAZJAR_EVENT, DONE);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
            }

            if (GameObject* System = me->FindNearestGameObject(GO_CONTROL_SYSTEM, 200.f))
                System->RemoveFlag(GAMEOBJECT_FLAGS, GO_FLAG_NOT_SELECTABLE);
        }

        void DespawnTornadoes()
        {
            std::list<Creature*> creatures;
            GetCreatureListWithEntryInGrid(creatures, me, NPC_TORNADO, 1000.0f);
       
            if (creatures.empty())
                return;
            
            for (std::list<Creature*>::iterator iter = creatures.begin(); iter != creatures.end(); ++iter)
                (*iter)->DespawnOrUnsummon();
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            if (SpawnCount == 0 && Phase == PHASE_WATERSPOUT)
            {
                me->RemoveAurasDueToSpell(SPELL_WATERSPOUT);
                me->RemoveAurasDueToSpell(SPELL_WATERSPOUT_SUMMON);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, false);
                SpawnCount = 3;
                SetCombatMovement(true);

                Phase = PHASE_NORMAL;
                Phased = false;

                DespawnTornadoes();

                events.ScheduleEvent(EVENT_SHOCK_BLAST, 22000);
                events.ScheduleEvent(EVENT_FUNGAL_SPORES, urand(8000, 13000));
                events.ScheduleEvent(EVENT_GEYSER, urand(11000, 16000));
            }

            if (me->HealthBelowPct(67) && Phase == PHASE_NORMAL && PhaseCount == 0)
            {
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, true);
                Talk(SAY_66_PRECENT);

                events.CancelEvent(EVENT_SHOCK_BLAST);
                events.CancelEvent(EVENT_FUNGAL_SPORES);
                events.CancelEvent(EVENT_GEYSER);
                events.ScheduleEvent(EVENT_END_PHASE_2, 61000);

                PhaseCount++;
                SetCombatMovement(false);
                Phase = PHASE_WATERSPOUT;
                me->NearTeleportTo(192.056f, 802.527f, 807.638f, 3.0f);
                DoCast(me, SPELL_WATERSPOUT);
                me->AddAura(SPELL_WATERSPOUT_SUMMON, me);
                for (uint8 i = 0; i < 2; i++)
                    me->SummonCreature(NPC_SUMMONED_WITCH, addPositions[0], TEMPSUMMON_CORPSE_TIMED_DESPAWN, 1000);
                me->SummonCreature(NPC_SUMMONED_GUARD, addPositions[0], TEMPSUMMON_CORPSE_DESPAWN, 1000);
            }

            if (me->HealthBelowPct(34) && Phase == PHASE_NORMAL && PhaseCount == 1)
            {
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, true);
                Talk(SAY_33_PRECENT);

                events.CancelEvent(EVENT_SHOCK_BLAST);
                events.CancelEvent(EVENT_FUNGAL_SPORES);
                events.CancelEvent(EVENT_GEYSER);
                events.ScheduleEvent(EVENT_END_PHASE_2, 61000);

                PhaseCount++;
                SetCombatMovement(false);
                Phase = PHASE_WATERSPOUT;
                me->NearTeleportTo(192.056f, 802.527f, 807.638f, 3.0f);
                DoCast(me, SPELL_WATERSPOUT);
                me->AddAura(SPELL_WATERSPOUT_SUMMON, me);
                for (uint8 i = 0; i < 2; i++)
                    me->SummonCreature(NPC_SUMMONED_WITCH, addPositions[0], TEMPSUMMON_CORPSE_DESPAWN, 1000);
                me->SummonCreature(NPC_SUMMONED_GUARD, addPositions[0], TEMPSUMMON_CORPSE_DESPAWN, 1000);
            }

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_FUNGAL_SPORES:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                            DoCast(target, SPELL_FUNGAL_SPORES);
                        events.ScheduleEvent(EVENT_FUNGAL_SPORES, urand(5000,7000));
                        break;

                    case EVENT_SHOCK_BLAST:
                        DoCastVictim(SPELL_SHOCK_BLAST);
                        events.ScheduleEvent(EVENT_SHOCK_BLAST, urand(12000,15000));
                        break;

                    case EVENT_GEYSER:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 1, 100, true))
                            DoCast(target, SPELL_SUMMON_GEYSER);
                        events.ScheduleEvent(EVENT_GEYSER, urand(13000,16000));
                        break;

                    case EVENT_END_PHASE_2:
                        me->RemoveAurasDueToSpell(SPELL_WATERSPOUT);
                        me->RemoveAurasDueToSpell(SPELL_WATERSPOUT_SUMMON);
                        me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, false);
                        SpawnCount = 3;
                        SetCombatMovement(true);
                        
                        Phase = PHASE_NORMAL;
                        Phased = false;

                        DespawnTornadoes();

                        events.ScheduleEvent(EVENT_SHOCK_BLAST, 22000);
                        events.ScheduleEvent(EVENT_FUNGAL_SPORES, urand(8000, 13000));
                        events.ScheduleEvent(EVENT_GEYSER, urand(11000, 16000));
                        break;

                    case EVENT_BERSERK:
                        DoCast(me, SPELL_BERSERK);
                        break;
                }
            }

            if (Phase == PHASE_NORMAL)
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_lady_nazjarAI (creature);
    }
};

/***********************
** WaterSpout trigger
************************/

class npc_waterspout_tornado : public CreatureScript // 48571
{
public:
    npc_waterspout_tornado() : CreatureScript("npc_waterspout_tornado") { }

    struct npc_waterspout_tornadoAI : public ScriptedAI
    {
        npc_waterspout_tornadoAI(Creature* creature) : ScriptedAI(creature)
        {
            creature->AddAura(SPELL_WATERSPOUT_VISUAL, creature);
            creature->SetWalk(false);
            creature->SetSpeed(MOVE_WALK, 0.5f);
            creature->SetSpeed(MOVE_RUN, 0.5f);
            creature->GetMotionMaster()->MovePoint(0, creature->GetPositionX() + frand(-30.0f, 30.0f), creature->GetPositionY() + frand(-30.0f, 30.0f), creature->GetPositionZ() + 3.0f);
            creature->SetReactState(REACT_PASSIVE);
            creature->DespawnOrUnsummon(15500);
        }

        EventMap events;
        Unit* playerInside;

        void EnterCombat(Unit* /*who*/)
        {
            uint8 numb = 0;
            Map::PlayerList const& players = me->GetMap()->GetPlayers();
            for (Map::PlayerList::const_iterator itr = players.begin(); itr != players.end(); ++itr)
                if (Player* player = itr->getSource())
                    ++numb;

            if (numb > 1) // Can't take solo player, causes boss to evade.
                events.ScheduleEvent(EVENT_SEARCH_PLAYER, 1500);
        }

        void UpdateAI(const uint32 diff)
        {
            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_SEARCH_PLAYER:
                        if (Player* target = me->SelectNearestPlayer(3.0f))
                        {
                            playerInside = target;
                            me->GetMotionMaster()->MovementExpired();
                            me->GetMotionMaster()->Clear();
                            me->AddAura(SPELL_WATERSPOUT_DMGAUR, target);
                            target->CastSpell(me, SPELL_ENTER_VEHICLE, true);
                            events.ScheduleEvent(EVENT_RELEASE_PLAYER, 5000);
                        }
                        else
                            events.ScheduleEvent(EVENT_SEARCH_PLAYER, 1000);
                        break;

                    case EVENT_RELEASE_PLAYER:
                        me->RemoveAurasDueToSpell(SPELL_ENTER_VEHICLE);
                        playerInside->RemoveAurasDueToSpell(SPELL_ENTER_VEHICLE);
                        me->DespawnOrUnsummon(1000);
                        break;
				}
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_waterspout_tornadoAI(creature);
    }
};

/***********************
** Geyser
************************/

class npc_nazjar_geyser : public CreatureScript // 40597
{
    public:
        npc_nazjar_geyser() : CreatureScript("npc_nazjar_geyser") { }

        struct npc_nazjar_geyserAI : public ScriptedAI
        {
            npc_nazjar_geyserAI(Creature* creature) : ScriptedAI(creature)
            {
                creature->AddAura(SPELL_GEYSER_AURA, creature);
                creature->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                creature->SetReactState(REACT_PASSIVE);
            }

            EventMap events;

            void EnterCombat(Unit* /*who*/)
            {
                events.ScheduleEvent(EVENT_BURST, 5000);
            }

            void UpdateAI(const uint32 diff)
            {
                events.Update(diff);
                
                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch(eventId)
                    {
                        case EVENT_BURST:
                            DoCast(me, SPELL_GEYSER_DAMAGE);
                            me->DespawnOrUnsummon(1500);
                            break;
			    	}
                }
            }
       };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_nazjar_geyserAI(creature);
    }
};

void AddSC_boss_lady_nazjar()
{
    new boss_lady_nazjar();
    new npc_waterspout_tornado();
    new npc_nazjar_geyser();
}