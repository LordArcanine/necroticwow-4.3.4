/*
 * Copyright 2013(C) Sky-Mist awarding credits to for initial research Forgotten Lands <http://www.forgottenlands.eu/>
 *
 * Your script was like arsch though, seriously. I had to fix most of the damn thing from scratch.
 * Also, you bleedin ponce, why don't you freakin check ure spells. Do it properly or bugger all. What a load a cack it was.
 *
 * Script completed: 100% - Requires Testing
 * This script depends loads on DB positioning - so better make sure Millhouse is on these {925.048f, 979.975f, 316.989f, 2.98568f}
 * and that the mobs are spawned as blizzlike as possible.
 * These are two spells... Wat du tei du?79515 82863 
 */

#include "ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Weather.h"

#include "the_stonecore.h"

enum Yells
{
    SAY_FINAL                                    = 0, // Ah-ha! I've got you right where I want you!
    SAY_TAUNT                                    = 1, // You're gonna be sorry!
    SAY_TAUNT_1                                  = 2, // Don't say I didn't warn ya!
    SAY_RETREAT                                  = 3, // Follow me if you dare!
    SAY_RETREAT_2                                = 4, // It's time for a tactical retreat!
    SAY_DOOM                                     = 5, // Now... witness the full power of Millhouse Manastorm!
    SAY_INTRO                                    = 6, // Prison taught me one very important lesson, well, two if you count how to hold your soap, but yes! SURVIVAL!
    
    SAY_WARNING                                  = 7  // Corborus borrows underground!
};

enum Spells
{

    /***** Corborus *****/
    
    SPELL_CRYSTAL_BARRAGE                        = 86881,
    SPELL_CRYSTAL_SHARD_SUMMON                   = 81637, // Requires spell script - casts 92012 every 500 ms in heroic
    SPELL_SUMMON_CRYSTAL_SHARD                   = 92012,
    SPELL_DAMPENING_WAVE                         = 82415, 
    SPELL_TRASHING_CHARGE_VISUAL                 = 81801, // First Visual
    SPELL_TRASHING_CHARGE_DAMAGE                 = 81828, // Damage during visual cast
    SPELL_TRASHING_CHARGE_TELE                   = 81839, /**Then tele to new position. 81816 summons mob thrashing charge**/
    SPELL_BURROW                                 = 81629, /**Actual burrow spell, works perfectly fine - casts 82190 on targets that is scripted to use 82188 - summons rockborer**/
    SPELL_SUMMON_ROCK_BORER                      = 82188,
    SPELL_SURFACE                                = 75764, // Comes to the surface for a bit. still needs an unaura for 81629.
    
    /***** Crystal Shard *****/
    SPELL_CRYSTAL_EXPLOSION                      = 92122,
    SPELL_CRYSTAL_PRESUMMON                      = 92176,
    
    
    /***** Millhouse Manastorm *****/
    
    SPELL_DOOM                                   = 86830,
    SPELL_DOOM_VISUAL                            = 86838,
    SPELL_TIGULE_BLEND                           = 81220,
    SPELL_FEAR                                   = 81442,
    SPELL_FROST_VOLLEY                           = 81440,
    SPELL_SHADOWFURY                             = 81441,
    SPELL_SHADOW_BOLT                            = 81439,
    SPELL_BLUR                                   = 81216
};

const Position FightLocations[4] = // Location where Millhouse runs to
{
    {925.048f, 979.975f, 316.989f, 2.98568f},
    {987.677f, 882.458f, 303.373f, 2.07248f},
    {1075.517f, 862.854f, 291.520f, 2.938915f},
    {1162.571f,  884.882f, 284.963f, 3.403443f}
};

enum Events
{
    /***** Corborus *****/
    EVENT_CRYSTAL_BARRAGE = 1,
    EVENT_SUMMON_CRYSTAL_SHARD,
    EVENT_DAMPENING_WAVE,
    EVENT_TRASHING_CHARGE,
    EVENT_TRASHING_CHARGE_VISUAL,
    EVENT_BURROW,
    
    /***** Millhouse *****/
    EVENT_SHADOW_BOLT,
    EVENT_FEAR,
    EVENT_SHADOWFURY,
    EVENT_FROST_VOLLEY,
    EVENT_RUN_AWAY,
    EVENT_DOOM,
    EVENT_DIE
};

class boss_corborus: public CreatureScript
{
    public:
        boss_corborus() : CreatureScript("boss_corborus") { }

    struct boss_corborusAI: public ScriptedAI
    {
        boss_corborusAI(Creature* creature) : ScriptedAI(creature), summons(me)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;
        SummonList summons;
        EventMap events;
        uint8 chargeCount;
        uint8 phase;
        Unit* ChargeTarget;

        void Reset()
        {
            events.Reset();
            summons.DespawnAll();

            if (instance)
                instance->SetData(DATA_CORBORUS_EVENT, NOT_STARTED);

            chargeCount = 0;
            phase = 0;
            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
            me->RemoveAurasDueToSpell(SPELL_BURROW);
        }   

        void EnterCombat(Unit* who)
        {
            if (instance)
            {
                instance->SetData(DATA_CORBORUS_EVENT, IN_PROGRESS);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me); // Add
            }

            events.ScheduleEvent(EVENT_CRYSTAL_BARRAGE, 7000);
            events.ScheduleEvent(EVENT_DAMPENING_WAVE, 11000);
            events.ScheduleEvent(EVENT_BURROW, 28000);
        }

        void EnterEvadeMode()
        {
			me->RemoveAllAuras();
            Reset();
            me->GetMotionMaster()->MoveTargetedHome();

            if (instance)
            {
                instance->SetData(DATA_CORBORUS_EVENT, FAIL);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
            }
        }

        void JustDied(Unit* killer)
        {
            summons.DespawnAll();

            if (instance)
            {
                instance->SetData(DATA_CORBORUS_EVENT, DONE);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
            }
        }

        void JustSummoned(Creature* summon)
        {
            summons.Summon(summon);
			summon->setActive(true);
 
			if (me->isInCombat())
			    summon->AI()->DoZoneInCombat();
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_CRYSTAL_BARRAGE:
                        if (phase == 0)
                        {
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                                DoCast(target, SPELL_CRYSTAL_BARRAGE);
                            events.ScheduleEvent(EVENT_CRYSTAL_BARRAGE, 14000);
                        }
                        break;

                    case EVENT_DAMPENING_WAVE:
                        if (phase == 0)
                        {
                            me->CastSpell(me->getVictim(), SPELL_DAMPENING_WAVE, true);
                            events.ScheduleEvent(EVENT_DAMPENING_WAVE, 8000);
                        }
                        break;

                    case EVENT_BURROW:
                        phase = 1;
                        if (chargeCount = 0)
                        {
                            Talk(SAY_WARNING);
                            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                            DoCast(me, SPELL_BURROW);
                        }
                        if (chargeCount < 5)
                        {
                            events.ScheduleEvent(EVENT_TRASHING_CHARGE_VISUAL, 4000);
                        } else
                        {
                            phase = 0;
                            chargeCount = 0;
                            events.ScheduleEvent(EVENT_BURROW, 28000);
                            events.RescheduleEvent(EVENT_CRYSTAL_BARRAGE, 13000);
                            events.RescheduleEvent(EVENT_DAMPENING_WAVE, 8000);
                            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                            me->RemoveAurasDueToSpell(SPELL_BURROW);
                        }
                        break;

                    case EVENT_TRASHING_CHARGE_VISUAL:
                        if (ChargeTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                        {
                            DoCast(ChargeTarget, SPELL_TRASHING_CHARGE_VISUAL);
                            DoCast(ChargeTarget, SPELL_TRASHING_CHARGE_DAMAGE);
                        }
                        events.ScheduleEvent(EVENT_TRASHING_CHARGE, 1000);
                        break;

                    case EVENT_TRASHING_CHARGE:
                        if (ChargeTarget)
                            DoCast(ChargeTarget, SPELL_TRASHING_CHARGE_TELE);
                        chargeCount++;
                        break;
                }
            }

            if(!me->HasAura(SPELL_BURROW))
            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_corborusAI(creature);
    }
};

/** NPC 43391 - DB **/
class npc_millhouse_corborus : public CreatureScript
{
    public:
        npc_millhouse_corborus() : CreatureScript("npc_millhouse_corborus") { }

    struct npc_millhouse_corborusAI : public CreatureAI
    {
        npc_millhouse_corborusAI(Creature* creature) : CreatureAI(creature)
        {
            instance = creature->GetInstanceScript();
        }

        EventMap events;
        bool IntroDone;
        uint32 CurrentLocation;
        InstanceScript* instance;

        void Reset()
        {
            events.Reset();
            CurrentLocation = 1;
            me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_MOD_TAUNT, true);
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_ATTACK_ME, true);
        }

        void MoveInLineOfSight(Unit* who)
        {
            if (!IntroDone && me->IsWithinDistInMap(who, 30.0f))
            {
                IntroDone = true;
                Talk(SAY_INTRO);
            }
        }

        void EnterCombat(Unit* who)
        {
            events.ScheduleEvent(EVENT_SHADOW_BOLT, 2000);
            events.ScheduleEvent(EVENT_RUN_AWAY, 2000);
        }
        
        void DamageTaken(Unit* /*attacker*/, uint32& damage)
        {
            if (damage >= me->GetHealth())
                damage = me->GetHealth() - 1;
        }

        void JustDied(Unit* killer)
        {
            me->SummonGameObject(GO_TWILIGHT_DOCUMENTS, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation(), 0, 0, 0, 0, 30000);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_SHADOW_BOLT:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                            DoCast(target, SPELL_SHADOW_BOLT);
                        events.ScheduleEvent(EVENT_FEAR, 2050);
                        break;

                    case EVENT_FEAR:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                            DoCast(target, SPELL_FEAR);
                        events.ScheduleEvent(EVENT_FROST_VOLLEY, 1550);
                        break;

                    case EVENT_SHADOWFURY:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                            DoCast(target, SPELL_SHADOWFURY);
                        events.ScheduleEvent(EVENT_SHADOW_BOLT, 500);
                        break;

                    case EVENT_FROST_VOLLEY:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                            DoCast(target, SPELL_FROST_VOLLEY);
                        events.ScheduleEvent(EVENT_SHADOWFURY, 3500);
                        break;

                    case EVENT_RUN_AWAY:
                        if (CurrentLocation < 4)
                        {
                            if (!me->FindNearestCreature(NPC_STONECORE_EARTHSHAPER, 20.0f, true) && !me->FindNearestCreature(NPC_STONECORE_WARBRINGER, 20.0f, true) && !me->FindNearestCreature(NPC_STONECORE_BERSERKER, 20.0f, true) &&!me->FindNearestCreature(NPC_STONECORE_BRUISER, 20.0f, true) || me->HealthBelowPct(1))
                            {
                                DoCast(me, SPELL_TIGULE_BLEND);
                                DoCast(me, SPELL_BLUR);
                                me->GetMotionMaster()->MovePoint(0, FightLocations[CurrentLocation].GetPositionX(), FightLocations[CurrentLocation].GetPositionY(), FightLocations[CurrentLocation].GetPositionZ());
                                CurrentLocation++;
                                Talk(SAY_RETREAT || SAY_RETREAT_2);
                                events.ScheduleEvent(EVENT_RUN_AWAY, 12000);
                            }else
                                events.ScheduleEvent(EVENT_RUN_AWAY, 11000);
                        }else
                        {
                            events.CancelEvent(EVENT_RUN_AWAY);
                            events.ScheduleEvent(EVENT_DOOM, 10000);
                        }
                        break;

                    case EVENT_DOOM:
                        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                        DoCast(me, SPELL_DOOM_VISUAL);
                        DoCast(me, SPELL_DOOM);
                        Talk(SAY_FINAL);
                        events.ScheduleEvent(EVENT_DIE, 15000);
                        break;

                    case EVENT_DIE:
                        if (Creature* corborus = me->FindNearestCreature(BOSS_CORBORUS, 40.0f, true))
                        {
                            corborus->GetMotionMaster()->MoveJump(me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), 1.0f, 1.0f);
                            corborus->Kill(me);

                            if (GameObject* wall = me->FindNearestGameObject(GO_CORBORUS_WALL, 100.0f))
                                wall->SetGoState(GO_STATE_ACTIVE);
                        }
                        break;
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_millhouse_corborusAI(creature);
    }
};

/** NPC 49267 - DB **/
class npc_corborus_crystal : public CreatureScript
{
    public:
        npc_corborus_crystal() : CreatureScript("npc_corborus_crystal") { }
            
        struct npc_corborus_crystalAI : public ScriptedAI
        {
            npc_corborus_crystalAI(Creature* creature) : ScriptedAI(creature)
            {
                instance = creature->GetInstanceScript();
                creature->SetSpeed(MOVE_RUN, 0.25f);
                creature->SetSpeed(MOVE_WALK, 0.25f);

                if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, NonTankTargetSelector(creature)))
                {
                    if (creature->Attack(target, true))
                        creature->GetMotionMaster()->MoveChase(target);

                    creature->AddThreat(target, 1000.0f);
                    creature->SetReactState(REACT_PASSIVE);
                    creature->CastSpell(target, SPELL_CRYSTAL_PRESUMMON, false);
                    // Not tauntable.
                    creature->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_MOD_TAUNT, true);
                    creature->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_ATTACK_ME, true);
                }

                m_uiRangeCheckTimer = 1000;    
            }

            InstanceScript* instance;
            uint32 m_uiRangeCheckTimer;

            void UpdateAI(const uint32 diff)
            {
                if (m_uiRangeCheckTimer <= diff)
                {
                    if (me->IsWithinDistInMap(me->getVictim(), 2.5f))
                    {
                        DoCast(me, SPELL_CRYSTAL_EXPLOSION);
                        me->getVictim()->AddAura(SPELL_CRYSTAL_EXPLOSION, me->getVictim());
                        me->DespawnOrUnsummon();
                    }
                    m_uiRangeCheckTimer = 1000;
                }
                else
                    m_uiRangeCheckTimer -= diff;
            }
        };
        
    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_corborus_crystalAI(creature);
    }
};

/** SPELLSCRIPT 81637 - DB **/
class spell_summon_crystals : public SpellScriptLoader // 81637
{
public:
    spell_summon_crystals() : SpellScriptLoader("spell_summon_crystals") { }

    class spell_summon_crystalsSpellScript : public SpellScript
    {
        PrepareSpellScript(spell_summon_crystalsSpellScript);

        bool Validate(SpellEntry const * spellEntry)
        {
            return true;
        }

        bool Load()
        {
            return true;
        }

        void HandleDummy(SpellEffIndex effIndex)
        {
            if (GetCaster()->GetMap()->IsHeroic())
                GetCaster()->CastSpell(GetCaster(), SPELL_SUMMON_CRYSTAL_SHARD, false);
        }

        void Register()
        {
            OnEffectHit += SpellEffectFn(spell_summon_crystalsSpellScript::HandleDummy,EFFECT_0,SPELL_EFFECT_FORCE_CAST);
        }
    };

    SpellScript* GetSpellScript() const
    {
        return new spell_summon_crystalsSpellScript();
    }
};

void AddSC_boss_corborus()
{
    new boss_corborus();
    new npc_millhouse_corborus();
    new npc_corborus_crystal();
    new spell_summon_crystals();
}