/*Copyright (C) 2012 SkyMist Project.
*
* Script 99% done. 1% = Testing live.
*
* THIS particular file is NOT free software; third-party users should NOT have access to it, redistribute it or modify it. :)
* This software is registered by the UK registered trademark services at (http://www.ipo.gov.uk/types/tm.htm) and is insured
* by copy-right laws. Any action that 
*/

#include "ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Weather.h"

#include "the_stonecore.h"

/***Note: Spell 78807 and 92426 - requires range in 15 yd ***/
/***Note: Spell 92427 - requires pre-cast so that 92426 works - 94661 damage***/
enum Yells
{
    OZRUK_AGGRO    = 1, //A protector has fallen. The World's Heart lies exposed!
    OZRUK_SHIELD   = 2, //Break yourselves upon my body. Feel the strength of the earth!
    OZRUK_KILL     = 3, //None may pass into the World's Heart!
    OZRUK_DEATH    = 4, //The cycle is complete.
    WARNING_SLAM   = 5, //Orzuk faces %target% and prepares to cast Ground Slam
    WARNING_SHAT   = 6  //Ozruk is preparing to cast shatter! Run away!
};
    
enum Spells
{
    SPELL_GROUND_SLAM  = 78903,  //Very violent. GTFO.
    SPELL_RUPTURE      = 80803,  //This - is implemented a little hacky.
    SPELL_RUPTURE_DMG  = 92381,
    SPELL_RUPTURE_SPK  = 92383,

    SPELL_ELEM_BULKWAR = 78939,
    SPELL_ELEM_SPIKE   = 78835,
    SPELL_SHATTER      = 78807,
    SPELL_PARALYZE     = 92426,
    SPELL_PRE_PARALYZE = 92427,
    SPELL_PARALYZE_DMG = 94661,
    SPELL_ENRAGE       = 80467
};

enum Events
{
    EVENT_GROUND_SLAM  = 1,
    EVENT_ELEM_BULKWAR,
    EVENT_ELEM_SPIKE,
    EVENT_SHATTER,
    EVENT_PARALYZE,
    EVENT_PARALYZE_DAMAGE,
    EVENT_ENRAGE
};

enum Creatures
{
    NPC_RUPTURE   = 49597
};

/*** Note: Doors are unknown. ***/
class boss_ozruk: public CreatureScript
{
    public:
        boss_ozruk() : CreatureScript("boss_ozruk") { }

    struct boss_ozrukAI: public ScriptedAI
    {
        boss_ozrukAI(Creature* creature) : ScriptedAI(creature), summons(me)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;
        SummonList summons;
        EventMap events;

        void Reset()
        {
            events.Reset();
            summons.DespawnAll();

            if (instance)
                instance->SetData(DATA_OZRUK_EVENT, NOT_STARTED);
        }

        void EnterEvadeMode()
        {
            me->RemoveAllAuras();
            Reset();
            me->GetMotionMaster()->MoveTargetedHome();

            if (instance)
            {
                instance->SetData(DATA_OZRUK_EVENT, FAIL);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
            }
        }

        void JustDied(Unit* killer)
        {
            Talk(OZRUK_DEATH);
            summons.DespawnAll();

            if (instance)
            {
                instance->SetData(DATA_OZRUK_EVENT, DONE);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
            }
        }

        void EnterCombat(Unit* /*who*/)
        {
            Talk(OZRUK_AGGRO);
            if (instance)
            {
                instance->SetData(DATA_OZRUK_EVENT, IN_PROGRESS);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me); // Add
            }

            events.ScheduleEvent(EVENT_GROUND_SLAM, 7000);
            events.ScheduleEvent(EVENT_ELEM_BULKWAR, 4000);
            events.ScheduleEvent(EVENT_ELEM_SPIKE, 12000);
            events.ScheduleEvent(EVENT_ENRAGE, 300000);
        }

        void JustSummoned(Creature* summon)
        {
            summons.Summon(summon);
			summon->setActive(true);
 
			if (me->isInCombat())
			    summon->AI()->DoZoneInCombat();
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {       
                    case EVENT_GROUND_SLAM:
                        Talk(WARNING_SLAM);
                        DoCastVictim(SPELL_GROUND_SLAM);
                        if(IsHeroic())
                        for (uint8 i = 0; i < 2; i++)
                            me->SummonCreature(NPC_RUPTURE, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), i = 1 ? me->GetOrientation() + 1.0f : me->GetOrientation() - 1.0f);
                        events.ScheduleEvent(EVENT_GROUND_SLAM, 12000);
                        break;
                        
                    case EVENT_ENRAGE:
                        DoCast(me, SPELL_ENRAGE);
                        break;
                        
                    case EVENT_ELEM_BULKWAR:
                        DoCast(me, SPELL_ELEM_BULKWAR);
                        events.ScheduleEvent(EVENT_ELEM_BULKWAR, 19500);
                        break;

                    case EVENT_ELEM_SPIKE:
                        Talk(OZRUK_SHIELD);
                        DoCast(me, SPELL_ELEM_SPIKE);
                        if(IsHeroic())
                        {
                            events.ScheduleEvent(EVENT_PARALYZE, 9500);
                            me->AddAura(SPELL_PRE_PARALYZE, me); // Paralyze spell does a check for this aura. Without it, it will not cast.
                        }
                        events.ScheduleEvent(EVENT_SHATTER, 10000);
                        break;
                        
                    case EVENT_PARALYZE:
                        DoCastAOE(SPELL_PARALYZE);
                        events.ScheduleEvent(EVENT_PARALYZE_DAMAGE, 7500);
                        break;

                    case EVENT_SHATTER:
                        Talk(WARNING_SHAT);
                        DoCastAOE(SPELL_SHATTER);
                        events.ScheduleEvent(EVENT_ELEM_SPIKE, 30000);
                        events.RescheduleEvent(EVENT_GROUND_SLAM, 8000);
                        events.RescheduleEvent(EVENT_ELEM_BULKWAR, 10000);
                        break;
                        
                    case EVENT_PARALYZE_DAMAGE:
                        {
                            std::list<HostileReference*> t_list = me->getThreatManager().getThreatList();
                            for (std::list<HostileReference*>::const_iterator itr = t_list.begin(); itr!= t_list.end(); ++itr)
                                if(Unit* target = Unit::GetUnit(*me, (*itr)->getUnitGuid()))
                                    if (target->HasAura(SPELL_PARALYZE))
                                        target->CastSpell(target, SPELL_PARALYZE_DMG, true);
                        }
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_ozrukAI(creature);
    }
};

class npc_rupture : public CreatureScript // 49597, 51422
{
public:
    npc_rupture() : CreatureScript("npc_rupture") { }

    struct npc_ruptureAI : public ScriptedAI
    {
        npc_ruptureAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;
        uint32 timerAura;
        
        void EnterCombat(Unit* who)
        {
                me->SetSpeed(MOVE_RUN, 0.8f);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                me->SetReactState(REACT_PASSIVE);
                timerAura = 100;
                float x, y, z;
                me->GetClosePoint(x, y, z, me->GetObjectSize() / 3, 100.0f);
                me->GetMotionMaster()->MovePoint(1, x, y, z);
                me->DespawnOrUnsummon(6000);
        }

        void UpdateAI(const uint32 diff)
        {
            if (timerAura <= diff)
            {
                if (Unit* target = me->FindNearestPlayer(2.0f, true))
                {
                    DoCast(target, SPELL_RUPTURE_DMG);
                    DoCast(target, SPELL_RUPTURE_SPK);
                }

                timerAura = 500;
            } else timerAura -= diff;
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_ruptureAI(creature);
    }
};
   
void AddSC_boss_ozruk()
{
    new boss_ozruk();
    new npc_rupture();
}