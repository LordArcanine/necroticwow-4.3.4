 /*
 * Copyright (C) 2013 SkyMist Project.
 *
 * Script 70% done.
 * Azil Force Grip is unscripted due core lacking the implementation of seat changing.
 * Azil Gravity Well deals no damage currently. Needs to also target friendly units.
 *
 * THIS particular file is NOT free software; third-party users should NOT have access to it, redistribute it or modify it. :)
 * This software is registered by the UK registered trademark services at (http://www.ipo.gov.uk/types/tm.htm) and is insured
 * by copy-right laws. Any person that uses this script without prior aproval of all Sky-Mist members, is liable to be held
 * responsible in a court of law. 
 *
 * Really motherfucking important - Azil is a vehicle with 903 as vehicle id
 */

#include"ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Weather.h"

#include "the_stonecore.h"

enum Yells
{
	AZIL_AGGRO    = 1, // "The world will be reborn in flames!"
	AZIL_SHARD    = 2, // "Feel the fury of earth!"
	AZIL_KILL     = 3, // "A sacrifice for you, master."
	AZIL_DEATH    = 4, // "For my death, countless more will fall. The burden is now yours to bear." 
	AZIL_GRIP     = 5, // "Witness the power bestowed upon me by Deathwing!"
	WARNING_PHASE = 6, // "Azil is heading for the Heart of Destruction."
	WARNING_CULTI = 7  // "Cultists burst into the chamber from the sides."
};

enum Spells
{
    SPELL_FORCE_GRIP                              = 79351,
    SPELL_GRAVITY_WELL_VISUAL_1                   = 79245,
    SPELL_GRAVITY_WELL_VISUAL_2                   = 79244,
    SPELL_GRAVITY_WELL_SUMMON                     = 79340,
    SPELL_GRAVITY_WELL_TRIGGER                    = 79333,
	SPELL_GRAVITY_WELL_DAMAGE                     = 79249,
	SPELL_GRAVITY_WELL_SHRINK                     = 92475,
    SPELL_CURSE_OF_BLOOD                          = 79345,
    SPELL_ENERGY_SHIELD                           = 79050,
    SPELL_SEISMIC_SHARD_VISUAL                    = 79009,
    SPELL_SEISMIC_SHARD_CHARGE                    = 79014,
    SPELL_SEISMIC_SHARD_SUMMON                    = 86860,
	SPELL_VISUAL_AZIL                             = 85654
};

enum Events
{
    EVENT_FORCE_GRIP = 1,
    EVENT_GRAVITY_WELL,
    EVENT_CURSE_OF_BLOOD,
    EVENT_ENERGY_SHIELD,
    EVENT_SEISMIC_SHARD,
    EVENT_GO_PHASE_2,
    EVENT_GO_PHASE_1,
    EVENT_SUMMON_DEVOUT_FOLLOWER,
    EVENT_SEISMIC_SHARD_THROW
};

enum NPCs
{
    NPC_SEISMIC_SHARD                             = 42355,
    NCP_DEVOUT_FOLLOWER                           = 42428,
    NPC_GRAVITY_WELL                              = 42499
};

const Position SeismicShardSpawnPosition[] =
{
    {1326.906f, 969.588f, 223.184f, 1.2579f},
    {1337.574f, 967.505f, 227.514f, 1.9117f},
    {1347.833f, 976.205f, 224.644f, 2.2017f},
};

const Position DevoutFollowerSpawnPosition[] =
{
    {1264.932f, 978.295f, 206.792f, 0.4099f},
	{1372.494f, 990.640f, 207.967f, 2.8219f},
	{1286.531f, 1045.944f, 209.941f, 4.6077f},
};

enum Points
{
    POINT_PHASE = 0
};

class boss_high_priestess_azil: public CreatureScript
{
    public:
        boss_high_priestess_azil() : CreatureScript("boss_high_priestess_azil") { }

    struct boss_high_priestess_azilAI: public ScriptedAI
    {
        boss_high_priestess_azilAI(Creature* creature) : ScriptedAI(creature), vehicle(creature->GetVehicleKit()), summons(me)
        {
            ASSERT(vehicle);
            instance = creature->GetInstanceScript();		
		}

        InstanceScript* instance;
        SummonList summons;
        EventMap events;
		Vehicle* vehicle;
        Creature* shards[3];
        uint8 shard_done;
        uint8 phase;

        void Reset()
        {
            events.Reset();
            summons.DespawnAll();
            phase = 0;
            shards[0] = NULL;
            shards[1] = NULL;
            shards[2] = NULL;
            shard_done = 0;

            if (instance)
                instance->SetData(DATA_HIGH_PRIESTESS_AZIL_EVENT, NOT_STARTED);
        }
		
		void EnterEvadeMode()
        {
			me->RemoveAllAuras();
            Reset();
            me->GetMotionMaster()->MoveTargetedHome();

            if (instance)
            {
                instance->SetData(DATA_HIGH_PRIESTESS_AZIL_EVENT, FAIL);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
	  		}
        }

        void EnterCombat(Unit* victim)
        {
			Talk(AZIL_AGGRO);
            events.ScheduleEvent(EVENT_CURSE_OF_BLOOD, 8000);
            events.ScheduleEvent(EVENT_FORCE_GRIP, 12000);
            events.ScheduleEvent(EVENT_GRAVITY_WELL, 10000);
            events.ScheduleEvent(EVENT_GO_PHASE_2, 60000);

            if(instance)
            {
                instance->SetData(DATA_HIGH_PRIESTESS_AZIL_EVENT, IN_PROGRESS);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
	  		}
        }

        void JustSummoned(Creature* summon)
        {
            summons.Summon(summon);
			summon->setActive(true);

			if (me->isInCombat())
			    summon->AI()->DoZoneInCombat();
        }
		
		void MovementInform(uint32 type, uint32 point)
        {
            if (type != POINT_MOTION_TYPE)
                return;

            switch (point)
            {
                case POINT_PHASE:
					me->SetFacingTo(me->GetHomePosition().GetOrientation());
                    me->SetReactState(REACT_PASSIVE);
                    me->SetHover(true);
                    me->SetDisableGravity(true);
                    me->SetCanFly(true);
					events.ScheduleEvent(EVENT_SEISMIC_SHARD, 3000);
                    events.ScheduleEvent(EVENT_SEISMIC_SHARD_THROW, urand(5000, 8000));
                    events.ScheduleEvent(EVENT_SUMMON_DEVOUT_FOLLOWER, 1000);
                    break;

				default:
                    break;
			}
		}
		
		void KilledUnit(Unit* /*victim*/)
        {
			Talk(AZIL_KILL);
        }

        void JustDied(Unit* killer)
        {
            summons.DespawnAll();
			
			Talk(AZIL_DEATH);
			
			if (instance)
            {
                instance->SetData(DATA_HIGH_PRIESTESS_AZIL_EVENT, DONE);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
			}
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_CURSE_OF_BLOOD:
                        DoCastVictim(SPELL_CURSE_OF_BLOOD);
                        events.ScheduleEvent(EVENT_CURSE_OF_BLOOD, 8000);
                        break;

                    case EVENT_FORCE_GRIP:
						Talk(AZIL_GRIP);
                        DoCastVictim(SPELL_FORCE_GRIP);
                        events.ScheduleEvent(EVENT_FORCE_GRIP, 14000);
                        break;

                    case EVENT_GRAVITY_WELL:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true, 0))
                            me->CastSpell(target, SPELL_GRAVITY_WELL_SUMMON, false);
                        events.ScheduleEvent(EVENT_GRAVITY_WELL, 10000);
                        break;

                    case EVENT_GO_PHASE_2:
                        phase = 1;
						Talk(WARNING_PHASE);
                        events.CancelEvent(EVENT_CURSE_OF_BLOOD);
                        events.CancelEvent(EVENT_FORCE_GRIP);
                        events.CancelEvent(EVENT_GRAVITY_WELL);
                        DoCast(me, SPELL_ENERGY_SHIELD);
						me->AddAura(SPELL_VISUAL_AZIL, me);
                        me->SetReactState(REACT_PASSIVE);
						me->GetMotionMaster()->MovePoint(POINT_PHASE, me->GetHomePosition().GetPositionX(), me->GetHomePosition().GetPositionY(), me->GetHomePosition().GetPositionZ()+15.0f);
                        break;

                    case EVENT_SEISMIC_SHARD:
						Talk(AZIL_SHARD);
                        for (int i = 0; i < 3; i++)
                        {
                            if (Creature* shard = me->SummonCreature(NPC_SEISMIC_SHARD, SeismicShardSpawnPosition[i], TEMPSUMMON_MANUAL_DESPAWN, 0, 0))
                            {
                                shard->AddAura(SPELL_SEISMIC_SHARD_VISUAL, shard);
                                shard->SetReactState(REACT_PASSIVE);
                                shard->SetCanFly(true);
                                shard->SetDisableGravity(true);
                                me->SetByteFlag(UNIT_FIELD_BYTES_1, 3, UNIT_BYTE1_FLAG_ALWAYS_STAND | UNIT_BYTE1_FLAG_HOVER);
                                shard->AddUnitMovementFlag(MOVEMENTFLAG_DISABLE_GRAVITY);
                                shard->SetDisplayId(11686);
                                shard->setFaction(14);
                                shard->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                                DoZoneInCombat(shard);
                                shards[i] = shard;
                            }
                        }
                        break;

                    case EVENT_SUMMON_DEVOUT_FOLLOWER:
						Talk(WARNING_CULTI);
						for (uint32 x = 0; x < 6; ++x)
							Creature* follower = me->SummonCreature(NCP_DEVOUT_FOLLOWER, DevoutFollowerSpawnPosition[urand(0, 2)], TEMPSUMMON_MANUAL_DESPAWN);
                        events.ScheduleEvent(EVENT_SUMMON_DEVOUT_FOLLOWER, 6000);
                        break;

                    case EVENT_SEISMIC_SHARD_THROW:
                        if (Creature* shard = shards[shard_done])
                        {
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true, 0))
                            {
                                shard->SetHealth(20);
                                shard->CastSpell(target, SPELL_SEISMIC_SHARD_CHARGE, false);
                                shard_done++;
                            }
                        }
                        if (shard_done != 3)
                            events.ScheduleEvent(EVENT_SEISMIC_SHARD_THROW, urand(5000, 8000));
                        else
                        {
                            events.CancelEvent(EVENT_SUMMON_DEVOUT_FOLLOWER);
                            events.ScheduleEvent(EVENT_GO_PHASE_1, 4000);
                            shard_done = 0;
                        }
                        break;

                    case EVENT_GO_PHASE_1:
                        phase = 0;
                        me->SetReactState(REACT_PASSIVE);
                        me->RemoveAura(SPELL_ENERGY_SHIELD);
                        me->Attack(me->getVictim(), false);
						me->RemoveAura(SPELL_VISUAL_AZIL);
                        me->GetMotionMaster()->MoveChase(me->getVictim(), 20.0f, 1.0f);
                        events.ScheduleEvent(EVENT_CURSE_OF_BLOOD, 10000);
                        events.ScheduleEvent(EVENT_FORCE_GRIP, 15000);
                        events.ScheduleEvent(EVENT_GRAVITY_WELL, 15000);
                        events.ScheduleEvent(EVENT_GO_PHASE_2, 60000);
                        break;
                }
            }

            if (phase == 0)
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_high_priestess_azilAI(creature);
    }
};

enum gravityEvents
{
    EVENT_AURA = 1,
    EVENT_DESPAWN,
    EVENT_RECAST,
};

class npc_gravity_well : public CreatureScript
{
public:
    npc_gravity_well() : CreatureScript("npc_gravity_well") { }

    struct npc_gravity_wellAI : public ScriptedAI
    {
        npc_gravity_wellAI(Creature* creature) : ScriptedAI(creature) 
        {
            creature->SetDisplayId(11686);
            creature->setFaction(14);
            creature->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_DISABLE_MOVE);
            creature->AddAura(SPELL_GRAVITY_WELL_VISUAL_1, creature);
            creature->SetReactState(REACT_PASSIVE);
            events.ScheduleEvent(EVENT_AURA, 4000);
            instance = creature->GetInstanceScript();
            unitsKilled = 0;
        }

        EventMap events;
        InstanceScript* instance;
        uint8 unitsKilled;

		void KilledUnit(Unit* /*victim*/)
        {
			unitsKilled++;
			if(IsHeroic())
			{
				switch (unitsKilled)
				{
					case 1:
						DoCast(me, SPELL_GRAVITY_WELL_SHRINK);
						return;
					case 2:
						DoCast(me, SPELL_GRAVITY_WELL_SHRINK);
						return;
					case 3:
						DoCast(me, SPELL_GRAVITY_WELL_SHRINK);
						return;
					case 4:
						DoCast(me, SPELL_GRAVITY_WELL_SHRINK);
						return;
					case 5:
						me->DespawnOrUnsummon();
						return;
				}
			}
        }

        void UpdateAI(const uint32 diff)
        {
            events.Update(diff);
            
            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_AURA:
                        me->RemoveAllAuras();
                        DoCast(me, SPELL_GRAVITY_WELL_TRIGGER);
						me->AddAura(SPELL_GRAVITY_WELL_DAMAGE, me);
                        me->AddAura(SPELL_GRAVITY_WELL_VISUAL_2, me);
                        events.ScheduleEvent(EVENT_DESPAWN, 20000);
                        break;

                    case EVENT_DESPAWN:
						if(!IsHeroic())
							me->DespawnOrUnsummon();
                        break;                        
                }
            }
        }

    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_gravity_wellAI (creature);
    }
};

void AddSC_boss_high_priestess_azil()
{
    new boss_high_priestess_azil();
    new npc_gravity_well();
}