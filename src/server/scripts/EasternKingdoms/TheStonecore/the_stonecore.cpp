/*
 * Copyright (C) 2013 Sky-Mist Server
 *
 * This program is NOT free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 */

#include "ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Weather.h"

#include "the_stonecore.h"

/***************************************TRASH SPELLS*************************************/
// Crystalspawn Giant (42810) Health: 536, 810 - 1, 202, 925
// update creature_template set
enum Spells 
{
    SPELL_QUAKE                                   = 81008,
    H_SPELL_QUAKE                                 = 92631,

    // IMP (43014) Health: 4, 468 - 7, 749, Mana: 16, 676 - 17, 816
    SPELL_FELL_FIREBALL                           = 80344,
    H_SPELL_FELL_FIREBALL                         = 92638,
	
	// Force of Earth
	SPELL_SHOCKWAVE_FORCE                         = 75417,
	SPELL_GROUND_POUND                            = 76590,

    // Rock Borer (43917, 42845) Health: 6, 702 - 11, 624
    SPELL_ROCK_BORE                               = 80028,
    H_SPELL_ROCK_BORE                             = 92630,

    // Stonecore Berserker (43430) Health: 312, 753 - 387, 450
    SPELL_SCHARGE                                 = 81574,
    SPELL_SPINNING_SLASH                          = 81568,

    // Stonecore Bruiser (42692) Health: 590, 491 - 1, 202, 925
    SPELL_BODY_SLAM                               = 80180,
    SPELL_SHOCKWAVE                               = 80195,
    H_SPELL_SHOCKWAVE                             = 92640,

    // Stonecore Earthshaper (43537) Health: 250, 201 - 309, 960, Mana: 19, 394
    SPELL_DUST_STORM                              = 81463,
    SPELL_FORCE_OF_EARTH                          = 81459,
    SPELL_GROUND_SHOCK                            = 81530,
    H_SPELL_GROUND_SHOCK                          = 92628,
    SPELL_LAVA_BURST                              = 81576,
    H_SPELL_LAVA_BURST                            = 92626,

    // Stonecore Flayer (42808) Health: 312, 753 - 387, 450
    SPELL_FLAY                                    = 79922,

    // Stonecore Magmalord (42789) Health: 312, 753 - 387, 450, Mana: 25, 014 - 26, 724
    SPELL_IGNITE                                  = 80151,
    H_SPELL_IGNITE                                = 92636,
    SPELL_MAGMA_ERUPTION                          = 80038,

    // Stonecore Rift Conjurer (42691) Health: 312, 753 - 387, 450, Mana: 16, 676 - 17, 816
    SPELL_DEMON_PORTAL                            = 80308,
    SPELL_SHADOWBOLT                              = 80279,
    H_SPELL_SHADOWBOLT                            = 92637,

    //Stonecore Sentry (42695) Health: 6, 702 - 11, 624

    // Stonecore Warbringer (42696) Health: 312, 753 - 387, 450
    SPELL_CLEAVE                                  = 15496,
    SPELL_RAGE                                    = 80158
};

enum eEvents 
{
    EVENT_NONE,
    EVENT_QUAKE,
    EVENT_FELL_FIREBALL,
    EVENT_BLUR,
    EVENT_DUST_STORM,
    EVENT_FORCE_OF_EARTH,
    EVENT_GROUND_SHOCK,
    EVENT_LAVA_BURST,
    EVENT_DESPAWN,
    EVENT_TIGULE,
    EVENT_ROCK_BORE,
    EVENT_SCHARGE,
    EVENT_SPINNING_SLASH,
    EVENT_BODY_SLAM,
    EVENT_FLAY,
    EVENT_IGNITE,
    EVENT_MAGMA_ERUPTION,
    EVENT_DEMON_PORTAL,
    EVENT_SHADOWBOLT,
    EVENT_CLEAVE,
    EVENT_RAGE,
	EVENT_SHOCKWAVE,
	EVENT_GROUND_POUND
};

// Crystalspawn Giant AI
class npc_crystalspawn_giant: public CreatureScript 
{
public:
    npc_crystalspawn_giant() : CreatureScript("npc_crystalspawn_giant") { }

    CreatureAI* GetAI(Creature* pCreature) const 
    {
        return new npc_crystalspawn_giantAI(pCreature);
    }

    struct npc_crystalspawn_giantAI: public ScriptedAI
    {
        npc_crystalspawn_giantAI(Creature *c) : ScriptedAI(c) { }

        EventMap events;

        void Reset() 
        {
            events.Reset();
        }

        void EnterCombat(Unit* /*who*/)
        {
            events.ScheduleEvent(EVENT_QUAKE, 5000 + urand(1000, 5000));
        }

        void UpdateAI(const uint32 diff) 
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent()) 
            {
                switch (eventId)
                {
                    case EVENT_QUAKE:
                        DoCast(me->getVictim(), SPELL_QUAKE);
                        events.ScheduleEvent(EVENT_QUAKE, 5000 + rand() % 5000);
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

// Imp AI
class npc_force_of_earth: public CreatureScript 
{
public: 
    npc_force_of_earth() : CreatureScript("npc_force_of_earth") { }

    CreatureAI* GetAI(Creature* pCreature) const 
    {
        return new npc_force_of_earthAI(pCreature);
    }

    struct npc_force_of_earthAI: public ScriptedAI
    {
        npc_force_of_earthAI(Creature *c) : ScriptedAI(c) { }

        EventMap events;

        void Reset()
        {
            events.Reset();
        }

        void EnterCombat(Unit* /*who*/) 
        {
            events.ScheduleEvent(EVENT_SHOCKWAVE, 4000);
			events.ScheduleEvent(EVENT_GROUND_POUND, 10000);
        }

        void UpdateAI(const uint32 diff) 
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent()) 
            {
                switch (eventId) 
                {
                    case EVENT_SHOCKWAVE:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
                            DoCast(target, SPELL_SHOCKWAVE_FORCE);
                        events.ScheduleEvent(EVENT_SHOCKWAVE, 9000);
                        break;

					case EVENT_GROUND_POUND:
                            DoCast(me, SPELL_GROUND_POUND);
                        events.ScheduleEvent(EVENT_GROUND_POUND, 12000);
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

// Imp AI
class npc_impp: public CreatureScript 
{
public: 
    npc_impp() : CreatureScript("npc_impp") { }

    CreatureAI* GetAI(Creature* pCreature) const 
    {
        return new npc_imppAI(pCreature);
    }

    struct npc_imppAI: public ScriptedAI
    {
        npc_imppAI(Creature *c) : ScriptedAI(c) { }

        EventMap events;

        void Reset()
        {
            events.Reset();
        }

        void EnterCombat(Unit* /*who*/) 
        {
            events.ScheduleEvent(EVENT_FELL_FIREBALL, 2000);
        }

        void UpdateAI(const uint32 diff) 
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent()) 
            {
                switch (eventId) 
                {
                    case EVENT_FELL_FIREBALL:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
                            DoCast(target, SPELL_FELL_FIREBALL);
                        events.ScheduleEvent(EVENT_FELL_FIREBALL, 2000);
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

// Rock Borer AI
class npc_rock_borer: public CreatureScript 
{
public:
    npc_rock_borer() : CreatureScript("npc_rock_borer") { }

    CreatureAI* GetAI(Creature* pCreature) const 
    {
        return new npc_rock_borerAI(pCreature);
    }

    struct npc_rock_borerAI: public ScriptedAI
    {
        npc_rock_borerAI(Creature *c) : ScriptedAI(c) { }

        EventMap events;

        void Reset() 
        {
            events.Reset();
        }

        void EnterCombat(Unit* /*who*/)
        {
            events.ScheduleEvent(EVENT_ROCK_BORE, 3000);
        }

        void UpdateAI(const uint32 diff) 
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent()) 
            {
                switch (eventId)
                {
                    case EVENT_ROCK_BORE:
                        if (Unit *target = me->getVictim())
                        {
                            if (!target->HasAura(SPELL_ROCK_BORE))
                            {
                                DoCast(target, SPELL_ROCK_BORE);
                            }
                        }
                        events.ScheduleEvent(EVENT_ROCK_BORE, 2000);
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

// Stonecore EarthshaperAI
class npc_stonecore_earthshaper: public CreatureScript 
{
public:
    npc_stonecore_earthshaper() : CreatureScript("npc_stonecore_earthshaper") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_stonecore_earthshaperAI(pCreature);
    }

    struct npc_stonecore_earthshaperAI: public ScriptedAI
    {
        npc_stonecore_earthshaperAI(Creature *c) : ScriptedAI(c) { }

        EventMap events;

        void Reset()
        {
            events.Reset();
        }

        void EnterCombat(Unit* /*who*/) 
        {
            events.ScheduleEvent(EVENT_FORCE_OF_EARTH, 30000);
            events.ScheduleEvent(EVENT_LAVA_BURST, 1000);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId) 
                {
                    case EVENT_DUST_STORM:
                        if (Unit *pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0))
                            DoCast(pTarget, SPELL_DUST_STORM);
						events.ScheduleEvent(EVENT_LAVA_BURST, 500);
                        break;

                    case EVENT_LAVA_BURST:
						if (Unit *pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0))
							if(!IsHeroic())
								{
									DoCast(pTarget, SPELL_LAVA_BURST);
								}else
									DoCast(pTarget, H_SPELL_LAVA_BURST);
						events.ScheduleEvent(EVENT_GROUND_SHOCK, 2100);
                        break;

                    case EVENT_FORCE_OF_EARTH:
						DoCast(me, SPELL_FORCE_OF_EARTH);
						events.ScheduleEvent(EVENT_DESPAWN, 5000);
                        break;

                    case EVENT_GROUND_SHOCK:
                        if (Unit *pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0))
                            if(!IsHeroic())
							{
								DoCast(pTarget, SPELL_GROUND_SHOCK);
							}else
								DoCast(pTarget, H_SPELL_GROUND_SHOCK);
                        events.ScheduleEvent(EVENT_DUST_STORM, 1000);
                        break;

                    case EVENT_DESPAWN:
						// Force of earth - NPC 43552 - are 380400 pe normal si 506000 pe heroic. Are Rage bar. Damage e cam de 12000n si 15000h pe bucata la fiecare 2,5 secunde. Hits very hard.
						me->SummonCreature(NPC_FORCE_OF_EARTH, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation(), TEMPSUMMON_CORPSE_DESPAWN, 20000);
                        me->DisappearAndDie();
                        break;
                }
            }
        }
    };
};

void AddSC_the_stonecore()
{
    new npc_crystalspawn_giant();
    new npc_impp();
    new npc_rock_borer();
	new npc_stonecore_earthshaper();
	new npc_force_of_earth();
}
