/*
 * Copyright 2013(C) Sky-Mist
 *
 *
 * These scripts belong to a small group of individual developers which you, who is reading, this
 * definetly do not belong to. As long as you are in the EEA - I will press charges against you
 * for using this without proper authority. Yes, that is possible quite easily due to copy-right
 * ownership and due to sky-mist being registered in the UK as a minor non-profit organization.
 * 
 * If that did not stop you from using this script, remember I am deranged and dangerous and I just
 * might have enough time to find you.
 *
 * Script completed: 100% - Requires Testing
 */

#include"ScriptPCH.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Weather.h"

#include "the_stonecore.h"

enum Spells
{
    /***** Slabhide *****/
    
    SPELL_ERUPTION                                = 80800, // Cast by lava on self. - 80798 might be visual dummy and 80801 periodic
    SPELL_SUMMON_LAVA_FISSURE                     = 80803,
    SPELL_LAVA_FISSURE_PREPARATION                = 80798,
    SPELL_STALACTITE_SUMMON                       = 80656, // Trigger Multiple Summon Spell  
    SPELL_SANDBLAST                               = 80807,
    SPELL_CRYSTAL                                 = 92265, // Crystal breath - 2.5 sec cast time + 6 seconds.
    
    /***** Stalactite *****/
    SPELL_STALACTITE_WARNING                      = 80654, // Warning for where it falls.
    SPELL_STALACTITE_VISUAL                       = 80643  // Multiple effects - triger spell 80647 - that needs to be set to do damage in 3yds(DMG)
};

enum Events
{
    EVENT_LAVA_FISSURE = 1,
    EVENT_STALACTITE,
    EVENT_STALACTITE_P1,
    EVENT_STALACTITE_P2,
    EVENT_SANDBLAST,
    EVENT_FLY_UP,
    EVENT_CRYSTAL
};

enum MovementPoints
{
    POINT_CENTER  = 1,
    POINT_TAKEOFF = 2,
    POINT_LAND    = 3,
    POINT_INTRO   = 4
};

class boss_slabhide: public CreatureScript
{
    public:
        boss_slabhide() : CreatureScript("boss_slabhide") { }

    struct boss_slabhideAI: public ScriptedAI
    {
        boss_slabhideAI(Creature* creature) : ScriptedAI(creature), summons(me)
        {
            instance = creature->GetInstanceScript();
            introDone = false;
        }

        InstanceScript* instance;
        SummonList summons;
        EventMap events;
        bool isFlyPhase, introDone;
        Position pos;

        void Reset()
        {
            events.Reset();
            summons.DespawnAll();

            if (instance)
                instance->SetData(DATA_SLABHIDE_EVENT, NOT_STARTED);

            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
            me->SetReactState(REACT_PASSIVE);
            me->SetByteFlag(UNIT_FIELD_BYTES_1, 3, 0x02);
            me->AddUnitMovementFlag(MOVEMENTFLAG_CAN_FLY | MOVEMENTFLAG_FLYING);

            isFlyPhase = false;
            me->SetReactState(REACT_AGGRESSIVE);
            me->SetHover(false);
            me->SetDisableGravity(false);
            me->SetCanFly(false);
        }

        void EnterCombat(Unit* attacker)
        {
            if (instance && introDone)
            {
                instance->SetData(DATA_SLABHIDE_EVENT, IN_PROGRESS);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me); // Add
            }

            events.ScheduleEvent(EVENT_LAVA_FISSURE, 7000);
            events.ScheduleEvent(EVENT_SANDBLAST, 9000);
            events.ScheduleEvent(EVENT_STALACTITE_P1, 28000);
        }

        void JustSummoned(Creature* summon)
        {
            summons.Summon(summon);
			summon->setActive(true);

            switch (summon->GetEntry())
            {
                case NPC_LAVA_FISSURE:
                    summon->CastSpell(summon, SPELL_LAVA_FISSURE_PREPARATION, true);
                    summon->AI()->DoAction(ACTION_START_LAVA_FISSURE);
                    break;
            }
        }
        
        void MoveInLineOfSight(Unit* who)
        {
            if (me->IsWithinDistInMap(who, 30.0f, false) && !introDone)
            {
                introDone = true;
                if (instance)
                {
                    instance->SetData(DATA_SLABHIDE_EVENT, IN_PROGRESS);
                    instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me); // Add
                }

                me->GetMotionMaster()->MovePoint(POINT_INTRO, 1196.447f, 1142.902f, 261.132f);
            }
        }

        void EnterEvadeMode()
        {
			me->RemoveAllAuras();
            Reset();
            me->GetMotionMaster()->MoveTargetedHome();

            if (instance)
            {
                instance->SetData(DATA_SLABHIDE_EVENT, FAIL);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
            }
        }

        void MovementInform(uint32 type, uint32 point)
        {
            if (type != POINT_MOTION_TYPE)
                return;

            switch (point)
            {
                case POINT_INTRO:
                    me->SetReactState(REACT_PASSIVE);
                    me->SetHover(true);
                    me->SetDisableGravity(true);
                    me->SetCanFly(true);
                    me->SetFacingTo(5.05000f);
                    events.ScheduleEvent(EVENT_STALACTITE_P2, 9000);
                    events.ScheduleEvent(EVENT_STALACTITE, 1000);
                    break;

                case POINT_CENTER:
                    me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                    me->SetReactState(REACT_PASSIVE);
                    me->SetHover(true);
                    me->SetDisableGravity(true);
                    me->SetCanFly(true);
                    pos.Relocate(me);
                    pos.m_positionZ += 18.0f;
                    me->GetMotionMaster()->MoveTakeoff(POINT_TAKEOFF, pos);
                    events.ScheduleEvent(EVENT_FLY_UP, 2000);
                    break;

                case POINT_TAKEOFF:
                    events.ScheduleEvent(EVENT_STALACTITE_P2, 7000);
                    if(IsHeroic())
                        events.ScheduleEvent(EVENT_CRYSTAL, 9000);
                    break;

                case POINT_LAND:
                    DoStartMovement(me->getVictim());
                    isFlyPhase = false;
                    me->SetHover(false);
                    me->SetDisableGravity(false);
                    me->SetCanFly(false);
                    events.ScheduleEvent(EVENT_LAVA_FISSURE, 7000);
                    events.ScheduleEvent(EVENT_SANDBLAST, 9000);
                    events.ScheduleEvent(EVENT_STALACTITE_P1, 28000);
                    break;
            }
        }
        
        void JustDied(Unit* killer)
        {
            summons.DespawnAll();

            if (instance)
            {
                instance->SetData(DATA_SLABHIDE_EVENT, DONE);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me); // Remove
            }
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_LAVA_FISSURE:
                        if (!isFlyPhase)
                        {
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 0.0f, true, 0))
                                DoCast(target, SPELL_SUMMON_LAVA_FISSURE, true);

                            events.ScheduleEvent(EVENT_LAVA_FISSURE, 12000);
                        }
                        break;

                    case EVENT_SANDBLAST:
                        if (!isFlyPhase)
                        {
                            DoCastVictim(SPELL_SANDBLAST);
                            events.ScheduleEvent(EVENT_SANDBLAST, 8000);
                        }
                        break;

                    case EVENT_STALACTITE_P1:
                        isFlyPhase = true;
                        me->SetReactState(REACT_PASSIVE);
                        me->GetMotionMaster()->MovePoint(POINT_CENTER, 1283.2111f, 1214.3529f, 247.118f);
                        break;

                    case EVENT_STALACTITE_P2:
                        isFlyPhase = false;
                        me->SetReactState(REACT_AGGRESSIVE);
                        me->RemoveUnitMovementFlag(MOVEMENTFLAG_DISABLE_GRAVITY);
                        me->GetMotionMaster()->MovePoint(POINT_LAND, 1283.2111f, 1214.3529f, 247.118f);
                        break;

                    case EVENT_CRYSTAL:
                        if (!isFlyPhase)
                            DoCastAOE(SPELL_CRYSTAL);
                        break;

                    case EVENT_FLY_UP:
                        me->GetMotionMaster()->MovePoint(POINT_TAKEOFF, pos);
                        events.ScheduleEvent(EVENT_STALACTITE, 1500);
                        break;

                    case EVENT_STALACTITE:
                        DoCast(me, SPELL_STALACTITE_SUMMON);
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
     };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_slabhideAI(creature);
    }
};

enum lavaFissureEvents
{
    EVENT_ERUPTION = 1,
    EVENT_DESPAWN
};

class npc_lava_fissure : public CreatureScript
{
public:
    npc_lava_fissure() : CreatureScript("npc_lava_fissure") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_lava_fissureAI(creature);
    };

    struct npc_lava_fissureAI : public ScriptedAI
    {
        npc_lava_fissureAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = creature->GetInstanceScript();
            me->SetReactState(REACT_PASSIVE);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE|UNIT_FLAG_NON_ATTACKABLE|UNIT_FLAG_NOT_SELECTABLE);
        }

        InstanceScript* instance;
        EventMap events;

        void Reset()
        {
            events.Reset();
        }

        void DoAction(const int32 actionId)
        {
            switch (actionId)
            {
                case ACTION_START_LAVA_FISSURE:
                    events.ScheduleEvent(EVENT_ERUPTION, 5000);
                    break;
            }
        }

        void UpdateAI(const uint32 diff)
        {
            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_ERUPTION:
                        DoCast(me, SPELL_ERUPTION, true);
                        if(IsHeroic())
                        {
                            events.ScheduleEvent(EVENT_DESPAWN, 10000);
                        } else
                        events.ScheduleEvent(EVENT_DESPAWN, 30000);
                        break;

                    case EVENT_DESPAWN:
                        me->DisappearAndDie();
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };                              
};

class npc_stalactite : public CreatureScript // 43159
{
public:
    npc_stalactite() : CreatureScript("npc_stalactite") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_stalactiteAI (creature);
    }

    struct npc_stalactiteAI : public ScriptedAI
    {
        npc_stalactiteAI(Creature* creature) : ScriptedAI(creature)
        {
            timerAura    = 500;
            timerImpact  = 3000;
            timerDespawn = 30000;
            creature->SetReactState(REACT_PASSIVE);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE|UNIT_FLAG_NON_ATTACKABLE|UNIT_FLAG_NOT_SELECTABLE);
        }

        uint32 timerAura;
        uint32 timerImpact;
        uint32 timerDespawn;

        void UpdateAI(const uint32 diff)
        {
            if (timerAura <= diff)
            {
                DoCast(me, SPELL_STALACTITE_WARNING);
            } else timerAura -= diff;

            if (timerImpact <= diff)
            {
                DoCast(me, SPELL_STALACTITE_VISUAL);
            } else timerImpact -= diff;

            if (timerDespawn <= diff)
            {
                me->DespawnOrUnsummon();
            } else timerDespawn -= diff;
        }
    };
};

void AddSC_boss_slabhide()
{
    new boss_slabhide();
    new npc_lava_fissure();
    new npc_stalactite();
}