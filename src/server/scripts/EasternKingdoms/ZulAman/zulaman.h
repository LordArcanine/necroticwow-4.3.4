 /*
 * Copyright (C) 2013 Skymist Project
 *
 * This file is NOT free software. You may NOT copy, redistribute it or modify it.
 */

#ifndef DEF_ZULAMAN_H
#define DEF_ZULAMAN_H

#define DATA_NALORAKKEVENT  1
#define DATA_AKILZONEVENT   2
#define DATA_JANALAIEVENT   3
#define DATA_HALAZZIEVENT   4
#define DATA_HEXLORDEVENT   5
#define DATA_DAAKARAEVENT   6
#define DATA_CHESTLOOTED    7
#define TYPE_RAND_VENDOR_1  8
#define TYPE_RAND_VENDOR_2  9

#endif

