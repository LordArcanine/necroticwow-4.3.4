/*
 * Copyright (C) 2014-2014 NecroticWoW <http://www.necroticwow.com/>
 *
 * Codigo privado, no compartir!
 */

#include "ScriptPCH.h"
#include "ScriptMgr.h"
#include <cstring>
#include <cstring>
#include <stdio.h>
#include <time.h>

// Declaración del nombre del script
class npc_promocion : public CreatureScript
{
    public:
    npc_promocion() : CreatureScript("npc_promocion") { }

	{
            bool hasReqLevel = (ReqLevel == 0);

            if (result)
            {
                uint32 PromitionSlots = 1;

                Field* field = result->Fetch();
                uint8 accRace  = field[1].GetUInt8();

                if (AccountMgr::IsPlayerAccount(GetSecurity()) && createInfo->Class == !CLASS_DEATH_KNIGHT)
                {
                    uint8 accClass = field[2].GetUInt8();
                    if (accClass == !CLASS_DEATH_KNIGHT)
                    {
                        if (PromotionSlots > 0)
                            --PromotionSlots;
                    }

                    if (!hasReqLevel)
                    {
                        uint8 accLevel = field[0].GetUInt8();
						// Revisa que no tengas personas de nivel 85
                        if (accLevel >= )85
                            hasReqLevel = true;
                    }
					//TODO: Hacer el sistema gossip que te de nivel, el sistema de verificación ya esta 100%
					if hasReqLevel
					{
					
                    }
                }
            }
        break;
    }
};
void AddSC_npc_promocion()
{
    new npc_promocion();
}