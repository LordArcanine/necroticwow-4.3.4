/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "Language.h"
#include "DatabaseEnv.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Opcodes.h"
#include "Log.h"
#include "ObjectMgr.h"
#include "SpellMgr.h"
#include "Player.h"
#include "GossipDef.h"
#include "UpdateMask.h"
#include "ObjectAccessor.h"
#include "Creature.h"
#include "Pet.h"
#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "ScriptMgr.h"
#include "CreatureAI.h"
#include "SpellInfo.h"

enum StableResultCode
{
    STABLE_ERR_NONE         = 0x00,                         // does nothing, just resets stable states
    STABLE_ERR_MONEY        = 0x01,                         // "you don't have enough money"
    STABLE_INVALID_SLOT     = 0x03,
    STABLE_SUCCESS_STABLE   = 0x08,                         // stable success
    STABLE_SUCCESS_UNSTABLE = 0x09,                         // unstable/swap success
    STABLE_SUCCESS_BUY_SLOT = 0x0A,                         // buy slot success
    STABLE_ERR_EXOTIC       = 0x0B,                         // "you are unable to control exotic creatures"
    STABLE_ERR_STABLE       = 0x0C,                         // "Internal pet error"
};

void WorldSession::HandleTabardVendorActivateOpcode(WorldPacket & recvData)
{
    uint64 guid;
    recvData >> guid;

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_TABARDDESIGNER);
    if (!unit)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: HandleTabardVendorActivateOpcode - Unit (GUID: %u) not found or you can not interact with him.", uint32(GUID_LOPART(guid)));
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    SendTabardVendorActivate(guid);
}

void WorldSession::SendTabardVendorActivate(uint64 guid)
{
    WorldPacket data(MSG_TABARDVENDOR_ACTIVATE, 8);
    data << guid;
    SendPacket(&data);
}

void WorldSession::HandleBankerActivateOpcode(WorldPacket & recvData)
{
    uint64 guid;

    sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: Received CMSG_BANKER_ACTIVATE");

    recvData >> guid;

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_BANKER);
    if (!unit)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: HandleBankerActivateOpcode - Unit (GUID: %u) not found or you can not interact with him.", uint32(GUID_LOPART(guid)));
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    SendShowBank(guid);
}

void WorldSession::SendShowBank(uint64 guid)
{
    WorldPacket data(SMSG_SHOW_BANK, 8);
    data << guid;
    SendPacket(&data);
}

void WorldSession::HandleTrainerListOpcode(WorldPacket & recvData)
{
    uint64 guid;

    recvData >> guid;
    SendTrainerList(guid);
}

void WorldSession::SendTrainerList(uint64 guid)
{
    std::string str = GetTrinityString(LANG_NPC_TAINER_HELLO);
    SendTrainerList(guid, str);
}

void WorldSession::SendTrainerList(uint64 guid, const std::string& strTitle)
{
    sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: SendTrainerList");

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_TRAINER);
    if (!unit)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: SendTrainerList - Unit (GUID: %u) not found or you can not interact with him.", uint32(GUID_LOPART(guid)));
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    // trainer list loaded at check;
    if (!unit->isCanTrainingOf(_player, true))
        return;

    CreatureTemplate const* ci = unit->GetCreatureTemplate();

    if (!ci)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: SendTrainerList - (GUID: %u) NO CREATUREINFO!", GUID_LOPART(guid));
        return;
    }

    TrainerSpellData const* trainer_spells = unit->GetTrainerSpells();
    if (!trainer_spells)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: SendTrainerList - Training spells not found for creature (GUID: %u Entry: %u)",
            GUID_LOPART(guid), unit->GetEntry());
        return;
    }

    WorldPacket data(SMSG_TRAINER_LIST, 8+4+4+trainer_spells->spellList.size()*38 + strTitle.size()+1);
    data << guid;
    data << uint32(trainer_spells->trainerType);
    data << uint32(1); // different value for each trainer, also found in CMSG_TRAINER_BUY_SPELL

    size_t count_pos = data.wpos();
    data << uint32(trainer_spells->spellList.size());

    // reputation discount
    float fDiscountMod = _player->GetReputationPriceDiscount(unit);
    bool can_learn_primary_prof = GetPlayer()->GetFreePrimaryProfessionPoints() > 0;

    uint32 count = 0;
    for (TrainerSpellMap::const_iterator itr = trainer_spells->spellList.begin(); itr != trainer_spells->spellList.end(); ++itr)
    {
        TrainerSpell const* tSpell = &itr->second;

        bool valid = true;
        bool primary_prof_first_rank = false;
        for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)
        {
            if (!tSpell->learnedSpell[i])
                continue;
            if (!_player->IsSpellFitByClassAndRace(tSpell->learnedSpell[i]))
            {
                valid = false;
                break;
            }
            SpellInfo const* learnedSpellInfo = sSpellMgr->GetSpellInfo(tSpell->learnedSpell[i]);
            if (learnedSpellInfo && learnedSpellInfo->IsPrimaryProfessionFirstRank())
                primary_prof_first_rank = true;
        }
        if (!valid)
            continue;

        TrainerSpellState state = _player->GetTrainerSpellState(tSpell);

        data << uint32(tSpell->spell);                      // learned spell (or cast-spell in profession case)
        data << uint8(state == TRAINER_SPELL_GREEN_DISABLED ? TRAINER_SPELL_GREEN : state);
        data << uint32(floor(tSpell->spellCost * fDiscountMod));

        data << uint8(tSpell->reqLevel);
        data << uint32(tSpell->reqSkill);
        data << uint32(tSpell->reqSkillValue);
        //prev + req or req + 0
        uint8 maxReq = 0;
        for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)
        {
            if (!tSpell->learnedSpell[i])
                continue;
            if (uint32 prevSpellId = sSpellMgr->GetPrevSpellInChain(tSpell->learnedSpell[i]))
            {
                data << uint32(prevSpellId);
                ++maxReq;
            }
            if (maxReq == 2)
                break;
            SpellsRequiringSpellMapBounds spellsRequired = sSpellMgr->GetSpellsRequiredForSpellBounds(tSpell->learnedSpell[i]);
            for (SpellsRequiringSpellMap::const_iterator itr2 = spellsRequired.first; itr2 != spellsRequired.second && maxReq < 3; ++itr2)
            {
                data << uint32(itr2->second);
                ++maxReq;
            }
            if (maxReq == 2)
                break;
        }
        while (maxReq < 2)
        {
            data << uint32(0);
            ++maxReq;
        }

        data << uint32(primary_prof_first_rank && can_learn_primary_prof ? 1 : 0);
        // primary prof. learn confirmation dialog
        data << uint32(primary_prof_first_rank ? 1 : 0);    // must be equal prev. field to have learn button in enabled state

        ++count;
    }

    data << strTitle;

    data.put<uint32>(count_pos, count);
    SendPacket(&data);
}

void WorldSession::HandleTrainerBuySpellOpcode(WorldPacket& recvData)
{
    uint64 guid;
    uint32 spellId;
    uint32 trainerId;

    recvData >> guid >> trainerId >> spellId;
    sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: Received CMSG_TRAINER_BUY_SPELL NpcGUID=%u, learn spell id is: %u", uint32(GUID_LOPART(guid)), spellId);

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_TRAINER);
    if (!unit)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: HandleTrainerBuySpellOpcode - Unit (GUID: %u) not found or you can not interact with him.", uint32(GUID_LOPART(guid)));
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    if (!unit->isCanTrainingOf(_player, true))
    {
        SendTrainerBuyFailed(guid, spellId, 0);
        return;
    }

    // check present spell in trainer spell list
    TrainerSpellData const* trainer_spells = unit->GetTrainerSpells();
    if (!trainer_spells)
    {
        SendTrainerBuyFailed(guid, spellId, 0);
        return;
    }

    // not found, cheat?
    TrainerSpell const* trainer_spell = trainer_spells->Find(spellId);
    if (!trainer_spell)
    {
        SendTrainerBuyFailed(guid, spellId, 0);
        return;
    }

    // can't be learn, cheat? Or double learn with lags...
    if (_player->GetTrainerSpellState(trainer_spell) != TRAINER_SPELL_GREEN)
    {
        SendTrainerBuyFailed(guid, spellId, 0);
        return;
    }

    // apply reputation discount
    uint32 nSpellCost = uint32(floor(trainer_spell->spellCost * _player->GetReputationPriceDiscount(unit)));

    // check money requirement
    if (!_player->HasEnoughMoney(uint64(nSpellCost)))
    {
        SendTrainerBuyFailed(guid, spellId, 1);
        return;
    }

    _player->ModifyMoney(-int64(nSpellCost));

    unit->SendPlaySpellVisualKit(179, 0);       // 53 SpellCastDirected
    _player->SendPlaySpellVisualKit(362, 1);    // 113 EmoteSalute

    // learn explicitly or cast explicitly
    if (trainer_spell->IsCastable())
        _player->CastSpell(_player, trainer_spell->spell, true);
    else
        _player->learnSpell(spellId, false);

    WorldPacket data(SMSG_TRAINER_BUY_SUCCEEDED, 12);
    data << uint64(guid);
    data << uint32(spellId);
    SendPacket(&data);
}

void WorldSession::SendTrainerBuyFailed(uint64 guid, uint32 spellId, uint32 reason)
{
    WorldPacket data(SMSG_TRAINER_BUY_FAILED, 16);
    data << uint64(guid);
    data << uint32(spellId);        // should be same as in packet from client
    data << uint32(reason);         // 1 == "Not enough money for trainer service." 0 == "Trainer service %d unavailable."
    SendPacket(&data);
}

void WorldSession::HandleGossipHelloOpcode(WorldPacket & recvData)
{
    sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: Received CMSG_GOSSIP_HELLO");

    uint64 guid;
    recvData >> guid;

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_NONE);
    if (!unit)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: HandleGossipHelloOpcode - Unit (GUID: %u) not found or you can not interact with him.", uint32(GUID_LOPART(guid)));
        return;
    }

    // set faction visible if needed
    if (FactionTemplateEntry const* factionTemplateEntry = sFactionTemplateStore.LookupEntry(unit->getFaction()))
        _player->GetReputationMgr().SetVisible(factionTemplateEntry);

    GetPlayer()->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_TALK);
    // remove fake death
    //if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
    //    GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    if (unit->isArmorer() || unit->isCivilian() || unit->isQuestGiver() || unit->isServiceProvider() || unit->isGuard())
        unit->StopMoving();

    // If spiritguide, no need for gossip menu, just put player into resurrect queue
    if (unit->isSpiritGuide())
    {
        Battleground* bg = _player->GetBattleground();
        if (bg)
        {
            bg->AddPlayerToResurrectQueue(unit->GetGUID(), _player->GetGUID());
            sBattlegroundMgr->SendAreaSpiritHealerQueryOpcode(_player, bg, unit->GetGUID());
            return;
        }
    }

    if (!sScriptMgr->OnGossipHello(_player, unit))
    {
//        _player->TalkedToCreature(unit->GetEntry(), unit->GetGUID());
        _player->PrepareGossipMenu(unit, unit->GetCreatureTemplate()->GossipMenuId, true);
        _player->SendPreparedGossip(unit);
    }
    unit->AI()->sGossipHello(_player);
}

/*void WorldSession::HandleGossipSelectOptionOpcode(WorldPacket & recvData)
{
    sLog->outDebug(LOG_FILTER_PACKETIO, "WORLD: CMSG_GOSSIP_SELECT_OPTION");

    uint32 option;
    uint32 unk;
    uint64 guid;
    std::string code = "";

    recvData >> guid >> unk >> option;

    if (_player->PlayerTalkClass->GossipOptionCoded(option))
    {
        sLog->outDebug(LOG_FILTER_PACKETIO, "reading string");
        recvData >> code;
        sLog->outDebug(LOG_FILTER_PACKETIO, "string read: %s", code.c_str());
    }

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_NONE);
    if (!unit)
    {
        sLog->outDebug(LOG_FILTER_PACKETIO, "WORLD: HandleGossipSelectOptionOpcode - Unit (GUID: %u) not found or you can't interact with him.", uint32(GUID_LOPART(guid)));
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    if (!code.empty())
    {
        if (!Script->GossipSelectWithCode(_player, unit, _player->PlayerTalkClass->GossipOptionSender (option), _player->PlayerTalkClass->GossipOptionAction(option), code.c_str()))
            unit->OnGossipSelect (_player, option);
    }
    else
    {
        if (!Script->OnGossipSelect (_player, unit, _player->PlayerTalkClass->GossipOptionSender (option), _player->PlayerTalkClass->GossipOptionAction (option)))
           unit->OnGossipSelect (_player, option);
    }
}*/

void WorldSession::HandleSpiritHealerActivateOpcode(WorldPacket & recvData)
{
    sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: CMSG_SPIRIT_HEALER_ACTIVATE");

    uint64 guid;

    recvData >> guid;

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_SPIRITHEALER);
    if (!unit)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: HandleSpiritHealerActivateOpcode - Unit (GUID: %u) not found or you can not interact with him.", uint32(GUID_LOPART(guid)));
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    SendSpiritResurrect();
}

void WorldSession::SendSpiritResurrect()
{
    _player->ResurrectPlayer(0.5f, true);

    _player->DurabilityLossAll(0.25f, true);

    // get corpse nearest graveyard
    WorldSafeLocsEntry const* corpseGrave = NULL;
    Corpse* corpse = _player->GetCorpse();
    if (corpse)
        corpseGrave = sObjectMgr->GetClosestGraveYard(
            corpse->GetPositionX(), corpse->GetPositionY(), corpse->GetPositionZ(), corpse->GetMapId(), _player->GetTeam());

    // now can spawn bones
    _player->SpawnCorpseBones();

    // teleport to nearest from corpse graveyard, if different from nearest to player ghost
    if (corpseGrave)
    {
        WorldSafeLocsEntry const* ghostGrave = sObjectMgr->GetClosestGraveYard(
            _player->GetPositionX(), _player->GetPositionY(), _player->GetPositionZ(), _player->GetMapId(), _player->GetTeam());

        if (corpseGrave != ghostGrave)
            _player->TeleportTo(corpseGrave->map_id, corpseGrave->x, corpseGrave->y, corpseGrave->z, _player->GetOrientation());
        // or update at original position
        else
            _player->UpdateObjectVisibility();
    }
    // or update at original position
    else
        _player->UpdateObjectVisibility();
}

void WorldSession::HandleBinderActivateOpcode(WorldPacket & recvData)
{
    uint64 npcGUID;
    recvData >> npcGUID;

    if (!GetPlayer()->IsInWorld() || !GetPlayer()->isAlive())
        return;

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(npcGUID, UNIT_NPC_FLAG_INNKEEPER);
    if (!unit)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: HandleBinderActivateOpcode - Unit (GUID: %u) not found or you can not interact with him.", uint32(GUID_LOPART(npcGUID)));
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    SendBindPoint(unit);
}

void WorldSession::SendBindPoint(Creature* npc)
{
    // prevent set homebind to instances in any case
    if (GetPlayer()->GetMap()->Instanceable())
        return;

    uint32 bindspell = 3286;

    // send spell for homebinding (3286)
    npc->CastSpell(_player, bindspell, true);

    WorldPacket data(SMSG_TRAINER_BUY_SUCCEEDED, 12);
    data << uint64(npc->GetGUID());
    data << uint32(bindspell);
    SendPacket(&data);

    _player->PlayerTalkClass->SendCloseGossip();
}

void WorldSession::HandleListStabledPetsOpcode(WorldPacket& recvData) // Opcode usage to display pets.
{
    sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: MSG_LIST_STABLED_PETS Received");

    uint64 npcGUID;
    recvData >> npcGUID;

    if (!CheckStableMaster(npcGUID))
        return;

    // Remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    // Remove mounts this fixes a bug where getting pet from stable while mounted deletes pet.
    if (GetPlayer()->IsMounted())
        GetPlayer()->RemoveAurasByType(SPELL_AURA_MOUNTED);

    SendOpenStable(npcGUID);
}

void WorldSession::SendOpenStable(uint64 guid) // Sent to display pet lists.
{
    if (!GetPlayer())
        return;

    sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: MSG_LIST_STABLED_PETS Send.");

    WorldPacket data(MSG_LIST_STABLED_PETS, 200);           // guess size

    data << uint64(guid);

    size_t wpos = data.wpos();
    data << uint8(0);                                       // place holder for slot show number

    data << uint8(GetPlayer()->m_stableSlots);              // maximum stable slots for the grey boxes.

    uint8 num = 0;                                          // counter for place holder

    // For current pets.
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SLOTS_DETAIL);

    stmt->setUInt32(0, GetPlayer()->GetGUIDLow());
    stmt->setUInt8(1, PET_SLOT_HUNTER_FIRST);
    stmt->setUInt8(2, PET_SLOT_HUNTER_LAST);

    PreparedQueryResult petResult = CharacterDatabase.Query(stmt);
    if (petResult)
    {
        do
        {
            Field* fields = petResult->Fetch();

            uint32 currPetSlot = fields[5].GetUInt8();
            uint32 currPetNumber = fields[1].GetUInt32();
            uint32 currPetEntry = fields[2].GetUInt32();
            uint32 currPetlevel = fields[3].GetUInt16();

            data << uint32(currPetSlot);           // pet slot ID
            data << uint32(currPetNumber);         // petnumber
            data << uint32(currPetEntry);          // creature entry
            data << uint32(currPetlevel);          // level
            data << fields[4].GetString();         // name
            data << uint8(1); // 1 = current pets, 2/3 = in stable (any from 4, 5, ... create problems with proper show)

            ++num;
	    }
        while (petResult->NextRow());
    }

    // For stabled pets.
    PreparedStatement* stmt2 = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SLOTS_DETAIL);

    stmt2->setUInt32(0, GetPlayer()->GetGUIDLow());
    stmt2->setUInt8(1, PET_SAVE_FIRST_STABLE_SLOT);
    stmt2->setUInt8(2, PET_SAVE_LAST_STABLE_SLOT);

    PreparedQueryResult result = CharacterDatabase.Query(stmt2);
    if (result)
    {
        do
        {
            Field* fields = result->Fetch();

            uint32 stabledPetSlot = fields[5].GetUInt8();
            uint32 stabledPetNumber = fields[1].GetUInt32();
            uint32 stabledPetEntry = fields[2].GetUInt32();
            uint32 stabledPetLevel = fields[3].GetUInt16();

            data << uint32(stabledPetSlot);            // pet slot ID
            data << uint32(stabledPetNumber);          // petnumber
            data << uint32(stabledPetEntry);           // creature entry
            data << uint32(stabledPetLevel);           // level
            data << fields[4].GetString();             // name
            data << uint8(2); // 1 = current pets, 2/3 = in stable (any from 4, 5, ... create problems with proper show)

            ++num;
        }
        while (result->NextRow());
    }

    data.put<uint8>(wpos, num);                             // set real data to placeholder
    SendPacket(&data);

    SendStableResult(STABLE_ERR_NONE);
}

void WorldSession::HandleSetPetSlot(WorldPacket& recvPacket) // Come stable / unstable / swap your pet! :)
{
    uint8 petSlot;
    uint32 petNumber;
    ObjectGuid guid;

    recvPacket >> petNumber >> petSlot; // pet number, target slot.

    guid[3] = recvPacket.ReadBit();
    guid[2] = recvPacket.ReadBit();
    guid[0] = recvPacket.ReadBit();
    guid[7] = recvPacket.ReadBit();
    guid[5] = recvPacket.ReadBit();
    guid[6] = recvPacket.ReadBit();
    guid[1] = recvPacket.ReadBit();
    guid[4] = recvPacket.ReadBit();

    recvPacket.ReadByteSeq(guid[5]);
    recvPacket.ReadByteSeq(guid[3]);
    recvPacket.ReadByteSeq(guid[1]);
    recvPacket.ReadByteSeq(guid[7]);
    recvPacket.ReadByteSeq(guid[4]);
    recvPacket.ReadByteSeq(guid[0]);
    recvPacket.ReadByteSeq(guid[6]);
    recvPacket.ReadByteSeq(guid[2]);

    sLog->outDebug(LOG_FILTER_NETWORKIO, "CMSG_SET_PET_SLOT with guid : %u and pet slot : %u and pet number : %u", uint32(guid), petSlot, petNumber);

    if (!GetPlayer()->isAlive())
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    if (!CheckStableMaster(guid))
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    // Remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    uint8 firstSlot = GetPlayer()->GetPetSlotByNumber(petNumber);

    if (petSlot > GetPlayer()->m_stableSlots || petSlot < 0) // Can't stable there.
        SendStableResult(STABLE_ERR_STABLE);
    if (firstSlot < PET_SAVE_FIRST_STABLE_SLOT && petSlot >= PET_SAVE_FIRST_STABLE_SLOT && GetPlayer()->GetPetEntryBySlotId(petSlot) == 0) // You stable a pet to a free slot in the stable.
        StablePet(petSlot, petNumber);
    else if (firstSlot < PET_SAVE_FIRST_STABLE_SLOT && petSlot < PET_SAVE_FIRST_STABLE_SLOT && GetPlayer()->GetPetEntryBySlotId(petSlot) == 0) // You stable a pet on another active slot.
    {
        if (firstSlot > petSlot)
            StablePet(petSlot, petNumber);
        else
            UnstablePet(petSlot, petNumber);
    }
    else if (firstSlot >= PET_SAVE_FIRST_STABLE_SLOT && petSlot >= PET_SAVE_FIRST_STABLE_SLOT && GetPlayer()->GetPetEntryBySlotId(petSlot) == 0) // You re-stable a pet to a free stable slot.
    {
        if (firstSlot > petSlot)
            StablePet(petSlot, petNumber);
        else
            UnstablePet(petSlot, petNumber);
    }
    else if (firstSlot >= PET_SAVE_FIRST_STABLE_SLOT && petSlot < PET_SAVE_FIRST_STABLE_SLOT && GetPlayer()->GetPetEntryBySlotId(petSlot) == 0) // You unstable a pet to a free active slot.
        UnstablePet(petSlot, petNumber);
    else if (GetPlayer()->GetPetEntryBySlotId(petSlot) != 0) // There is already a pet there, swap places between pets.
        SwapPets(firstSlot, petSlot, petNumber);
    else
	{
        sLog->outString("CMSG_SET_PET_SLOT error, unknown action for pet slot : %u and pet number : %u", petSlot, petNumber);
        SendStableResult(STABLE_INVALID_SLOT);
    }

    SendOpenStable(guid);
}

void WorldSession::StablePet(uint8 petSlot, uint32 petNumber) // For stabling a pet.
{
    if (!GetPlayer())
        return;

    uint8 fromSlot = GetPlayer()->GetPetSlotByNumber(petNumber);
    uint32 petEntry = GetPlayer()->GetPetEntryBySlotId(fromSlot);
    uint32 petLevel = GetPlayer()->GetPetLevelBySlotId(fromSlot);

    if (fromSlot == 0) // If player stables his current pet.
        _player->RemovePet(_player->GetPet(), PetSaveMode(petSlot));
    if (petSlot == 0)
    {
        Pet* newPet = new Pet(_player, HUNTER_PET);
        if (!newPet->LoadPetFromDB(_player, petEntry, petNumber))
        {
            delete newPet;
            newPet = NULL;
            SendStableResult(STABLE_ERR_STABLE);
            return;
        }
    }

    // Do it.
    CharacterDatabase.PExecute("UPDATE character_pet SET slot='%u' WHERE id='%u' AND owner ='%u'", petSlot, petNumber, GetPlayer()->GetGUIDLow());

    SendStableResult(STABLE_SUCCESS_STABLE);
}

void WorldSession::UnstablePet(uint8 petSlot, uint32 petNumber) // For unstabling a pet.
{
    if (!GetPlayer())
        return;

    uint8 fromSlot = GetPlayer()->GetPetSlotByNumber(petNumber);
    uint32 petEntry = GetPlayer()->GetPetEntryBySlotId(fromSlot);
    uint32 petLevel = GetPlayer()->GetPetLevelBySlotId(fromSlot);

    if (!petEntry)
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    CreatureTemplate const* creatureInfo = sObjectMgr->GetCreatureTemplate(petEntry);

    if (!creatureInfo)
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    // If can't tame exotic pets.
    if (creatureInfo && !creatureInfo->isTameable(false))
    {
        SendStableResult(STABLE_ERR_EXOTIC);
        return;
    }

    if (petSlot == 0) // Unstable as current pet.
    {
        Pet* newPet = new Pet(_player, HUNTER_PET);
        if (!newPet->LoadPetFromDB(_player, petEntry, petNumber))
        {
            delete newPet;
            newPet = NULL;
            SendStableResult(STABLE_ERR_STABLE);
            return;
        }
    }

    // Do it.
    CharacterDatabase.PExecute("UPDATE character_pet SET slot='%u' WHERE id='%u' AND owner ='%u'", petSlot, petNumber, GetPlayer()->GetGUIDLow());

    SendStableResult(STABLE_SUCCESS_UNSTABLE);
}

void WorldSession::SwapPets(uint8 firstSlot, uint8 targetSlot, uint32 petNumber) // For swapping two pets.
{
    if (!GetPlayer())
        return;

    // Current slot for pet to be moved.
    uint8 fromSlot = GetPlayer()->GetPetSlotByNumber(petNumber);

    // Get the first pet stuff.
    uint32 firstPetEntry = GetPlayer()->GetPetEntryBySlotId(fromSlot);
    uint32 firstPetLevel = GetPlayer()->GetPetLevelBySlotId(fromSlot);

    // Get the second pet stuff.
    uint32 secondPetNumber = GetPlayer()->GetPetNumberBySlotId(targetSlot);
    uint32 secondPetLevel = GetPlayer()->GetPetLevelBySlotId(targetSlot);
    uint32 secondPetEntry = GetPlayer()->GetPetEntryBySlotId(targetSlot);

    if (!firstPetEntry || !secondPetEntry)
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    CreatureTemplate const* creatureInfo = sObjectMgr->GetCreatureTemplate(firstPetEntry);
    CreatureTemplate const* creatureInfo2 = sObjectMgr->GetCreatureTemplate(secondPetEntry);

    if (!creatureInfo || !creatureInfo2)
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    // If can't tame exotic pets.
    if (creatureInfo && !creatureInfo->isTameable(false) || creatureInfo2 && !creatureInfo2->isTameable(false))
    {
        SendStableResult(STABLE_ERR_EXOTIC);
        return;
    }

    if (fromSlot == 0 || targetSlot == 0) // If we swap a pet with our current pet.
    {
        Pet* pet = _player->GetPet();
        if (!pet || !pet->isAlive())
        {
            SendStableResult(STABLE_ERR_STABLE);
            return;
        }
        
        // move alive pet to swapped pet slot.
        CharacterDatabase.PExecute("UPDATE character_pet SET slot='%u' WHERE id='%u' AND owner ='%u'", fromSlot, secondPetNumber, GetPlayer()->GetGUIDLow());
        CharacterDatabase.PExecute("UPDATE character_pet SET slot='%u' WHERE id='%u' AND owner ='%u'", targetSlot, petNumber, GetPlayer()->GetGUIDLow());

        if (targetSlot == 0)
            _player->RemovePet(pet, PetSaveMode(fromSlot));

        // summon unstabled pet.
        Pet* newPet = new Pet(_player);
        if (targetSlot == 0)
        {
            if (!newPet->LoadPetFromDB(_player, firstPetEntry, petNumber))
            {
                delete newPet;
                SendStableResult(STABLE_ERR_STABLE);
            }
        }
        else if (fromSlot == 0)
        {
            if (!newPet->LoadPetFromDB(_player, secondPetEntry, secondPetNumber))
            {
                delete newPet;
                SendStableResult(STABLE_ERR_STABLE);
            }
        }
    }
    else // Cases which do not imply moving active pet.
    {
        CharacterDatabase.PExecute("UPDATE character_pet SET slot='%u' WHERE id='%u' AND owner ='%u'", fromSlot, secondPetNumber, GetPlayer()->GetGUIDLow());
        CharacterDatabase.PExecute("UPDATE character_pet SET slot='%u' WHERE id='%u' AND owner ='%u'", targetSlot, petNumber, GetPlayer()->GetGUIDLow());
    }

    SendStableResult(targetSlot >= PET_SAVE_FIRST_STABLE_SLOT ? STABLE_SUCCESS_STABLE : STABLE_SUCCESS_UNSTABLE);
}

void WorldSession::SendStableResult(uint8 res)
{
    WorldPacket data(SMSG_STABLE_RESULT, 1);
    data << uint8(res);
    SendPacket(&data);
}

void WorldSession::HandleRepairItemOpcode(WorldPacket& recvData)
{
    sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: CMSG_REPAIR_ITEM");

    uint64 npcGUID, itemGUID;
    uint8 guildBank;                                        // new in 2.3.2, bool that means from guild bank money

    recvData >> npcGUID >> itemGUID >> guildBank;

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(npcGUID, UNIT_NPC_FLAG_REPAIR);
    if (!unit)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: HandleRepairItemOpcode - Unit (GUID: %u) not found or you can not interact with him.", uint32(GUID_LOPART(npcGUID)));
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    // reputation discount
    float discountMod = _player->GetReputationPriceDiscount(unit);

    if (itemGUID)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "ITEM: Repair item, itemGUID = %u, npcGUID = %u", GUID_LOPART(itemGUID), GUID_LOPART(npcGUID));

        Item* item = _player->GetItemByGuid(itemGUID);
        if (item)
            _player->DurabilityRepair(item->GetPos(), true, discountMod, guildBank);
    }
    else
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "ITEM: Repair all items, npcGUID = %u", GUID_LOPART(npcGUID));
        _player->DurabilityRepairAll(true, discountMod, guildBank);
    }
}

